--
-- PostgreSQL database cluster dump
--

SET default_transaction_read_only = off;

SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;

--
-- Drop databases (except postgres and template1)
--

DROP DATABASE video;




--
-- Drop roles
--

DROP ROLE serrrgggeee;
DROP ROLE video;


--
-- Roles
--

CREATE ROLE serrrgggeee;
ALTER ROLE serrrgggeee WITH SUPERUSER INHERIT CREATEROLE CREATEDB LOGIN REPLICATION BYPASSRLS PASSWORD 'SCRAM-SHA-256$4096:pp7wA8eA5Y0FEz1twNRBXQ==$qDzBR9/G/wcO7peZK6YSS/nwrHdYvH+ky9t2fLAvl6w=:/ssXILqYf+8aW8tJfyBdCFhI0pTpZifH28fOI8x3MYc=';
CREATE ROLE video;
ALTER ROLE video WITH SUPERUSER INHERIT CREATEROLE CREATEDB LOGIN REPLICATION BYPASSRLS PASSWORD 'SCRAM-SHA-256$4096:pp7wA8eA5Y0FEz1twNRBXQ==$qDzBR9/G/wcO7peZK6YSS/nwrHdYvH+ky9t2fLAvl6w=:/ssXILqYf+8aW8tJfyBdCFhI0pTpZifH28fOI8x3MYc=';

--
-- User Configurations
--








--
-- Databases
--

--
-- Database "template1" dump
--

--
-- PostgreSQL database dump
--

-- Dumped from database version 15.0
-- Dumped by pg_dump version 15.0

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

UPDATE pg_catalog.pg_database SET datistemplate = false WHERE datname = 'template1';
DROP DATABASE template1;
--
-- Name: template1; Type: DATABASE; Schema: -; Owner: video
--

CREATE DATABASE template1 WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE_PROVIDER = libc LOCALE = 'en_US.utf8';


ALTER DATABASE template1 OWNER TO video;

\connect template1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: DATABASE template1; Type: COMMENT; Schema: -; Owner: video
--

COMMENT ON DATABASE template1 IS 'default template for new databases';


--
-- Name: template1; Type: DATABASE PROPERTIES; Schema: -; Owner: video
--

ALTER DATABASE template1 IS_TEMPLATE = true;


\connect template1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: DATABASE template1; Type: ACL; Schema: -; Owner: video
--

REVOKE CONNECT,TEMPORARY ON DATABASE template1 FROM PUBLIC;
GRANT CONNECT ON DATABASE template1 TO PUBLIC;


--
-- PostgreSQL database dump complete
--

--
-- Database "postgres" dump
--

--
-- PostgreSQL database dump
--

-- Dumped from database version 15.0
-- Dumped by pg_dump version 15.0

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

DROP DATABASE postgres;
--
-- Name: postgres; Type: DATABASE; Schema: -; Owner: video
--

CREATE DATABASE postgres WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE_PROVIDER = libc LOCALE = 'en_US.utf8';


ALTER DATABASE postgres OWNER TO video;

\connect postgres

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: DATABASE postgres; Type: COMMENT; Schema: -; Owner: video
--

COMMENT ON DATABASE postgres IS 'default administrative connection database';


--
-- PostgreSQL database dump complete
--

--
-- Database "video" dump
--

--
-- PostgreSQL database dump
--

-- Dumped from database version 15.0
-- Dumped by pg_dump version 15.0

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: video; Type: DATABASE; Schema: -; Owner: video
--

CREATE DATABASE video WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE_PROVIDER = libc LOCALE = 'en_US.utf8';


ALTER DATABASE video OWNER TO video;

\connect video

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: public; Type: SCHEMA; Schema: -; Owner: video
--

-- *not* creating schema, since initdb creates it


ALTER SCHEMA public OWNER TO video;

--
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: video
--

COMMENT ON SCHEMA public IS '';


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: account_emailaddress; Type: TABLE; Schema: public; Owner: video
--

CREATE TABLE public.account_emailaddress (
    id integer NOT NULL,
    email character varying(254) NOT NULL,
    verified boolean NOT NULL,
    "primary" boolean NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE public.account_emailaddress OWNER TO video;

--
-- Name: account_emailaddress_id_seq; Type: SEQUENCE; Schema: public; Owner: video
--

CREATE SEQUENCE public.account_emailaddress_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.account_emailaddress_id_seq OWNER TO video;

--
-- Name: account_emailaddress_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: video
--

ALTER SEQUENCE public.account_emailaddress_id_seq OWNED BY public.account_emailaddress.id;


--
-- Name: account_emailconfirmation; Type: TABLE; Schema: public; Owner: video
--

CREATE TABLE public.account_emailconfirmation (
    id integer NOT NULL,
    created timestamp with time zone NOT NULL,
    sent timestamp with time zone,
    key character varying(64) NOT NULL,
    email_address_id integer NOT NULL
);


ALTER TABLE public.account_emailconfirmation OWNER TO video;

--
-- Name: account_emailconfirmation_id_seq; Type: SEQUENCE; Schema: public; Owner: video
--

CREATE SEQUENCE public.account_emailconfirmation_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.account_emailconfirmation_id_seq OWNER TO video;

--
-- Name: account_emailconfirmation_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: video
--

ALTER SEQUENCE public.account_emailconfirmation_id_seq OWNED BY public.account_emailconfirmation.id;


--
-- Name: auth_group; Type: TABLE; Schema: public; Owner: video
--

CREATE TABLE public.auth_group (
    id integer NOT NULL,
    name character varying(150) NOT NULL
);


ALTER TABLE public.auth_group OWNER TO video;

--
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: video
--

CREATE SEQUENCE public.auth_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_id_seq OWNER TO video;

--
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: video
--

ALTER SEQUENCE public.auth_group_id_seq OWNED BY public.auth_group.id;


--
-- Name: auth_group_permissions; Type: TABLE; Schema: public; Owner: video
--

CREATE TABLE public.auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_group_permissions OWNER TO video;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: video
--

CREATE SEQUENCE public.auth_group_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_permissions_id_seq OWNER TO video;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: video
--

ALTER SEQUENCE public.auth_group_permissions_id_seq OWNED BY public.auth_group_permissions.id;


--
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: video
--

CREATE TABLE public.auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE public.auth_permission OWNER TO video;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: video
--

CREATE SEQUENCE public.auth_permission_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_permission_id_seq OWNER TO video;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: video
--

ALTER SEQUENCE public.auth_permission_id_seq OWNED BY public.auth_permission.id;


--
-- Name: auth_user; Type: TABLE; Schema: public; Owner: video
--

CREATE TABLE public.auth_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    username character varying(150) NOT NULL,
    first_name character varying(150) NOT NULL,
    last_name character varying(150) NOT NULL,
    email character varying(254) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL
);


ALTER TABLE public.auth_user OWNER TO video;

--
-- Name: auth_user_groups; Type: TABLE; Schema: public; Owner: video
--

CREATE TABLE public.auth_user_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE public.auth_user_groups OWNER TO video;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: video
--

CREATE SEQUENCE public.auth_user_groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_groups_id_seq OWNER TO video;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: video
--

ALTER SEQUENCE public.auth_user_groups_id_seq OWNED BY public.auth_user_groups.id;


--
-- Name: auth_user_id_seq; Type: SEQUENCE; Schema: public; Owner: video
--

CREATE SEQUENCE public.auth_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_id_seq OWNER TO video;

--
-- Name: auth_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: video
--

ALTER SEQUENCE public.auth_user_id_seq OWNED BY public.auth_user.id;


--
-- Name: auth_user_user_permissions; Type: TABLE; Schema: public; Owner: video
--

CREATE TABLE public.auth_user_user_permissions (
    id integer NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_user_user_permissions OWNER TO video;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: video
--

CREATE SEQUENCE public.auth_user_user_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_user_permissions_id_seq OWNER TO video;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: video
--

ALTER SEQUENCE public.auth_user_user_permissions_id_seq OWNED BY public.auth_user_user_permissions.id;


--
-- Name: authtoken_token; Type: TABLE; Schema: public; Owner: video
--

CREATE TABLE public.authtoken_token (
    key character varying(40) NOT NULL,
    created timestamp with time zone NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE public.authtoken_token OWNER TO video;

--
-- Name: chunks_chunk; Type: TABLE; Schema: public; Owner: video
--

CREATE TABLE public.chunks_chunk (
    id integer NOT NULL,
    key character varying(255) NOT NULL,
    content text NOT NULL
);


ALTER TABLE public.chunks_chunk OWNER TO video;

--
-- Name: chunks_chunk_id_seq; Type: SEQUENCE; Schema: public; Owner: video
--

CREATE SEQUENCE public.chunks_chunk_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.chunks_chunk_id_seq OWNER TO video;

--
-- Name: chunks_chunk_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: video
--

ALTER SEQUENCE public.chunks_chunk_id_seq OWNED BY public.chunks_chunk.id;


--
-- Name: chunks_group; Type: TABLE; Schema: public; Owner: video
--

CREATE TABLE public.chunks_group (
    id integer NOT NULL,
    key character varying(255) NOT NULL,
    content text NOT NULL
);


ALTER TABLE public.chunks_group OWNER TO video;

--
-- Name: chunks_group_id_seq; Type: SEQUENCE; Schema: public; Owner: video
--

CREATE SEQUENCE public.chunks_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.chunks_group_id_seq OWNER TO video;

--
-- Name: chunks_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: video
--

ALTER SEQUENCE public.chunks_group_id_seq OWNED BY public.chunks_group.id;


--
-- Name: chunks_image; Type: TABLE; Schema: public; Owner: video
--

CREATE TABLE public.chunks_image (
    id integer NOT NULL,
    key character varying(255) NOT NULL,
    image character varying(255) NOT NULL
);


ALTER TABLE public.chunks_image OWNER TO video;

--
-- Name: chunks_image_id_seq; Type: SEQUENCE; Schema: public; Owner: video
--

CREATE SEQUENCE public.chunks_image_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.chunks_image_id_seq OWNER TO video;

--
-- Name: chunks_image_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: video
--

ALTER SEQUENCE public.chunks_image_id_seq OWNED BY public.chunks_image.id;


--
-- Name: chunks_media; Type: TABLE; Schema: public; Owner: video
--

CREATE TABLE public.chunks_media (
    id integer NOT NULL,
    key character varying(255) NOT NULL,
    title character varying(64) NOT NULL,
    "desc" character varying(256),
    media character varying(256)
);


ALTER TABLE public.chunks_media OWNER TO video;

--
-- Name: chunks_media_id_seq; Type: SEQUENCE; Schema: public; Owner: video
--

CREATE SEQUENCE public.chunks_media_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.chunks_media_id_seq OWNER TO video;

--
-- Name: chunks_media_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: video
--

ALTER SEQUENCE public.chunks_media_id_seq OWNED BY public.chunks_media.id;


--
-- Name: comunicate_chat; Type: TABLE; Schema: public; Owner: video
--

CREATE TABLE public.comunicate_chat (
    id integer NOT NULL,
    message text NOT NULL,
    user_info jsonb NOT NULL,
    created timestamp with time zone NOT NULL,
    updated timestamp with time zone NOT NULL,
    room_id integer,
    active boolean NOT NULL,
    shared boolean NOT NULL,
    shared_room_id integer
);


ALTER TABLE public.comunicate_chat OWNER TO video;

--
-- Name: comunicate_chat_id_seq; Type: SEQUENCE; Schema: public; Owner: video
--

CREATE SEQUENCE public.comunicate_chat_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.comunicate_chat_id_seq OWNER TO video;

--
-- Name: comunicate_chat_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: video
--

ALTER SEQUENCE public.comunicate_chat_id_seq OWNED BY public.comunicate_chat.id;


--
-- Name: comunicate_language; Type: TABLE; Schema: public; Owner: video
--

CREATE TABLE public.comunicate_language (
    id integer NOT NULL,
    name character varying(128) NOT NULL,
    active boolean NOT NULL,
    article character varying(128) NOT NULL,
    code character varying(28) NOT NULL,
    comment character varying(128) NOT NULL,
    directionality character varying(3) NOT NULL,
    natural_name character varying(128) NOT NULL
);


ALTER TABLE public.comunicate_language OWNER TO video;

--
-- Name: comunicate_language_id_seq; Type: SEQUENCE; Schema: public; Owner: video
--

CREATE SEQUENCE public.comunicate_language_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.comunicate_language_id_seq OWNER TO video;

--
-- Name: comunicate_language_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: video
--

ALTER SEQUENCE public.comunicate_language_id_seq OWNED BY public.comunicate_language.id;


--
-- Name: comunicate_member; Type: TABLE; Schema: public; Owner: video
--

CREATE TABLE public.comunicate_member (
    id integer NOT NULL,
    created timestamp with time zone NOT NULL,
    updated timestamp with time zone NOT NULL,
    active boolean NOT NULL,
    room_id integer,
    social_account_id integer
);


ALTER TABLE public.comunicate_member OWNER TO video;

--
-- Name: comunicate_member_id_seq; Type: SEQUENCE; Schema: public; Owner: video
--

CREATE SEQUENCE public.comunicate_member_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.comunicate_member_id_seq OWNER TO video;

--
-- Name: comunicate_member_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: video
--

ALTER SEQUENCE public.comunicate_member_id_seq OWNED BY public.comunicate_member.id;


--
-- Name: comunicate_room; Type: TABLE; Schema: public; Owner: video
--

CREATE TABLE public.comunicate_room (
    id integer NOT NULL,
    name character varying(128) NOT NULL,
    created timestamp with time zone NOT NULL,
    updated timestamp with time zone NOT NULL,
    language_id integer,
    subject_id integer,
    active boolean NOT NULL,
    social_account_id integer
);


ALTER TABLE public.comunicate_room OWNER TO video;

--
-- Name: comunicate_room_id_seq; Type: SEQUENCE; Schema: public; Owner: video
--

CREATE SEQUENCE public.comunicate_room_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.comunicate_room_id_seq OWNER TO video;

--
-- Name: comunicate_room_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: video
--

ALTER SEQUENCE public.comunicate_room_id_seq OWNED BY public.comunicate_room.id;


--
-- Name: comunicate_subject; Type: TABLE; Schema: public; Owner: video
--

CREATE TABLE public.comunicate_subject (
    id integer NOT NULL,
    name character varying(128) NOT NULL,
    comment character varying(128) NOT NULL,
    active boolean NOT NULL
);


ALTER TABLE public.comunicate_subject OWNER TO video;

--
-- Name: comunicate_subject_id_seq; Type: SEQUENCE; Schema: public; Owner: video
--

CREATE SEQUENCE public.comunicate_subject_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.comunicate_subject_id_seq OWNER TO video;

--
-- Name: comunicate_subject_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: video
--

ALTER SEQUENCE public.comunicate_subject_id_seq OWNED BY public.comunicate_subject.id;


--
-- Name: django_admin_log; Type: TABLE; Schema: public; Owner: video
--

CREATE TABLE public.django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id integer NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE public.django_admin_log OWNER TO video;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: public; Owner: video
--

CREATE SEQUENCE public.django_admin_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_admin_log_id_seq OWNER TO video;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: video
--

ALTER SEQUENCE public.django_admin_log_id_seq OWNED BY public.django_admin_log.id;


--
-- Name: django_celery_beat_clockedschedule; Type: TABLE; Schema: public; Owner: video
--

CREATE TABLE public.django_celery_beat_clockedschedule (
    id integer NOT NULL,
    clocked_time timestamp with time zone NOT NULL
);


ALTER TABLE public.django_celery_beat_clockedschedule OWNER TO video;

--
-- Name: django_celery_beat_clockedschedule_id_seq; Type: SEQUENCE; Schema: public; Owner: video
--

CREATE SEQUENCE public.django_celery_beat_clockedschedule_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_celery_beat_clockedschedule_id_seq OWNER TO video;

--
-- Name: django_celery_beat_clockedschedule_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: video
--

ALTER SEQUENCE public.django_celery_beat_clockedschedule_id_seq OWNED BY public.django_celery_beat_clockedschedule.id;


--
-- Name: django_celery_beat_crontabschedule; Type: TABLE; Schema: public; Owner: video
--

CREATE TABLE public.django_celery_beat_crontabschedule (
    id integer NOT NULL,
    minute character varying(240) NOT NULL,
    hour character varying(96) NOT NULL,
    day_of_week character varying(64) NOT NULL,
    day_of_month character varying(124) NOT NULL,
    month_of_year character varying(64) NOT NULL,
    timezone character varying(63) NOT NULL
);


ALTER TABLE public.django_celery_beat_crontabschedule OWNER TO video;

--
-- Name: django_celery_beat_crontabschedule_id_seq; Type: SEQUENCE; Schema: public; Owner: video
--

CREATE SEQUENCE public.django_celery_beat_crontabschedule_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_celery_beat_crontabschedule_id_seq OWNER TO video;

--
-- Name: django_celery_beat_crontabschedule_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: video
--

ALTER SEQUENCE public.django_celery_beat_crontabschedule_id_seq OWNED BY public.django_celery_beat_crontabschedule.id;


--
-- Name: django_celery_beat_intervalschedule; Type: TABLE; Schema: public; Owner: video
--

CREATE TABLE public.django_celery_beat_intervalschedule (
    id integer NOT NULL,
    every integer NOT NULL,
    period character varying(24) NOT NULL
);


ALTER TABLE public.django_celery_beat_intervalschedule OWNER TO video;

--
-- Name: django_celery_beat_intervalschedule_id_seq; Type: SEQUENCE; Schema: public; Owner: video
--

CREATE SEQUENCE public.django_celery_beat_intervalschedule_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_celery_beat_intervalschedule_id_seq OWNER TO video;

--
-- Name: django_celery_beat_intervalschedule_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: video
--

ALTER SEQUENCE public.django_celery_beat_intervalschedule_id_seq OWNED BY public.django_celery_beat_intervalschedule.id;


--
-- Name: django_celery_beat_periodictask; Type: TABLE; Schema: public; Owner: video
--

CREATE TABLE public.django_celery_beat_periodictask (
    id integer NOT NULL,
    name character varying(200) NOT NULL,
    task character varying(200) NOT NULL,
    args text NOT NULL,
    kwargs text NOT NULL,
    queue character varying(200),
    exchange character varying(200),
    routing_key character varying(200),
    expires timestamp with time zone,
    enabled boolean NOT NULL,
    last_run_at timestamp with time zone,
    total_run_count integer NOT NULL,
    date_changed timestamp with time zone NOT NULL,
    description text NOT NULL,
    crontab_id integer,
    interval_id integer,
    solar_id integer,
    one_off boolean NOT NULL,
    start_time timestamp with time zone,
    priority integer,
    headers text NOT NULL,
    clocked_id integer,
    expire_seconds integer,
    CONSTRAINT django_celery_beat_periodictask_expire_seconds_check CHECK ((expire_seconds >= 0)),
    CONSTRAINT django_celery_beat_periodictask_priority_check CHECK ((priority >= 0)),
    CONSTRAINT django_celery_beat_periodictask_total_run_count_check CHECK ((total_run_count >= 0))
);


ALTER TABLE public.django_celery_beat_periodictask OWNER TO video;

--
-- Name: django_celery_beat_periodictask_id_seq; Type: SEQUENCE; Schema: public; Owner: video
--

CREATE SEQUENCE public.django_celery_beat_periodictask_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_celery_beat_periodictask_id_seq OWNER TO video;

--
-- Name: django_celery_beat_periodictask_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: video
--

ALTER SEQUENCE public.django_celery_beat_periodictask_id_seq OWNED BY public.django_celery_beat_periodictask.id;


--
-- Name: django_celery_beat_periodictasks; Type: TABLE; Schema: public; Owner: video
--

CREATE TABLE public.django_celery_beat_periodictasks (
    ident smallint NOT NULL,
    last_update timestamp with time zone NOT NULL
);


ALTER TABLE public.django_celery_beat_periodictasks OWNER TO video;

--
-- Name: django_celery_beat_solarschedule; Type: TABLE; Schema: public; Owner: video
--

CREATE TABLE public.django_celery_beat_solarschedule (
    id integer NOT NULL,
    event character varying(24) NOT NULL,
    latitude numeric(9,6) NOT NULL,
    longitude numeric(9,6) NOT NULL
);


ALTER TABLE public.django_celery_beat_solarschedule OWNER TO video;

--
-- Name: django_celery_beat_solarschedule_id_seq; Type: SEQUENCE; Schema: public; Owner: video
--

CREATE SEQUENCE public.django_celery_beat_solarschedule_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_celery_beat_solarschedule_id_seq OWNER TO video;

--
-- Name: django_celery_beat_solarschedule_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: video
--

ALTER SEQUENCE public.django_celery_beat_solarschedule_id_seq OWNED BY public.django_celery_beat_solarschedule.id;


--
-- Name: django_content_type; Type: TABLE; Schema: public; Owner: video
--

CREATE TABLE public.django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE public.django_content_type OWNER TO video;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: public; Owner: video
--

CREATE SEQUENCE public.django_content_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_content_type_id_seq OWNER TO video;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: video
--

ALTER SEQUENCE public.django_content_type_id_seq OWNED BY public.django_content_type.id;


--
-- Name: django_migrations; Type: TABLE; Schema: public; Owner: video
--

CREATE TABLE public.django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE public.django_migrations OWNER TO video;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: video
--

CREATE SEQUENCE public.django_migrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_migrations_id_seq OWNER TO video;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: video
--

ALTER SEQUENCE public.django_migrations_id_seq OWNED BY public.django_migrations.id;


--
-- Name: django_session; Type: TABLE; Schema: public; Owner: video
--

CREATE TABLE public.django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE public.django_session OWNER TO video;

--
-- Name: django_site; Type: TABLE; Schema: public; Owner: video
--

CREATE TABLE public.django_site (
    id integer NOT NULL,
    domain character varying(100) NOT NULL,
    name character varying(50) NOT NULL
);


ALTER TABLE public.django_site OWNER TO video;

--
-- Name: django_site_id_seq; Type: SEQUENCE; Schema: public; Owner: video
--

CREATE SEQUENCE public.django_site_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_site_id_seq OWNER TO video;

--
-- Name: django_site_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: video
--

ALTER SEQUENCE public.django_site_id_seq OWNED BY public.django_site.id;


--
-- Name: djangoseo_mymetadatamodel; Type: TABLE; Schema: public; Owner: video
--

CREATE TABLE public.djangoseo_mymetadatamodel (
    id integer NOT NULL,
    title character varying(68) NOT NULL,
    description character varying(155) NOT NULL,
    keywords character varying(511) NOT NULL,
    heading character varying(511) NOT NULL,
    _content_type_id integer NOT NULL
);


ALTER TABLE public.djangoseo_mymetadatamodel OWNER TO video;

--
-- Name: djangoseo_mymetadatamodel_id_seq; Type: SEQUENCE; Schema: public; Owner: video
--

CREATE SEQUENCE public.djangoseo_mymetadatamodel_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.djangoseo_mymetadatamodel_id_seq OWNER TO video;

--
-- Name: djangoseo_mymetadatamodel_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: video
--

ALTER SEQUENCE public.djangoseo_mymetadatamodel_id_seq OWNED BY public.djangoseo_mymetadatamodel.id;


--
-- Name: djangoseo_mymetadatamodelinstance; Type: TABLE; Schema: public; Owner: video
--

CREATE TABLE public.djangoseo_mymetadatamodelinstance (
    id integer NOT NULL,
    title character varying(68) NOT NULL,
    description character varying(155) NOT NULL,
    keywords character varying(511) NOT NULL,
    heading character varying(511) NOT NULL,
    _path character varying(255) NOT NULL,
    _object_id integer NOT NULL,
    _content_type_id integer NOT NULL,
    CONSTRAINT djangoseo_mymetadatamodelinstance__object_id_check CHECK ((_object_id >= 0))
);


ALTER TABLE public.djangoseo_mymetadatamodelinstance OWNER TO video;

--
-- Name: djangoseo_mymetadatamodelinstance_id_seq; Type: SEQUENCE; Schema: public; Owner: video
--

CREATE SEQUENCE public.djangoseo_mymetadatamodelinstance_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.djangoseo_mymetadatamodelinstance_id_seq OWNER TO video;

--
-- Name: djangoseo_mymetadatamodelinstance_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: video
--

ALTER SEQUENCE public.djangoseo_mymetadatamodelinstance_id_seq OWNED BY public.djangoseo_mymetadatamodelinstance.id;


--
-- Name: djangoseo_mymetadatapath; Type: TABLE; Schema: public; Owner: video
--

CREATE TABLE public.djangoseo_mymetadatapath (
    id integer NOT NULL,
    title character varying(68) NOT NULL,
    description character varying(155) NOT NULL,
    keywords character varying(511) NOT NULL,
    heading character varying(511) NOT NULL,
    _path character varying(255) NOT NULL
);


ALTER TABLE public.djangoseo_mymetadatapath OWNER TO video;

--
-- Name: djangoseo_mymetadatapath_id_seq; Type: SEQUENCE; Schema: public; Owner: video
--

CREATE SEQUENCE public.djangoseo_mymetadatapath_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.djangoseo_mymetadatapath_id_seq OWNER TO video;

--
-- Name: djangoseo_mymetadatapath_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: video
--

ALTER SEQUENCE public.djangoseo_mymetadatapath_id_seq OWNED BY public.djangoseo_mymetadatapath.id;


--
-- Name: djangoseo_mymetadataview; Type: TABLE; Schema: public; Owner: video
--

CREATE TABLE public.djangoseo_mymetadataview (
    id integer NOT NULL,
    title character varying(68) NOT NULL,
    description character varying(155) NOT NULL,
    keywords character varying(511) NOT NULL,
    heading character varying(511) NOT NULL,
    _view character varying(255) NOT NULL
);


ALTER TABLE public.djangoseo_mymetadataview OWNER TO video;

--
-- Name: djangoseo_mymetadataview_id_seq; Type: SEQUENCE; Schema: public; Owner: video
--

CREATE SEQUENCE public.djangoseo_mymetadataview_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.djangoseo_mymetadataview_id_seq OWNER TO video;

--
-- Name: djangoseo_mymetadataview_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: video
--

ALTER SEQUENCE public.djangoseo_mymetadataview_id_seq OWNED BY public.djangoseo_mymetadataview.id;


--
-- Name: easy_thumbnails_source; Type: TABLE; Schema: public; Owner: video
--

CREATE TABLE public.easy_thumbnails_source (
    id integer NOT NULL,
    storage_hash character varying(40) NOT NULL,
    name character varying(255) NOT NULL,
    modified timestamp with time zone NOT NULL
);


ALTER TABLE public.easy_thumbnails_source OWNER TO video;

--
-- Name: easy_thumbnails_source_id_seq; Type: SEQUENCE; Schema: public; Owner: video
--

CREATE SEQUENCE public.easy_thumbnails_source_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.easy_thumbnails_source_id_seq OWNER TO video;

--
-- Name: easy_thumbnails_source_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: video
--

ALTER SEQUENCE public.easy_thumbnails_source_id_seq OWNED BY public.easy_thumbnails_source.id;


--
-- Name: easy_thumbnails_thumbnail; Type: TABLE; Schema: public; Owner: video
--

CREATE TABLE public.easy_thumbnails_thumbnail (
    id integer NOT NULL,
    storage_hash character varying(40) NOT NULL,
    name character varying(255) NOT NULL,
    modified timestamp with time zone NOT NULL,
    source_id integer NOT NULL
);


ALTER TABLE public.easy_thumbnails_thumbnail OWNER TO video;

--
-- Name: easy_thumbnails_thumbnail_id_seq; Type: SEQUENCE; Schema: public; Owner: video
--

CREATE SEQUENCE public.easy_thumbnails_thumbnail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.easy_thumbnails_thumbnail_id_seq OWNER TO video;

--
-- Name: easy_thumbnails_thumbnail_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: video
--

ALTER SEQUENCE public.easy_thumbnails_thumbnail_id_seq OWNED BY public.easy_thumbnails_thumbnail.id;


--
-- Name: easy_thumbnails_thumbnaildimensions; Type: TABLE; Schema: public; Owner: video
--

CREATE TABLE public.easy_thumbnails_thumbnaildimensions (
    id integer NOT NULL,
    thumbnail_id integer NOT NULL,
    width integer,
    height integer,
    CONSTRAINT easy_thumbnails_thumbnaildimensions_height_check CHECK ((height >= 0)),
    CONSTRAINT easy_thumbnails_thumbnaildimensions_width_check CHECK ((width >= 0))
);


ALTER TABLE public.easy_thumbnails_thumbnaildimensions OWNER TO video;

--
-- Name: easy_thumbnails_thumbnaildimensions_id_seq; Type: SEQUENCE; Schema: public; Owner: video
--

CREATE SEQUENCE public.easy_thumbnails_thumbnaildimensions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.easy_thumbnails_thumbnaildimensions_id_seq OWNER TO video;

--
-- Name: easy_thumbnails_thumbnaildimensions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: video
--

ALTER SEQUENCE public.easy_thumbnails_thumbnaildimensions_id_seq OWNED BY public.easy_thumbnails_thumbnaildimensions.id;


--
-- Name: lesson_article; Type: TABLE; Schema: public; Owner: video
--

CREATE TABLE public.lesson_article (
    id integer NOT NULL,
    title character varying(128),
    description character varying(128),
    text text,
    answer character varying(128),
    active boolean NOT NULL,
    created timestamp with time zone NOT NULL,
    updated timestamp with time zone NOT NULL,
    user_id integer,
    lesson_id integer
);


ALTER TABLE public.lesson_article OWNER TO video;

--
-- Name: lesson_article_id_seq; Type: SEQUENCE; Schema: public; Owner: video
--

CREATE SEQUENCE public.lesson_article_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.lesson_article_id_seq OWNER TO video;

--
-- Name: lesson_article_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: video
--

ALTER SEQUENCE public.lesson_article_id_seq OWNED BY public.lesson_article.id;


--
-- Name: lesson_lesson; Type: TABLE; Schema: public; Owner: video
--

CREATE TABLE public.lesson_lesson (
    id integer NOT NULL,
    title character varying(128),
    description character varying(128),
    category integer NOT NULL,
    active boolean NOT NULL,
    created timestamp with time zone NOT NULL,
    updated timestamp with time zone NOT NULL,
    user_id integer
);


ALTER TABLE public.lesson_lesson OWNER TO video;

--
-- Name: lesson_lesson_id_seq; Type: SEQUENCE; Schema: public; Owner: video
--

CREATE SEQUENCE public.lesson_lesson_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.lesson_lesson_id_seq OWNER TO video;

--
-- Name: lesson_lesson_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: video
--

ALTER SEQUENCE public.lesson_lesson_id_seq OWNED BY public.lesson_lesson.id;


--
-- Name: oauth2_provider_accesstoken; Type: TABLE; Schema: public; Owner: video
--

CREATE TABLE public.oauth2_provider_accesstoken (
    id bigint NOT NULL,
    token character varying(255) NOT NULL,
    expires timestamp with time zone NOT NULL,
    scope text NOT NULL,
    application_id bigint,
    user_id integer,
    created timestamp with time zone NOT NULL,
    updated timestamp with time zone NOT NULL,
    source_refresh_token_id bigint,
    id_token_id bigint
);


ALTER TABLE public.oauth2_provider_accesstoken OWNER TO video;

--
-- Name: oauth2_provider_accesstoken_id_seq; Type: SEQUENCE; Schema: public; Owner: video
--

CREATE SEQUENCE public.oauth2_provider_accesstoken_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.oauth2_provider_accesstoken_id_seq OWNER TO video;

--
-- Name: oauth2_provider_accesstoken_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: video
--

ALTER SEQUENCE public.oauth2_provider_accesstoken_id_seq OWNED BY public.oauth2_provider_accesstoken.id;


--
-- Name: oauth2_provider_application; Type: TABLE; Schema: public; Owner: video
--

CREATE TABLE public.oauth2_provider_application (
    id bigint NOT NULL,
    client_id character varying(100) NOT NULL,
    redirect_uris text NOT NULL,
    client_type character varying(32) NOT NULL,
    authorization_grant_type character varying(32) NOT NULL,
    client_secret character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    user_id integer,
    skip_authorization boolean NOT NULL,
    created timestamp with time zone NOT NULL,
    updated timestamp with time zone NOT NULL,
    algorithm character varying(5) NOT NULL
);


ALTER TABLE public.oauth2_provider_application OWNER TO video;

--
-- Name: oauth2_provider_application_id_seq; Type: SEQUENCE; Schema: public; Owner: video
--

CREATE SEQUENCE public.oauth2_provider_application_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.oauth2_provider_application_id_seq OWNER TO video;

--
-- Name: oauth2_provider_application_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: video
--

ALTER SEQUENCE public.oauth2_provider_application_id_seq OWNED BY public.oauth2_provider_application.id;


--
-- Name: oauth2_provider_grant; Type: TABLE; Schema: public; Owner: video
--

CREATE TABLE public.oauth2_provider_grant (
    id bigint NOT NULL,
    code character varying(255) NOT NULL,
    expires timestamp with time zone NOT NULL,
    redirect_uri text NOT NULL,
    scope text NOT NULL,
    application_id bigint NOT NULL,
    user_id integer NOT NULL,
    created timestamp with time zone NOT NULL,
    updated timestamp with time zone NOT NULL,
    code_challenge character varying(128) NOT NULL,
    code_challenge_method character varying(10) NOT NULL,
    nonce character varying(255) NOT NULL,
    claims text NOT NULL
);


ALTER TABLE public.oauth2_provider_grant OWNER TO video;

--
-- Name: oauth2_provider_grant_id_seq; Type: SEQUENCE; Schema: public; Owner: video
--

CREATE SEQUENCE public.oauth2_provider_grant_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.oauth2_provider_grant_id_seq OWNER TO video;

--
-- Name: oauth2_provider_grant_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: video
--

ALTER SEQUENCE public.oauth2_provider_grant_id_seq OWNED BY public.oauth2_provider_grant.id;


--
-- Name: oauth2_provider_idtoken; Type: TABLE; Schema: public; Owner: video
--

CREATE TABLE public.oauth2_provider_idtoken (
    id bigint NOT NULL,
    jti uuid NOT NULL,
    expires timestamp with time zone NOT NULL,
    scope text NOT NULL,
    created timestamp with time zone NOT NULL,
    updated timestamp with time zone NOT NULL,
    application_id bigint,
    user_id integer
);


ALTER TABLE public.oauth2_provider_idtoken OWNER TO video;

--
-- Name: oauth2_provider_idtoken_id_seq; Type: SEQUENCE; Schema: public; Owner: video
--

CREATE SEQUENCE public.oauth2_provider_idtoken_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.oauth2_provider_idtoken_id_seq OWNER TO video;

--
-- Name: oauth2_provider_idtoken_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: video
--

ALTER SEQUENCE public.oauth2_provider_idtoken_id_seq OWNED BY public.oauth2_provider_idtoken.id;


--
-- Name: oauth2_provider_refreshtoken; Type: TABLE; Schema: public; Owner: video
--

CREATE TABLE public.oauth2_provider_refreshtoken (
    id bigint NOT NULL,
    token character varying(255) NOT NULL,
    access_token_id bigint,
    application_id bigint NOT NULL,
    user_id integer NOT NULL,
    created timestamp with time zone NOT NULL,
    updated timestamp with time zone NOT NULL,
    revoked timestamp with time zone
);


ALTER TABLE public.oauth2_provider_refreshtoken OWNER TO video;

--
-- Name: oauth2_provider_refreshtoken_id_seq; Type: SEQUENCE; Schema: public; Owner: video
--

CREATE SEQUENCE public.oauth2_provider_refreshtoken_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.oauth2_provider_refreshtoken_id_seq OWNER TO video;

--
-- Name: oauth2_provider_refreshtoken_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: video
--

ALTER SEQUENCE public.oauth2_provider_refreshtoken_id_seq OWNED BY public.oauth2_provider_refreshtoken.id;


--
-- Name: social_auth_association; Type: TABLE; Schema: public; Owner: video
--

CREATE TABLE public.social_auth_association (
    id bigint NOT NULL,
    server_url character varying(255) NOT NULL,
    handle character varying(255) NOT NULL,
    secret character varying(255) NOT NULL,
    issued integer NOT NULL,
    lifetime integer NOT NULL,
    assoc_type character varying(64) NOT NULL
);


ALTER TABLE public.social_auth_association OWNER TO video;

--
-- Name: social_auth_association_id_seq; Type: SEQUENCE; Schema: public; Owner: video
--

CREATE SEQUENCE public.social_auth_association_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.social_auth_association_id_seq OWNER TO video;

--
-- Name: social_auth_association_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: video
--

ALTER SEQUENCE public.social_auth_association_id_seq OWNED BY public.social_auth_association.id;


--
-- Name: social_auth_code; Type: TABLE; Schema: public; Owner: video
--

CREATE TABLE public.social_auth_code (
    id bigint NOT NULL,
    email character varying(254) NOT NULL,
    code character varying(32) NOT NULL,
    verified boolean NOT NULL,
    "timestamp" timestamp with time zone NOT NULL
);


ALTER TABLE public.social_auth_code OWNER TO video;

--
-- Name: social_auth_code_id_seq; Type: SEQUENCE; Schema: public; Owner: video
--

CREATE SEQUENCE public.social_auth_code_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.social_auth_code_id_seq OWNER TO video;

--
-- Name: social_auth_code_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: video
--

ALTER SEQUENCE public.social_auth_code_id_seq OWNED BY public.social_auth_code.id;


--
-- Name: social_auth_nonce; Type: TABLE; Schema: public; Owner: video
--

CREATE TABLE public.social_auth_nonce (
    id bigint NOT NULL,
    server_url character varying(255) NOT NULL,
    "timestamp" integer NOT NULL,
    salt character varying(65) NOT NULL
);


ALTER TABLE public.social_auth_nonce OWNER TO video;

--
-- Name: social_auth_nonce_id_seq; Type: SEQUENCE; Schema: public; Owner: video
--

CREATE SEQUENCE public.social_auth_nonce_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.social_auth_nonce_id_seq OWNER TO video;

--
-- Name: social_auth_nonce_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: video
--

ALTER SEQUENCE public.social_auth_nonce_id_seq OWNED BY public.social_auth_nonce.id;


--
-- Name: social_auth_partial; Type: TABLE; Schema: public; Owner: video
--

CREATE TABLE public.social_auth_partial (
    id bigint NOT NULL,
    token character varying(32) NOT NULL,
    next_step smallint NOT NULL,
    backend character varying(32) NOT NULL,
    "timestamp" timestamp with time zone NOT NULL,
    data jsonb NOT NULL,
    CONSTRAINT social_auth_partial_next_step_check CHECK ((next_step >= 0))
);


ALTER TABLE public.social_auth_partial OWNER TO video;

--
-- Name: social_auth_partial_id_seq; Type: SEQUENCE; Schema: public; Owner: video
--

CREATE SEQUENCE public.social_auth_partial_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.social_auth_partial_id_seq OWNER TO video;

--
-- Name: social_auth_partial_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: video
--

ALTER SEQUENCE public.social_auth_partial_id_seq OWNED BY public.social_auth_partial.id;


--
-- Name: social_auth_usersocialauth; Type: TABLE; Schema: public; Owner: video
--

CREATE TABLE public.social_auth_usersocialauth (
    id bigint NOT NULL,
    provider character varying(32) NOT NULL,
    uid character varying(255) NOT NULL,
    user_id integer NOT NULL,
    created timestamp with time zone NOT NULL,
    modified timestamp with time zone NOT NULL,
    extra_data jsonb NOT NULL
);


ALTER TABLE public.social_auth_usersocialauth OWNER TO video;

--
-- Name: social_auth_usersocialauth_id_seq; Type: SEQUENCE; Schema: public; Owner: video
--

CREATE SEQUENCE public.social_auth_usersocialauth_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.social_auth_usersocialauth_id_seq OWNER TO video;

--
-- Name: social_auth_usersocialauth_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: video
--

ALTER SEQUENCE public.social_auth_usersocialauth_id_seq OWNED BY public.social_auth_usersocialauth.id;


--
-- Name: socialaccount_socialaccount; Type: TABLE; Schema: public; Owner: video
--

CREATE TABLE public.socialaccount_socialaccount (
    id integer NOT NULL,
    provider character varying(200) NOT NULL,
    uid character varying(191) NOT NULL,
    last_login timestamp with time zone NOT NULL,
    date_joined timestamp with time zone NOT NULL,
    extra_data jsonb NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE public.socialaccount_socialaccount OWNER TO video;

--
-- Name: socialaccount_socialaccount_id_seq; Type: SEQUENCE; Schema: public; Owner: video
--

CREATE SEQUENCE public.socialaccount_socialaccount_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.socialaccount_socialaccount_id_seq OWNER TO video;

--
-- Name: socialaccount_socialaccount_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: video
--

ALTER SEQUENCE public.socialaccount_socialaccount_id_seq OWNED BY public.socialaccount_socialaccount.id;


--
-- Name: socialaccount_socialapp; Type: TABLE; Schema: public; Owner: video
--

CREATE TABLE public.socialaccount_socialapp (
    id integer NOT NULL,
    provider character varying(30) NOT NULL,
    name character varying(40) NOT NULL,
    client_id character varying(191) NOT NULL,
    secret character varying(191) NOT NULL,
    key character varying(191) NOT NULL,
    provider_id character varying(200) NOT NULL,
    settings jsonb NOT NULL
);


ALTER TABLE public.socialaccount_socialapp OWNER TO video;

--
-- Name: socialaccount_socialapp_id_seq; Type: SEQUENCE; Schema: public; Owner: video
--

CREATE SEQUENCE public.socialaccount_socialapp_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.socialaccount_socialapp_id_seq OWNER TO video;

--
-- Name: socialaccount_socialapp_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: video
--

ALTER SEQUENCE public.socialaccount_socialapp_id_seq OWNED BY public.socialaccount_socialapp.id;


--
-- Name: socialaccount_socialapp_sites; Type: TABLE; Schema: public; Owner: video
--

CREATE TABLE public.socialaccount_socialapp_sites (
    id integer NOT NULL,
    socialapp_id integer NOT NULL,
    site_id integer NOT NULL
);


ALTER TABLE public.socialaccount_socialapp_sites OWNER TO video;

--
-- Name: socialaccount_socialapp_sites_id_seq; Type: SEQUENCE; Schema: public; Owner: video
--

CREATE SEQUENCE public.socialaccount_socialapp_sites_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.socialaccount_socialapp_sites_id_seq OWNER TO video;

--
-- Name: socialaccount_socialapp_sites_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: video
--

ALTER SEQUENCE public.socialaccount_socialapp_sites_id_seq OWNED BY public.socialaccount_socialapp_sites.id;


--
-- Name: socialaccount_socialtoken; Type: TABLE; Schema: public; Owner: video
--

CREATE TABLE public.socialaccount_socialtoken (
    id integer NOT NULL,
    token text NOT NULL,
    token_secret text NOT NULL,
    expires_at timestamp with time zone,
    account_id integer NOT NULL,
    app_id integer
);


ALTER TABLE public.socialaccount_socialtoken OWNER TO video;

--
-- Name: socialaccount_socialtoken_id_seq; Type: SEQUENCE; Schema: public; Owner: video
--

CREATE SEQUENCE public.socialaccount_socialtoken_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.socialaccount_socialtoken_id_seq OWNER TO video;

--
-- Name: socialaccount_socialtoken_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: video
--

ALTER SEQUENCE public.socialaccount_socialtoken_id_seq OWNED BY public.socialaccount_socialtoken.id;


--
-- Name: video_chat_token; Type: TABLE; Schema: public; Owner: video
--

CREATE TABLE public.video_chat_token (
    id integer NOT NULL,
    key character varying(200) NOT NULL,
    created timestamp with time zone NOT NULL,
    updated timestamp with time zone NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE public.video_chat_token OWNER TO video;

--
-- Name: video_chat_token_id_seq; Type: SEQUENCE; Schema: public; Owner: video
--

CREATE SEQUENCE public.video_chat_token_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.video_chat_token_id_seq OWNER TO video;

--
-- Name: video_chat_token_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: video
--

ALTER SEQUENCE public.video_chat_token_id_seq OWNED BY public.video_chat_token.id;


--
-- Name: video_chat_userprofile; Type: TABLE; Schema: public; Owner: video
--

CREATE TABLE public.video_chat_userprofile (
    id integer NOT NULL,
    avatar character varying(100) NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE public.video_chat_userprofile OWNER TO video;

--
-- Name: video_chat_userprofile_id_seq; Type: SEQUENCE; Schema: public; Owner: video
--

CREATE SEQUENCE public.video_chat_userprofile_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.video_chat_userprofile_id_seq OWNER TO video;

--
-- Name: video_chat_userprofile_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: video
--

ALTER SEQUENCE public.video_chat_userprofile_id_seq OWNED BY public.video_chat_userprofile.id;


--
-- Name: account_emailaddress id; Type: DEFAULT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.account_emailaddress ALTER COLUMN id SET DEFAULT nextval('public.account_emailaddress_id_seq'::regclass);


--
-- Name: account_emailconfirmation id; Type: DEFAULT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.account_emailconfirmation ALTER COLUMN id SET DEFAULT nextval('public.account_emailconfirmation_id_seq'::regclass);


--
-- Name: auth_group id; Type: DEFAULT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.auth_group ALTER COLUMN id SET DEFAULT nextval('public.auth_group_id_seq'::regclass);


--
-- Name: auth_group_permissions id; Type: DEFAULT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('public.auth_group_permissions_id_seq'::regclass);


--
-- Name: auth_permission id; Type: DEFAULT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.auth_permission ALTER COLUMN id SET DEFAULT nextval('public.auth_permission_id_seq'::regclass);


--
-- Name: auth_user id; Type: DEFAULT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.auth_user ALTER COLUMN id SET DEFAULT nextval('public.auth_user_id_seq'::regclass);


--
-- Name: auth_user_groups id; Type: DEFAULT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.auth_user_groups ALTER COLUMN id SET DEFAULT nextval('public.auth_user_groups_id_seq'::regclass);


--
-- Name: auth_user_user_permissions id; Type: DEFAULT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.auth_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('public.auth_user_user_permissions_id_seq'::regclass);


--
-- Name: chunks_chunk id; Type: DEFAULT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.chunks_chunk ALTER COLUMN id SET DEFAULT nextval('public.chunks_chunk_id_seq'::regclass);


--
-- Name: chunks_group id; Type: DEFAULT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.chunks_group ALTER COLUMN id SET DEFAULT nextval('public.chunks_group_id_seq'::regclass);


--
-- Name: chunks_image id; Type: DEFAULT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.chunks_image ALTER COLUMN id SET DEFAULT nextval('public.chunks_image_id_seq'::regclass);


--
-- Name: chunks_media id; Type: DEFAULT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.chunks_media ALTER COLUMN id SET DEFAULT nextval('public.chunks_media_id_seq'::regclass);


--
-- Name: comunicate_chat id; Type: DEFAULT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.comunicate_chat ALTER COLUMN id SET DEFAULT nextval('public.comunicate_chat_id_seq'::regclass);


--
-- Name: comunicate_language id; Type: DEFAULT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.comunicate_language ALTER COLUMN id SET DEFAULT nextval('public.comunicate_language_id_seq'::regclass);


--
-- Name: comunicate_member id; Type: DEFAULT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.comunicate_member ALTER COLUMN id SET DEFAULT nextval('public.comunicate_member_id_seq'::regclass);


--
-- Name: comunicate_room id; Type: DEFAULT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.comunicate_room ALTER COLUMN id SET DEFAULT nextval('public.comunicate_room_id_seq'::regclass);


--
-- Name: comunicate_subject id; Type: DEFAULT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.comunicate_subject ALTER COLUMN id SET DEFAULT nextval('public.comunicate_subject_id_seq'::regclass);


--
-- Name: django_admin_log id; Type: DEFAULT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.django_admin_log ALTER COLUMN id SET DEFAULT nextval('public.django_admin_log_id_seq'::regclass);


--
-- Name: django_celery_beat_clockedschedule id; Type: DEFAULT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.django_celery_beat_clockedschedule ALTER COLUMN id SET DEFAULT nextval('public.django_celery_beat_clockedschedule_id_seq'::regclass);


--
-- Name: django_celery_beat_crontabschedule id; Type: DEFAULT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.django_celery_beat_crontabschedule ALTER COLUMN id SET DEFAULT nextval('public.django_celery_beat_crontabschedule_id_seq'::regclass);


--
-- Name: django_celery_beat_intervalschedule id; Type: DEFAULT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.django_celery_beat_intervalschedule ALTER COLUMN id SET DEFAULT nextval('public.django_celery_beat_intervalschedule_id_seq'::regclass);


--
-- Name: django_celery_beat_periodictask id; Type: DEFAULT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.django_celery_beat_periodictask ALTER COLUMN id SET DEFAULT nextval('public.django_celery_beat_periodictask_id_seq'::regclass);


--
-- Name: django_celery_beat_solarschedule id; Type: DEFAULT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.django_celery_beat_solarschedule ALTER COLUMN id SET DEFAULT nextval('public.django_celery_beat_solarschedule_id_seq'::regclass);


--
-- Name: django_content_type id; Type: DEFAULT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.django_content_type ALTER COLUMN id SET DEFAULT nextval('public.django_content_type_id_seq'::regclass);


--
-- Name: django_migrations id; Type: DEFAULT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.django_migrations ALTER COLUMN id SET DEFAULT nextval('public.django_migrations_id_seq'::regclass);


--
-- Name: django_site id; Type: DEFAULT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.django_site ALTER COLUMN id SET DEFAULT nextval('public.django_site_id_seq'::regclass);


--
-- Name: djangoseo_mymetadatamodel id; Type: DEFAULT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.djangoseo_mymetadatamodel ALTER COLUMN id SET DEFAULT nextval('public.djangoseo_mymetadatamodel_id_seq'::regclass);


--
-- Name: djangoseo_mymetadatamodelinstance id; Type: DEFAULT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.djangoseo_mymetadatamodelinstance ALTER COLUMN id SET DEFAULT nextval('public.djangoseo_mymetadatamodelinstance_id_seq'::regclass);


--
-- Name: djangoseo_mymetadatapath id; Type: DEFAULT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.djangoseo_mymetadatapath ALTER COLUMN id SET DEFAULT nextval('public.djangoseo_mymetadatapath_id_seq'::regclass);


--
-- Name: djangoseo_mymetadataview id; Type: DEFAULT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.djangoseo_mymetadataview ALTER COLUMN id SET DEFAULT nextval('public.djangoseo_mymetadataview_id_seq'::regclass);


--
-- Name: easy_thumbnails_source id; Type: DEFAULT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.easy_thumbnails_source ALTER COLUMN id SET DEFAULT nextval('public.easy_thumbnails_source_id_seq'::regclass);


--
-- Name: easy_thumbnails_thumbnail id; Type: DEFAULT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.easy_thumbnails_thumbnail ALTER COLUMN id SET DEFAULT nextval('public.easy_thumbnails_thumbnail_id_seq'::regclass);


--
-- Name: easy_thumbnails_thumbnaildimensions id; Type: DEFAULT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.easy_thumbnails_thumbnaildimensions ALTER COLUMN id SET DEFAULT nextval('public.easy_thumbnails_thumbnaildimensions_id_seq'::regclass);


--
-- Name: lesson_article id; Type: DEFAULT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.lesson_article ALTER COLUMN id SET DEFAULT nextval('public.lesson_article_id_seq'::regclass);


--
-- Name: lesson_lesson id; Type: DEFAULT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.lesson_lesson ALTER COLUMN id SET DEFAULT nextval('public.lesson_lesson_id_seq'::regclass);


--
-- Name: oauth2_provider_accesstoken id; Type: DEFAULT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.oauth2_provider_accesstoken ALTER COLUMN id SET DEFAULT nextval('public.oauth2_provider_accesstoken_id_seq'::regclass);


--
-- Name: oauth2_provider_application id; Type: DEFAULT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.oauth2_provider_application ALTER COLUMN id SET DEFAULT nextval('public.oauth2_provider_application_id_seq'::regclass);


--
-- Name: oauth2_provider_grant id; Type: DEFAULT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.oauth2_provider_grant ALTER COLUMN id SET DEFAULT nextval('public.oauth2_provider_grant_id_seq'::regclass);


--
-- Name: oauth2_provider_idtoken id; Type: DEFAULT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.oauth2_provider_idtoken ALTER COLUMN id SET DEFAULT nextval('public.oauth2_provider_idtoken_id_seq'::regclass);


--
-- Name: oauth2_provider_refreshtoken id; Type: DEFAULT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.oauth2_provider_refreshtoken ALTER COLUMN id SET DEFAULT nextval('public.oauth2_provider_refreshtoken_id_seq'::regclass);


--
-- Name: social_auth_association id; Type: DEFAULT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.social_auth_association ALTER COLUMN id SET DEFAULT nextval('public.social_auth_association_id_seq'::regclass);


--
-- Name: social_auth_code id; Type: DEFAULT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.social_auth_code ALTER COLUMN id SET DEFAULT nextval('public.social_auth_code_id_seq'::regclass);


--
-- Name: social_auth_nonce id; Type: DEFAULT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.social_auth_nonce ALTER COLUMN id SET DEFAULT nextval('public.social_auth_nonce_id_seq'::regclass);


--
-- Name: social_auth_partial id; Type: DEFAULT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.social_auth_partial ALTER COLUMN id SET DEFAULT nextval('public.social_auth_partial_id_seq'::regclass);


--
-- Name: social_auth_usersocialauth id; Type: DEFAULT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.social_auth_usersocialauth ALTER COLUMN id SET DEFAULT nextval('public.social_auth_usersocialauth_id_seq'::regclass);


--
-- Name: socialaccount_socialaccount id; Type: DEFAULT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.socialaccount_socialaccount ALTER COLUMN id SET DEFAULT nextval('public.socialaccount_socialaccount_id_seq'::regclass);


--
-- Name: socialaccount_socialapp id; Type: DEFAULT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.socialaccount_socialapp ALTER COLUMN id SET DEFAULT nextval('public.socialaccount_socialapp_id_seq'::regclass);


--
-- Name: socialaccount_socialapp_sites id; Type: DEFAULT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.socialaccount_socialapp_sites ALTER COLUMN id SET DEFAULT nextval('public.socialaccount_socialapp_sites_id_seq'::regclass);


--
-- Name: socialaccount_socialtoken id; Type: DEFAULT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.socialaccount_socialtoken ALTER COLUMN id SET DEFAULT nextval('public.socialaccount_socialtoken_id_seq'::regclass);


--
-- Name: video_chat_token id; Type: DEFAULT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.video_chat_token ALTER COLUMN id SET DEFAULT nextval('public.video_chat_token_id_seq'::regclass);


--
-- Name: video_chat_userprofile id; Type: DEFAULT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.video_chat_userprofile ALTER COLUMN id SET DEFAULT nextval('public.video_chat_userprofile_id_seq'::regclass);


--
-- Data for Name: account_emailaddress; Type: TABLE DATA; Schema: public; Owner: video
--

COPY public.account_emailaddress (id, email, verified, "primary", user_id) FROM stdin;
1	crackkc@mail.ru	f	t	5
2	video@gmail.com	f	t	6
3	video1@gmail.com	f	t	7
4	nosacevaulia53@gmail.com	t	t	8
\.


--
-- Data for Name: account_emailconfirmation; Type: TABLE DATA; Schema: public; Owner: video
--

COPY public.account_emailconfirmation (id, created, sent, key, email_address_id) FROM stdin;
\.


--
-- Data for Name: auth_group; Type: TABLE DATA; Schema: public; Owner: video
--

COPY public.auth_group (id, name) FROM stdin;
\.


--
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: public; Owner: video
--

COPY public.auth_group_permissions (id, group_id, permission_id) FROM stdin;
\.


--
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: public; Owner: video
--

COPY public.auth_permission (id, name, content_type_id, codename) FROM stdin;
1	Can add log entry	1	add_logentry
2	Can change log entry	1	change_logentry
3	Can delete log entry	1	delete_logentry
4	Can add group	2	add_group
5	Can change group	2	change_group
6	Can delete group	2	delete_group
7	Can add permission	3	add_permission
8	Can change permission	3	change_permission
9	Can delete permission	3	delete_permission
10	Can add user	4	add_user
11	Can change user	4	change_user
12	Can delete user	4	delete_user
13	Can add content type	5	add_contenttype
14	Can change content type	5	change_contenttype
15	Can delete content type	5	delete_contenttype
16	Can add session	6	add_session
17	Can change session	6	change_session
18	Can delete session	6	delete_session
19	Can add photo	7	add_photo
20	Can change photo	7	change_photo
21	Can delete photo	7	delete_photo
22	Can add place	8	add_place
23	Can change place	8	change_place
24	Can delete place	8	delete_place
25	Can add thumbnail dimensions	9	add_thumbnaildimensions
26	Can change thumbnail dimensions	9	change_thumbnaildimensions
27	Can delete thumbnail dimensions	9	delete_thumbnaildimensions
28	Can add thumbnail	10	add_thumbnail
29	Can change thumbnail	10	change_thumbnail
30	Can delete thumbnail	10	delete_thumbnail
31	Can add source	11	add_source
32	Can change source	11	change_source
33	Can delete source	11	delete_source
34	Can add article	12	add_article
35	Can change article	12	change_article
36	Can delete article	12	delete_article
37	Can add book	13	add_book
38	Can change book	13	change_book
39	Can delete book	13	delete_book
40	Can add organisation	14	add_organisation
41	Can change organisation	14	change_organisation
42	Can delete organisation	14	delete_organisation
43	Can add site	15	add_site
44	Can change site	15	change_site
45	Can delete site	15	delete_site
46	Can add Text Chunk	16	add_chunk
47	Can change Text Chunk	16	change_chunk
48	Can delete Text Chunk	16	delete_chunk
49	Can add Group Chunk	17	add_group
50	Can change Group Chunk	17	change_group
51	Can delete Group Chunk	17	delete_group
52	Can add Image Chunk	18	add_image
53	Can change Image Chunk	18	change_image
54	Can delete Image Chunk	18	delete_image
55	Can add Media Chunk	19	add_media
56	Can change Media Chunk	19	change_media
57	Can delete Media Chunk	19	delete_media
58	Can add my metadata (Path)	20	add_mymetadatapath
59	Can change my metadata (Path)	20	change_mymetadatapath
60	Can delete my metadata (Path)	20	delete_mymetadatapath
61	Can add my metadata (Model Instance)	21	add_mymetadatamodelinstance
62	Can change my metadata (Model Instance)	21	change_mymetadatamodelinstance
63	Can delete my metadata (Model Instance)	21	delete_mymetadatamodelinstance
64	Can add my metadata (Model)	22	add_mymetadatamodel
65	Can change my metadata (Model)	22	change_mymetadatamodel
66	Can delete my metadata (Model)	22	delete_mymetadatamodel
67	Can add my metadata (View)	23	add_mymetadataview
68	Can change my metadata (View)	23	change_mymetadataview
69	Can delete my metadata (View)	23	delete_mymetadataview
70	Can add enumeration	24	add_enumeration
71	Can change enumeration	24	change_enumeration
72	Can delete enumeration	24	delete_enumeration
73	Can add photo	25	add_photo
74	Can change photo	25	change_photo
75	Can delete photo	25	delete_photo
76	Can add count parse	26	add_countparse
77	Can change count parse	26	change_countparse
78	Can delete count parse	26	delete_countparse
79	Can add parsing	27	add_parsing
80	Can change parsing	27	change_parsing
81	Can delete parsing	27	delete_parsing
82	Can add type page	28	add_typepage
83	Can change type page	28	change_typepage
84	Can delete type page	28	delete_typepage
85	Can add solar event	29	add_solarschedule
86	Can change solar event	29	change_solarschedule
87	Can delete solar event	29	delete_solarschedule
88	Can add clocked	30	add_clockedschedule
89	Can change clocked	30	change_clockedschedule
90	Can delete clocked	30	delete_clockedschedule
91	Can add periodic tasks	31	add_periodictasks
92	Can change periodic tasks	31	change_periodictasks
93	Can delete periodic tasks	31	delete_periodictasks
94	Can add periodic task	32	add_periodictask
95	Can change periodic task	32	change_periodictask
96	Can delete periodic task	32	delete_periodictask
97	Can add interval	33	add_intervalschedule
98	Can change interval	33	change_intervalschedule
99	Can delete interval	33	delete_intervalschedule
100	Can add crontab	34	add_crontabschedule
101	Can change crontab	34	change_crontabschedule
102	Can delete crontab	34	delete_crontabschedule
103	Can view log entry	1	view_logentry
104	Can view permission	3	view_permission
105	Can view group	2	view_group
106	Can view user	4	view_user
107	Can view content type	5	view_contenttype
108	Can view session	6	view_session
109	Can view site	15	view_site
110	Can add seo	35	add_seo
111	Can change seo	35	change_seo
112	Can delete seo	35	delete_seo
113	Can view seo	35	view_seo
114	Can view count parse	26	view_countparse
115	Can view parsing	27	view_parsing
116	Can view type page	28	view_typepage
117	Can view source	11	view_source
118	Can view thumbnail	10	view_thumbnail
119	Can view thumbnail dimensions	9	view_thumbnaildimensions
120	Can view article	12	view_article
121	Can view photo	7	view_photo
122	Can view place	8	view_place
123	Can view book	13	view_book
124	Can view Организация	14	view_organisation
125	Can view Фотография	25	view_photo
126	Can view Text Chunk	16	view_chunk
127	Can view Group Chunk	17	view_group
128	Can view Image Chunk	18	view_image
129	Can view Media Chunk	19	view_media
130	Can view enumeration	24	view_enumeration
131	Can view crontab	34	view_crontabschedule
132	Can view interval	33	view_intervalschedule
133	Can view periodic task	32	view_periodictask
134	Can view periodic tasks	31	view_periodictasks
135	Can view solar event	29	view_solarschedule
136	Can view clocked	30	view_clockedschedule
137	Can add language	36	add_language
138	Can change language	36	change_language
139	Can delete language	36	delete_language
140	Can view language	36	view_language
141	Can add subject	37	add_subject
142	Can change subject	37	change_subject
143	Can delete subject	37	delete_subject
144	Can view subject	37	view_subject
145	Can add chat	38	add_chat
146	Can change chat	38	change_chat
147	Can delete chat	38	delete_chat
148	Can view chat	38	view_chat
149	Can add room	39	add_room
150	Can change room	39	change_room
151	Can delete room	39	delete_room
152	Can view room	39	view_room
153	Can add member	40	add_member
154	Can change member	40	change_member
155	Can delete member	40	delete_member
156	Can view member	40	view_member
157	Can add lesson	41	add_lesson
158	Can change lesson	41	change_lesson
159	Can delete lesson	41	delete_lesson
160	Can view lesson	41	view_lesson
161	Can add article	42	add_article
162	Can change article	42	change_article
163	Can delete article	42	delete_article
164	Can view article	42	view_article
165	Can add email address	43	add_emailaddress
166	Can change email address	43	change_emailaddress
167	Can delete email address	43	delete_emailaddress
168	Can view email address	43	view_emailaddress
169	Can add email confirmation	44	add_emailconfirmation
170	Can change email confirmation	44	change_emailconfirmation
171	Can delete email confirmation	44	delete_emailconfirmation
172	Can view email confirmation	44	view_emailconfirmation
173	Can add social account	45	add_socialaccount
174	Can change social account	45	change_socialaccount
175	Can delete social account	45	delete_socialaccount
176	Can view social account	45	view_socialaccount
177	Can add social application	46	add_socialapp
178	Can change social application	46	change_socialapp
179	Can delete social application	46	delete_socialapp
180	Can view social application	46	view_socialapp
181	Can add social application token	47	add_socialtoken
182	Can change social application token	47	change_socialtoken
183	Can delete social application token	47	delete_socialtoken
184	Can view social application token	47	view_socialtoken
185	Can add Token	48	add_token
186	Can change Token	48	change_token
187	Can delete Token	48	delete_token
188	Can view Token	48	view_token
189	Can add token	49	add_tokenproxy
190	Can change token	49	change_tokenproxy
191	Can delete token	49	delete_tokenproxy
192	Can view token	49	view_tokenproxy
193	Can add application	50	add_application
194	Can change application	50	change_application
195	Can delete application	50	delete_application
196	Can view application	50	view_application
197	Can add access token	51	add_accesstoken
198	Can change access token	51	change_accesstoken
199	Can delete access token	51	delete_accesstoken
200	Can view access token	51	view_accesstoken
201	Can add grant	52	add_grant
202	Can change grant	52	change_grant
203	Can delete grant	52	delete_grant
204	Can view grant	52	view_grant
205	Can add refresh token	53	add_refreshtoken
206	Can change refresh token	53	change_refreshtoken
207	Can delete refresh token	53	delete_refreshtoken
208	Can view refresh token	53	view_refreshtoken
209	Can add id token	54	add_idtoken
210	Can change id token	54	change_idtoken
211	Can delete id token	54	delete_idtoken
212	Can view id token	54	view_idtoken
213	Can add association	55	add_association
214	Can change association	55	change_association
215	Can delete association	55	delete_association
216	Can view association	55	view_association
217	Can add code	56	add_code
218	Can change code	56	change_code
219	Can delete code	56	delete_code
220	Can view code	56	view_code
221	Can add nonce	57	add_nonce
222	Can change nonce	57	change_nonce
223	Can delete nonce	57	delete_nonce
224	Can view nonce	57	view_nonce
225	Can add user social auth	58	add_usersocialauth
226	Can change user social auth	58	change_usersocialauth
227	Can delete user social auth	58	delete_usersocialauth
228	Can view user social auth	58	view_usersocialauth
229	Can add partial	59	add_partial
230	Can change partial	59	change_partial
231	Can delete partial	59	delete_partial
232	Can view partial	59	view_partial
233	Can add token	60	add_token
234	Can change token	60	change_token
235	Can delete token	60	delete_token
236	Can view token	60	view_token
237	Can add user profile	61	add_userprofile
238	Can change user profile	61	change_userprofile
239	Can delete user profile	61	delete_userprofile
240	Can view user profile	61	view_userprofile
241	Can add tg user	62	add_tguser
242	Can change tg user	62	change_tguser
243	Can delete tg user	62	delete_tguser
244	Can view tg user	62	view_tguser
245	Can add token	63	add_token
246	Can change token	63	change_token
247	Can delete token	63	delete_token
248	Can view token	63	view_token
249	Can add user profile	64	add_userprofile
250	Can change user profile	64	change_userprofile
251	Can delete user profile	64	delete_userprofile
252	Can view user profile	64	view_userprofile
\.


--
-- Data for Name: auth_user; Type: TABLE DATA; Schema: public; Owner: video
--

COPY public.auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) FROM stdin;
2	pbkdf2_sha256$30000$2Qea5LhK2p6X$fjMW63QenVpXnhe9KVGdhwKxF4EMr7pQIog7XTDhVAk=	2017-03-14 09:35:09.934817+00	f	other				t	t	2017-03-14 08:48:49+00
3	pbkdf2_sha256$180000$OuR0U9SOgMyW$X6dHLrGWnZyNngm6x2QqKJp9foA/RAY/t6suLYeXtBo=	2023-03-20 14:36:42.542342+00	f	tatyana	Татьяна			t	t	2018-05-07 06:32:58+00
4	pbkdf2_sha256$180000$LEMkVuc6zkE1$PS1aRnuoXdh+v7JVlAYYxHlxU6i0Km5rxf9MhC38/Xw=	2021-11-15 20:29:36+00	f	eolani				t	t	2019-08-28 20:17:21+00
6	pbkdf2_sha256$180000$eg0bpDhzkBOF$dqlGsOZ21NRvaStfUYS/eC+YTp+mWMeVanhLKiI7nk8=	2022-04-18 20:17:05.099366+00	f	video0			video@gmail.com	f	t	2022-04-18 20:17:03.725843+00
7	pbkdf2_sha256$180000$kpGlS3yCC01i$8NuL9K9xSQADCI5pewMEvX1HSlOuObmy1IjcoEoii4U=	2022-05-15 18:29:38.374011+00	f	video1			video1@gmail.com	f	t	2022-05-15 18:29:36.285181+00
5	pbkdf2_sha256$180000$HfoSJLyh7m7o$SxW1GjDCEkCUNmrQW5XaEV31SrsqTotY6v2MYkC5WFU=	2022-08-30 20:17:49.397088+00	f	crackkc			crackkc@mail.ru	f	t	2022-04-15 19:30:02.954305+00
9	pbkdf2_sha256$180000$pHL7OfCkx7m3$jSJRCuAs+YGWLGFbEzjPL4tyqjaZW4Vbdlo9AbQEA0I=	\N	f	yulia				t	t	2023-03-13 19:50:33+00
65		\N	f	Баги 				f	t	2023-10-13 17:11:19.586312+00
20		\N	f	juli1v	Юлия💜			f	t	2023-10-13 09:09:02.30114+00
47		\N	f	Байр 	Байр			f	t	2023-10-13 17:11:12.091859+00
69		\N	f	qwaszx20012017	Ирина	Малахова		f	t	2023-10-13 17:11:20.369606+00
79		\N	f	Эльхан Авдиев	Эльхан	Авдиев		f	t	2023-10-13 17:11:26.112548+00
36		\N	f	Нина Шведова	Нина	Шведова		f	t	2023-10-13 17:11:05.347221+00
98		\N	f	Li9534	Li			f	t	2023-10-13 17:11:39.405172+00
94		\N	f	Нина 	Нина			f	t	2023-10-13 17:11:36.821998+00
48		\N	f	Юлия Липова	Юлия	Липова		f	t	2023-10-13 17:11:12.741506+00
96		\N	f	Зоя 				f	t	2023-10-13 17:11:37.472677+00
86		\N	f	Bikulka00	Виктория			f	t	2023-10-13 17:11:28.925889+00
33		\N	f	🐯 	🐯			f	t	2023-10-13 17:11:04.964207+00
19		\N	f	Svetik2421	Светлана			f	t	2023-10-13 09:09:02.2955+00
135		\N	f	Анна Хугаева				f	t	2023-10-13 17:12:06.672324+00
57		\N	f	Andrei 	Andrei			f	t	2023-10-13 17:11:17.920541+00
50		\N	f	сергей 	сергей			f	t	2023-10-13 17:11:14.431642+00
56		\N	f	Сергей Ку	Сергей	Ку		f	t	2023-10-13 17:11:17.559516+00
52		\N	f	Шама 	Шама			f	t	2023-10-13 17:11:15.01809+00
23		\N	f	Анна 	Анна			f	t	2023-10-13 17:04:38.770951+00
97		\N	f	Лёха 	Лёха			f	t	2023-10-13 17:11:37.987327+00
85		\N	f	Ирина Борзилова	Ирина	Борзилова		f	t	2023-10-13 17:11:28.699949+00
67		\N	f	Kristinka_0005	Кристина👸			f	t	2023-10-13 17:11:20.357634+00
42		\N	f	svetik_Love_LEV	Svetik	Levhenko		f	t	2023-10-13 17:11:10.167975+00
44		\N	f	Лора 	Лора			f	t	2023-10-13 17:11:10.486832+00
41		\N	f	Соня Утемова	Соня	Утемова		f	t	2023-10-13 17:11:09.499216+00
80		\N	f	Natalia_Labzi	Наталья			f	t	2023-10-13 17:11:28.221552+00
78		\N	f	Дима 	Дима			f	t	2023-10-13 17:11:25.729102+00
92		\N	f	AbrosimovaMarina	Марина	Абросимова		f	t	2023-10-13 17:11:32.162114+00
54		\N	f	Ксюша 	Ксюша			f	t	2023-10-13 17:11:16.490775+00
30		\N	f	татьяна сидорова	татьяна	сидорова		f	t	2023-10-13 17:04:48.133748+00
31		\N	f	Татьяна🌹 	Татьяна🌹			f	t	2023-10-13 17:04:48.150526+00
60		\N	f	ROMAN K.	ROMAN	K.		f	t	2023-10-13 17:11:18.127588+00
18		\N	f	agent_maksik	Максим	Тютюник		f	t	2023-10-13 09:09:02.093618+00
16		\N	f	Fadeeva_El	Елена🕊			f	t	2023-10-13 08:42:37.134039+00
89		\N	f	Ольга 	Ольга			f	t	2023-10-13 17:11:31.518683+00
81		\N	f	Мария 	Мария			f	t	2023-10-13 17:11:28.227852+00
11		\N	f	Volgbondareva	Мариша			f	t	2023-10-12 18:49:06.053258+00
22		\N	f	Yohunop	38228			f	t	2023-10-13 09:09:02.63691+00
90		\N	f	Мария Мирошникова	Мария	Мирошникова		f	t	2023-10-13 17:11:31.524395+00
87		\N	f	🗿 	🗿			f	t	2023-10-13 17:11:29.646543+00
99		\N	f	Николай 	Николай			f	t	2023-10-13 17:11:39.415971+00
74		\N	f	75 	75			f	t	2023-10-13 17:11:22.460773+00
115		\N	f	БАХАДИР 				f	t	2023-10-13 17:11:49.7913+00
38		\N	f	Валентинка🌸 	Валентинка🌸			f	t	2023-10-13 17:11:07.750043+00
17		\N	f	Irina_lazutkina	Ирина	Лазуткина		f	t	2023-10-13 09:08:59.954158+00
83		\N	f	Kopknbj Ko	Kopknbj	Ko		f	t	2023-10-13 17:11:28.672575+00
45		\N	f	cvetochekkk345218043	Алия	Валиуллина		f	t	2023-10-13 17:11:11.266564+00
72		\N	f	Оксана Малькова	Оксана	Малькова		f	t	2023-10-13 17:11:21.488985+00
53		\N	f	Bereslavk	Елена			f	t	2023-10-13 17:11:15.916948+00
27		\N	f	Юрий Неженцев	Юрий	Неженцев		f	t	2023-10-13 17:04:42.410297+00
28		\N	f	Елена 	Елена			f	t	2023-10-13 17:04:42.416707+00
75		\N	f	Не важно 	Не важно			f	t	2023-10-13 17:11:23.008597+00
73		\N	f	Евгений 	Евгений			f	t	2023-10-13 17:11:21.888163+00
77		\N	f	Anna. 	Anna.			f	t	2023-10-13 17:11:23.532506+00
88		\N	f	ivanaparin_okt	Ivan	Aparin		f	t	2023-10-13 17:11:30.442465+00
58		\N	f	451601 	451601			f	t	2023-10-13 17:11:18.116909+00
34		\N	f	Александр 	Александр			f	t	2023-10-13 17:11:04.970783+00
39		\N	f	lovezloy1	Илья			f	t	2023-10-13 17:11:08.747118+00
63		\N	f	nikitaivano2	Никита			f	t	2023-10-13 17:11:18.163503+00
64		\N	f	Вера 	Вера			f	t	2023-10-13 17:11:19.281123+00
49		\N	f	юра 	юра			f	t	2023-10-13 17:11:14.420109+00
21		\N	f	VRBOXxxx	Таммк			f	t	2023-10-13 09:09:02.615724+00
46		\N	f	Алёна Давыдова 	Алёна Давыдова			f	t	2023-10-13 17:11:11.853152+00
93		\N	f	Milena_sv_99	Милена			f	t	2023-10-13 17:11:32.712864+00
112		\N	f	Володя 				f	t	2023-10-13 17:11:49.206831+00
84		\N	f	Irina20012001	Ирина			f	t	2023-10-13 17:11:28.677573+00
91		\N	f	Биж Апо	Биж	Апо		f	t	2023-10-13 17:11:31.856859+00
108		\N	f	massekhothdeforestation				f	t	2023-10-13 17:11:46.772794+00
71		\N	f	Виктория 	Виктория			f	t	2023-10-13 17:11:21.484196+00
61		\N	f	Екатерина 	Екатерина			f	t	2023-10-13 17:11:18.132617+00
24		\N	f	Diana 	Diana			f	t	2023-10-13 17:04:39.948154+00
137		\N	f	Olga S				f	t	2023-10-13 17:12:07.298931+00
154		\N	f	Chuzhoy Chuzhoy				f	t	2023-10-13 17:12:25.210173+00
201		\N	f	Не за мужем 				f	t	2023-10-13 17:13:40.039258+00
205		\N	f	Torent_2023	Вадик			f	t	2023-10-20 18:44:39.371894+00
204		\N	f	елена 	елена			f	t	2023-10-20 18:44:34.108388+00
158		\N	f	Sima14	®Сера¥има®			f	t	2023-10-13 17:12:30.02996+00
152		\N	f	Ахи 🙃⚘	Ахи	🙃⚘		f	t	2023-10-13 17:12:22.88002+00
145		\N	f	sokol_okt	Юлия	Захряпина		f	t	2023-10-13 17:12:14.442392+00
142		\N	f	Лариса 	Лариса			f	t	2023-10-13 17:12:10.250951+00
224		\N	f	Zaga0579	Зага			f	t	2023-10-20 19:34:32.858729+00
134		\N	f	Инна 	Инна			f	t	2023-10-13 17:12:05.866628+00
121		\N	f	И Г	И	Г		f	t	2023-10-13 17:11:59.17928+00
116		\N	f	Татьяна Мухина	Татьяна	Мухина		f	t	2023-10-13 17:11:50.556244+00
113		\N	f	Angry Droid	Angry	Droid		f	t	2023-10-13 17:11:49.212789+00
200		\N	f	Salamchikc	Борисыч			f	t	2023-10-13 17:13:36.741114+00
181		\N	f	Елена Рубежанская				f	t	2023-10-13 17:13:17.718684+00
170		\N	f	Ведьмочка 				f	t	2023-10-13 17:12:44.443782+00
159		\N	f	Юрьевна 	Юрьевна			f	t	2023-10-13 17:12:31.575658+00
218		\N	f	Даша 	Даша			f	t	2023-10-20 19:34:32.809223+00
222		\N	f	Александр Пименов	Александр	Пименов		f	t	2023-10-20 19:34:32.84596+00
104		\N	f	kudamsss	🖤			f	t	2023-10-13 17:11:45.813754+00
117		\N	f	Алексей Шахов	Алексей	Шахов		f	t	2023-10-13 17:11:51.142158+00
221		\N	f	Има 34/95❤	Има	34/95❤		f	t	2023-10-20 19:34:32.842754+00
167		\N	f	С 1977	С	1977		f	t	2023-10-13 17:12:39.444549+00
187		\N	f	Андрей Алексеев	Андрей	Алексеев		f	t	2023-10-13 17:13:21.144862+00
138		\N	f	DestinyNO	Оль	Оль		f	t	2023-10-13 17:12:07.303977+00
166		\N	f	Nadezdaaa78	Надежда			f	t	2023-10-13 17:12:38.937596+00
146		\N	f	Д.Е.В 	Д.Е.В			f	t	2023-10-13 17:12:14.98197+00
118		\N	f	marinabulyk	Марина	Булык		f	t	2023-10-13 17:11:55.697071+00
150		\N	f	Anna_asdf	Anna			f	t	2023-10-13 17:12:21.756094+00
103		\N	f	Oooyymau	Руся			f	t	2023-10-13 17:11:45.495195+00
151		\N	f	Владимир 	Владимир			f	t	2023-10-13 17:12:22.867759+00
173		\N	f	Evgenia 				f	t	2023-10-13 17:12:59.885055+00
105		\N	f	Марина 	Марина			f	t	2023-10-13 17:11:46.067926+00
164		\N	f	Lidiapopo	lidia			f	t	2023-10-13 17:12:38.582018+00
185		\N	f	Nikola 	Nikola			f	t	2023-10-13 17:13:19.930627+00
186		\N	f	nastya 	nastya			f	t	2023-10-13 17:13:20.208563+00
171		\N	f	Фёдор Дуенко	Фёдор	Дуенко		f	t	2023-10-13 17:12:46.031717+00
190		\N	f	Рома 				f	t	2023-10-13 17:13:25.623792+00
179		\N	f	nkasss12	Nastya			f	t	2023-10-13 17:13:17.462617+00
220		\N	f	Мурад Нуриев	Мурад	Нуриев		f	t	2023-10-20 19:34:32.829136+00
107		\N	f	TatoskaU	Татьяна Улесова			f	t	2023-10-13 17:11:46.267937+00
176		\N	f	Алексей 	Алексей			f	t	2023-10-13 17:13:16.145131+00
225		\N	f	Светлана Сергеева	Светлана	Сергеева		f	t	2023-10-20 19:34:32.86542+00
223		\N	f	Растаман 	Растаман			f	t	2023-10-20 19:34:32.849863+00
207		\N	f	GRubezh22	G	R		f	t	2023-10-20 18:44:46.767941+00
155		\N	f	DDM347	Rzhi			f	t	2023-10-13 17:12:26.839064+00
114		\N	f	Nataliyu1977	Н	К		f	t	2023-10-13 17:11:49.415589+00
168		\N	f	Полина 				f	t	2023-10-13 17:12:39.814589+00
144		\N	f	Nastashka34	Natali.			f	t	2023-10-13 17:12:13.601364+00
132		\N	f	1 Надежда	1	Надежда		f	t	2023-10-13 17:12:04.798568+00
131		\N	f	Андрей 	Андрей			f	t	2023-10-13 17:12:04.09081+00
191		\N	f	Наталия.Константин ..Милана 				f	t	2023-10-13 17:13:27.5789+00
163		\N	f	Sabr 				f	t	2023-10-13 17:12:36.50135+00
197		\N	f	SergeyZhukov77				f	t	2023-10-13 17:13:33.922062+00
178		\N	f	vaneckg				f	t	2023-10-13 17:13:17.048241+00
123		\N	f	&&.::нэмо 	&&.::нэмо			f	t	2023-10-13 17:11:59.681907+00
124		\N	f	Денис 	Денис			f	t	2023-10-13 17:11:59.687274+00
127		\N	f	Натали 	Натали			f	t	2023-10-13 17:12:02.363582+00
130		\N	f	люба харламова 	люба харламова			f	t	2023-10-13 17:12:03.84053+00
133		\N	f	Илья 	Илья			f	t	2023-10-13 17:12:05.858715+00
184		\N	f	"КУРИЛКА"ОКТ-34 	"КУРИЛКА"ОКТ-34			f	t	2023-10-13 17:13:18.647466+00
102		\N	f	Gelya_Krasnova	Ангелина	Краснова		f	t	2023-10-13 17:11:45.472371+00
160		\N	f	Ирина 	Ирина			f	t	2023-10-13 17:12:33.174239+00
111		\N	f	Gazo_vik	Владимир	Попов		f	t	2023-10-13 17:11:48.777143+00
119		\N	f	lola20_37	Хилола	Шамаева		f	t	2023-10-13 17:11:56.158495+00
153		\N	f	Лейла 	Лейла			f	t	2023-10-13 17:12:25.195195+00
172		\N	f	Qwerty Qwertyyuiip	Qwerty	Qwertyyuiip		f	t	2023-10-13 17:12:50.001924+00
196		\N	f	☠️ 	☠️			f	t	2023-10-13 17:13:32.01882+00
129		\N	f	list2458	Татьянка			f	t	2023-10-13 17:12:02.848853+00
165		\N	f	1209 	1209			f	t	2023-10-13 17:12:38.593699+00
148		\N	f	musa 161	musa	161		f	t	2023-10-13 17:12:16.466587+00
209		\N	f	✨Аlena✨ 				f	t	2023-10-20 18:44:48.711167+00
180		\N	f	Али Бередзе	Али	Бередзе		f	t	2023-10-13 17:13:17.707971+00
156		\N	f	💜 				f	t	2023-10-13 17:12:28.151937+00
101		\N	f	Inna Kriannikova	Inna	Kriannikova		f	t	2023-10-13 17:11:43.868714+00
169		\N	f	Ali_Nohso				f	t	2023-10-13 17:12:43.242407+00
149		\N	f	soooooooqa	Marina			f	t	2023-10-13 17:12:17.46797+00
141		\N	f	khaydarov_96	Navruz			f	t	2023-10-13 17:12:08.620501+00
143		\N	f	Хава 	Хава			f	t	2023-10-13 17:12:12.917856+00
157		\N	f	SV_1980	64856			f	t	2023-10-13 17:12:29.698952+00
182		\N	f	Олеся Александровна				f	t	2023-10-13 17:13:17.727137+00
183		\N	f	94935 				f	t	2023-10-13 17:13:18.145977+00
109		\N	f	MD_permanent_KRD	Margarita_Permanent			f	t	2023-10-13 17:11:47.960687+00
188		\N	f	Владимир Холеев				f	t	2023-10-13 17:13:24.300927+00
174		\N	f	Krasavchik_134	Александр			f	t	2023-10-13 17:13:11.701094+00
192		\N	f	nikolaj91				f	t	2023-10-13 17:13:28.584922+00
193		\N	f	MEBELBOND				f	t	2023-10-13 17:13:29.746841+00
194		\N	f	NOHCNO iz ZUMSOY 				f	t	2023-10-13 17:13:30.448786+00
195		\N	f	Пётр Ерков	Пётр	Ерков		f	t	2023-10-13 17:13:31.838877+00
199		\N	f	kiss one 				f	t	2023-10-13 17:13:36.205932+00
234		\N	f	mama_soblazn	маленький бамбук мечты			f	t	2023-10-20 19:34:32.924382+00
261		\N	f	Ольга Берко 	Ольга Берко			f	t	2023-10-20 19:34:33.047317+00
276		\N	f	🌺Наталья🌺 	🌺Наталья🌺			f	t	2023-10-20 19:34:33.114068+00
325		\N	f	(∆) 	(∆)			f	t	2023-10-20 19:34:33.312928+00
212		\N	f	Дмитрий Манджиев	Дмитрий	Манджиев		f	t	2023-10-20 18:45:29.051786+00
289		\N	f	ислам 	ислам			f	t	2023-10-20 19:34:33.165193+00
290		\N	f	Last_mistake_134	Last_mistake_34			f	t	2023-10-20 19:34:33.171787+00
273		\N	f	Ivan 	Ivan			f	t	2023-10-20 19:34:33.105839+00
242		\N	f	АЛИНА Фёдорова	АЛИНА	Фёдорова		f	t	2023-10-20 19:34:32.966723+00
247		\N	f	diana_yuryevna2000	Диана	Юрьевна		f	t	2023-10-20 19:34:32.993315+00
238		\N	f	Олег Кухтенко 	Олег Кухтенко			f	t	2023-10-20 19:34:32.94863+00
301		\N	f	Ннаталья 	Ннаталья			f	t	2023-10-20 19:34:33.218051+00
302		\N	f	Сергей Морозов	Сергей	Морозов		f	t	2023-10-20 19:34:33.221654+00
304		\N	f	mang_1l	Liza♡			f	t	2023-10-20 19:34:33.229646+00
291		\N	f	-_-_-_-_- 	-_-_-_-_-			f	t	2023-10-20 19:34:33.175415+00
292		\N	f	Наталья Л	Наталья	Л		f	t	2023-10-20 19:34:33.186628+00
293		\N	f	Nikolai4508	Вероника			f	t	2023-10-20 19:34:33.193121+00
294		\N	f	Smirnat79	Наталья			f	t	2023-10-20 19:34:33.195922+00
295		\N	f	Казбек 	Казбек			f	t	2023-10-20 19:34:33.198584+00
316		\N	f	ЕВГЕНИЙ 	ЕВГЕНИЙ			f	t	2023-10-20 19:34:33.278476+00
216		\N	f	Диван 	Диван			f	t	2023-10-20 19:34:32.800571+00
251		\N	f	Татьяна Ч	Татьяна	Ч		f	t	2023-10-20 19:34:33.011556+00
236		\N	f	128282 	128282			f	t	2023-10-20 19:34:32.935658+00
245		\N	f	Елена Элиаури	Елена	Элиаури		f	t	2023-10-20 19:34:32.987517+00
246		\N	f	сергей голованов	сергей	голованов		f	t	2023-10-20 19:34:32.990359+00
250		\N	f	Сергей Ч	Сергей	Ч		f	t	2023-10-20 19:34:33.007149+00
252		\N	f	Иван Чеботарев 	Иван Чеботарев			f	t	2023-10-20 19:34:33.015237+00
254		\N	f	ivan Genadievicn	ivan	Genadievicn		f	t	2023-10-20 19:34:33.021639+00
255		\N	f	miss_lady_24	Катя			f	t	2023-10-20 19:34:33.024712+00
256		\N	f	Иван Выдрыч	Иван	Выдрыч		f	t	2023-10-20 19:34:33.030039+00
258		\N	f	ольга павлова 	ольга павлова			f	t	2023-10-20 19:34:33.037117+00
270		\N	f	Надежда Чурсина	Надежда	Чурсина		f	t	2023-10-20 19:34:33.092156+00
265		\N	f	Милашка 	Милашка			f	t	2023-10-20 19:34:33.064927+00
277		\N	f	Оксана 	Оксана			f	t	2023-10-20 19:34:33.117813+00
267		\N	f	Natasha190885	Наталья			f	t	2023-10-20 19:34:33.074473+00
268		\N	f	Людмила Клещина	Людмила	Клещина		f	t	2023-10-20 19:34:33.081652+00
264		\N	f	Uvb155	Иван	Васильевич		f	t	2023-10-20 19:34:33.060677+00
274		\N	f	Ибрагим 	Ибрагим			f	t	2023-10-20 19:34:33.108529+00
275		\N	f	... 	...			f	t	2023-10-20 19:34:33.11123+00
298		\N	f	Татьяна 	Татьяна			f	t	2023-10-20 19:34:33.206463+00
272		\N	f	irinkaign	Ирина			f	t	2023-10-20 19:34:33.102309+00
241		\N	f	Duana20	Диана	Мовенко		f	t	2023-10-20 19:34:32.959562+00
299		\N	f	Стася Черкасова	Стася	Черкасова		f	t	2023-10-20 19:34:33.209441+00
263		\N	f	ylo_knit	Ольга	Яковлева		f	t	2023-10-20 19:34:33.054444+00
226		\N	f	лилия 	лилия			f	t	2023-10-20 19:34:32.868407+00
227		\N	f	Сайдулаев 	Сайдулаев			f	t	2023-10-20 19:34:32.871988+00
230		\N	f	Галина Астахова	Галина	Астахова		f	t	2023-10-20 19:34:32.882085+00
231		\N	f	Vladkinst	Владислав			f	t	2023-10-20 19:34:32.884864+00
260		\N	f	Танюха 	Танюха			f	t	2023-10-20 19:34:33.044638+00
233		\N	f	tarelkar	emptiness			f	t	2023-10-20 19:34:32.895301+00
278		\N	f	Saurabh Plieva	Saurabh	Plieva		f	t	2023-10-20 19:34:33.122006+00
232		\N	f	Илья Аввакумов	Илья	Аввакумов		f	t	2023-10-20 19:34:32.887609+00
279		\N	f	OksanaVolga	Зинченко Оксана			f	t	2023-10-20 19:34:33.126081+00
280		\N	f	Barakuda38	Людмила			f	t	2023-10-20 19:34:33.128744+00
317		\N	f	v_auuuY	anechka			f	t	2023-10-20 19:34:33.281136+00
284		\N	f	Aleksandr_19877	Александр	Решетов		f	t	2023-10-20 19:34:33.146355+00
285		\N	f	DenisMarader	Ден			f	t	2023-10-20 19:34:33.151483+00
288		\N	f	уе 	уе			f	t	2023-10-20 19:34:33.162631+00
296		\N	f	Артем Макаров	Артем	Макаров		f	t	2023-10-20 19:34:33.201254+00
305		\N	f	NatalyaKireeva	Наталья Киреева			f	t	2023-10-20 19:34:33.232465+00
306		\N	f	LinaKona	Элина Коврыгина Нутрициолог❄️			f	t	2023-10-20 19:34:33.235109+00
307		\N	f	Lexus034	Алексей			f	t	2023-10-20 19:34:33.237704+00
312		\N	f	Иван Хаустов	Иван	Хаустов		f	t	2023-10-20 19:34:33.260815+00
313		\N	f	Елена Кривозубова	Елена	Кривозубова		f	t	2023-10-20 19:34:33.268909+00
314		\N	f	Vanish 	Vanish			f	t	2023-10-20 19:34:33.272256+00
281		\N	f	Nadya_094	Надежда			f	t	2023-10-20 19:34:33.135284+00
326		\N	f	Рашидат Гасанова	Рашидат	Гасанова		f	t	2023-10-20 19:34:33.315573+00
322		\N	f	🌴 🌾	🌴	🌾		f	t	2023-10-20 19:34:33.297267+00
287		\N	f	GITA659	Гулира	Гарифуллина		f	t	2023-10-20 19:34:33.159974+00
323		\N	f	Natalya_29_k	𝓝𝓪𝓽𝓪𝓵𝔂𝓪	💜		f	t	2023-10-20 19:34:33.304761+00
324		\N	f	екатерина 	екатерина			f	t	2023-10-20 19:34:33.309204+00
319		\N	f	Ksenia 	Ksenia			f	t	2023-10-20 19:34:33.288497+00
327		\N	f	Г А	Г	А		f	t	2023-10-20 19:34:33.319225+00
329		\N	f	Надежда Кутыга	Надежда	Кутыга		f	t	2023-10-20 19:34:33.32445+00
330		\N	f	Фатима 	Фатима			f	t	2023-10-20 19:34:33.328843+00
331		\N	f	TanyaKlykova	Татьяна К			f	t	2023-10-20 19:34:33.331424+00
333		\N	f	Курбан Шихахмедов	Курбан	Шихахмедов		f	t	2023-10-20 19:34:33.340454+00
334		\N	f	Struhkova_91	Стрючкова Татьяна			f	t	2023-10-20 19:34:33.349046+00
237		\N	f	Сергей 	Сергей			f	t	2023-10-20 19:34:32.945542+00
239		\N	f	Людмила Андреева	Людмила	Андреева		f	t	2023-10-20 19:34:32.952615+00
243		\N	f	Ivan_Sheremetov	Иван	Шереметов		f	t	2023-10-20 19:34:32.975922+00
375		\N	f	CheltyBotik	...77...			f	t	2023-10-20 19:34:33.49642+00
377		\N	f	Anna 	Anna			f	t	2023-10-20 19:34:33.503166+00
433		\N	f	Юнус Лечиев	Юнус	Лечиев		f	t	2023-10-20 19:34:33.692876+00
344		\N	f	tatyanatarasova	Татьяна			f	t	2023-10-20 19:34:33.388337+00
345		\N	f	S_sanechka_a	🩶			f	t	2023-10-20 19:34:33.39561+00
346		\N	f	гульнара 	гульнара			f	t	2023-10-20 19:34:33.400242+00
442		\N	f	ab_roman1	Роман			f	t	2023-10-20 19:34:33.728855+00
348		\N	f	a_ssssss98	Alena	Saenko		f	t	2023-10-20 19:34:33.40598+00
350		\N	f	An Vikulova	An	Vikulova		f	t	2023-10-20 19:34:33.411389+00
351		\N	f	Алекс Ермишкин	Алекс	Ермишкин		f	t	2023-10-20 19:34:33.414083+00
352		\N	f	Хамза ханк	Хамза	ханк		f	t	2023-10-20 19:34:33.416783+00
353		\N	f	Okm_93	Ольга			f	t	2023-10-20 19:34:33.422883+00
354		\N	f	SvetlanaKvitko	Светлана	Квитко		f	t	2023-10-20 19:34:33.425836+00
356		\N	f	jsjsjs901	эрагон.			f	t	2023-10-20 19:34:33.43653+00
357		\N	f	serg_585	Сергей			f	t	2023-10-20 19:34:33.439274+00
358		\N	f	sergeynovok	Сергей			f	t	2023-10-20 19:34:33.441974+00
359		\N	f	Елена Здорик	Елена	Здорик		f	t	2023-10-20 19:34:33.444744+00
360		\N	f	ArtemovaYulya	Юлия			f	t	2023-10-20 19:34:33.447489+00
361		\N	f	😇 	😇			f	t	2023-10-20 19:34:33.450181+00
363		\N	f	melonchik13	Дмитрий	Григорьев		f	t	2023-10-20 19:34:33.455904+00
364		\N	f	Оксана Сорокина	Оксана	Сорокина		f	t	2023-10-20 19:34:33.459635+00
365		\N	f	А 	А			f	t	2023-10-20 19:34:33.465138+00
368		\N	f	Roman 	Roman			f	t	2023-10-20 19:34:33.473996+00
369		\N	f	Сергей Сердюков	Сергей	Сердюков		f	t	2023-10-20 19:34:33.477131+00
370		\N	f	keegm	Владимир			f	t	2023-10-20 19:34:33.480104+00
371		\N	f	62324 	62324			f	t	2023-10-20 19:34:33.482729+00
372		\N	f	sergej7070	Sergej			f	t	2023-10-20 19:34:33.485504+00
373		\N	f	Darzzopt	Darzzopt			f	t	2023-10-20 19:34:33.488694+00
374		\N	f	obuhovaphoto	Tatyana	Obuhova		f	t	2023-10-20 19:34:33.492325+00
378		\N	f	vso1989	Dmitriy	Avvakumov		f	t	2023-10-20 19:34:33.505805+00
379		\N	f	Rina090393	Екатерина			f	t	2023-10-20 19:34:33.508466+00
380		\N	f	Stanislav Dmitrienko	Stanislav	Dmitrienko		f	t	2023-10-20 19:34:33.511266+00
381		\N	f	dotkaaaaaaa	Платон			f	t	2023-10-20 19:34:33.513892+00
383		\N	f	kris_08_05	Кристина			f	t	2023-10-20 19:34:33.520617+00
384		\N	f	solo1938	Светлана	Лаптева		f	t	2023-10-20 19:34:33.524346+00
385		\N	f	doncov1993 evgenii	doncov1993	evgenii		f	t	2023-10-20 19:34:33.52709+00
461		\N	f	Юра Скворцов	Юра	Скворцов		f	t	2023-10-20 19:34:33.805225+00
462		\N	f	Sinelnikova_L	Любовь	Синельникова		f	t	2023-10-20 19:34:33.808386+00
463		\N	f	Vertuchka 	Vertuchka			f	t	2023-10-20 19:34:33.811266+00
310		\N	f	........ 	........			f	t	2023-10-20 19:34:33.249571+00
311		\N	f	ВМ 	ВМ			f	t	2023-10-20 19:34:33.255434+00
335		\N	f	М@тылёk 	М@тылёk			f	t	2023-10-20 19:34:33.354683+00
337		\N	f	89044269815 	89044269815			f	t	2023-10-20 19:34:33.361107+00
420		\N	f	Lina090708	lina			f	t	2023-10-20 19:34:33.652172+00
422		\N	f	E_V_Star	Елена	Стародубцева		f	t	2023-10-20 19:34:33.658761+00
423		\N	f	Денис Жалюк	Денис	Жалюк		f	t	2023-10-20 19:34:33.661462+00
424		\N	f	Людмила Окрушко 	Людмила Окрушко			f	t	2023-10-20 19:34:33.66426+00
426		\N	f	Светлана Коденчук	Светлана	Коденчук		f	t	2023-10-20 19:34:33.669694+00
427		\N	f	Яна Незборецкая	Яна	Незборецкая		f	t	2023-10-20 19:34:33.672439+00
428		\N	f	Анна Цыбулина	Анна	Цыбулина		f	t	2023-10-20 19:34:33.675966+00
429		\N	f	viktorisims	Listen			f	t	2023-10-20 19:34:33.679311+00
431		\N	f	юлия 	юлия			f	t	2023-10-20 19:34:33.685336+00
432		\N	f	KrasotkaEvgesha	Евгения			f	t	2023-10-20 19:34:33.690139+00
446		\N	f	Tatyana Sokolova	Tatyana	Sokolova		f	t	2023-10-20 19:34:33.747096+00
387		\N	f	Любовь 	Любовь			f	t	2023-10-20 19:34:33.53281+00
388		\N	f	AirTelekom	Air Telekom Technologies			f	t	2023-10-20 19:34:33.535589+00
389		\N	f	Белый 	Белый			f	t	2023-10-20 19:34:33.53828+00
390		\N	f	Bf_Bh	BF&BH			f	t	2023-10-20 19:34:33.540931+00
394		\N	f	Роза Хайронова	Роза	Хайронова		f	t	2023-10-20 19:34:33.562344+00
395		\N	f	SKazar1969	Kazarnitckaia	Svetlana		f	t	2023-10-20 19:34:33.568052+00
396		\N	f	Irina_Morozova_0022	ИРИНА			f	t	2023-10-20 19:34:33.570831+00
397		\N	f	Стас Кухтенко	Стас	Кухтенко		f	t	2023-10-20 19:34:33.573605+00
399		\N	f	Tata_Zlata_Cvyatik	43909			f	t	2023-10-20 19:34:33.579308+00
401		\N	f	Каролина Каро	Каролина	Каро		f	t	2023-10-20 19:34:33.58779+00
402		\N	f	Андрей Макеев	Андрей	Макеев		f	t	2023-10-20 19:34:33.590977+00
403		\N	f	Viktoria Tereshchuk	Viktoria	Tereshchuk		f	t	2023-10-20 19:34:33.594704+00
405		\N	f	ЕЛЕНА СОЛОВЬЕВА	ЕЛЕНА	СОЛОВЬЕВА		f	t	2023-10-20 19:34:33.601115+00
406		\N	f	Шахселем 	Шахселем			f	t	2023-10-20 19:34:33.603754+00
407		\N	f	Пашали 	Пашали			f	t	2023-10-20 19:34:33.608286+00
409		\N	f	Сашка Кучеров	Сашка	Кучеров		f	t	2023-10-20 19:34:33.616237+00
410		\N	f	Ekaterina_vlg34	Екатерина			f	t	2023-10-20 19:34:33.619324+00
412		\N	f	Galinka🌺 	Galinka🌺			f	t	2023-10-20 19:34:33.6261+00
413		\N	f	Dsv 	Dsv			f	t	2023-10-20 19:34:33.629269+00
441		\N	f	Сергей Мурзин	Сергей	Мурзин		f	t	2023-10-20 19:34:33.726015+00
443		\N	f	Мария Иванова	Мария	Иванова		f	t	2023-10-20 19:34:33.731571+00
434		\N	f	Дмитрий 	Дмитрий			f	t	2023-10-20 19:34:33.696749+00
435		\N	f	Алёнка 	Алёнка			f	t	2023-10-20 19:34:33.700158+00
436		\N	f	98426 oLeg9AcOZkT	98426	oLeg9AcOZkT		f	t	2023-10-20 19:34:33.706423+00
437		\N	f	Женя 	Женя			f	t	2023-10-20 19:34:33.712805+00
439		\N	f	Баатр Муканов	Баатр	Муканов		f	t	2023-10-20 19:34:33.718843+00
440		\N	f	Maks34 Rus Maks34 Rus	Maks34 Rus	Maks34 Rus		f	t	2023-10-20 19:34:33.723363+00
414		\N	f	Onegog628	onegog			f	t	2023-10-20 19:34:33.632039+00
339		\N	f	vikches_14	🫶🤍			f	t	2023-10-20 19:34:33.369127+00
340		\N	f	Marina_zaich	Marina	Zaich		f	t	2023-10-20 19:34:33.375857+00
342		\N	f	OlesyaLvovna	Олеся			f	t	2023-10-20 19:34:33.381396+00
501		\N	f	ZenyAnubis	Zeny			f	t	2023-10-20 19:34:33.945665+00
511		\N	f	alexa_1166	𝐀𝐥 🫦			f	t	2023-10-20 19:34:33.977919+00
518		\N	f	deadnasvai	🕸			f	t	2023-10-20 19:34:34.00318+00
535		\N	f	Мария Широкова	Мария	Широкова		f	t	2023-10-20 19:34:34.05743+00
536		\N	f	анна аннс лескина	анна	аннс лескина		f	t	2023-10-20 19:34:34.060708+00
537		\N	f	64997 	64997			f	t	2023-10-20 19:34:34.064426+00
539		\N	f	ne_magor_95	⚔️			f	t	2023-10-20 19:34:34.069826+00
521		\N	f	AnanevaMarusa	Маруся	Ананьева		f	t	2023-10-20 19:34:34.01156+00
528		\N	f	Дарья 	Дарья			f	t	2023-10-20 19:34:34.034407+00
529		\N	f	Lyubov160122	Кучерова	Любовь		f	t	2023-10-20 19:34:34.038262+00
530		\N	f	Ирина Коваленко	Ирина	Коваленко		f	t	2023-10-20 19:34:34.04097+00
531		\N	f	Елена Елена	Елена	Елена		f	t	2023-10-20 19:34:34.043827+00
533		\N	f	fkkkkk_134	ne_smotri			f	t	2023-10-20 19:34:34.051311+00
534		\N	f	Мадоша 	Мадоша			f	t	2023-10-20 19:34:34.054432+00
519		\N	f	Marina 	Marina			f	t	2023-10-20 19:34:34.005792+00
520		\N	f	velikavala05	Валя	Полухина		f	t	2023-10-20 19:34:34.008476+00
540		\N	f	DAP181501	Настя			f	t	2023-10-20 19:34:34.074907+00
541		\N	f	We9564	ㅤ	ㅤ		f	t	2023-10-20 19:34:34.078528+00
542		\N	f	Adlan_83	198318	198318		f	t	2023-10-20 19:34:34.081938+00
543		\N	f	ooolka20	Оля			f	t	2023-10-20 19:34:34.088916+00
544		\N	f	sq_borz	Амир			f	t	2023-10-20 19:34:34.09283+00
545		\N	f	Тумиша 	Тумиша			f	t	2023-10-20 19:34:34.095458+00
546		\N	f	Alkapon17	Алькапон	Андрей		f	t	2023-10-20 19:34:34.099281+00
548		\N	f	Кондрат 	Кондрат			f	t	2023-10-20 19:34:34.105517+00
549		\N	f	Светлана Николаева	Светлана	Николаева		f	t	2023-10-20 19:34:34.108562+00
550		\N	f	86929 Джабир	86929	Джабир		f	t	2023-10-20 19:34:34.112602+00
552		\N	f	Zalina5606	Зали	Берс		f	t	2023-10-20 19:34:34.118673+00
553		\N	f	🤗 .	🤗	.		f	t	2023-10-20 19:34:34.121512+00
554		\N	f	melnikova_raisa_82	Раиса	Мельникова		f	t	2023-10-20 19:34:34.124231+00
555		\N	f	Вячеслав 	Вячеслав			f	t	2023-10-20 19:34:34.126928+00
557		\N	f	60896 	60896			f	t	2023-10-20 19:34:34.132283+00
558		\N	f	89275136216 Сергей	89275136216	Сергей		f	t	2023-10-20 19:34:34.134925+00
559		\N	f	SWETLANADUENKO0206	СВЕТЛАНА			f	t	2023-10-20 19:34:34.138332+00
560		\N	f	АGG 	АGG			f	t	2023-10-20 19:34:34.141242+00
561		\N	f	dd 	dd			f	t	2023-10-20 19:34:34.144444+00
564		\N	f	Светлана Коновалова	Светлана	Коновалова		f	t	2023-10-20 19:34:34.153009+00
459		\N	f	Миша 	Миша			f	t	2023-10-20 19:34:33.794083+00
460		\N	f	,.... 	,....			f	t	2023-10-20 19:34:33.797264+00
445		\N	f	.... 	....			f	t	2023-10-20 19:34:33.74423+00
449		\N	f	Артак Григорян	Артак	Григорян		f	t	2023-10-20 19:34:33.757281+00
451		\N	f	Mariya6891	Мария	Аникина		f	t	2023-10-20 19:34:33.764653+00
452		\N	f	Ирина Кобышева 	Ирина Кобышева			f	t	2023-10-20 19:34:33.767609+00
453		\N	f	vikusus	Викус			f	t	2023-10-20 19:34:33.771584+00
455		\N	f	No Name	No	Name		f	t	2023-10-20 19:34:33.780955+00
456		\N	f	Марина Брешенкова	Марина	Брешенкова		f	t	2023-10-20 19:34:33.783975+00
457		\N	f	Юрий 	Юрий			f	t	2023-10-20 19:34:33.786661+00
507		\N	f	Наталья Божко	Наталья	Божко		f	t	2023-10-20 19:34:33.965708+00
516		\N	f	Алена 	Алена			f	t	2023-10-20 19:34:33.997551+00
418		\N	f	89275438918 	89275438918			f	t	2023-10-20 19:34:33.643439+00
464		\N	f	Эдгар Едигарян	Эдгар	Едигарян		f	t	2023-10-20 19:34:33.814266+00
465		\N	f	Xasanbek 	Xasanbek			f	t	2023-10-20 19:34:33.819469+00
467		\N	f	BKarina_23	Карина			f	t	2023-10-20 19:34:33.828307+00
468		\N	f	Ermakova Ylia	Ermakova	Ylia		f	t	2023-10-20 19:34:33.831918+00
470		\N	f	53304 	53304			f	t	2023-10-20 19:34:33.83857+00
471		\N	f	sssoksan	Синько Оксана			f	t	2023-10-20 19:34:33.843786+00
472		\N	f	Виктор 	Виктор			f	t	2023-10-20 19:34:33.846629+00
474		\N	f	pasivtut	пасивный	Доход		f	t	2023-10-20 19:34:33.853382+00
475		\N	f	slepoi12	неизвестно			f	t	2023-10-20 19:34:33.856368+00
476		\N	f	irichka134	Irina	Chesnokova		f	t	2023-10-20 19:34:33.863726+00
477		\N	f	светлана 	светлана			f	t	2023-10-20 19:34:33.867353+00
479		\N	f	Ромашкина .	Ромашкина	.		f	t	2023-10-20 19:34:33.872772+00
480		\N	f	rezortod7	jak			f	t	2023-10-20 19:34:33.875472+00
483		\N	f	Алина 	Алина			f	t	2023-10-20 19:34:33.884099+00
484		\N	f	Павел Малышев	Павел	Малышев		f	t	2023-10-20 19:34:33.886755+00
485		\N	f	righ55	афтон			f	t	2023-10-20 19:34:33.889509+00
486		\N	f	наталья юртайкина 	наталья юртайкина			f	t	2023-10-20 19:34:33.892896+00
487		\N	f	Ольга Прохорова	Ольга	Прохорова		f	t	2023-10-20 19:34:33.900723+00
489		\N	f	Elya 	Elya			f	t	2023-10-20 19:34:33.906436+00
491		\N	f	Татьяна Утянская	Татьяна	Утянская		f	t	2023-10-20 19:34:33.912288+00
492		\N	f	Наталья Курина	Наталья	Курина		f	t	2023-10-20 19:34:33.91509+00
493		\N	f	totesheya	Андрей			f	t	2023-10-20 19:34:33.917826+00
495		\N	f	solloveva83	Ольга			f	t	2023-10-20 19:34:33.924186+00
496		\N	f	🥥 	🥥			f	t	2023-10-20 19:34:33.927522+00
497		\N	f	r_usachev	R_USачев			f	t	2023-10-20 19:34:33.930898+00
498		\N	f	Анна Богук 	Анна Богук			f	t	2023-10-20 19:34:33.936194+00
499		\N	f	Алла 	Алла			f	t	2023-10-20 19:34:33.939702+00
500		\N	f	Сируи 	Сируи			f	t	2023-10-20 19:34:33.942739+00
502		\N	f	Rustam 	Rustam			f	t	2023-10-20 19:34:33.949065+00
504		\N	f	АНДРЕЙ 	АНДРЕЙ			f	t	2023-10-20 19:34:33.956018+00
505		\N	f	Татьяна Нидзий	Татьяна	Нидзий		f	t	2023-10-20 19:34:33.959503+00
506		\N	f	Светик 	Светик			f	t	2023-10-20 19:34:33.962567+00
508		\N	f	Joker777Zm	Николай			f	t	2023-10-20 19:34:33.968726+00
510		\N	f	Lb 	Lb			f	t	2023-10-20 19:34:33.974705+00
512		\N	f	Фомина Анна	Фомина	Анна		f	t	2023-10-20 19:34:33.980874+00
513		\N	f	Берсерк 	Берсерк			f	t	2023-10-20 19:34:33.983987+00
517		\N	f	...... 	......			f	t	2023-10-20 19:34:34.000369+00
566		\N	f	Veronika 	Veronika			f	t	2023-10-20 19:34:34.158552+00
571		\N	f	Нина Ковалёва	Нина	Ковалёва		f	t	2023-10-20 19:34:34.175405+00
577		\N	f	Nikolai89998119159	Николай	89998119159		f	t	2023-10-20 19:34:34.192409+00
583		\N	f	emmilia_18	emmilia_18			f	t	2023-10-20 19:34:34.221918+00
607		\N	f	GradnaVolge34	Volga34			f	t	2023-10-20 19:34:34.300569+00
524		\N	f	kristinka161	Кристина			f	t	2023-10-20 19:34:34.021375+00
645		\N	f	a_l_09_1	Al			f	t	2023-10-20 19:34:34.43134+00
640		\N	f	Imanushka1609	Anastasia	Kotenko		f	t	2023-10-20 19:34:34.41416+00
592		\N	f	f0ma1haut	fum al-ḥūt			f	t	2023-10-20 19:34:34.251524+00
523		\N	f	natali270976	Наталья	Ковалева		f	t	2023-10-20 19:34:34.018266+00
229		\N	f	Надежда 	Надежда			f	t	2023-10-20 19:34:32.879331+00
641		\N	f	Асад 	Асад			f	t	2023-10-20 19:34:34.419737+00
642		\N	f	ААС 	ААС			f	t	2023-10-20 19:34:34.422875+00
646		\N	f	Dima_31032001	Дмитрий			f	t	2023-10-20 19:34:34.435321+00
647		\N	f	Ульяна 	Ульяна			f	t	2023-10-20 19:34:34.4401+00
648		\N	f	Елена К	Елена	К		f	t	2023-10-20 19:34:34.443131+00
650		\N	f	pavuchok_0	Chi			f	t	2023-10-20 19:34:34.452004+00
651		\N	f	💞💞💞 	💞💞💞			f	t	2023-10-20 19:34:34.457223+00
652		\N	f	Anna Anna	Anna	Anna		f	t	2023-10-20 19:34:34.460135+00
654		\N	f	Ирина Фомина	Ирина	Фомина		f	t	2023-10-20 19:34:34.467725+00
655		\N	f	Viktoriya13031980	Виктория			f	t	2023-10-20 19:34:34.470858+00
656		\N	f	Елена Воробьёва	Елена	Воробьёва		f	t	2023-10-20 19:34:34.473954+00
657		\N	f	Егор Нестеров	Егор	Нестеров		f	t	2023-10-20 19:34:34.477131+00
659		\N	f	Isotope239 	Isotope239			f	t	2023-10-20 19:34:34.484916+00
660		\N	f	Raisa 	Raisa			f	t	2023-10-20 19:34:34.487764+00
661		\N	f	Владимир Нижник	Владимир	Нижник		f	t	2023-10-20 19:34:34.49253+00
662		\N	f	Ирина Игнатьева	Ирина	Игнатьева		f	t	2023-10-20 19:34:34.495301+00
663		\N	f	Denisdmb2002 Denisdmb2002	Denisdmb2002	Denisdmb2002		f	t	2023-10-20 19:34:34.497958+00
664		\N	f	jannyl_ka	Жаннулька			f	t	2023-10-20 19:34:34.501114+00
666		\N	f	Иван Владимирович 	Иван Владимирович			f	t	2023-10-20 19:34:34.507147+00
667		\N	f	Myrtazai	Муртазали	Исаев		f	t	2023-10-20 19:34:34.509992+00
669		\N	f	Valeria_17022	Валерия			f	t	2023-10-20 19:34:34.518917+00
672		\N	f	Аренда 	Аренда			f	t	2023-10-20 19:34:34.529204+00
674		\N	f	З Д	З	Д		f	t	2023-10-20 19:34:34.534777+00
716		\N	f	amelchenkotrofim	Трофим	Амельченко		f	t	2023-10-20 19:34:34.683477+00
567		\N	f	artemka0292	Артем			f	t	2023-10-20 19:34:34.161823+00
569		\N	f	Галина Проскурнова	Галина	Проскурнова		f	t	2023-10-20 19:34:34.168112+00
570		\N	f	Nikolay 	Nikolay			f	t	2023-10-20 19:34:34.172071+00
572		\N	f	koval1982	николай			f	t	2023-10-20 19:34:34.178051+00
573		\N	f	Татьяна Зинченко	Татьяна	Зинченко		f	t	2023-10-20 19:34:34.180804+00
574		\N	f	DD896	D	D		f	t	2023-10-20 19:34:34.183387+00
576		\N	f	Oksana 	Oksana			f	t	2023-10-20 19:34:34.189679+00
578		\N	f	Basirat 	Basirat			f	t	2023-10-20 19:34:34.195116+00
580		\N	f	Я 	Я			f	t	2023-10-20 19:34:34.208226+00
581		\N	f	natalimirono	Наталья	Миронова		f	t	2023-10-20 19:34:34.211436+00
584		\N	f	Нина Нина	Нина	Нина		f	t	2023-10-20 19:34:34.22463+00
585		\N	f	лара 	лара			f	t	2023-10-20 19:34:34.227457+00
587		\N	f	Анна Дмитриенко	Анна	Дмитриенко		f	t	2023-10-20 19:34:34.233828+00
588		\N	f	Alinka0005555	Алина			f	t	2023-10-20 19:34:34.23733+00
589		\N	f	Gegckvape	Я просто я			f	t	2023-10-20 19:34:34.243017+00
591		\N	f	вера баландина	вера	баландина		f	t	2023-10-20 19:34:34.248735+00
211		\N	f	Папа 	Папа			f	t	2023-10-20 18:45:29.042759+00
593		\N	f	Вики 	Вики			f	t	2023-10-20 19:34:34.254398+00
595		\N	f	Galina Galina	Galina	Galina		f	t	2023-10-20 19:34:34.260335+00
596		\N	f	Элла 	Элла			f	t	2023-10-20 19:34:34.264418+00
597		\N	f	Sylaskaa	Анастасия Сулацкая			f	t	2023-10-20 19:34:34.268357+00
598		\N	f	🌹🌹🌹 	🌹🌹🌹			f	t	2023-10-20 19:34:34.273156+00
599		\N	f	karamel_ika	Рина	Лик		f	t	2023-10-20 19:34:34.277018+00
600		\N	f	Dvenarx666sk102	Артур			f	t	2023-10-20 19:34:34.279996+00
601		\N	f	Анфиса 	Анфиса			f	t	2023-10-20 19:34:34.28277+00
602		\N	f	Лана 	Лана			f	t	2023-10-20 19:34:34.285509+00
605		\N	f	Юлия Короткова	Юлия	Короткова		f	t	2023-10-20 19:34:34.294203+00
606		\N	f	Лидия 	Лидия			f	t	2023-10-20 19:34:34.297253+00
608		\N	f	erkovanad	Надежда			f	t	2023-10-20 19:34:34.303345+00
610		\N	f	ваван 	ваван			f	t	2023-10-20 19:34:34.310678+00
611		\N	f	alisck 	alisck			f	t	2023-10-20 19:34:34.314154+00
612		\N	f	Masya0108	мася			f	t	2023-10-20 19:34:34.319118+00
613		\N	f	Волк пожизни 	Волк пожизни			f	t	2023-10-20 19:34:34.325607+00
614		\N	f	Volga0126	.			f	t	2023-10-20 19:34:34.328896+00
616		\N	f	Виталий Кузьмин	Виталий	Кузьмин		f	t	2023-10-20 19:34:34.334339+00
617		\N	f	Таня 	Таня			f	t	2023-10-20 19:34:34.336915+00
618		\N	f	Nadezhda0907	Надежда	Ламскова		f	t	2023-10-20 19:34:34.341358+00
619		\N	f	Инна Попова	Инна	Попова		f	t	2023-10-20 19:34:34.344069+00
621		\N	f	Наталья Миронова	Наталья	Миронова		f	t	2023-10-20 19:34:34.349345+00
622		\N	f	Maga 	Maga			f	t	2023-10-20 19:34:34.352143+00
623		\N	f	Алексей Еременко	Алексей	Еременко		f	t	2023-10-20 19:34:34.357088+00
624		\N	f	Svetlana 	Svetlana			f	t	2023-10-20 19:34:34.359822+00
629		\N	f	Nadinka189	Надежда	Витальевна		f	t	2023-10-20 19:34:34.377758+00
630		\N	f	Valentina 	Valentina			f	t	2023-10-20 19:34:34.380722+00
632		\N	f	Katya Kasyanova	Katya	Kasyanova		f	t	2023-10-20 19:34:34.387189+00
633		\N	f	Olga8899	Helga			f	t	2023-10-20 19:34:34.390878+00
635		\N	f	Bigbro34	Black _134_rus			f	t	2023-10-20 19:34:34.397374+00
636		\N	f	Галина .	Галина	.		f	t	2023-10-20 19:34:34.400302+00
637		\N	f	nata18119	Наталья	Рябцева		f	t	2023-10-20 19:34:34.402959+00
638		\N	f	okt475	Елена	Николаевна		f	t	2023-10-20 19:34:34.405695+00
710		\N	f	Таня Гузева	Таня	Гузева		f	t	2023-10-20 19:34:34.664342+00
713		\N	f	Ольга Зубкова	Ольга	Зубкова		f	t	2023-10-20 19:34:34.673356+00
625		\N	f	Сыроежкина Наталья Алексеевна 	Сыроежкина Наталья Алексеевна			f	t	2023-10-20 19:34:34.362648+00
253		\N	f	Татьяна Путинцева	Татьяна	Путинцева		f	t	2023-10-20 19:34:33.018803+00
719		\N	f	Ксения 	Ксения			f	t	2023-10-20 19:34:34.692046+00
752		\N	f	Li Di	Li	Di		f	t	2023-11-06 16:59:19.694349+00
753		\N	f	lik_as_lex	💁🏼‍♀️			f	t	2023-11-06 16:59:19.704068+00
668		\N	f	Дубовцова Ольга	Дубовцова	Ольга		f	t	2023-10-20 19:34:34.516141+00
675		\N	f	Баудин Тазабаев. 	Баудин Тазабаев.			f	t	2023-10-20 19:34:34.538187+00
682		\N	f	Маликова Ольга	Маликова	Ольга		f	t	2023-10-20 19:34:34.562497+00
522		\N	f	ne_magor_894	😎			f	t	2023-10-20 19:34:34.015058+00
649		\N	f	Ольга Алоева	Ольга	Алоева		f	t	2023-10-20 19:34:34.44829+00
29		\N	f	Наталья Сергеевна 	Наталья Сергеевна			f	t	2023-10-13 17:04:45.603284+00
683		\N	f	Люда Зубко	Люда	Зубко		f	t	2023-10-20 19:34:34.565156+00
128		\N	f	madina491	M			f	t	2023-10-13 17:12:02.368533+00
219		\N	f	Oktyabrsky_134	Анастасия Николаевна			f	t	2023-10-20 19:34:32.823594+00
208		\N	f	Марина Федорова	Марина	Федорова		f	t	2023-10-20 18:44:48.317381+00
228		\N	f	Макс Румянцев	Макс	Румянцев		f	t	2023-10-20 19:34:32.875254+00
210		\N	f	Stativkina_Olga	Стативкина	Оля		f	t	2023-10-20 18:45:28.375244+00
40		\N	f	Киселёва Олеся	Киселёва	Олеся		f	t	2023-10-13 17:11:09.491814+00
235		\N	f	Ткаченко Татьяна 	Ткаченко Татьяна			f	t	2023-10-20 19:34:32.9274+00
240		\N	f	василий бакаев	василий	бакаев		f	t	2023-10-20 19:34:32.956494+00
147		\N	f	Татьяна Кондратюк	Татьяна	Кондратюк		f	t	2023-10-13 17:12:16.455191+00
244		\N	f	EkaterinaSharni	Шарнина Катерина	Екатерина		f	t	2023-10-20 19:34:32.983732+00
248		\N	f	Илья Буров	Илья	Буров		f	t	2023-10-20 19:34:32.997453+00
249		\N	f	Болдырева Надежда	Болдырева	Надежда		f	t	2023-10-20 19:34:33.004466+00
66		\N	f	Мачиева Анастасия	Мачиева	Анастасия		f	t	2023-10-13 17:11:19.594546+00
515		\N	f	Лола Климова	Лола	Климова		f	t	2023-10-20 19:34:33.990906+00
687		\N	f	SavincevaS	Светлана			f	t	2023-10-20 19:34:34.580185+00
43		\N	f	Mari_Bogdanova1	Мария	Богданова		f	t	2023-10-13 17:11:10.173188+00
688		\N	f	Elena1581	Елена			f	t	2023-10-20 19:34:34.583115+00
579		\N	f	Наталья Сергеева	Наталья	Сергеева		f	t	2023-10-20 19:34:34.197922+00
628		\N	f	Юляшка 	Юляшка			f	t	2023-10-20 19:34:34.375105+00
627		\N	f	nadezhdak26	Надежда			f	t	2023-10-20 19:34:34.370367+00
681		\N	f	Екатерина Овсянникова	Екатерина	Овсянникова		f	t	2023-10-20 19:34:34.558667+00
685		\N	f	Valentina_H	Валентина			f	t	2023-10-20 19:34:34.572319+00
603		\N	f	ольга торопцева	ольга	торопцева		f	t	2023-10-20 19:34:34.288165+00
684		\N	f	Basota_07	Илюшa			f	t	2023-10-20 19:34:34.567988+00
562		\N	f	Иван Шашокин	Иван	Шашокин		f	t	2023-10-20 19:34:34.14759+00
631		\N	f	a_tear_of_rain	.			f	t	2023-10-20 19:34:34.383683+00
693		\N	f	Елена Солодунова	Елена	Солодунова		f	t	2023-10-20 19:34:34.598725+00
689		\N	f	лада галда 	лада галда			f	t	2023-10-20 19:34:34.58772+00
691		\N	f	yyyyylyana	Ульяна			f	t	2023-10-20 19:34:34.593202+00
695		\N	f	JuliaVShulova	Yulia	Shulova		f	t	2023-10-20 19:34:34.606609+00
694		\N	f	Валя 	Валя			f	t	2023-10-20 19:34:34.602098+00
526		\N	f	𝐝𝐮𝐥𝐚𝐭𝐦𝐞𝐫𝐳𝐚𝐞𝐯𝐚 	𝐝𝐮𝐥𝐚𝐭𝐦𝐞𝐫𝐳𝐚𝐞𝐯𝐚			f	t	2023-10-20 19:34:34.028875+00
696		\N	f	Лугачев Валентин Васильевич 	Лугачев Валентин Васильевич			f	t	2023-10-20 19:34:34.610868+00
698		\N	f	Евгения 	Евгения			f	t	2023-10-20 19:34:34.616743+00
699		\N	f	V.G 	V.G			f	t	2023-10-20 19:34:34.620087+00
700		\N	f	Татьяна Сухина	Татьяна	Сухина		f	t	2023-10-20 19:34:34.623175+00
702		\N	f	Наталья Саклакова	Наталья	Саклакова		f	t	2023-10-20 19:34:34.637156+00
703		\N	f	Елена Светличная	Елена	Светличная		f	t	2023-10-20 19:34:34.640111+00
704		\N	f	Елена Бакаева	Елена	Бакаева		f	t	2023-10-20 19:34:34.642999+00
706		\N	f	A S	A	S		f	t	2023-10-20 19:34:34.648728+00
707		\N	f	elenochka41	Лена			f	t	2023-10-20 19:34:34.651402+00
708		\N	f	Лина 	Лина			f	t	2023-10-20 19:34:34.65519+00
711		\N	f	Валентина Пискун	Валентина	Пискун		f	t	2023-10-20 19:34:34.667743+00
712		\N	f	🌺Анжела 🌺	🌺Анжела	🌺		f	t	2023-10-20 19:34:34.670533+00
715		\N	f	Квинт Наталья	Квинт	Наталья		f	t	2023-10-20 19:34:34.68066+00
717		\N	f	елизавета 	елизавета			f	t	2023-10-20 19:34:34.686086+00
718		\N	f	kiss_polinka	Polina			f	t	2023-10-20 19:34:34.688869+00
720		\N	f	Диана 	Диана			f	t	2023-10-20 19:34:34.696909+00
62		\N	f	YuliyaSorokina07	Юлия	Сорокина		f	t	2023-10-13 17:11:18.139196+00
215		\N	f	Показать Комнату	Показать	Комнату		f	t	2023-10-20 19:34:32.797613+00
32		\N	f	𝓘𝓼𝓵𝓪𝓶𝓹𝓻𝓸 	𝓘𝓼𝓵𝓪𝓶𝓹𝓻𝓸			f	t	2023-10-13 17:04:48.681394+00
709		\N	f	. 	.			f	t	2023-10-20 19:34:34.658183+00
120		\N	f	Екатерина Слета	Екатерина	Слета		f	t	2023-10-13 17:11:58.922918+00
259		\N	f	Елена Сиротина	Елена	Сиротина		f	t	2023-10-20 19:34:33.04179+00
750		\N	f	Walentina 	Walentina			f	t	2023-11-06 16:59:19.682933+00
751		\N	f	klmnks0	-S.			f	t	2023-11-06 16:59:19.688715+00
677		\N	f	Серж 	Серж			f	t	2023-10-20 19:34:34.547643+00
678		\N	f	Ирина Понамарева	Ирина	Понамарева		f	t	2023-10-20 19:34:34.550379+00
679		\N	f	Tanya14784	Татьяна	Рубежанская		f	t	2023-10-20 19:34:34.553095+00
421		\N	f	александр лысенко	александр	лысенко		f	t	2023-10-20 19:34:33.654986+00
447		\N	f	JONI93	~тормози~			f	t	2023-10-20 19:34:33.749921+00
509		\N	f	Lesaj521	Олеся	Мякишева		f	t	2023-10-20 19:34:33.971714+00
125		\N	f	Александр Кучеров	Александр	Кучеров		f	t	2023-10-13 17:12:00.65586+00
35		\N	f	Людмила 	Людмила			f	t	2023-10-13 17:11:05.328928+00
315		\N	f	viktoriaryabitskova	Виктория			f	t	2023-10-20 19:34:33.275469+00
318		\N	f	Светлана Парамонова	Светлана	Парамонова		f	t	2023-10-20 19:34:33.28372+00
386		\N	f	Ольга Сердюкова	Ольга	Сердюкова		f	t	2023-10-20 19:34:33.530014+00
271		\N	f	Света Черноиванова	Света	Черноиванова		f	t	2023-10-20 19:34:33.098642+00
286		\N	f	Сергей Большаков	Сергей	Большаков		f	t	2023-10-20 19:34:33.155778+00
257		\N	f	Валентина 	Валентина			f	t	2023-10-20 19:34:33.033265+00
297		\N	f	Елена Болучевская	Елена	Болучевская		f	t	2023-10-20 19:34:33.203909+00
300		\N	f	Светлана Калитвинцева	Светлана	Калитвинцева		f	t	2023-10-20 19:34:33.213069+00
95		\N	f	Игорь Безусов	Игорь	Безусов		f	t	2023-10-13 17:11:37.178808+00
362		\N	f	AnyaChistyakova	Аня	Энергопрактик. Аксесс барс, Фейслифт		f	t	2023-10-20 19:34:33.453+00
76		\N	f	Александр Ращевский	Александр	Ращевский		f	t	2023-10-13 17:11:23.013743+00
367		\N	f	Михаил Михайлович	Михаил	Михайлович		f	t	2023-10-20 19:34:33.471077+00
466		\N	f	s_kaifom_999	Алиев	Оджалан		f	t	2023-10-20 19:34:33.825402+00
527		\N	f	Ирина Клинкова	Ирина	Клинкова		f	t	2023-10-20 19:34:34.031661+00
110		\N	f	Елена Астахова	Елена	Астахова		f	t	2023-10-13 17:11:48.44443+00
321		\N	f	Снежана Меркулова	Снежана	Меркулова		f	t	2023-10-20 19:34:33.293622+00
328		\N	f	VLG_EVGENY_TRETYACHENKO	Евгений	Третьяченко		f	t	2023-10-20 19:34:33.321805+00
332		\N	f	Евгений Герасимов	Евгений	Герасимов		f	t	2023-10-20 19:34:33.336906+00
122		\N	f	Пономарев Сергей	Пономарев	Сергей		f	t	2023-10-13 17:11:59.419544+00
126		\N	f	Эльвира Зугумова	Эльвира	Зугумова		f	t	2023-10-13 17:12:02.356988+00
338		\N	f	Anna1785	Анна	Никитина		f	t	2023-10-20 19:34:33.365101+00
347		\N	f	Виктор Митрохин	Виктор	Митрохин		f	t	2023-10-20 19:34:33.403267+00
349		\N	f	Валентина Орешкина	Валентина	Орешкина		f	t	2023-10-20 19:34:33.408655+00
136		\N	f	Наталья Лубинская 	Наталья Лубинская			f	t	2023-10-13 17:12:07.046682+00
140		\N	f	Райганат Курбановна!	Райганат	Курбановна!		f	t	2023-10-13 17:12:08.404429+00
355		\N	f	эрагон 	эрагон			f	t	2023-10-20 19:34:33.433766+00
282		\N	f	boroda_34	Андрей			f	t	2023-10-20 19:34:33.139048+00
366		\N	f	Elena_12_65	Елена Слета			f	t	2023-10-20 19:34:33.468273+00
308		\N	f	Светлана Кострыкина	Светлана	Кострыкина		f	t	2023-10-20 19:34:33.240614+00
376		\N	f	anna_sarycheva79	Анна	Сарычева		f	t	2023-10-20 19:34:33.499348+00
382		\N	f	Елизавета 	Елизавета			f	t	2023-10-20 19:34:33.516612+00
303		\N	f	Анжелика Щиголь	Анжелика	Щиголь		f	t	2023-10-20 19:34:33.224799+00
392		\N	f	111553 Владимир	111553	Владимир		f	t	2023-10-20 19:34:33.549205+00
393		\N	f	константин костенко	константин	костенко		f	t	2023-10-20 19:34:33.558165+00
398		\N	f	Оксана Реунова	Оксана	Реунова		f	t	2023-10-20 19:34:33.576432+00
309		\N	f	Максим 	Максим			f	t	2023-10-20 19:34:33.244972+00
419		\N	f	Синько Галина	Синько	Галина		f	t	2023-10-20 19:34:33.647834+00
411		\N	f	Наталья Суходолова	Наталья	Суходолова		f	t	2023-10-20 19:34:33.622682+00
415		\N	f	Артур Бисинаев	Артур	Бисинаев		f	t	2023-10-20 19:34:33.634716+00
416		\N	f	Джума Багандова	Джума	Багандова		f	t	2023-10-20 19:34:33.637462+00
161		\N	f	Игорь Васильевич 	Игорь Васильевич			f	t	2023-10-13 17:12:33.686696+00
106		\N	f	Карина Корсунова	Карина	Корсунова		f	t	2023-10-13 17:11:46.2584+00
425		\N	f	Адрей Григойев	Адрей	Григойев		f	t	2023-10-20 19:34:33.666958+00
162		\N	f	Татьяна Васильева	Татьяна	Васильева		f	t	2023-10-13 17:12:33.692328+00
438		\N	f	༺ 𝓐𝓷𝓪𝓼𝓽𝓪𝓼𝓲𝔂𝓪 ༻ 	༺ 𝓐𝓷𝓪𝓼𝓽𝓪𝓼𝓲𝔂𝓪 ༻			f	t	2023-10-20 19:34:33.716051+00
444		\N	f	Дарья Сергеевна	Дарья	Сергеевна		f	t	2023-10-20 19:34:33.736611+00
450		\N	f	natata34	Бахарева Н	Наташа		f	t	2023-10-20 19:34:33.760013+00
454		\N	f	9044000153 nata2350	9044000153	nata2350		f	t	2023-10-20 19:34:33.775217+00
458		\N	f	🕊Анастасия🕊 	🕊Анастасия🕊			f	t	2023-10-20 19:34:33.791381+00
473		\N	f	EGr29	Гринько Елена			f	t	2023-10-20 19:34:33.850564+00
469		\N	f	Елена Карижская	Елена	Карижская		f	t	2023-10-20 19:34:33.835551+00
408		\N	f	Денис Сиговцев	Денис	Сиговцев		f	t	2023-10-20 19:34:33.612385+00
478		\N	f	Владимир булгаков	Владимир	булгаков		f	t	2023-10-20 19:34:33.870187+00
482		\N	f	Рома Прокопенко	Рома	Прокопенко		f	t	2023-10-20 19:34:33.881311+00
488		\N	f	abdulyashik	*:･ﾟ✧ 𝒩𝒾𝓀𝒶 .｡.:*♡			f	t	2023-10-20 19:34:33.903616+00
490		\N	f	Татьяна Абачиева	Татьяна	Абачиева		f	t	2023-10-20 19:34:33.909383+00
494		\N	f	Елена Гончарова	Елена	Гончарова		f	t	2023-10-20 19:34:33.920628+00
514		\N	f	dmitry_alekseevich134	dmitry_alekseevich			f	t	2023-10-20 19:34:33.987057+00
68		\N	f	Окна Века Балконы лоджии.	Окна Века	Балконы лоджии.		f	t	2023-10-13 17:11:20.363692+00
343		\N	f	Elena_A_Sologubova	Елена			f	t	2023-10-20 19:34:33.385198+00
59		\N	f	alekseevna08030	Evgeniya	Pankratova		f	t	2023-10-13 17:11:18.122605+00
404		\N	f	Nemnova	Немнова	Юлия		f	t	2023-10-20 19:34:33.597425+00
665		\N	f	Лилия Рахметова	Лилия	Рахметова		f	t	2023-10-20 19:34:34.503821+00
575		\N	f	Ростовские Росстовские	Ростовские	Росстовские		f	t	2023-10-20 19:34:34.185991+00
671	pbkdf2_sha256$600000$vm3gOn50146jmuRIHH74Uq$nY2fulbOYaKAVq2nPDhEMlpwXOUPJh8WSwe6ZjUzoz4=	\N	f	Воскресенье 	Воскресенье			f	t	2023-10-20 19:34:34+00
736		\N	f	Cossack777nation	⚖️			f	t	2023-11-06 16:59:19.588983+00
568		\N	f	Белокопытов Александр	Белокопытов	Александр		f	t	2023-10-20 19:34:34.165064+00
723		\N	f	Василий Костенко	Василий	Костенко		f	t	2023-11-06 16:59:19.518299+00
747		\N	f	Никулина Алёна	Никулина	Алёна		f	t	2023-11-06 16:59:19.668984+00
748		\N	f	Provodnikenergii	Ekaterina	Kuleshova		f	t	2023-11-06 16:59:19.674077+00
590		\N	f	Anna90efim	Анна Ефимова			f	t	2023-10-20 19:34:34.245828+00
177		\N	f	Владимир Ипатов	Владимир	Ипатов		f	t	2023-10-13 17:13:16.851375+00
532		\N	f	Елена Провоторова	Елена	Провоторова		f	t	2023-10-20 19:34:34.046863+00
744		\N	f	Valkeriyy	Ольга			f	t	2023-11-06 16:59:19.648077+00
551		\N	f	Rumiya1708	Румия	Сулейманова		f	t	2023-10-20 19:34:34.115949+00
37		\N	f	Галина 	Галина			f	t	2023-10-13 17:11:07.740538+00
556		\N	f	Мариша 	Мариша			f	t	2023-10-20 19:34:34.129648+00
697		\N	f	Юрий Барабаш	Юрий	Барабаш		f	t	2023-10-20 19:34:34.613715+00
732		\N	f	Д 	Д			f	t	2023-11-06 16:59:19.567781+00
582		\N	f	Сергей Давидович	Сергей	Давидович		f	t	2023-10-20 19:34:34.217297+00
594		\N	f	Ксения Гришина	Ксения	Гришина		f	t	2023-10-20 19:34:34.257258+00
604		\N	f	знакомый🥳 	знакомый🥳			f	t	2023-10-20 19:34:34.291251+00
609		\N	f	Zoa_22aZZa	Зоя	Дроздова		f	t	2023-10-20 19:34:34.308048+00
615		\N	f	Иван Колгатин	Иван	Колгатин		f	t	2023-10-20 19:34:34.33153+00
620		\N	f	Татьяна Татьяна	Татьяна	Татьяна		f	t	2023-10-20 19:34:34.346653+00
626		\N	f	Полина Мельникова	Полина	Мельникова		f	t	2023-10-20 19:34:34.36729+00
634		\N	f	Исмаил Слоев	Исмаил	Слоев		f	t	2023-10-20 19:34:34.394392+00
735		\N	f	Татьяна Косенко	Татьяна	Косенко		f	t	2023-11-06 16:59:19.583047+00
670		\N	f	Юрий Кучеров	Юрий	Кучеров		f	t	2023-10-20 19:34:34.521747+00
189		\N	f	Oksana_V_K	Оксана	Курахтенкова		f	t	2023-10-13 17:13:24.516296+00
653		\N	f	Галина Яковлева 	Галина Яковлева			f	t	2023-10-20 19:34:34.464033+00
701		\N	f	Арсений Колптков	Арсений	Колптков		f	t	2023-10-20 19:34:34.631617+00
658		\N	f	Надежда Ковалева	Надежда	Ковалева		f	t	2023-10-20 19:34:34.481896+00
680		\N	f	Сердюков Александр	Сердюков	Александр		f	t	2023-10-20 19:34:34.555858+00
639		\N	f	Kiissa5	Кристина	Тихонова		f	t	2023-10-20 19:34:34.41087+00
686		\N	f	Mihail_H	Mihail			f	t	2023-10-20 19:34:34.575392+00
726		\N	f	Ирина Утемова	Ирина	Утемова		f	t	2023-11-06 16:59:19.531824+00
742		\N	f	LyudmilaBuzina	Людмила	...		f	t	2023-11-06 16:59:19.642274+00
202		\N	f	Галина Костенко	Галина	Костенко		f	t	2023-10-13 17:13:40.04467+00
738		\N	f	Iren_fv	Irina			f	t	2023-11-06 16:59:19.618178+00
745		\N	f	Madina 	Madina			f	t	2023-11-06 16:59:19.659117+00
740		\N	f	Роман Романов	Роман	Романов		f	t	2023-11-06 16:59:19.628075+00
741		\N	f	64174 Лапешко	64174	Лапешко		f	t	2023-11-06 16:59:19.635137+00
644		\N	f	NATALIA 	NATALIA			f	t	2023-10-20 19:34:34.428534+00
724		\N	f	AleksNik_777	`¡¡¡			f	t	2023-11-06 16:59:19.525339+00
725		\N	f	Асхаб Решедов	Асхаб	Решедов		f	t	2023-11-06 16:59:19.528194+00
217		\N	f	Анна Парикмахер 	Анна Парикмахер			f	t	2023-10-20 19:34:32.803415+00
727		\N	f	Sergo2532	Неизвестный	Пользователь		f	t	2023-11-06 16:59:19.535449+00
25		\N	f	Светлана 	Светлана			f	t	2023-10-13 17:04:41.842434+00
729		\N	f	Галина Ч	Галина	Ч		f	t	2023-11-06 16:59:19.547289+00
728		\N	f	Соломон 	Соломон			f	t	2023-11-06 16:59:19.538469+00
731		\N	f	наталия власова	наталия	власова		f	t	2023-11-06 16:59:19.564757+00
417		\N	f	naran09 naran098	naran09	naran098		f	t	2023-10-20 19:34:33.640408+00
749		\N	f	Мариам Узунян	Мариам	Узунян		f	t	2023-11-06 16:59:19.67698+00
538		\N	f	Ketrin261187	Екатерина	Прохорова		f	t	2023-10-20 19:34:34.067183+00
430		\N	f	Александра 	Александра			f	t	2023-10-20 19:34:33.682564+00
733		\N	f	Лера🤍 	Лера🤍			f	t	2023-11-06 16:59:19.572818+00
743		\N	f	SERGEI1983serg	Sergei			f	t	2023-11-06 16:59:19.645107+00
737		\N	f	Тёма 	Тёма			f	t	2023-11-06 16:59:19.596751+00
676		\N	f	Вера Филимонова 	Вера Филимонова			f	t	2023-10-20 19:34:34.544636+00
714		\N	f	Ольга Сергеева	Ольга	Сергеева		f	t	2023-10-20 19:34:34.677898+00
705		\N	f	Ольга Прохоренко	Ольга	Прохоренко		f	t	2023-10-20 19:34:34.645882+00
721		\N	f	Валерия 	Валерия			f	t	2023-10-20 19:34:34.700654+00
722		\N	f	Яна 	Яна			f	t	2023-11-06 16:59:19.514787+00
739		\N	f	masterSan Sane4ka	masterSan	Sane4ka		f	t	2023-11-06 16:59:19.621064+00
730		\N	f	MMA_ks	Максим			f	t	2023-11-06 16:59:19.552222+00
565		\N	f	lena Yakovleva	lena	Yakovleva		f	t	2023-10-20 19:34:34.155693+00
269		\N	f	Светлана Галиева	Светлана	Галиева		f	t	2023-10-20 19:34:33.084579+00
214		\N	f	Ilya160887	Илья			f	t	2023-10-20 19:34:32.791205+00
175		\N	f	Наташа 	Наташа			f	t	2023-10-13 17:13:13.339728+00
746		\N	f	Tim 	Tim			f	t	2023-11-06 16:59:19.666079+00
734		\N	f	m m	m	m		f	t	2023-11-06 16:59:19.575797+00
547		\N	f	Любовь Гурьева	Любовь	Гурьева		f	t	2023-10-20 19:34:34.102878+00
690		\N	f	xzdrfc	МирБезХохляндий			f	t	2023-10-20 19:34:34.59043+00
100		\N	f	Анастасия Ипатова	Анастасия	Ипатова		f	t	2023-10-13 17:11:43.854606+00
791		\N	f	Xarovixh	Zeny			f	t	2023-11-06 16:59:20.716513+00
586		\N	f	Екатерина Шерстобитова 	Екатерина Шерстобитова			f	t	2023-10-20 19:34:34.23064+00
503		\N	f	Валентина Аввакумова	Валентина	Аввакумова		f	t	2023-10-20 19:34:33.952437+00
786		\N	f	krizzzzzzzzzzzzzzzzz	Ангелина			f	t	2023-11-06 16:59:19.898214+00
525		\N	f	Надежда Владимировна	Надежда	Владимировна		f	t	2023-10-20 19:34:34.024191+00
762		\N	f	Каскад Каскадов	Каскад	Каскадов		f	t	2023-11-06 16:59:19.761112+00
341		\N	f	Сайпулла Кайдышев	Сайпулла	Кайдышев		f	t	2023-10-20 19:34:33.378632+00
26		\N	f	Татьяна Чернокозова	Татьяна	Чернокозова		f	t	2023-10-13 17:04:41.848334+00
792		\N	f	Nikolai9998	Николай			f	t	2023-11-06 16:59:20.870585+00
789		\N	f	александр 	александр			f	t	2023-11-06 16:59:20.509714+00
563		\N	f	Надежда Лоскутова	Надежда	Лоскутова		f	t	2023-10-20 19:34:34.150293+00
788		\N	f	Annа 	Annа			f	t	2023-11-06 16:59:20.401117+00
794		\N	f	Наталья Алексеевна 🧑‍🏫 	Наталья Алексеевна 🧑‍🏫			f	t	2023-11-06 16:59:20.994782+00
391		\N	f	Елена Смирнова	Елена	Смирнова		f	t	2023-10-20 19:34:33.543626+00
769		\N	f	Катерина 	Катерина			f	t	2023-11-06 16:59:19.790913+00
673		\N	f	Татьяна Незванова	Татьяна	Незванова		f	t	2023-10-20 19:34:34.532032+00
692		\N	f	Евгения Кривозубова	Евгения	Кривозубова		f	t	2023-10-20 19:34:34.59581+00
203		\N	f	Ekateruna34	Екатерина	Белокопытова		f	t	2023-10-13 17:13:42.307388+00
198	pbkdf2_sha256$600000$qR7ipqnHGQcUFyxrqpgWNa$GBPSLZNFv27EYzJehaYX36swD9du0DVqnQCZX2HJ/wQ=	\N	f	Юлия 	Юлия			f	t	2023-10-13 17:13:34.95305+00
266		\N	f	Дмитрий Волгоград-Домодедова	Дмитрий	Волгоград-Домодедова		f	t	2023-10-20 19:34:33.070744+00
754		\N	f	🥰 	🥰			f	t	2023-11-06 16:59:19.714424+00
755		\N	f	Леха Дьяков	Леха	Дьяков		f	t	2023-11-06 16:59:19.716429+00
400		\N	f	Оля Чугай	Оля	Чугай		f	t	2023-10-20 19:34:33.582645+00
756		\N	f	Наталья Косова	Наталья	Косова		f	t	2023-11-06 16:59:19.72345+00
757		\N	f	Йа Фаттах1 	Йа Фаттах1			f	t	2023-11-06 16:59:19.734611+00
283		\N	f	Ульяна Решетова	Ульяна	Решетова		f	t	2023-10-20 19:34:33.14373+00
758		\N	f	Софья 	Софья			f	t	2023-11-06 16:59:19.74258+00
760		\N	f	YULIY96K	Юляшка			f	t	2023-11-06 16:59:19.753161+00
761		\N	f	Евгений Фёдоров	Евгений	Фёдоров		f	t	2023-11-06 16:59:19.75821+00
780		\N	f	malik_909	Никитосик			f	t	2023-11-06 16:59:19.861492+00
763		\N	f	Indira_Abdurashidova	Индира	Султановна		f	t	2023-11-06 16:59:19.764071+00
795		\N	f	Владимир Носачёв	Владимир	Носачёв		f	t	2023-11-06 17:02:12.634023+00
764		\N	f	Верочка Куц	Верочка	Куц		f	t	2023-11-06 16:59:19.766894+00
765		\N	f	М.В 	М.В			f	t	2023-11-06 16:59:19.773927+00
766		\N	f	Tanya 	Tanya			f	t	2023-11-06 16:59:19.782173+00
796		\N	f	549288 	549288			f	t	2023-11-06 17:02:13.652304+00
777		\N	f	Софья Капкейки/бенто/торты	Софья	Капкейки/бенто/торты		f	t	2023-11-06 16:59:19.84749+00
767		\N	f	TanE4ek2000	Татьяна	Захарова		f	t	2023-11-06 16:59:19.785131+00
783		\N	f	Tel 	Tel			f	t	2023-11-06 16:59:19.874706+00
206		\N	f	Залёшина(Мамедова) Гульнара	Залёшина(Мамедова)	Гульнара		f	t	2023-10-20 18:44:45.139118+00
770		\N	f	Yuliya_Spitsyna	Liana	Pugacheva		f	t	2023-11-06 16:59:19.806958+00
773		\N	f	magazinmegabytekalachnadonu	МАГАЗИН МЕГАБАЙТ			f	t	2023-11-06 16:59:19.829733+00
774		\N	f	NikitaDubnyuk	Никита	Дубнюк		f	t	2023-11-06 16:59:19.832754+00
778		\N	f	pavnikolaevich	Павел	Николаевич		f	t	2023-11-06 16:59:19.850537+00
779		\N	f	Aleksandr 	Aleksandr			f	t	2023-11-06 16:59:19.857732+00
782		\N	f	AngeLina_0622	Геля			f	t	2023-11-06 16:59:19.871691+00
70		\N	f	Елена Дуенко （Зеленская）	Елена	Дуенко （Зеленская）		f	t	2023-10-13 17:11:21.047823+00
262		\N	f	Александр Яковлев натяжные потолки	Александр	Яковлев натяжные потолки		f	t	2023-10-20 19:34:33.051087+00
797		\N	f	Светлана Шахова	Светлана	Шахова		f	t	2023-11-06 17:02:13.840011+00
787		\N	f	821996 	821996			f	t	2023-11-06 16:59:19.903312+00
781		\N	f	Валерия Покоева	Валерия	Покоева		f	t	2023-11-06 16:59:19.868718+00
82		\N	f	Наталья 	Наталья			f	t	2023-10-13 17:11:28.64546+00
336		\N	f	Анастасия 	Анастасия			f	t	2023-10-20 19:34:33.357458+00
798		\N	f	Мирослав Мельничук	Мирослав	Мельничук		f	t	2023-11-06 17:02:13.895885+00
784		\N	f	elna_134	елена			f	t	2023-11-06 16:59:19.891307+00
55		\N	f	Юлия Ромашкина	Юлия	Ромашкина		f	t	2023-10-13 17:11:16.499968+00
51		\N	f	Геннадий Кияшко	Геннадий	Кияшко		f	t	2023-10-13 17:11:15.004243+00
643		\N	f	unearthly263	꧁༺ 𝓓𝓪𝓻𝔂𝓪 ༻꧂			f	t	2023-10-20 19:34:34.425759+00
768		\N	f	Annacher91	Анюта	)		f	t	2023-11-06 16:59:19.788118+00
785		\N	f	gelusik_088	геля			f	t	2023-11-06 16:59:19.893392+00
776		\N	f	Максим Максимов	Максим	Максимов		f	t	2023-11-06 16:59:19.838623+00
320		\N	f	lSoSnal	Анастасия | Философ и художница			f	t	2023-10-20 19:34:33.29101+00
790		\N	f	JONI93bat	~даня~👑			f	t	2023-11-06 16:59:20.572577+00
793		\N	f	emcxxq	🫦			f	t	2023-11-06 16:59:20.896548+00
481		\N	f	Кульченко Юрий	Кульченко	Юрий		f	t	2023-10-20 19:34:33.878403+00
771		\N	f	Любовь Путилина.	Любовь	Путилина.		f	t	2023-11-06 16:59:19.813834+00
775		\N	f	dzhanneski	жанна			f	t	2023-11-06 16:59:19.835701+00
448		\N	f	Ирина Юрченко	Ирина	Юрченко		f	t	2023-10-20 19:34:33.754596+00
772		\N	f	Наська 	Наська			f	t	2023-11-06 16:59:19.826903+00
801		\N	f	Movladi 				f	t	2023-11-06 17:22:45.705546+00
800		\N	f	Юлия Александровна				f	t	2023-11-06 17:22:28.761799+00
802		\N	f	🧚‍♀️ 				f	t	2023-11-06 17:23:02.334513+00
803		\N	f	Александр Коваленко				f	t	2023-11-06 17:23:39.979481+00
804		\N	f	Six_trap				f	t	2023-11-06 18:44:08.037111+00
805		\N	f	Angelok_2002				f	t	2023-11-06 18:52:14.060361+00
799		\N	f	....... 	.......			f	t	2023-11-06 17:02:13.897947+00
759		\N	f	Татьяна Симонцева Татьяна Симонцева	Татьяна Симонцева	Татьяна Симонцева		f	t	2023-11-06 16:59:19.748292+00
139		\N	f	Татьяна Понамареаа	Татьяна	Понамареаа		f	t	2023-10-13 17:12:07.681865+00
806	pbkdf2_sha256$600000$6Of94ODuhTHVCVxVFW9Sbf$G6RvTn7d48IE6PrHCyCe8CW086wUwon1DRSk5gwfChY=	\N	f	video_admin	Видео	Админ	crackc@mail.ru	f	t	2024-01-31 12:32:00+00
1	pbkdf2_sha256$600000$CXKJXYSDHC9LFfUFQwBj54$J5VkgzRjDkGOSplSKK1trmulr75WTbi2We/IQwLnC6M=	2024-02-17 17:14:26.64559+00	t	video	Сергей	Носачёв	video@gmail.com	t	t	2016-10-12 11:14:50+00
8	!g3tjGjkSGyR7AjHVI1LLNQIDGczgLC8q6xrUmmsh	2024-01-21 17:54:17.477541+00	f	nosacevaulia53	юлия	носачёва	nosacevaulia53@gmail.com	f	t	2023-02-01 22:48:16.876267+00
807	!D0w7n3QrQJ967T8Q03geVgfL85JTUMATdvTYBsAY	2024-02-15 17:04:13.370141+00	f	user	Юлия	Носачева		f	t	2024-02-02 12:26:03.745794+00
10	pbkdf2_sha256$600000$TYJmEirzNFmT2IxQcZUsjM$K5JOpqNGvhoKylnQUYoL0Pz2DUhD+tXOgCzaoO37m8Q=	2024-02-25 12:19:42.463132+00	t	root				t	t	2023-07-02 13:13:24.871953+00
\.


--
-- Data for Name: auth_user_groups; Type: TABLE DATA; Schema: public; Owner: video
--

COPY public.auth_user_groups (id, user_id, group_id) FROM stdin;
\.


--
-- Data for Name: auth_user_user_permissions; Type: TABLE DATA; Schema: public; Owner: video
--

COPY public.auth_user_user_permissions (id, user_id, permission_id) FROM stdin;
1	2	34
2	2	35
3	2	37
4	2	38
5	2	71
6	2	70
7	2	40
8	2	41
9	2	22
10	2	23
11	3	40
12	3	41
14	3	73
15	3	74
16	3	75
17	4	34
18	4	35
19	4	36
20	4	137
21	4	138
22	4	139
23	4	140
24	4	141
25	4	142
26	4	143
27	4	144
28	4	145
29	4	146
30	4	147
31	4	148
32	4	149
33	4	150
34	4	151
35	4	152
36	4	153
37	4	154
38	4	155
39	4	156
40	4	157
41	4	158
42	4	159
43	4	160
44	4	161
45	4	162
46	4	163
47	4	164
\.


--
-- Data for Name: authtoken_token; Type: TABLE DATA; Schema: public; Owner: video
--

COPY public.authtoken_token (key, created, user_id) FROM stdin;
b40fb5e0052dee34c2182a684deb8458c79e7d9d	2022-04-15 19:30:03.560427+00	5
684b3beacf90d94b828beb171f4f69b2cf554a41	2022-04-18 20:17:04.61927+00	6
bd844552667e2ae76eb0676acf242526a1a1458b	2022-05-15 18:29:36.786609+00	7
5358019fce2bbf4f9fcfd1e2b4d6817ad32b0bc0	2022-08-30 20:51:15.909484+00	1
\.


--
-- Data for Name: chunks_chunk; Type: TABLE DATA; Schema: public; Owner: video
--

COPY public.chunks_chunk (id, key, content) FROM stdin;
\.


--
-- Data for Name: chunks_group; Type: TABLE DATA; Schema: public; Owner: video
--

COPY public.chunks_group (id, key, content) FROM stdin;
\.


--
-- Data for Name: chunks_image; Type: TABLE DATA; Schema: public; Owner: video
--

COPY public.chunks_image (id, key, image) FROM stdin;
\.


--
-- Data for Name: chunks_media; Type: TABLE DATA; Schema: public; Owner: video
--

COPY public.chunks_media (id, key, title, "desc", media) FROM stdin;
\.


--
-- Data for Name: comunicate_chat; Type: TABLE DATA; Schema: public; Owner: video
--

COPY public.comunicate_chat (id, message, user_info, created, updated, room_id, active, shared, shared_room_id) FROM stdin;
2	000	{"Ht": "video@gmail.com", "ID": "117272817956961682145", "Pe": "Sergei Nosachyev", "US": "117272817956961682145", "wJ": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "zS": "Nosachyev", "zU": "Sergei", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "socketId": "D1smfWFW2hsSYH1zAAAC"}	2021-09-07 21:54:34+00	2021-09-07 21:54:34+00	\N	f	f	\N
62	000	{"$S": "Nosachyev", "ID": "117272817956961682145", "JU": "Sergei", "Re": "Sergei Nosachyev", "Yt": "video@gmail.com", "oK": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "uT": "117272817956961682145", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "email": "video@gmail.com", "socketId": "UtCKDE-amG6bJkveAAAD"}	2021-10-28 20:01:34+00	2021-10-28 20:01:34+00	\N	t	f	\N
68	jjj\n	{"$S": "Nosachyev", "ID": "117272817956961682145", "JU": "Sergei", "Re": "Sergei Nosachyev", "Yt": "video@gmail.com", "oK": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "uT": "117272817956961682145", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "email": "video@gmail.com", "socketId": "5dw5HSqKSfTsWePfAAAD"}	2021-10-29 17:32:25+00	2021-10-29 17:32:25+00	\N	t	f	\N
69	yyy\n	{"$S": "Nosachyev", "ID": "117272817956961682145", "JU": "Sergei", "Re": "Sergei Nosachyev", "Yt": "video@gmail.com", "oK": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "uT": "117272817956961682145", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "email": "video@gmail.com", "socketId": "ZEop6Osu2yHFMQjsAAAJ"}	2021-10-29 17:33:26+00	2021-10-29 17:33:26+00	\N	t	f	\N
3	аааа\n	{"Ht": "video@gmail.com", "ID": "117272817956961682145", "Pe": "Sergei Nosachyev", "US": "117272817956961682145", "wJ": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "zS": "Nosachyev", "zU": "Sergei", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "socketId": "2_3f9OZHG9dFDVIRAAAF"}	2021-09-13 18:06:57+00	2021-09-13 18:06:57+00	\N	f	f	\N
63	uuu	{"$S": "Nosachyev", "ID": "117272817956961682145", "JU": "Sergei", "Re": "Sergei Nosachyev", "Yt": "video@gmail.com", "oK": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "uT": "117272817956961682145", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "email": "video@gmail.com", "socketId": "dLm0yUHLVNGtnR6IAAAF"}	2021-10-28 20:19:46+00	2021-10-28 20:19:46+00	\N	t	f	\N
19	fad	{"Ht": "video@gmail.com", "ID": "117272817956961682145", "Pe": "Sergei Nosachyev", "US": "117272817956961682145", "wJ": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "zS": "Nosachyev", "zU": "Sergei", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "socketId": "DDnCfEIVlzLMF1T2AAAD"}	2021-09-13 21:03:36+00	2021-09-13 21:03:36+00	\N	t	f	\N
64	fffff	{"$S": "Nosachyev", "ID": "117272817956961682145", "JU": "Sergei", "Re": "Sergei Nosachyev", "Yt": "video@gmail.com", "oK": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "uT": "117272817956961682145", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "email": "video@gmail.com", "socketId": "rOLkjphDLd4do1VFAAAC"}	2021-10-28 20:22:18+00	2021-10-28 20:22:18+00	\N	t	f	\N
65	l;'	{"$S": "Nosachyev", "ID": "117272817956961682145", "JU": "Sergei", "Re": "Sergei Nosachyev", "Yt": "video@gmail.com", "oK": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "uT": "117272817956961682145", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "email": "video@gmail.com", "socketId": "rOLkjphDLd4do1VFAAAC"}	2021-10-28 20:22:34+00	2021-10-28 20:22:34+00	\N	t	f	\N
66	ooo	{"$S": "Nosachyev", "ID": "117272817956961682145", "JU": "Sergei", "Re": "Sergei Nosachyev", "Yt": "video@gmail.com", "oK": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "uT": "117272817956961682145", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "email": "video@gmail.com", "socketId": "olkN2z2yakQXNwyCAAAF"}	2021-10-28 20:28:53+00	2021-10-28 20:28:53+00	\N	t	f	\N
49	fff	{"HU": "Sergei", "ID": "117272817956961682145", "Re": "Sergei Nosachyev", "Tt": "video@gmail.com", "YS": "Nosachyev", "lK": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "sT": "117272817956961682145", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "socketId": "yYeJZjjK8AW5ZoRvAAAH"}	2021-10-01 17:03:50+00	2021-10-01 17:03:50+00	\N	t	f	\N
50	ffff	{"HU": "Sergei", "ID": "117272817956961682145", "Re": "Sergei Nosachyev", "Tt": "video@gmail.com", "YS": "Nosachyev", "lK": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "sT": "117272817956961682145", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "socketId": "yYeJZjjK8AW5ZoRvAAAH"}	2021-10-01 17:04:00+00	2021-10-01 17:04:00+00	\N	t	f	\N
51	ffff	{"HU": "Sergei", "ID": "117272817956961682145", "Re": "Sergei Nosachyev", "Tt": "video@gmail.com", "YS": "Nosachyev", "lK": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "sT": "117272817956961682145", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "socketId": "yYeJZjjK8AW5ZoRvAAAH"}	2021-10-01 17:04:19+00	2021-10-01 17:04:19+00	\N	t	f	\N
5	Привет еще\n	{"Ht": "video@gmail.com", "ID": "117272817956961682145", "Pe": "Sergei Nosachyev", "US": "117272817956961682145", "wJ": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "zS": "Nosachyev", "zU": "Sergei", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "socketId": "2_3f9OZHG9dFDVIRAAAF"}	2021-09-13 19:51:37+00	2021-09-13 19:51:37+00	\N	t	f	\N
8	fff	{"Ht": "video@gmail.com", "ID": "117272817956961682145", "Pe": "Sergei Nosachyev", "US": "117272817956961682145", "wJ": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "zS": "Nosachyev", "zU": "Sergei", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "socketId": "SHDArBJDmPxgQAIkAAAC"}	2021-09-13 20:35:01+00	2021-09-13 20:35:01+00	\N	t	f	\N
9	ggg\n	{"Ht": "video@gmail.com", "ID": "117272817956961682145", "Pe": "Sergei Nosachyev", "US": "117272817956961682145", "wJ": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "zS": "Nosachyev", "zU": "Sergei", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "socketId": "SHDArBJDmPxgQAIkAAAC"}	2021-09-13 20:40:15+00	2021-09-13 20:40:15+00	\N	t	f	\N
10	fff\n	{"Ht": "video@gmail.com", "ID": "117272817956961682145", "Pe": "Sergei Nosachyev", "US": "117272817956961682145", "wJ": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "zS": "Nosachyev", "zU": "Sergei", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "socketId": "SHDArBJDmPxgQAIkAAAC"}	2021-09-13 20:42:12+00	2021-09-13 20:42:12+00	\N	t	f	\N
11	fff	{"Ht": "video@gmail.com", "ID": "117272817956961682145", "Pe": "Sergei Nosachyev", "US": "117272817956961682145", "wJ": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "zS": "Nosachyev", "zU": "Sergei", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "socketId": "SHDArBJDmPxgQAIkAAAC"}	2021-09-13 20:44:32+00	2021-09-13 20:44:32+00	\N	t	f	\N
12	fff	{"Ht": "video@gmail.com", "ID": "117272817956961682145", "Pe": "Sergei Nosachyev", "US": "117272817956961682145", "wJ": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "zS": "Nosachyev", "zU": "Sergei", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "socketId": "SHDArBJDmPxgQAIkAAAC"}	2021-09-13 20:46:26+00	2021-09-13 20:46:26+00	\N	t	f	\N
13	fff	{"Ht": "video@gmail.com", "ID": "117272817956961682145", "Pe": "Sergei Nosachyev", "US": "117272817956961682145", "wJ": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "zS": "Nosachyev", "zU": "Sergei", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "socketId": "SHDArBJDmPxgQAIkAAAC"}	2021-09-13 20:48:36+00	2021-09-13 20:48:36+00	\N	t	f	\N
15	ttt	{"Ht": "video@gmail.com", "ID": "117272817956961682145", "Pe": "Sergei Nosachyev", "US": "117272817956961682145", "wJ": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "zS": "Nosachyev", "zU": "Sergei", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "socketId": "SHDArBJDmPxgQAIkAAAC"}	2021-09-13 20:50:55+00	2021-09-13 20:50:55+00	\N	t	f	\N
30	edit\nthird\n\n\n\n	{"Ht": "video@gmail.com", "ID": "117272817956961682145", "Pe": "Sergei Nosachyev", "US": "117272817956961682145", "wJ": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "zS": "Nosachyev", "zU": "Sergei", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "socketId": "BypzwLIWHz_uwNfRAAAH"}	2021-09-13 21:38:55+00	2021-09-13 21:38:55+00	\N	t	f	\N
16	hhg\n	{"Ht": "video@gmail.com", "ID": "117272817956961682145", "Pe": "Sergei Nosachyev", "US": "117272817956961682145", "wJ": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "zS": "Nosachyev", "zU": "Sergei", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "socketId": "SHDArBJDmPxgQAIkAAAC"}	2021-09-13 20:52:30+00	2021-09-13 20:52:30+00	\N	t	f	\N
17	hhh	{"Ht": "video@gmail.com", "ID": "117272817956961682145", "Pe": "Sergei Nosachyev", "US": "117272817956961682145", "wJ": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "zS": "Nosachyev", "zU": "Sergei", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "socketId": "SHDArBJDmPxgQAIkAAAC"}	2021-09-13 20:54:38+00	2021-09-13 20:54:38+00	\N	t	f	\N
18	fffss456	{"Ht": "video@gmail.com", "ID": "117272817956961682145", "Pe": "Sergei Nosachyev", "US": "117272817956961682145", "wJ": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "zS": "Nosachyev", "zU": "Sergei", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "socketId": "SHDArBJDmPxgQAIkAAAC"}	2021-09-13 20:57:21+00	2021-09-13 20:57:21+00	\N	t	f	\N
6	Привет Всем\n\n	{"Ht": "video@gmail.com", "ID": "117272817956961682145", "Pe": "Sergei Nosachyev", "US": "117272817956961682145", "wJ": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "zS": "Nosachyev", "zU": "Sergei", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "socketId": "MUvt-I0qFAJNopDRAAAB"}	2021-09-13 20:12:31+00	2021-09-13 20:12:31+00	\N	t	f	\N
7	Привет всем\n\n	{"Ht": "video@gmail.com", "ID": "117272817956961682145", "Pe": "Sergei Nosachyev", "US": "117272817956961682145", "wJ": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "zS": "Nosachyev", "zU": "Sergei", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "socketId": "SHDArBJDmPxgQAIkAAAC"}	2021-09-13 20:30:39+00	2021-09-13 20:30:39+00	\N	t	f	\N
20	afa	{"Ht": "video@gmail.com", "ID": "117272817956961682145", "Pe": "Sergei Nosachyev", "US": "117272817956961682145", "wJ": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "zS": "Nosachyev", "zU": "Sergei", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "socketId": "8aN21ZNsIz6wpOR0AAAE"}	2021-09-13 21:04:46+00	2021-09-13 21:04:46+00	\N	t	f	\N
21	fds	{"Ht": "video@gmail.com", "ID": "117272817956961682145", "Pe": "Sergei Nosachyev", "US": "117272817956961682145", "wJ": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "zS": "Nosachyev", "zU": "Sergei", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "socketId": "QVO8UbCp4MYsxBbJAAAA"}	2021-09-13 21:07:41+00	2021-09-13 21:07:41+00	\N	t	f	\N
22	faw	{"Ht": "video@gmail.com", "ID": "117272817956961682145", "Pe": "Sergei Nosachyev", "US": "117272817956961682145", "wJ": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "zS": "Nosachyev", "zU": "Sergei", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "socketId": "XPYGOQ3uxFUOHLJWAAAC"}	2021-09-13 21:08:33+00	2021-09-13 21:08:33+00	\N	t	f	\N
23	564	{"Ht": "video@gmail.com", "ID": "117272817956961682145", "Pe": "Sergei Nosachyev", "US": "117272817956961682145", "wJ": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "zS": "Nosachyev", "zU": "Sergei", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "socketId": "dXVP2YIPEbnvKUOpAAAB"}	2021-09-13 21:09:21+00	2021-09-13 21:09:21+00	\N	t	f	\N
24	fkh	{"Ht": "video@gmail.com", "ID": "117272817956961682145", "Pe": "Sergei Nosachyev", "US": "117272817956961682145", "wJ": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "zS": "Nosachyev", "zU": "Sergei", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "socketId": "CZUaRl7rgrPYR-lSAAAB"}	2021-09-13 21:11:37+00	2021-09-13 21:11:37+00	\N	t	f	\N
25	ffff	{"Ht": "video@gmail.com", "ID": "117272817956961682145", "Pe": "Sergei Nosachyev", "US": "117272817956961682145", "wJ": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "zS": "Nosachyev", "zU": "Sergei", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "socketId": "L3kwdwWj5rYyex9cAAAC"}	2021-09-13 21:17:15+00	2021-09-13 21:17:15+00	\N	t	f	\N
26	ffs	{"Ht": "video@gmail.com", "ID": "117272817956961682145", "Pe": "Sergei Nosachyev", "US": "117272817956961682145", "wJ": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "zS": "Nosachyev", "zU": "Sergei", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "socketId": "QI_BNvs2swnonJMEAAAD"}	2021-09-13 21:23:46+00	2021-09-13 21:23:46+00	\N	t	f	\N
27	fds	{"Ht": "video@gmail.com", "ID": "117272817956961682145", "Pe": "Sergei Nosachyev", "US": "117272817956961682145", "wJ": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "zS": "Nosachyev", "zU": "Sergei", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "socketId": "s9hpGAnnnJ67w7fpAAAE"}	2021-09-13 21:26:37+00	2021-09-13 21:26:37+00	\N	t	f	\N
28	fdg	{"Ht": "video@gmail.com", "ID": "117272817956961682145", "Pe": "Sergei Nosachyev", "US": "117272817956961682145", "wJ": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "zS": "Nosachyev", "zU": "Sergei", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "socketId": "y3Cn9QAByoB1QxzgAAAF"}	2021-09-13 21:35:28+00	2021-09-13 21:35:28+00	\N	t	f	\N
29	123	{"Ht": "video@gmail.com", "ID": "117272817956961682145", "Pe": "Sergei Nosachyev", "US": "117272817956961682145", "wJ": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "zS": "Nosachyev", "zU": "Sergei", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "socketId": "D6sucZSMhl4yIzR2AAAG"}	2021-09-13 21:36:28+00	2021-09-13 21:36:28+00	\N	t	f	\N
31	fddd	{"Ht": "video@gmail.com", "ID": "117272817956961682145", "Pe": "Sergei Nosachyev", "US": "117272817956961682145", "wJ": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "zS": "Nosachyev", "zU": "Sergei", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "socketId": "BypzwLIWHz_uwNfRAAAH"}	2021-09-13 21:39:05+00	2021-09-13 21:39:05+00	\N	t	f	\N
32	123	{"Ht": "video@gmail.com", "ID": "117272817956961682145", "Pe": "Sergei Nosachyev", "US": "117272817956961682145", "wJ": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "zS": "Nosachyev", "zU": "Sergei", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "socketId": "zeaE3ItjAHUz_MHLAAAI"}	2021-09-13 21:39:57+00	2021-09-13 21:39:57+00	\N	t	f	\N
33	ggg	{"Ht": "video@gmail.com", "ID": "117272817956961682145", "Pe": "Sergei Nosachyev", "US": "117272817956961682145", "wJ": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "zS": "Nosachyev", "zU": "Sergei", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "socketId": "UHyWfnisfbgjnjFkAAAA"}	2021-09-14 16:43:30+00	2021-09-14 16:43:30+00	\N	t	f	\N
34	hhh	{"Ht": "video@gmail.com", "ID": "117272817956961682145", "Pe": "Sergei Nosachyev", "US": "117272817956961682145", "wJ": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "zS": "Nosachyev", "zU": "Sergei", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "socketId": "fb-fsP9Zrr0c9Lf5AAAB"}	2021-09-14 16:54:55+00	2021-09-14 16:54:55+00	\N	t	f	\N
35	666	{"Ht": "video@gmail.com", "ID": "117272817956961682145", "Pe": "Sergei Nosachyev", "US": "117272817956961682145", "wJ": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "zS": "Nosachyev", "zU": "Sergei", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "socketId": "uZaTpaBl5Ghc2e-TAAAA"}	2021-09-14 16:58:41+00	2021-09-14 16:58:41+00	\N	t	f	\N
14	fff	{"Ht": "video@gmail.com", "ID": "117272817956961682145", "Pe": "Sergei Nosachyev", "US": "117272817956961682145", "wJ": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "zS": "Nosachyev", "zU": "Sergei", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "socketId": "SHDArBJDmPxgQAIkAAAC"}	2021-09-13 20:49:52+00	2021-09-13 20:49:52+00	\N	t	f	\N
36	Привет updae\n	{"Ht": "video@gmail.com", "ID": "117272817956961682145", "Pe": "Sergei Nosachyev", "US": "117272817956961682145", "wJ": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "zS": "Nosachyev", "zU": "Sergei", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "socketId": "wTC9L8Gmtj9YOF1tAAAA"}	2021-09-14 17:10:08+00	2021-09-14 17:10:08+00	\N	t	f	\N
37	edit\n	{"Ht": "video@gmail.com", "ID": "117272817956961682145", "Pe": "Sergei Nosachyev", "US": "117272817956961682145", "wJ": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "zS": "Nosachyev", "zU": "Sergei", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "socketId": "Ul7rP5lOLAVYiU_jAAAB"}	2021-09-14 17:16:30+00	2021-09-14 17:16:30+00	\N	t	f	\N
38	gggg	{"Ht": "video@gmail.com", "ID": "117272817956961682145", "Pe": "Sergei Nosachyev", "US": "117272817956961682145", "wJ": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "zS": "Nosachyev", "zU": "Sergei", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "socketId": "YLmmtr2olJ1Z21ntAAAB"}	2021-09-14 17:22:00+00	2021-09-14 17:22:00+00	\N	t	f	\N
39	test\n	{"Ht": "video@gmail.com", "ID": "117272817956961682145", "Pe": "Sergei Nosachyev", "US": "117272817956961682145", "wJ": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "zS": "Nosachyev", "zU": "Sergei", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "socketId": "2yurW1aUQ5tcTndVAAAC"}	2021-09-14 17:28:22+00	2021-09-14 17:28:22+00	\N	t	f	\N
40	hsr	{"Ht": "video@gmail.com", "ID": "117272817956961682145", "Pe": "Sergei Nosachyev", "US": "117272817956961682145", "wJ": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "zS": "Nosachyev", "zU": "Sergei", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "socketId": "DbRGGySmC1PlkiGeAAAD"}	2021-09-14 17:29:53+00	2021-09-14 17:29:53+00	\N	t	f	\N
41	редактированное \nсообщение\n	{"Ht": "video@gmail.com", "ID": "117272817956961682145", "Pe": "Sergei Nosachyev", "US": "117272817956961682145", "wJ": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "zS": "Nosachyev", "zU": "Sergei", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "socketId": "Z-_Vmr_ZGWSbr8HrAAAE"}	2021-09-14 17:33:09+00	2021-09-14 17:33:09+00	\N	t	f	\N
42	и вам приветп	{"Ht": "video@gmail.com", "ID": "117272817956961682145", "Pe": "Sergei Nosachyev", "US": "117272817956961682145", "wJ": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "zS": "Nosachyev", "zU": "Sergei", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "socketId": "enPay3GGAbRvaADIAAAB"}	2021-09-14 17:56:31+00	2021-09-21 21:34:33.788549+00	\N	t	f	\N
45	\nбб	{"HU": "Sergei", "ID": "117272817956961682145", "Re": "Sergei Nosachyev", "Tt": "video@gmail.com", "YS": "Nosachyev", "lK": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "sT": "117272817956961682145", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "socketId": "NBLIm8q4cV5EOW9fAAAN"}	2021-09-27 19:51:01+00	2021-09-27 19:51:01+00	\N	t	f	\N
46	uuu	{"HU": "Sergei", "ID": "117272817956961682145", "Re": "Sergei Nosachyev", "Tt": "video@gmail.com", "YS": "Nosachyev", "lK": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "sT": "117272817956961682145", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "socketId": "k0RAmp2217ZMZTIWAAAP"}	2021-09-27 19:57:08+00	2021-09-27 19:57:08+00	\N	t	f	\N
47	fff	{"HU": "Sergei", "ID": "117272817956961682145", "Re": "Sergei Nosachyev", "Tt": "video@gmail.com", "YS": "Nosachyev", "lK": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "sT": "117272817956961682145", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "socketId": "WEtdPvLGmRKJVZhDAAAT"}	2021-09-27 19:59:27+00	2021-09-27 19:59:27+00	\N	t	f	\N
43	ПРИВЕТ!111	{"Ht": "video@gmail.com", "ID": "117272817956961682145", "Pe": "Sergei Nosachyev", "US": "117272817956961682145", "wJ": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "zS": "Nosachyev", "zU": "Sergei", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "socketId": "enPay3GGAbRvaADIAAAB"}	2021-09-14 17:57:18+00	2021-09-26 19:49:05.585102+00	\N	t	f	\N
48	 nnn	{"HU": "Sergei", "ID": "117272817956961682145", "Re": "Sergei Nosachyev", "Tt": "video@gmail.com", "YS": "Nosachyev", "lK": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "sT": "117272817956961682145", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "socketId": "UfizG8_SnTuDFgjGAAAV"}	2021-09-27 20:00:52+00	2021-09-27 20:00:52+00	\N	t	f	\N
44	Ого работает работает\n	{"HU": "Sergei", "ID": "117272817956961682145", "Re": "Sergei Nosachyev", "Tt": "video@gmail.com", "YS": "Nosachyev", "lK": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "sT": "117272817956961682145", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "socketId": "K6fgFZkjA63CPAQXAAAD"}	2021-09-27 19:33:54+00	2021-09-27 19:39:34.984703+00	\N	t	f	\N
67	888	{"$S": "Nosachyev", "ID": "117272817956961682145", "JU": "Sergei", "Re": "Sergei Nosachyev", "Yt": "video@gmail.com", "oK": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "uT": "117272817956961682145", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "email": "video@gmail.com", "socketId": "jl0SOEwu9bsQCCFIAAAC"}	2021-10-28 20:30:31+00	2021-10-28 20:30:31+00	\N	t	f	\N
60	нннн\n\n	{"HU": "Sergei", "ID": "117272817956961682145", "Re": "Sergei Nosachyev", "Tt": "video@gmail.com", "YS": "Nosachyev", "lK": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "sT": "117272817956961682145", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "socketId": "mSrQcO6goO8dYwYDAAAH"}	2021-10-15 20:39:37+00	2021-10-15 20:39:37+00	\N	t	f	\N
52	hhh	{"HU": "Sergei", "ID": "117272817956961682145", "Re": "Sergei Nosachyev", "Tt": "video@gmail.com", "YS": "Nosachyev", "lK": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "sT": "117272817956961682145", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "socketId": "AoBZyaT3qMGpDaQ2AAAF"}	2021-10-09 20:15:43+00	2021-10-09 20:15:43+00	\N	t	f	\N
53	hh	{"HU": "Sergei", "ID": "117272817956961682145", "Re": "Sergei Nosachyev", "Tt": "video@gmail.com", "YS": "Nosachyev", "lK": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "sT": "117272817956961682145", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "socketId": "txqmTrj0RuY_7J-GAAAF"}	2021-10-09 20:19:30+00	2021-10-09 20:19:30+00	\N	t	f	\N
54	777	{"HU": "Sergei", "ID": "117272817956961682145", "Re": "Sergei Nosachyev", "Tt": "video@gmail.com", "YS": "Nosachyev", "lK": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "sT": "117272817956961682145", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "socketId": "RgT07SvDRfaTV5S1AAAB"}	2021-10-09 21:20:14+00	2021-10-09 21:20:14+00	\N	t	f	\N
55	888	{"HU": "Sergei", "ID": "117272817956961682145", "Re": "Sergei Nosachyev", "Tt": "video@gmail.com", "YS": "Nosachyev", "lK": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "sT": "117272817956961682145", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "socketId": "n7cmfkGf22WoMgI8AAAD"}	2021-10-10 18:13:30+00	2021-10-10 18:13:30+00	\N	t	f	\N
56	000	{"HU": "Sergei", "ID": "117272817956961682145", "Re": "Sergei Nosachyev", "Tt": "video@gmail.com", "YS": "Nosachyev", "lK": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "sT": "117272817956961682145", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "socketId": "n7cmfkGf22WoMgI8AAAD"}	2021-10-10 18:13:36+00	2021-10-10 18:13:36+00	\N	t	f	\N
57	ooo	{"HU": "Sergei", "ID": "117272817956961682145", "Re": "Sergei Nosachyev", "Tt": "video@gmail.com", "YS": "Nosachyev", "lK": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "sT": "117272817956961682145", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "socketId": "PZBmKGP7JSlTCBBsAAAD"}	2021-10-10 18:15:23+00	2021-10-10 18:15:23+00	\N	t	f	\N
58	555	{"HU": "Sergei", "ID": "117272817956961682145", "Re": "Sergei Nosachyev", "Tt": "video@gmail.com", "YS": "Nosachyev", "lK": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "sT": "117272817956961682145", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "socketId": "PZBmKGP7JSlTCBBsAAAD"}	2021-10-10 18:15:27+00	2021-10-10 18:15:27+00	\N	t	f	\N
59	777	{"HU": "Sergei", "ID": "117272817956961682145", "Re": "Sergei Nosachyev", "Tt": "video@gmail.com", "YS": "Nosachyev", "lK": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "sT": "117272817956961682145", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "socketId": "PZBmKGP7JSlTCBBsAAAD"}	2021-10-10 18:15:31+00	2021-10-10 18:15:31+00	\N	t	f	\N
61	777	{"$S": "Nosachyev", "ID": "117272817956961682145", "JU": "Sergei", "Re": "Sergei Nosachyev", "Yt": "video@gmail.com", "oK": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "uT": "117272817956961682145", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "email": "video@gmail.com", "socketId": "h9CO5SJjdqqNyB8SAAAD"}	2021-10-27 22:22:16+00	2021-10-27 22:27:16.385753+00	\N	t	f	\N
81	Не слышно \n	{"ID": "113775862545894265568", "IX": "Teacher", "RM": "https://lh3.googleusercontent.com/a/AATXAJyN9OH2US18OD3t_61m43wGg5YauCCHzfRJJBjv=s96-c", "YV": "Lani", "jf": "Teacher Lani", "jv": "eolanischool@gmail.com", "sW": "113775862545894265568", "img": "https://lh3.googleusercontent.com/a/AATXAJyN9OH2US18OD3t_61m43wGg5YauCCHzfRJJBjv=s96-c", "name": "Teacher Lani", "email": "eolanischool@gmail.com", "socketId": "q0T-np52vthbTd0pAAAl"}	2021-11-22 18:05:25+00	2021-11-22 18:05:25+00	\N	t	f	\N
82	Блин 😩!	{"ID": "110440839651729452910", "IX": "Lani", "RM": "https://lh3.googleusercontent.com/a-/AOh14GhL169GDaC_kl2L-ZinFIaucSj_cIcMVPkYTjkN=s96-c", "YV": "Eco_friendly", "jf": "Lani Eco_friendly", "jv": "eslinterworld@gmail.com", "sW": "110440839651729452910", "img": "https://lh3.googleusercontent.com/a-/AOh14GhL169GDaC_kl2L-ZinFIaucSj_cIcMVPkYTjkN=s96-c", "name": "Lani Eco_friendly", "email": "eslinterworld@gmail.com"}	2021-11-22 18:07:34+00	2021-12-08 22:20:17.961011+00	\N	t	f	\N
91	fff\n	{"GW": "117272817956961682145", "ID": "117272817956961682145", "VX": "Sergei", "hf": "Sergei Nosachyev", "kW": "Nosachyev", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "pv": "video@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "email": "video@gmail.com"}	2021-12-14 20:36:59+00	2021-12-14 20:36:59+00	\N	t	f	\N
92	ttt	{"GW": "117272817956961682145", "ID": "117272817956961682145", "VX": "Sergei", "hf": "Sergei Nosachyev", "kW": "Nosachyev", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "pv": "video@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "email": "video@gmail.com", "socketId": "YyaWLIoxWIAqd4i3AAAF"}	2021-12-14 20:39:32+00	2021-12-14 20:39:32+00	\N	t	f	\N
83	ты слышишь?i	{"GW": "117272817956961682145", "ID": "117272817956961682145", "VX": "Sergei", "hf": "Sergei Nosachyev", "kW": "Nosachyev", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "pv": "video@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "email": "video@gmail.com"}	2021-11-22 18:08:32+00	2021-12-08 22:10:36.622345+00	\N	t	f	\N
93	fdf	{"GW": "117272817956961682145", "ID": "117272817956961682145", "VX": "Sergei", "hf": "Sergei Nosachyev", "kW": "Nosachyev", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "pv": "video@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "email": "video@gmail.com", "socketId": "kNVfYyeHYHdkIovZAAAH"}	2021-12-14 20:40:41+00	2021-12-14 20:40:41+00	\N	t	f	\N
70	oooo	{"$S": "Nosachyev", "ID": "117272817956961682145", "JU": "Sergei", "Re": "Sergei Nosachyev", "Yt": "video@gmail.com", "oK": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "uT": "117272817956961682145", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "email": "video@gmail.com"}	2021-11-04 07:28:50+00	2021-11-04 07:28:50+00	\N	t	f	\N
97	fff	{"GW": "117272817956961682145", "ID": "117272817956961682145", "VX": "Sergei", "hf": "Sergei Nosachyev", "kW": "Nosachyev", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "pv": "video@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "email": "video@gmail.com", "socketId": "FCmD43VJ6bpsfveKAAAJ"}	2021-12-14 20:46:35+00	2021-12-14 20:46:35+00	\N	t	f	\N
187	щщщ\n	{"ID": 1, "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Сергей", "email": "crackkc@mail.ru"}	2022-04-03 11:17:00+00	2022-04-03 11:17:00+00	\N	t	f	\N
98	fff	{"GW": "117272817956961682145", "ID": "117272817956961682145", "VX": "Sergei", "hf": "Sergei Nosachyev", "kW": "Nosachyev", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "pv": "video@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "email": "video@gmail.com", "socketId": "FCmD43VJ6bpsfveKAAAJ"}	2021-12-14 20:46:43+00	2021-12-14 20:46:43+00	\N	t	f	\N
99	fff	{"GW": "117272817956961682145", "ID": "117272817956961682145", "VX": "Sergei", "hf": "Sergei Nosachyev", "kW": "Nosachyev", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "pv": "video@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "email": "video@gmail.com"}	2021-12-14 20:49:15+00	2021-12-14 20:49:15+00	\N	t	f	\N
119	Тпр\n	{"DW": "107728481804504324824", "ID": "107728481804504324824", "SX": "юлия", "gW": "носачёва", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjpHClJ_UNBkYwIUYlTCJBNnmQud2aKYMdlT8nf=s96-c", "nf": "юлия носачёва", "nv": "nosacevaulia53@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjpHClJ_UNBkYwIUYlTCJBNnmQud2aKYMdlT8nf=s96-c", "name": "юлия носачёва", "email": "nosacevaulia53@gmail.com"}	2021-12-16 18:51:44+00	2021-12-16 18:51:44+00	\N	t	f	\N
120	Оорпп	{"DW": "107728481804504324824", "ID": "107728481804504324824", "SX": "юлия", "gW": "носачёва", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjpHClJ_UNBkYwIUYlTCJBNnmQud2aKYMdlT8nf=s96-c", "nf": "юлия носачёва", "nv": "nosacevaulia53@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjpHClJ_UNBkYwIUYlTCJBNnmQud2aKYMdlT8nf=s96-c", "name": "юлия носачёва", "email": "nosacevaulia53@gmail.com"}	2021-12-16 18:56:24+00	2021-12-16 18:56:24+00	\N	t	f	\N
121	Ррапап	{"DW": "107728481804504324824", "ID": "107728481804504324824", "SX": "юлия", "gW": "носачёва", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjpHClJ_UNBkYwIUYlTCJBNnmQud2aKYMdlT8nf=s96-c", "nf": "юлия носачёва", "nv": "nosacevaulia53@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjpHClJ_UNBkYwIUYlTCJBNnmQud2aKYMdlT8nf=s96-c", "name": "юлия носачёва", "email": "nosacevaulia53@gmail.com"}	2021-12-16 18:56:31+00	2021-12-16 18:56:31+00	\N	t	f	\N
122	длтлдтлд\n	{"DW": "117272817956961682145", "ID": "117272817956961682145", "SX": "Sergei", "gW": "Nosachyev", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "nf": "Sergei Nosachyev", "nv": "video@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "email": "video@gmail.com"}	2021-12-16 18:58:08+00	2021-12-16 18:58:08+00	\N	t	f	\N
123	ододло\n	{"DW": "117272817956961682145", "ID": "117272817956961682145", "SX": "Sergei", "gW": "Nosachyev", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "nf": "Sergei Nosachyev", "nv": "video@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "email": "video@gmail.com"}	2021-12-16 18:58:12+00	2021-12-16 18:58:12+00	\N	t	f	\N
94	fff	{"GW": "117272817956961682145", "ID": "117272817956961682145", "VX": "Sergei", "hf": "Sergei Nosachyev", "kW": "Nosachyev", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "pv": "video@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "email": "video@gmail.com", "socketId": "kNVfYyeHYHdkIovZAAAH"}	2021-12-14 20:42:37+00	2021-12-14 20:42:37+00	\N	t	f	\N
95	123	{"GW": "117272817956961682145", "ID": "117272817956961682145", "VX": "Sergei", "hf": "Sergei Nosachyev", "kW": "Nosachyev", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "pv": "video@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "email": "video@gmail.com", "socketId": "kNVfYyeHYHdkIovZAAAH"}	2021-12-14 20:43:08+00	2021-12-14 20:43:08+00	\N	t	f	\N
96	dffd	{"GW": "117272817956961682145", "ID": "117272817956961682145", "VX": "Sergei", "hf": "Sergei Nosachyev", "kW": "Nosachyev", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "pv": "video@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "email": "video@gmail.com", "socketId": "kNVfYyeHYHdkIovZAAAH"}	2021-12-14 20:45:58+00	2021-12-14 20:45:58+00	\N	t	f	\N
124	олдодолдщо\n	{"DW": "117272817956961682145", "ID": "117272817956961682145", "SX": "Sergei", "gW": "Nosachyev", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "nf": "Sergei Nosachyev", "nv": "video@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "email": "video@gmail.com"}	2021-12-16 18:58:14+00	2021-12-16 18:58:14+00	\N	t	f	\N
71	Гвгвг	{"$S": "носачёва", "ID": "107728481804504324824", "JU": "юлия", "Re": "юлия носачёва", "Yt": "nosacevaulia53@gmail.com", "oK": "https://lh3.googleusercontent.com/a-/AOh14GjpHClJ_UNBkYwIUYlTCJBNnmQud2aKYMdlT8nf=s96-c", "uT": "107728481804504324824", "img": "https://lh3.googleusercontent.com/a-/AOh14GjpHClJ_UNBkYwIUYlTCJBNnmQud2aKYMdlT8nf=s96-c", "name": "юлия носачёва", "email": "nosacevaulia53@gmail.com"}	2021-11-04 13:29:27+00	2021-11-04 13:29:27+00	\N	t	f	\N
100	 Ооо \n	{"DW": "107728481804504324824", "ID": "107728481804504324824", "SX": "юлия", "gW": "носачёва", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjpHClJ_UNBkYwIUYlTCJBNnmQud2aKYMdlT8nf=s96-c", "nf": "юлия носачёва", "nv": "nosacevaulia53@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjpHClJ_UNBkYwIUYlTCJBNnmQud2aKYMdlT8nf=s96-c", "name": "юлия носачёва", "email": "nosacevaulia53@gmail.com"}	2021-12-16 16:52:08+00	2021-12-16 16:52:08+00	\N	t	f	\N
101	Ннр	{"DW": "107728481804504324824", "ID": "107728481804504324824", "SX": "юлия", "gW": "носачёва", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjpHClJ_UNBkYwIUYlTCJBNnmQud2aKYMdlT8nf=s96-c", "nf": "юлия носачёва", "nv": "nosacevaulia53@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjpHClJ_UNBkYwIUYlTCJBNnmQud2aKYMdlT8nf=s96-c", "name": "юлия носачёва", "email": "nosacevaulia53@gmail.com"}	2021-12-16 16:55:25+00	2021-12-16 16:55:25+00	\N	t	f	\N
102	Рдгнк \n	{"DW": "107728481804504324824", "ID": "107728481804504324824", "SX": "юлия", "gW": "носачёва", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjpHClJ_UNBkYwIUYlTCJBNnmQud2aKYMdlT8nf=s96-c", "nf": "юлия носачёва", "nv": "nosacevaulia53@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjpHClJ_UNBkYwIUYlTCJBNnmQud2aKYMdlT8nf=s96-c", "name": "юлия носачёва", "email": "nosacevaulia53@gmail.com"}	2021-12-16 16:55:42+00	2021-12-16 16:55:42+00	\N	t	f	\N
103	Пнол\n	{"DW": "107728481804504324824", "ID": "107728481804504324824", "SX": "юлия", "gW": "носачёва", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjpHClJ_UNBkYwIUYlTCJBNnmQud2aKYMdlT8nf=s96-c", "nf": "юлия носачёва", "nv": "nosacevaulia53@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjpHClJ_UNBkYwIUYlTCJBNnmQud2aKYMdlT8nf=s96-c", "name": "юлия носачёва", "email": "nosacevaulia53@gmail.com"}	2021-12-16 16:55:59+00	2021-12-16 16:55:59+00	\N	t	f	\N
104	Рпррм\n	{"DW": "107728481804504324824", "ID": "107728481804504324824", "SX": "юлия", "gW": "носачёва", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjpHClJ_UNBkYwIUYlTCJBNnmQud2aKYMdlT8nf=s96-c", "nf": "юлия носачёва", "nv": "nosacevaulia53@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjpHClJ_UNBkYwIUYlTCJBNnmQud2aKYMdlT8nf=s96-c", "name": "юлия носачёва", "email": "nosacevaulia53@gmail.com"}	2021-12-16 16:56:38+00	2021-12-16 16:56:38+00	\N	t	f	\N
105	Паплл\n	{"DW": "107728481804504324824", "ID": "107728481804504324824", "SX": "юлия", "gW": "носачёва", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjpHClJ_UNBkYwIUYlTCJBNnmQud2aKYMdlT8nf=s96-c", "nf": "юлия носачёва", "nv": "nosacevaulia53@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjpHClJ_UNBkYwIUYlTCJBNnmQud2aKYMdlT8nf=s96-c", "name": "юлия носачёва", "email": "nosacevaulia53@gmail.com"}	2021-12-16 16:58:44+00	2021-12-16 16:58:44+00	\N	t	f	\N
106	Хорл\n	{"DW": "107728481804504324824", "ID": "107728481804504324824", "SX": "юлия", "gW": "носачёва", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjpHClJ_UNBkYwIUYlTCJBNnmQud2aKYMdlT8nf=s96-c", "nf": "юлия носачёва", "nv": "nosacevaulia53@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjpHClJ_UNBkYwIUYlTCJBNnmQud2aKYMdlT8nf=s96-c", "name": "юлия носачёва", "email": "nosacevaulia53@gmail.com"}	2021-12-16 16:59:38+00	2021-12-16 16:59:38+00	\N	t	f	\N
107	Нрпр	{"DW": "107728481804504324824", "ID": "107728481804504324824", "SX": "юлия", "gW": "носачёва", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjpHClJ_UNBkYwIUYlTCJBNnmQud2aKYMdlT8nf=s96-c", "nf": "юлия носачёва", "nv": "nosacevaulia53@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjpHClJ_UNBkYwIUYlTCJBNnmQud2aKYMdlT8nf=s96-c", "name": "юлия носачёва", "email": "nosacevaulia53@gmail.com"}	2021-12-16 18:47:58+00	2021-12-16 18:47:58+00	\N	t	f	\N
108	Ро\n	{"DW": "107728481804504324824", "ID": "107728481804504324824", "SX": "юлия", "gW": "носачёва", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjpHClJ_UNBkYwIUYlTCJBNnmQud2aKYMdlT8nf=s96-c", "nf": "юлия носачёва", "nv": "nosacevaulia53@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjpHClJ_UNBkYwIUYlTCJBNnmQud2aKYMdlT8nf=s96-c", "name": "юлия носачёва", "email": "nosacevaulia53@gmail.com"}	2021-12-16 18:48:41+00	2021-12-16 18:48:41+00	\N	t	f	\N
109	Олрр\n	{"DW": "107728481804504324824", "ID": "107728481804504324824", "SX": "юлия", "gW": "носачёва", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjpHClJ_UNBkYwIUYlTCJBNnmQud2aKYMdlT8nf=s96-c", "nf": "юлия носачёва", "nv": "nosacevaulia53@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjpHClJ_UNBkYwIUYlTCJBNnmQud2aKYMdlT8nf=s96-c", "name": "юлия носачёва", "email": "nosacevaulia53@gmail.com"}	2021-12-16 18:48:48+00	2021-12-16 18:48:48+00	\N	t	f	\N
110	Рлоп\n	{"DW": "107728481804504324824", "ID": "107728481804504324824", "SX": "юлия", "gW": "носачёва", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjpHClJ_UNBkYwIUYlTCJBNnmQud2aKYMdlT8nf=s96-c", "nf": "юлия носачёва", "nv": "nosacevaulia53@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjpHClJ_UNBkYwIUYlTCJBNnmQud2aKYMdlT8nf=s96-c", "name": "юлия носачёва", "email": "nosacevaulia53@gmail.com"}	2021-12-16 18:48:51+00	2021-12-16 18:48:51+00	\N	t	f	\N
111	Ть\n	{"DW": "107728481804504324824", "ID": "107728481804504324824", "SX": "юлия", "gW": "носачёва", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjpHClJ_UNBkYwIUYlTCJBNnmQud2aKYMdlT8nf=s96-c", "nf": "юлия носачёва", "nv": "nosacevaulia53@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjpHClJ_UNBkYwIUYlTCJBNnmQud2aKYMdlT8nf=s96-c", "name": "юлия носачёва", "email": "nosacevaulia53@gmail.com"}	2021-12-16 18:50:07+00	2021-12-16 18:50:07+00	\N	t	f	\N
72	ллл	{"$S": "Nosachyev", "ID": "117272817956961682145", "JU": "Sergei", "Re": "Sergei Nosachyev", "Yt": "video@gmail.com", "oK": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "uT": "117272817956961682145", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "email": "video@gmail.com"}	2021-11-11 17:02:27+00	2021-11-11 17:02:27+00	\N	t	f	\N
112	Прор\n	{"DW": "107728481804504324824", "ID": "107728481804504324824", "SX": "юлия", "gW": "носачёва", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjpHClJ_UNBkYwIUYlTCJBNnmQud2aKYMdlT8nf=s96-c", "nf": "юлия носачёва", "nv": "nosacevaulia53@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjpHClJ_UNBkYwIUYlTCJBNnmQud2aKYMdlT8nf=s96-c", "name": "юлия носачёва", "email": "nosacevaulia53@gmail.com"}	2021-12-16 18:50:16+00	2021-12-16 18:50:16+00	\N	t	f	\N
113	Ррр\n	{"DW": "107728481804504324824", "ID": "107728481804504324824", "SX": "юлия", "gW": "носачёва", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjpHClJ_UNBkYwIUYlTCJBNnmQud2aKYMdlT8nf=s96-c", "nf": "юлия носачёва", "nv": "nosacevaulia53@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjpHClJ_UNBkYwIUYlTCJBNnmQud2aKYMdlT8nf=s96-c", "name": "юлия носачёва", "email": "nosacevaulia53@gmail.com"}	2021-12-16 18:50:20+00	2021-12-16 18:50:20+00	\N	t	f	\N
73	hi\n	{"ID": "113775862545894265568", "NS": "Lani", "SJ": "https://lh3.googleusercontent.com/a/AATXAJyN9OH2US18OD3t_61m43wGg5YauCCHzfRJJBjv=s96-c", "Se": "Teacher Lani", "Tt": "eolanischool@gmail.com", "hT": "113775862545894265568", "wU": "Teacher", "img": "https://lh3.googleusercontent.com/a/AATXAJyN9OH2US18OD3t_61m43wGg5YauCCHzfRJJBjv=s96-c", "name": "Teacher Lani", "email": "eolanischool@gmail.com"}	2021-11-15 16:23:20+00	2021-11-15 16:23:20+00	\N	t	f	\N
74	hi\n	{"$S": "Nosachyev", "ID": "117272817956961682145", "JU": "Sergei", "Re": "Sergei Nosachyev", "Yt": "video@gmail.com", "oK": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "uT": "117272817956961682145", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "email": "video@gmail.com"}	2021-11-15 16:23:33+00	2021-11-15 16:23:33+00	\N	t	f	\N
75	how are you?\n	{"ID": "113775862545894265568", "NS": "Lani", "SJ": "https://lh3.googleusercontent.com/a/AATXAJyN9OH2US18OD3t_61m43wGg5YauCCHzfRJJBjv=s96-c", "Se": "Teacher Lani", "Tt": "eolanischool@gmail.com", "hT": "113775862545894265568", "wU": "Teacher", "img": "https://lh3.googleusercontent.com/a/AATXAJyN9OH2US18OD3t_61m43wGg5YauCCHzfRJJBjv=s96-c", "name": "Teacher Lani", "email": "eolanischool@gmail.com"}	2021-11-15 16:23:58+00	2021-11-15 16:23:58+00	\N	t	f	\N
76	I`m fine\n	{"$S": "Nosachyev", "ID": "117272817956961682145", "JU": "Sergei", "Re": "Sergei Nosachyev", "Yt": "video@gmail.com", "oK": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "uT": "117272817956961682145", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "email": "video@gmail.com"}	2021-11-15 16:24:07+00	2021-11-15 16:24:07+00	\N	t	f	\N
77	can you hear me?\n	{"ID": "113775862545894265568", "NS": "Lani", "SJ": "https://lh3.googleusercontent.com/a/AATXAJyN9OH2US18OD3t_61m43wGg5YauCCHzfRJJBjv=s96-c", "Se": "Teacher Lani", "Tt": "eolanischool@gmail.com", "hT": "113775862545894265568", "wU": "Teacher", "img": "https://lh3.googleusercontent.com/a/AATXAJyN9OH2US18OD3t_61m43wGg5YauCCHzfRJJBjv=s96-c", "name": "Teacher Lani", "email": "eolanischool@gmail.com"}	2021-11-15 16:27:45+00	2021-11-15 16:27:45+00	\N	t	f	\N
78	i cannot hear you\n	{"ID": "113775862545894265568", "NS": "Lani", "SJ": "https://lh3.googleusercontent.com/a/AATXAJyN9OH2US18OD3t_61m43wGg5YauCCHzfRJJBjv=s96-c", "Se": "Teacher Lani", "Tt": "eolanischool@gmail.com", "hT": "113775862545894265568", "wU": "Teacher", "img": "https://lh3.googleusercontent.com/a/AATXAJyN9OH2US18OD3t_61m43wGg5YauCCHzfRJJBjv=s96-c", "name": "Teacher Lani", "email": "eolanischool@gmail.com"}	2021-11-15 16:28:18+00	2021-11-15 16:28:18+00	\N	t	f	\N
114	Грв\n	{"DW": "107728481804504324824", "ID": "107728481804504324824", "SX": "юлия", "gW": "носачёва", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjpHClJ_UNBkYwIUYlTCJBNnmQud2aKYMdlT8nf=s96-c", "nf": "юлия носачёва", "nv": "nosacevaulia53@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjpHClJ_UNBkYwIUYlTCJBNnmQud2aKYMdlT8nf=s96-c", "name": "юлия носачёва", "email": "nosacevaulia53@gmail.com"}	2021-12-16 18:51:31+00	2021-12-16 18:51:31+00	\N	t	f	\N
115	Рркп\n	{"DW": "107728481804504324824", "ID": "107728481804504324824", "SX": "юлия", "gW": "носачёва", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjpHClJ_UNBkYwIUYlTCJBNnmQud2aKYMdlT8nf=s96-c", "nf": "юлия носачёва", "nv": "nosacevaulia53@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjpHClJ_UNBkYwIUYlTCJBNnmQud2aKYMdlT8nf=s96-c", "name": "юлия носачёва", "email": "nosacevaulia53@gmail.com"}	2021-12-16 18:51:35+00	2021-12-16 18:51:35+00	\N	t	f	\N
79	hi im here \n	{"ID": "113775862545894265568", "NS": "Lani", "SJ": "https://lh3.googleusercontent.com/a/AATXAJyN9OH2US18OD3t_61m43wGg5YauCCHzfRJJBjv=s96-c", "Se": "Teacher Lani", "Tt": "eolanischool@gmail.com", "hT": "113775862545894265568", "wU": "Teacher", "img": "https://lh3.googleusercontent.com/a/AATXAJyN9OH2US18OD3t_61m43wGg5YauCCHzfRJJBjv=s96-c", "name": "Teacher Lani", "email": "eolanischool@gmail.com"}	2021-11-15 19:36:29+00	2021-11-15 19:36:29+00	\N	t	f	\N
80	but i cannot hear you\n	{"ID": "113775862545894265568", "NS": "Lani", "SJ": "https://lh3.googleusercontent.com/a/AATXAJyN9OH2US18OD3t_61m43wGg5YauCCHzfRJJBjv=s96-c", "Se": "Teacher Lani", "Tt": "eolanischool@gmail.com", "hT": "113775862545894265568", "wU": "Teacher", "img": "https://lh3.googleusercontent.com/a/AATXAJyN9OH2US18OD3t_61m43wGg5YauCCHzfRJJBjv=s96-c", "name": "Teacher Lani", "email": "eolanischool@gmail.com"}	2021-11-15 19:36:35+00	2021-11-15 19:36:35+00	\N	t	f	\N
188	vvv\n	{"ID": 1, "img": "https://r.mradx.net/pictures/99/36744E.jpg", "name": "Сергей", "email": "crackkc@mail.ru"}	2022-04-03 13:31:51+00	2022-04-03 13:31:51+00	\N	t	f	\N
116	Паокр\n	{"DW": "107728481804504324824", "ID": "107728481804504324824", "SX": "юлия", "gW": "носачёва", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjpHClJ_UNBkYwIUYlTCJBNnmQud2aKYMdlT8nf=s96-c", "nf": "юлия носачёва", "nv": "nosacevaulia53@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjpHClJ_UNBkYwIUYlTCJBNnmQud2aKYMdlT8nf=s96-c", "name": "юлия носачёва", "email": "nosacevaulia53@gmail.com"}	2021-12-16 18:51:38+00	2021-12-16 18:51:38+00	\N	t	f	\N
117	Ьпврн\n	{"DW": "107728481804504324824", "ID": "107728481804504324824", "SX": "юлия", "gW": "носачёва", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjpHClJ_UNBkYwIUYlTCJBNnmQud2aKYMdlT8nf=s96-c", "nf": "юлия носачёва", "nv": "nosacevaulia53@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjpHClJ_UNBkYwIUYlTCJBNnmQud2aKYMdlT8nf=s96-c", "name": "юлия носачёва", "email": "nosacevaulia53@gmail.com"}	2021-12-16 18:51:40+00	2021-12-16 18:51:40+00	\N	t	f	\N
118	Рамуп\n	{"DW": "107728481804504324824", "ID": "107728481804504324824", "SX": "юлия", "gW": "носачёва", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjpHClJ_UNBkYwIUYlTCJBNnmQud2aKYMdlT8nf=s96-c", "nf": "юлия носачёва", "nv": "nosacevaulia53@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjpHClJ_UNBkYwIUYlTCJBNnmQud2aKYMdlT8nf=s96-c", "name": "юлия носачёва", "email": "nosacevaulia53@gmail.com"}	2021-12-16 18:51:42+00	2021-12-16 18:51:42+00	\N	t	f	\N
125	ъзшхрхшщрхщ\n	{"DW": "117272817956961682145", "ID": "117272817956961682145", "SX": "Sergei", "gW": "Nosachyev", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "nf": "Sergei Nosachyev", "nv": "video@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "email": "video@gmail.com"}	2021-12-16 18:58:20+00	2021-12-16 18:58:20+00	\N	t	f	\N
126	Ррр\n	{"DW": "107728481804504324824", "ID": "107728481804504324824", "SX": "юлия", "gW": "носачёва", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjpHClJ_UNBkYwIUYlTCJBNnmQud2aKYMdlT8nf=s96-c", "nf": "юлия носачёва", "nv": "nosacevaulia53@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjpHClJ_UNBkYwIUYlTCJBNnmQud2aKYMdlT8nf=s96-c", "name": "юлия носачёва", "email": "nosacevaulia53@gmail.com"}	2021-12-16 18:59:52+00	2021-12-16 18:59:52+00	\N	t	f	\N
127	Ооаро\n	{"DW": "107728481804504324824", "ID": "107728481804504324824", "SX": "юлия", "gW": "носачёва", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjpHClJ_UNBkYwIUYlTCJBNnmQud2aKYMdlT8nf=s96-c", "nf": "юлия носачёва", "nv": "nosacevaulia53@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjpHClJ_UNBkYwIUYlTCJBNnmQud2aKYMdlT8nf=s96-c", "name": "юлия носачёва", "email": "nosacevaulia53@gmail.com"}	2021-12-16 19:00:02+00	2021-12-16 19:00:02+00	\N	t	f	\N
128	Ополел\n	{"DW": "107728481804504324824", "ID": "107728481804504324824", "SX": "юлия", "gW": "носачёва", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjpHClJ_UNBkYwIUYlTCJBNnmQud2aKYMdlT8nf=s96-c", "nf": "юлия носачёва", "nv": "nosacevaulia53@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjpHClJ_UNBkYwIUYlTCJBNnmQud2aKYMdlT8nf=s96-c", "name": "юлия носачёва", "email": "nosacevaulia53@gmail.com"}	2021-12-16 19:00:09+00	2021-12-16 19:00:09+00	\N	t	f	\N
129	,jbk;jbk;;\n	{"DW": "117272817956961682145", "ID": "117272817956961682145", "SX": "Sergei", "gW": "Nosachyev", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "nf": "Sergei Nosachyev", "nv": "video@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "email": "video@gmail.com", "socketId": "UerL6aiCoDrWIxucAABA"}	2021-12-16 19:00:18+00	2021-12-16 19:00:18+00	\N	t	f	\N
130	njknjkn\n	{"DW": "117272817956961682145", "ID": "117272817956961682145", "SX": "Sergei", "gW": "Nosachyev", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "nf": "Sergei Nosachyev", "nv": "video@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "email": "video@gmail.com", "socketId": "UerL6aiCoDrWIxucAABA"}	2021-12-16 19:00:20+00	2021-12-16 19:00:20+00	\N	t	f	\N
131	njnk\n	{"DW": "117272817956961682145", "ID": "117272817956961682145", "SX": "Sergei", "gW": "Nosachyev", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "nf": "Sergei Nosachyev", "nv": "video@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "email": "video@gmail.com", "socketId": "UerL6aiCoDrWIxucAABA"}	2021-12-16 19:00:21+00	2021-12-16 19:00:21+00	\N	t	f	\N
132	jkjnjn\n	{"DW": "117272817956961682145", "ID": "117272817956961682145", "SX": "Sergei", "gW": "Nosachyev", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "nf": "Sergei Nosachyev", "nv": "video@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "email": "video@gmail.com", "socketId": "UerL6aiCoDrWIxucAABA"}	2021-12-16 19:00:22+00	2021-12-16 19:00:22+00	\N	t	f	\N
133	jnbjkn\n	{"DW": "117272817956961682145", "ID": "117272817956961682145", "SX": "Sergei", "gW": "Nosachyev", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "nf": "Sergei Nosachyev", "nv": "video@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "email": "video@gmail.com", "socketId": "UerL6aiCoDrWIxucAABA"}	2021-12-16 19:00:23+00	2021-12-16 19:00:23+00	\N	t	f	\N
134	Лорр\n	{"DW": "107728481804504324824", "ID": "107728481804504324824", "SX": "юлия", "gW": "носачёва", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjpHClJ_UNBkYwIUYlTCJBNnmQud2aKYMdlT8nf=s96-c", "nf": "юлия носачёва", "nv": "nosacevaulia53@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjpHClJ_UNBkYwIUYlTCJBNnmQud2aKYMdlT8nf=s96-c", "name": "юлия носачёва", "email": "nosacevaulia53@gmail.com"}	2021-12-16 19:03:33+00	2021-12-16 19:03:33+00	\N	t	f	\N
135	Ддщролр\n	{"DW": "107728481804504324824", "ID": "107728481804504324824", "SX": "юлия", "gW": "носачёва", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjpHClJ_UNBkYwIUYlTCJBNnmQud2aKYMdlT8nf=s96-c", "nf": "юлия носачёва", "nv": "nosacevaulia53@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjpHClJ_UNBkYwIUYlTCJBNnmQud2aKYMdlT8nf=s96-c", "name": "юлия носачёва", "email": "nosacevaulia53@gmail.com"}	2021-12-16 19:03:40+00	2021-12-16 19:03:40+00	\N	t	f	\N
136	Пртт\n	{"DW": "107728481804504324824", "ID": "107728481804504324824", "SX": "юлия", "gW": "носачёва", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjpHClJ_UNBkYwIUYlTCJBNnmQud2aKYMdlT8nf=s96-c", "nf": "юлия носачёва", "nv": "nosacevaulia53@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjpHClJ_UNBkYwIUYlTCJBNnmQud2aKYMdlT8nf=s96-c", "name": "юлия носачёва", "email": "nosacevaulia53@gmail.com"}	2021-12-16 19:03:45+00	2021-12-16 19:03:45+00	\N	t	f	\N
137	kkk\n	{"DW": "117272817956961682145", "ID": "117272817956961682145", "SX": "Sergei", "gW": "Nosachyev", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "nf": "Sergei Nosachyev", "nv": "video@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "email": "video@gmail.com", "socketId": "QddncTxCNkBPr5f_AABO"}	2021-12-16 19:07:25+00	2021-12-16 19:07:25+00	\N	t	f	\N
138	kkk\n	{"DW": "117272817956961682145", "ID": "117272817956961682145", "SX": "Sergei", "gW": "Nosachyev", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "nf": "Sergei Nosachyev", "nv": "video@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "email": "video@gmail.com", "socketId": "QddncTxCNkBPr5f_AABO"}	2021-12-16 19:07:30+00	2021-12-16 19:07:30+00	\N	t	f	\N
139	jjj\n	{"DW": "117272817956961682145", "ID": "117272817956961682145", "SX": "Sergei", "gW": "Nosachyev", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "nf": "Sergei Nosachyev", "nv": "video@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "email": "video@gmail.com", "socketId": "QddncTxCNkBPr5f_AABO"}	2021-12-16 19:07:34+00	2021-12-16 19:07:34+00	\N	t	f	\N
140	Аапг\n	{"DW": "107728481804504324824", "ID": "107728481804504324824", "SX": "юлия", "gW": "носачёва", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjpHClJ_UNBkYwIUYlTCJBNnmQud2aKYMdlT8nf=s96-c", "nf": "юлия носачёва", "nv": "nosacevaulia53@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjpHClJ_UNBkYwIUYlTCJBNnmQud2aKYMdlT8nf=s96-c", "name": "юлия носачёва", "email": "nosacevaulia53@gmail.com"}	2021-12-16 19:07:53+00	2021-12-16 19:07:53+00	\N	t	f	\N
141	Ёекл\n	{"DW": "107728481804504324824", "ID": "107728481804504324824", "SX": "юлия", "gW": "носачёва", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjpHClJ_UNBkYwIUYlTCJBNnmQud2aKYMdlT8nf=s96-c", "nf": "юлия носачёва", "nv": "nosacevaulia53@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjpHClJ_UNBkYwIUYlTCJBNnmQud2aKYMdlT8nf=s96-c", "name": "юлия носачёва", "email": "nosacevaulia53@gmail.com"}	2021-12-16 19:07:59+00	2021-12-16 19:07:59+00	\N	t	f	\N
142	Реоен\n	{"DW": "107728481804504324824", "ID": "107728481804504324824", "SX": "юлия", "gW": "носачёва", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjpHClJ_UNBkYwIUYlTCJBNnmQud2aKYMdlT8nf=s96-c", "nf": "юлия носачёва", "nv": "nosacevaulia53@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjpHClJ_UNBkYwIUYlTCJBNnmQud2aKYMdlT8nf=s96-c", "name": "юлия носачёва", "email": "nosacevaulia53@gmail.com"}	2021-12-16 19:08:03+00	2021-12-16 19:08:03+00	\N	t	f	\N
143	Едеедщ\n	{"DW": "107728481804504324824", "ID": "107728481804504324824", "SX": "юлия", "gW": "носачёва", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjpHClJ_UNBkYwIUYlTCJBNnmQud2aKYMdlT8nf=s96-c", "nf": "юлия носачёва", "nv": "nosacevaulia53@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjpHClJ_UNBkYwIUYlTCJBNnmQud2aKYMdlT8nf=s96-c", "name": "юлия носачёва", "email": "nosacevaulia53@gmail.com"}	2021-12-16 19:08:07+00	2021-12-16 19:08:07+00	\N	t	f	\N
144	Епше\n	{"DW": "107728481804504324824", "ID": "107728481804504324824", "SX": "юлия", "gW": "носачёва", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjpHClJ_UNBkYwIUYlTCJBNnmQud2aKYMdlT8nf=s96-c", "nf": "юлия носачёва", "nv": "nosacevaulia53@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjpHClJ_UNBkYwIUYlTCJBNnmQud2aKYMdlT8nf=s96-c", "name": "юлия носачёва", "email": "nosacevaulia53@gmail.com"}	2021-12-16 19:08:10+00	2021-12-16 19:08:10+00	\N	t	f	\N
145	gggg\n	{"DW": "117272817956961682145", "ID": "117272817956961682145", "SX": "Sergei", "gW": "Nosachyev", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "nf": "Sergei Nosachyev", "nv": "video@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "email": "video@gmail.com", "socketId": "QddncTxCNkBPr5f_AABO"}	2021-12-16 19:08:39+00	2021-12-16 19:08:39+00	\N	t	f	\N
146	hgghgh\n	{"DW": "117272817956961682145", "ID": "117272817956961682145", "SX": "Sergei", "gW": "Nosachyev", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "nf": "Sergei Nosachyev", "nv": "video@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "email": "video@gmail.com", "socketId": "QddncTxCNkBPr5f_AABO"}	2021-12-16 19:08:51+00	2021-12-16 19:08:51+00	\N	t	f	\N
147	juuu\n	{"DW": "117272817956961682145", "ID": "117272817956961682145", "SX": "Sergei", "gW": "Nosachyev", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "nf": "Sergei Nosachyev", "nv": "video@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "email": "video@gmail.com", "socketId": "QddncTxCNkBPr5f_AABO"}	2021-12-16 19:08:55+00	2021-12-16 19:08:55+00	\N	t	f	\N
148	u\n	{"DW": "117272817956961682145", "ID": "117272817956961682145", "SX": "Sergei", "gW": "Nosachyev", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "nf": "Sergei Nosachyev", "nv": "video@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "email": "video@gmail.com", "socketId": "QddncTxCNkBPr5f_AABO"}	2021-12-16 19:08:57+00	2021-12-16 19:08:57+00	\N	t	f	\N
149	uuuu\n	{"DW": "117272817956961682145", "ID": "117272817956961682145", "SX": "Sergei", "gW": "Nosachyev", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "nf": "Sergei Nosachyev", "nv": "video@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "email": "video@gmail.com", "socketId": "QddncTxCNkBPr5f_AABO"}	2021-12-16 19:08:58+00	2021-12-16 19:08:58+00	\N	t	f	\N
150	jjjjj\n	{"DW": "117272817956961682145", "ID": "117272817956961682145", "SX": "Sergei", "gW": "Nosachyev", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "nf": "Sergei Nosachyev", "nv": "video@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "email": "video@gmail.com", "socketId": "AOoTrWL6eW9BIQ5KAABc"}	2021-12-16 19:10:58+00	2021-12-16 19:10:58+00	\N	t	f	\N
151	ijij\n	{"DW": "117272817956961682145", "ID": "117272817956961682145", "SX": "Sergei", "gW": "Nosachyev", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "nf": "Sergei Nosachyev", "nv": "video@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "email": "video@gmail.com", "socketId": "AOoTrWL6eW9BIQ5KAABc"}	2021-12-16 19:11:04+00	2021-12-16 19:11:04+00	\N	t	f	\N
152	jjijiji\n	{"DW": "117272817956961682145", "ID": "117272817956961682145", "SX": "Sergei", "gW": "Nosachyev", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "nf": "Sergei Nosachyev", "nv": "video@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "email": "video@gmail.com", "socketId": "AOoTrWL6eW9BIQ5KAABc"}	2021-12-16 19:11:08+00	2021-12-16 19:11:08+00	\N	t	f	\N
153	jijijij\n	{"DW": "117272817956961682145", "ID": "117272817956961682145", "SX": "Sergei", "gW": "Nosachyev", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "nf": "Sergei Nosachyev", "nv": "video@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "email": "video@gmail.com", "socketId": "AOoTrWL6eW9BIQ5KAABc"}	2021-12-16 19:11:12+00	2021-12-16 19:11:12+00	\N	t	f	\N
154	jijijijjjiji\n	{"DW": "117272817956961682145", "ID": "117272817956961682145", "SX": "Sergei", "gW": "Nosachyev", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "nf": "Sergei Nosachyev", "nv": "video@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "email": "video@gmail.com", "socketId": "AOoTrWL6eW9BIQ5KAABc"}	2021-12-16 19:11:16+00	2021-12-16 19:11:16+00	\N	t	f	\N
155	jijijj\n	{"DW": "117272817956961682145", "ID": "117272817956961682145", "SX": "Sergei", "gW": "Nosachyev", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "nf": "Sergei Nosachyev", "nv": "video@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "email": "video@gmail.com", "socketId": "AOoTrWL6eW9BIQ5KAABc"}	2021-12-16 19:11:18+00	2021-12-16 19:11:18+00	\N	t	f	\N
156	hhgg\n	{"DW": "117272817956961682145", "ID": "117272817956961682145", "SX": "Sergei", "gW": "Nosachyev", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "nf": "Sergei Nosachyev", "nv": "video@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "email": "video@gmail.com", "socketId": "AOoTrWL6eW9BIQ5KAABc"}	2021-12-16 19:11:23+00	2021-12-16 19:11:23+00	\N	t	f	\N
157	mmm\n	{"DW": "117272817956961682145", "ID": "117272817956961682145", "SX": "Sergei", "gW": "Nosachyev", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "nf": "Sergei Nosachyev", "nv": "video@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "email": "video@gmail.com"}	2021-12-16 19:13:25+00	2021-12-16 19:13:25+00	\N	t	f	\N
158	kkkkkk\n	{"DW": "117272817956961682145", "ID": "117272817956961682145", "SX": "Sergei", "gW": "Nosachyev", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "nf": "Sergei Nosachyev", "nv": "video@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "email": "video@gmail.com"}	2021-12-16 19:13:37+00	2021-12-16 19:13:37+00	\N	t	f	\N
159	Парл\n	{"DW": "107728481804504324824", "ID": "107728481804504324824", "SX": "юлия", "gW": "носачёва", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjpHClJ_UNBkYwIUYlTCJBNnmQud2aKYMdlT8nf=s96-c", "nf": "юлия носачёва", "nv": "nosacevaulia53@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjpHClJ_UNBkYwIUYlTCJBNnmQud2aKYMdlT8nf=s96-c", "name": "юлия носачёва", "email": "nosacevaulia53@gmail.com"}	2021-12-16 19:13:58+00	2021-12-16 19:13:58+00	\N	t	f	\N
160	Пппе\n	{"DW": "107728481804504324824", "ID": "107728481804504324824", "SX": "юлия", "gW": "носачёва", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjpHClJ_UNBkYwIUYlTCJBNnmQud2aKYMdlT8nf=s96-c", "nf": "юлия носачёва", "nv": "nosacevaulia53@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjpHClJ_UNBkYwIUYlTCJBNnmQud2aKYMdlT8nf=s96-c", "name": "юлия носачёва", "email": "nosacevaulia53@gmail.com"}	2021-12-16 19:14:01+00	2021-12-16 19:14:01+00	\N	t	f	\N
161	ihih\n	{"DW": "117272817956961682145", "ID": "117272817956961682145", "SX": "Sergei", "gW": "Nosachyev", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "nf": "Sergei Nosachyev", "nv": "video@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "email": "video@gmail.com", "socketId": "V4wnDFKXv6KBNkllAABl"}	2021-12-16 19:17:21+00	2021-12-16 19:17:21+00	\N	t	f	\N
162	Нпрн\n	{"DW": "107728481804504324824", "ID": "107728481804504324824", "SX": "юлия", "gW": "носачёва", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjpHClJ_UNBkYwIUYlTCJBNnmQud2aKYMdlT8nf=s96-c", "nf": "юлия носачёва", "nv": "nosacevaulia53@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjpHClJ_UNBkYwIUYlTCJBNnmQud2aKYMdlT8nf=s96-c", "name": "юлия носачёва", "email": "nosacevaulia53@gmail.com"}	2021-12-16 19:17:38+00	2021-12-16 19:17:38+00	\N	t	f	\N
163	Орро	{"DW": "107728481804504324824", "ID": "107728481804504324824", "SX": "юлия", "gW": "носачёва", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjpHClJ_UNBkYwIUYlTCJBNnmQud2aKYMdlT8nf=s96-c", "nf": "юлия носачёва", "nv": "nosacevaulia53@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjpHClJ_UNBkYwIUYlTCJBNnmQud2aKYMdlT8nf=s96-c", "name": "юлия носачёва", "email": "nosacevaulia53@gmail.com"}	2021-12-16 19:33:36+00	2021-12-16 19:33:36+00	\N	t	f	\N
164	Оолдл	{"DW": "107728481804504324824", "ID": "107728481804504324824", "SX": "юлия", "gW": "носачёва", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjpHClJ_UNBkYwIUYlTCJBNnmQud2aKYMdlT8nf=s96-c", "nf": "юлия носачёва", "nv": "nosacevaulia53@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjpHClJ_UNBkYwIUYlTCJBNnmQud2aKYMdlT8nf=s96-c", "name": "юлия носачёва", "email": "nosacevaulia53@gmail.com"}	2021-12-16 19:35:45+00	2021-12-16 19:35:45+00	\N	t	f	\N
165	Ппп\n	{"DW": "107728481804504324824", "ID": "107728481804504324824", "SX": "юлия", "gW": "носачёва", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjpHClJ_UNBkYwIUYlTCJBNnmQud2aKYMdlT8nf=s96-c", "nf": "юлия носачёва", "nv": "nosacevaulia53@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjpHClJ_UNBkYwIUYlTCJBNnmQud2aKYMdlT8nf=s96-c", "name": "юлия носачёва", "email": "nosacevaulia53@gmail.com"}	2021-12-16 19:38:26+00	2021-12-16 19:38:26+00	\N	t	f	\N
166	Мпп\n	{"DW": "107728481804504324824", "ID": "107728481804504324824", "SX": "юлия", "gW": "носачёва", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjpHClJ_UNBkYwIUYlTCJBNnmQud2aKYMdlT8nf=s96-c", "nf": "юлия носачёва", "nv": "nosacevaulia53@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjpHClJ_UNBkYwIUYlTCJBNnmQud2aKYMdlT8nf=s96-c", "name": "юлия носачёва", "email": "nosacevaulia53@gmail.com", "socketId": "PxRf_5_Q_9owuY8bAAB-"}	2021-12-16 19:39:21+00	2021-12-16 19:39:21+00	\N	t	f	\N
167	Мпо\n	{"DW": "107728481804504324824", "ID": "107728481804504324824", "SX": "юлия", "gW": "носачёва", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjpHClJ_UNBkYwIUYlTCJBNnmQud2aKYMdlT8nf=s96-c", "nf": "юлия носачёва", "nv": "nosacevaulia53@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjpHClJ_UNBkYwIUYlTCJBNnmQud2aKYMdlT8nf=s96-c", "name": "юлия носачёва", "email": "nosacevaulia53@gmail.com"}	2021-12-16 19:43:55+00	2021-12-16 19:43:55+00	\N	t	f	\N
168	kj;vj\n	{"DW": "117272817956961682145", "ID": "117272817956961682145", "SX": "Sergei", "gW": "Nosachyev", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "nf": "Sergei Nosachyev", "nv": "video@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "email": "video@gmail.com"}	2021-12-16 19:46:31+00	2021-12-16 19:46:31+00	\N	t	f	\N
169	Арпрр\n	{"DW": "107728481804504324824", "ID": "107728481804504324824", "SX": "юлия", "gW": "носачёва", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjpHClJ_UNBkYwIUYlTCJBNnmQud2aKYMdlT8nf=s96-c", "nf": "юлия носачёва", "nv": "nosacevaulia53@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjpHClJ_UNBkYwIUYlTCJBNnmQud2aKYMdlT8nf=s96-c", "name": "юлия носачёва", "email": "nosacevaulia53@gmail.com"}	2021-12-16 19:46:49+00	2021-12-16 19:46:49+00	\N	t	f	\N
170	Ррееп\n	{"DW": "107728481804504324824", "ID": "107728481804504324824", "SX": "юлия", "gW": "носачёва", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjpHClJ_UNBkYwIUYlTCJBNnmQud2aKYMdlT8nf=s96-c", "nf": "юлия носачёва", "nv": "nosacevaulia53@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjpHClJ_UNBkYwIUYlTCJBNnmQud2aKYMdlT8nf=s96-c", "name": "юлия носачёва", "email": "nosacevaulia53@gmail.com"}	2021-12-16 19:46:55+00	2021-12-16 19:46:55+00	\N	t	f	\N
171	дрянь\n	{"DW": "117272817956961682145", "ID": "117272817956961682145", "SX": "Sergei", "gW": "Nosachyev", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "nf": "Sergei Nosachyev", "nv": "video@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "email": "video@gmail.com"}	2021-12-27 19:06:52+00	2021-12-27 19:06:52+00	\N	t	f	\N
172	fff	{"DW": "117272817956961682145", "ID": "117272817956961682145", "SX": "Sergei", "gW": "Nosachyev", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "nf": "Sergei Nosachyev", "nv": "video@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "email": "video@gmail.com"}	2021-12-27 19:22:22+00	2021-12-27 19:22:22+00	\N	t	f	\N
173	iii	{"DW": "117272817956961682145", "ID": "117272817956961682145", "SX": "Sergei", "gW": "Nosachyev", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "nf": "Sergei Nosachyev", "nv": "video@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "email": "video@gmail.com"}	2021-12-27 19:23:25+00	2021-12-27 19:23:25+00	\N	t	f	\N
174	rrr\n	{"DW": "117272817956961682145", "ID": "117272817956961682145", "SX": "Sergei", "gW": "Nosachyev", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "nf": "Sergei Nosachyev", "nv": "video@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "email": "video@gmail.com", "socketId": "UGTc4O3T-g1RPl5nAAES"}	2021-12-27 19:32:07+00	2021-12-27 19:32:07+00	\N	t	f	\N
175	ffffff\n	{"DW": "117272817956961682145", "ID": "117272817956961682145", "SX": "Sergei", "gW": "Nosachyev", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "nf": "Sergei Nosachyev", "nv": "video@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "email": "video@gmail.com"}	2021-12-27 19:34:11+00	2021-12-27 19:34:11+00	\N	t	f	\N
176	ffffff\n	{"DW": "117272817956961682145", "ID": "117272817956961682145", "SX": "Sergei", "gW": "Nosachyev", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "nf": "Sergei Nosachyev", "nv": "video@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "email": "video@gmail.com"}	2021-12-27 19:37:24+00	2021-12-27 19:37:24+00	\N	t	f	\N
177	hhhh\n	{"DW": "117272817956961682145", "ID": "117272817956961682145", "SX": "Sergei", "gW": "Nosachyev", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "nf": "Sergei Nosachyev", "nv": "video@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "email": "video@gmail.com"}	2021-12-27 19:38:54+00	2021-12-27 19:38:54+00	\N	t	f	\N
178	rrrrr\n	{"DW": "117272817956961682145", "ID": "117272817956961682145", "SX": "Sergei", "gW": "Nosachyev", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "nf": "Sergei Nosachyev", "nv": "video@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "email": "video@gmail.com"}	2021-12-27 19:40:08+00	2021-12-27 19:40:08+00	\N	t	f	\N
179	ttt\n	{"DW": "117272817956961682145", "ID": "117272817956961682145", "SX": "Sergei", "gW": "Nosachyev", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "nf": "Sergei Nosachyev", "nv": "video@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "email": "video@gmail.com"}	2021-12-27 19:41:45+00	2021-12-27 19:41:45+00	\N	t	f	\N
180	fff	{"DW": "117272817956961682145", "ID": "117272817956961682145", "SX": "Sergei", "gW": "Nosachyev", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "nf": "Sergei Nosachyev", "nv": "video@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "email": "video@gmail.com"}	2021-12-27 19:49:35+00	2021-12-27 19:49:35+00	\N	t	f	\N
181	rrrr\n	{"DW": "117272817956961682145", "ID": "117272817956961682145", "SX": "Sergei", "gW": "Nosachyev", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "nf": "Sergei Nosachyev", "nv": "video@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "email": "video@gmail.com"}	2021-12-27 19:50:50+00	2021-12-27 19:50:50+00	\N	t	f	\N
182	dddd\n	{"DW": "117272817956961682145", "ID": "117272817956961682145", "SX": "Sergei", "gW": "Nosachyev", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "nf": "Sergei Nosachyev", "nv": "video@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "email": "video@gmail.com"}	2021-12-27 19:51:14+00	2021-12-27 19:51:14+00	\N	t	f	\N
183	llll\n	{"DW": "117272817956961682145", "ID": "117272817956961682145", "SX": "Sergei", "gW": "Nosachyev", "nN": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "nf": "Sergei Nosachyev", "nv": "video@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "email": "video@gmail.com"}	2021-12-27 19:52:38+00	2021-12-27 19:52:38+00	\N	t	f	\N
184	Hi	{"AN": "https://lh3.googleusercontent.com/a-/AOh14GjpHClJ_UNBkYwIUYlTCJBNnmQud2aKYMdlT8nf=s96-c", "ID": "107728481804504324824", "SW": "107728481804504324824", "hY": "юлия", "sf": "юлия носачёва", "vW": "носачёва", "zv": "nosacevaulia53@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjpHClJ_UNBkYwIUYlTCJBNnmQud2aKYMdlT8nf=s96-c", "name": "юлия носачёва", "email": "nosacevaulia53@gmail.com"}	2022-03-27 09:29:51+00	2022-03-27 09:29:51+00	\N	t	f	\N
185	жопа\n	{"AN": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "ID": "117272817956961682145", "SW": "117272817956961682145", "hY": "Sergei", "sf": "Sergei Nosachyev", "vW": "Nosachyev", "zv": "video@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjwXvUMDuMuodSlAJ7s9_AuIs4qYljDms5C4_JJTg=s96-c", "name": "Sergei Nosachyev", "email": "video@gmail.com", "socketId": "CQclN-iYh5L0sYWMAAcd"}	2022-04-02 18:04:04+00	2022-04-02 18:04:04+00	\N	t	f	\N
186	Хрюник	{"AN": "https://lh3.googleusercontent.com/a-/AOh14GjpHClJ_UNBkYwIUYlTCJBNnmQud2aKYMdlT8nf=s96-c", "ID": "107728481804504324824", "SW": "107728481804504324824", "hY": "юлия", "sf": "юлия носачёва", "vW": "носачёва", "zv": "nosacevaulia53@gmail.com", "img": "https://lh3.googleusercontent.com/a-/AOh14GjpHClJ_UNBkYwIUYlTCJBNnmQud2aKYMdlT8nf=s96-c", "name": "юлия носачёва", "email": "nosacevaulia53@gmail.com"}	2022-04-02 18:04:51+00	2022-04-02 18:04:51+00	\N	t	f	\N
189	Ftdf	{"ID": "107728481804504324824", "OZ": "юлия", "VO": "https://lh3.googleusercontent.com/a/AGNmyxaDsWF0OvC_dBLy4KodKlha-TafRuRPdkwKy-R-=s96-c", "eY": "носачёва", "jw": "nosacevaulia53@gmail.com", "xY": "107728481804504324824", "yf": "юлия носачёва", "img": "https://lh3.googleusercontent.com/a/AGNmyxaDsWF0OvC_dBLy4KodKlha-TafRuRPdkwKy-R-=s96-c", "name": "юлия носачёва", "email": "nosacevaulia53@gmail.com", "auth_site": "google"}	2023-02-25 18:17:22+00	2023-02-25 18:17:22+00	\N	t	f	\N
\.


--
-- Data for Name: comunicate_language; Type: TABLE DATA; Schema: public; Owner: video
--

COPY public.comunicate_language (id, name, active, article, code, comment, directionality, natural_name) FROM stdin;
1	Boro	f	en:Boro language	bo		ltr	बड़
2	Afar	f	en:Afar language	aa		ltr	Afar
3	Abkhazian	f	en:Abkhazian language	ab		ltr	Аҧсуа
4	Afrikaans	f	en:Afrikaans language	af		ltr	Afrikaans
5	Akan	f	en:Akan language	ak		ltr	Akana
6	Alemannic	f	en:Alemannic language	als	en:ISO 639-3: gsw (als is en:Tosk Albanian)	ltr	Alemannisch
7	Amharic	f	en:Amharic language	am		ltr	አማርኛ
8	Aragonese	f	en:Aragonese language	an		ltr	Aragonés
9	Angal	f	en:Angal language	ang		ltr	Angal Heneng
10	Anglo-Saxon / Old English	f	en:Anglo-Saxon language	ang		ltr	Englisc
11	Arabic	f	en:Arabic language	ar		rtl	العربية
12	Aramaic	f	en:Assyrian Neo-Aramaic language	arc		rtl	ܣܘܪܬ
13	Assamese	f	en:Assamese language	as		ltr	অসমীয়া
14	Asturian	f	en:Asturian language	ast		ltr	Asturianu
15	Avar	f	en:Avar language	av		ltr	Авар
16	Awadhi	f	en:Awadhi language	awa		ltr	Awadhi
17	Aymara	f	en:Aymara language	ay		ltr	Aymar
18	Azerbaijani	f	en:Azerbaijani language	az		ltr	Azərbaycanca / آذربايجان
19	Bashkir	f	en:Bashkir language	ba		ltr	Башҡорт
20	Bavarian	f	en:Bavarian language	bar		ltr	Boarisch
21	Samogitian	f	en:Samogitian language	bat-smg	ISO 639-3: sgs	ltr	Žemaitėška
22	Bikol	f	en:Bikol language	bcl	ISO: 639-2:bik (also known as Central Bicolano)	ltr	Bikol Central
23	Belarusian	f	en:Belarusian language	be		ltr	Беларуская
24	Belarusian (Taraškievica)	f	en:Taraškievica	be-x-old		ltr	Беларуская (тарашкевіца)
25	Bulgarian	f	bg:Български език	bg		ltr	Български
26	Bihari	f	en:Bihari language	bh		ltr	भोजपुरी
27	Bislama	f	en:Bislama language	bi		ltr	Bislama
28	Bambara	f	en:Bambara language	bm		ltr	Bamanankan
29	Bengali	f	en:Bengali language	bn		ltr	বাংলা
30	Tibetan	f	en:Tibetan language	bo		ltr	བོད་ཡིག / Bod skad
31	Bishnupriya Manipuri	f	en:Bishnupriya Manipuri language	bpy		ltr	ইমার ঠার/বিষ্ণুপ্রিয়া মণিপুরী
32	Breton	f	en:Breton language	br		ltr	Brezhoneg
33	Bosnian	f	en:Bosnian language	bs		ltr	Bosanski
34	Buginese	f	en:Buginese language	bug		ltr	ᨅᨔ ᨕᨘᨁᨗ / Basa Ugi
35	Buriat (Russia)	f	en:Buryat language	bxr		ltr	Буряад хэлэн
36	Catalan	f	ca:Català	ca		ltr	Català
37	Min Dong Chinese	f	en:Min Dong language	cdo		ltr	Mìng-dĕ̤ng-ngṳ̄ / 閩東語
38	Chechen	f	en:Chechen language	ce		ltr	Нохчийн
39	Cebuano	f	en:Cebuano language	ceb		ltr	Sinugboanong Binisaya
40	Chamorro	f	en:Chamorro language	ch		ltr	Chamoru
41	Choctaw	f	en:Choctaw language	cho		ltr	Choctaw
42	Cherokee	f	en:Cherokee language	chr		ltr	ᏣᎳᎩ
43	Cheyenne	f	en:Cheyenne language	chy		ltr	Tsetsêhestâhese
44	Kurdish (Sorani)	f	en:Sorani	ckb		rtl	کوردی
45	Corsican	f	en:Corsican language	co		ltr	Corsu
46	Cree	f	en:Cree language	cr		ltr	Nehiyaw
47	Czech	f	cs:Čeština	cs		ltr	Česky
48	Kashubian	f	en:Kashubian language	csb		ltr	Kaszëbsczi
49	Old Church Slavonic / Old Bulgarian	f	en:Old Church Slavonic	cu		ltr	словѣньскъ / slověnĭskŭ
50	Chuvash	f	en:Chuvash language	cv		ltr	Чăваш
51	Welsh	f	en:Welsh language	cy		ltr	Cymraeg
52	Danish	f	da:Dansk (sprog)	da		ltr	Dansk
53	German	f	de:Deutsche Sprache	de		ltr	Deutsch
54	Dimli	f	diq:Zazaki	diq		ltr	Zazaki
55	Lower Sorbian	f	en:Lower Sorbian language	dsb		ltr	Dolnoserbski
56	Divehi	f	en:Divehi language	dv		rtl	ދިވެހިބަސް
57	Dzongkha	f	en:Dzongkha language	dz		ltr	ཇོང་ཁ
58	Ewe	f	en:Ewe language	ee		ltr	Ɛʋɛ
59	Greek	f	el:Ελληνική γλώσσα	el		ltr	Ελληνικά
60	English	f	en:English language	en		ltr	English
61	Esperanto	f	eo:Esperanto	eo		ltr	Esperanto
62	Spanish	f	es:Idioma español	es		ltr	Español
63	Estonian	f	en:Estonian language	et		ltr	Eesti
64	Basque	f	en:Basque language	eu		ltr	Euskara
65	Extremaduran	f	en:Extremaduran language	ext		ltr	Estremeñu
66	Persian	f	en:Persian language	fa		rtl	فارسی
67	Peul	f	en:Peul language	ff		ltr	Fulfulde
68	Finnish	f	fi:Suomen kieli	fi		ltr	Suomi
69	Võro	f	en:Võro language	fiu-vro	ISO 639-3: vro	ltr	Võro
70	Fijian	f	en:Fijian language	fj		ltr	Na Vosa Vakaviti
71	Faroese	f	en:Faroese language	fo		ltr	Føroyskt
72	French	f	fr:Français	fr		ltr	Français
73	Arpitan / Franco-Provençal	f	en:Franco-Provençal/Arpitan language	frp		ltr	Arpitan / francoprovençal
74	Friulian	f	en:Friulian language	fur		ltr	Furlan
75	West Frisian	f	en:West Frisian language	fy		ltr	Frysk
76	Irish	f	en:Irish Gaelic language	ga		ltr	Gaeilge
77	Gan Chinese	f	en:Gan language	gan		ltr	贛語
78	Garhwali	f	en:Garhwali language	gbm		ltr	गढ़वळी
79	Scottish Gaelic	f	en:Scottish Gaelic language	gd		ltr	Gàidhlig
80	Gilbertese	f	en:Gilbertese language	gil		ltr	Taetae ni kiribati
81	Galician	f	en:Galician language	gl		ltr	Galego
82	Guarani	f	en:Guarani language	gn		ltr	Avañe'ẽ
83	Gothic	f	en:Gothic language	got		ltr	gutisk
84	Gujarati	f	en:Gujarati language	gu		ltr	ગુજરાતી
85	Manx	f	en:Manx language	gv		ltr	Gaelg
86	Hausa	f	en:Hausa language	ha		rtl	هَوُسَ
87	Hakka Chinese	f	Hakka language	hak		ltr	客家語/Hak-kâ-ngî
88	Hawaiian	f	en:Hawaiian language	haw		ltr	Hawai`i
89	Hebrew	f	he:עברית	he		rtl	עברית
90	Hindi	f	en:Hindi language	hi		ltr	हिन्दी
91	Hiri Motu	f	en:Hiri Motu language	ho		ltr	Hiri Motu
92	Croatian	f	en:Croatian language	hr		ltr	Hrvatski
93	Haitian	f	en:Haitian language	ht		ltr	Krèyol ayisyen
94	Hungarian	f	hu:Magyar nyelv	hu		ltr	Magyar
95	Armenian	f	en:Armenian language	hy		ltr	Հայերեն
96	Herero	f	en:Herero language	hz		ltr	Otsiherero
97	Interlingua	f	en:Interlingua language	ia		ltr	Interlingua
98	Indonesian	f	id:Bahasa Indonesia	id		ltr	Bahasa Indonesia
99	Interlingue	f	en:Interlingue language	ie		ltr	Interlingue
100	Igbo	f	en:Igbo language	ig		ltr	Igbo
101	Sichuan Yi	f	en:Sichuan Yi language	ii		ltr	ꆇꉙ / 四川彝语
102	Inupiak	f	en:Inupiak language	ik		ltr	Iñupiak
103	Ilokano	f	en:Ilokano language	ilo		ltr	Ilokano
104	Ingush	f	en:Ingush language	inh		ltr	ГӀалгӀай
105	Ido	f	en:Ido language	io		ltr	Ido
106	Icelandic	f	en:Icelandic language	is		ltr	Íslenska
107	Italian	f	it:Lingua italiana	it		ltr	Italiano
108	Inuktitut	f	en:Inuktitut language	iu		ltr	ᐃᓄᒃᑎᑐᑦ
109	Japanese	f	ja:日本語	ja		ltr	日本語
110	Lojban	f	en:Lojban language	jbo		ltr	Lojban
111	Javanese	f	en:Javanese language	jv		ltr	Basa Jawa
112	Georgian	f	en:Georgian language	ka		ltr	ქართული
113	Kongo	f	en:Kongo language	kg		ltr	KiKongo
114	Kikuyu	f	en:Kikuyu language	ki		ltr	Gĩkũyũ
115	Kuanyama	f	en:Kuanyama language	kj		ltr	Kuanyama
116	Kazakh	f	en:Kazakh language	kk		ltr	Қазақша
117	Greenlandic	f	en:Greenlandic language	kl		ltr	Kalaallisut
118	Cambodian	f	en:Khmer language	km		ltr	ភាសាខ្មែរ
119	Kannada	f	en:Kannada language	kn		ltr	ಕನ್ನಡ
120	Khowar	f	en:Khowar language	khw		rtl	کھوار
121	Korean	f	en:Korean language	ko		ltr	한국어
122	Kanuri	f	en:Kanuri language	kr		ltr	Kanuri
123	Kashmiri	f	en:Kashmiri language	ks		rtl	कश्मीरी / كشميري
124	Ripuarian	f	en:Ripuarian language	ksh		ltr	Ripoarisch
125	Kurdish (Kurmanji)	f	en:Kurmanji	ku		ltr	Kurdî
126	Komi	f	en:Komi language	kv		ltr	Коми
127	Cornish	f	en:Cornish language	kw		ltr	Kernewek
128	Kirghiz	f	en:Kirghiz language	ky		ltr	Kırgızca / Кыргызча
129	Latin	f	en:Latin language	la		ltr	Latina
130	Ladino / Judeo-Spanish	f	en:Ladino language	lad		ltr	Dzhudezmo / Djudeo-Espanyol
131	Lango	f	en:Lango	lan		ltr	Leb Lango / Luo
132	Luxembourgish	f	en:Luxembourgish language	lb		ltr	Lëtzebuergesch
133	Ganda	f	en:Luganda language	lg		ltr	Luganda
134	Limburgian	f	en:Limburgian language	li		ltr	Limburgs
135	Ligurian	f	en:Ligurian language	lij		ltr	Líguru
136	Lombard	f	en:Lombard language	lmo		ltr	Lumbaart
137	Lingala	f	en:Lingala language	ln		ltr	Lingála
138	Laotian	f	en:Lao language	lo		ltr	ລາວ / Pha xa lao
139	Laz	f	Laz language	lzz		ltr	Lazuri / ლაზური
140	Lithuanian	f	en:Lithuanian language	lt		ltr	Lietuvių
141	Latvian	f	en:Latvian language	lv		ltr	Latviešu
142	Banyumasan	f	en:Banyumasan language	map-bms	!no ISO 639 language code	ltr	Basa Banyumasan
143	Malagasy	f	en:Malagasy language	mg		ltr	Malagasy
144	Mandarin	f	en:Mandarin language	man		ltr	官話/官话
145	Marshallese	f	en:Marshallese language	mh		ltr	Kajin Majel / Ebon
146	Maori	f	en:Maori language	mi		ltr	Māori
147	Minangkabau	f	en:Minangkabau language	min		ltr	Minangkabau
148	Macedonian	f	en:Macedonian language	mk		ltr	Македонски
149	Malayalam	f	en:Malayalam language	ml		ltr	മലയാളം
150	Mongolian	f	en:Mongolian language	mn		ltr	Монгол
151	Moldovan	f	en:Moldovan language	mo		ltr	Moldovenească
152	Marathi	f	en:Marathi language	mr		ltr	मराठी
153	Mara	f	en:Mara language	mrh		ltr	Mara
154	Malay	f	en:Malay language	ms		ltr	Bahasa Melayu
155	Maltese	f	en:Maltese language	mt		ltr	bil-Malti
156	Creek / Muskogee	f	en:Muscogee language	mus		ltr	Mvskoke
157	Mirandese	f	mwl:Lhéngua mirandesa	mwl	ISO 639-3: mwl	ltr	Mirandés
158	Burmese	f	en:Burmese language	my		ltr	Myanmasa
159	Nauruan	f	en:Nauruan language	na		ltr	Dorerin Naoero
160	Nahuatl	f	en:Nahuatl language	nah		ltr	Nahuatl
161	Neapolitan	f	en:Neapolitan language	nap		ltr	Nnapulitano
162	North Ndebele	f	en:Northern Ndebele language	nd		ltr	Sindebele
163	Low German / Low Saxon	f	en:Low Saxon language	nds		ltr	Plattdüütsch
164	Dutch Low Saxon	f	en:Dutch Low Saxon language	nds-nl	no code for that language, but several codes for constituent languages	ltr	Nedersaksisch
165	Nepali	f	en:Nepali language	ne		ltr	नेपाली
249	West Flemish	f	en:West Flemish language	vls		ltr	West-Vlaoms
166	Newar	f	en:Nepal Bhasa language	new		ltr	नेपालभाषा / Newah Bhaye
167	Ndonga	f	en:Ndonga language	ng		ltr	Oshiwambo
168	Dutch	f	nl:Nederlands	nl		ltr	Nederlands
169	Norwegian Nynorsk	f	en:Nynorsk language	nn		ltr	Norsk (nynorsk)
170	Norwegian	f	no:Norsk	no		ltr	Norsk (bokmål / riksmål)
171	South Ndebele	f	en:Southern Ndebele language	nr		ltr	isiNdebele
172	Northern Sotho	f	en:Northern Sotho language	nso		ltr	Sesotho sa Leboa / Sepedi
173	Norman	f	en:Norman language	nrm	Non-ISO 639 code (nrm is a language in East Malaysia)	ltr	Nouormand / Normaund
174	Navajo	f	en:Navajo language	nv		ltr	Diné bizaad
175	Chichewa	f	en:Chichewa language	ny		ltr	Chi-Chewa
176	Occitan	f	en:Occitan language	oc		ltr	Occitan
177	Ojibwa	f	en:Ojibwe language	oj		ltr	ᐊᓂᔑᓈᐯᒧᐎᓐ / Anishinaabemowin
178	Oromo	f	en:Oromo language	om		ltr	Oromoo
179	Oriya	f	en:Oriya language	or		ltr	ଓଡ଼ିଆ
180	Ossetian / Ossetic	f	en:Ossetian language	os		ltr	Иронау
181	Panjabi / Punjabi	f	en:Punjabi language	pa		ltr	ਪੰਜਾਬੀ / पंजाबी / پنجابي
182	Pangasinan	f	en:Pangasinan language	pag		ltr	Pangasinan
183	Kapampangan	f	en:Kapampangan language	pam		ltr	Kapampangan
184	Papiamentu	f	en:Papiamentu language	pap		ltr	Papiamentu
185	Pennsylvania German	f	en:Pennsylvania German language	pdc		ltr	Deitsch
186	Pali	f	en:Pali language	pi		ltr	Pāli / पाऴि
187	Norfolk	f	en:Norfolk language	pih		ltr	Norfuk
188	Polish	f	pl:Język polski	pl		ltr	Polski
189	Piedmontese	f	en:Piedmontese language	pms		ltr	Piemontèis
190	Pashto	f	en:Pashto language	ps		rtl	پښتو
191	Portuguese	f	pt:Língua portuguesa	pt		ltr	Português
192	Quechua	f	en:Quechua language	qu		ltr	Runa Simi
193	Raeto Romance	f	en:Romansh language	rm		ltr	Rumantsch
194	Romani	f	en:Romani language	rmy		ltr	Romani / रोमानी
195	Kirundi	f	en:Kirundi language	rn		ltr	Kirundi
196	Romanian	f	ro:Limba română	ro		ltr	Română
197	Aromanian	f	en:Aromanian language	roa-rup	ISO 639-2: rup	ltr	Armâneashti
198	Russian	f	ru:Русский язык	ru		ltr	Русский
199	Rwandi	f	en:Kinyarwanda language	rw		ltr	Kinyarwandi
200	Sanskrit	f	en:Sanskrit language	sa		ltr	संस्कृतम्
201	Sardinian	f	en:Sardinian language	sc		ltr	Sardu
202	Sicilian	f	en:Sicilian language	scn		ltr	Sicilianu
203	Scots	f	en:Scots language	sco		ltr	Scots
204	Sindhi	f	en:Sindhi language	sd		ltr	सिनधि
205	Northern Sami	f	en:Northern Sami language	se		ltr	Davvisámegiella
206	Sango	f	en:Sango language	sg		ltr	Sängö
207	Serbo-Croatian	f	en:Serbo-Croatian language	sh	deprecated	ltr	Srpskohrvatski / Српскохрватски
208	Sinhalese	f	en:Sinhalese language	si		ltr	සිංහල
209	Simple English	f	en:Simple English language	simple		ltr	Simple English
210	Slovak	f	sk:Slovenčina	sk		ltr	Slovenčina
211	Slovenian	f	sl:Slovenščina	sl		ltr	Slovenščina
212	Samoan	f	en:Samoan language	sm		ltr	Gagana Samoa
213	Shona	f	en:Shona language	sn		ltr	chiShona
214	Somalia	f	en:Somali language	so		ltr	Soomaaliga
215	Albanian	f	en:Albanian language	sq		ltr	Shqip
216	Serbian	f	sr:Српски језик	sr		ltr	Српски
217	Swati	f	en:Swati language	ss		ltr	SiSwati
218	Southern Sotho	f	en:Sesotho language	st		ltr	Sesotho
219	Sundanese	f	en:Sundanese language	su		ltr	Basa Sunda
220	Swedish	f	sv:Svenska	sv		ltr	Svenska
221	Swahili	f	en:Swahili language	sw		ltr	Kiswahili
222	Tamil	f	en:Tamil language	ta		ltr	தமிழ்
223	Telugu	f	en:Telugu language	te		ltr	తెలుగు
224	Tetum	f	en:Tetum language	tet		ltr	Tetun
225	Tajik	f	en:Tajik language	tg		ltr	Тоҷикӣ
226	Thai	f	en:Thai language	th		ltr	ไทย / Phasa Thai
227	Tigrinya	f	en:Tigrinya language	ti		ltr	ትግርኛ
228	Turkmen	f	en:Turkmen language	tk		ltr	Туркмен / تركمن
229	Tagalog	f	en:Tagalog language	tl		ltr	Tagalog
230	Klingon	f	en:Klingon language	tlh		ltr	tlhIngan-Hol
231	Tswana	f	en:Tswana language	tn		ltr	Setswana
232	Tonga	f	en:Tongan language	to		ltr	Lea Faka-Tonga
233	Tok Pisin	f	en:Tok Pisin language	tpi		ltr	Tok Pisin
234	Turkish	f	en:Turkish language	tr		ltr	Türkçe
235	Tsonga	f	en:Tsonga language	ts		ltr	Xitsonga
236	Tatar	f	en:Tatar language	tt		ltr	Tatarça
237	Tumbuka	f	en:Tumbuka language	tum		ltr	chiTumbuka
238	Twi	f	en:Twi language	tw		ltr	Twi
239	Tahitian	f	en:Tahitian language	ty		ltr	Reo Mā`ohi
240	Udmurt	f	en:Udmurt language	udm		ltr	Удмурт кыл
241	Uyghur	f	en:Uyghur language	ug		ltr	Uyƣurqə / ئۇيغۇرچە
242	Ukrainian	f	en:Ukrainian language	uk		ltr	Українська
243	Urdu	f	en:Urdu language	ur		rtl	اردو
244	Uzbek	f	en:Uzbek language	uz		ltr	Ўзбек
245	Uzbeki Afghanistan	f	en:Uzbek Afghan language	uz_AF		rtl	اوزبیکی
246	Venda	f	en:Venda language	ve		ltr	Tshivenḓa
247	Vietnamese	f	en:Vietnamese language	vi		ltr	Việtnam
248	Venetian	f	en:Venetian language	vec		ltr	Vèneto
250	Volapük	f	en:Volapük language	vo		ltr	Volapük
251	Walloon	f	en:Walloon language	wa		ltr	Walon
252	Waray / Samar-Leyte Visayan	f	en:Waray language	war		ltr	Winaray / Binisaya Lineyte-Samarnon
253	Wolof	f	en:Wolof language	wo		ltr	Wollof
254	Kalmyk	f	en:Kalmyk language	xal		ltr	Хальмг
255	Xhosa	f	en:Xhosan language	xh		ltr	isiXhosa
256	Megrelian	f	Megrelian language	xmf		ltr	მარგალური
257	Yiddish	f	en:Yiddish language	yi		rtl	ייִדיש
258	Yoruba	f	en:Yoruba language	yo		ltr	Yorùbá
259	Zhuang	f	en:Zhuang language	za		ltr	Cuengh / Tôô / 壮语
260	Chinese	f	zh:汉语	zh		ltr	中文
261	Classical Chinese	f	zh:文言文	zh-classical	ISO 639-3: lzh	ltr	文言
262	Minnan	f	en:Min Nan language	zh-min-nan	ISO 639-3: nan	ltr	Bân-lâm-gú
263	Cantonese	f	zh-yue:粵語	zh-yue	ISO 639-3: yue	ltr	粵語 / 粤语
264	Zulu	f	en:Zulu language	zu		ltr	isiZulu
265	Traditional Chinese	f	en:Chinese language	closed-zh-tw	Closed	ltr	‪中文(台灣)‬
266	Norwegian Bokmål	f	en:Bokmål	nb	Redirects to no	ltr	Norsk (bokmål)
267	Traditional Chinese	f	en:Chinese language	zh-tw	Redirects to zh	ltr	‪中文(台灣)‬
268	tokipona	f	en:Toki Pona	tokipona	moved to http://tokipona.wikia.com/	ltr	tokipona
\.


--
-- Data for Name: comunicate_member; Type: TABLE DATA; Schema: public; Owner: video
--

COPY public.comunicate_member (id, created, updated, active, room_id, social_account_id) FROM stdin;
348	2024-02-14 17:54:13.344769+00	2024-02-18 05:15:05.13479+00	f	367	3
350	2024-02-15 07:54:56.945222+00	2024-02-15 17:25:53.056795+00	f	367	4
349	2024-02-15 07:50:39.066039+00	2024-02-17 17:14:30.636231+00	t	367	1
\.


--
-- Data for Name: comunicate_room; Type: TABLE DATA; Schema: public; Owner: video
--

COPY public.comunicate_room (id, name, created, updated, language_id, subject_id, active, social_account_id) FROM stdin;
367	fff	2024-02-14 14:31:52.375544+00	2024-02-15 16:54:04.015257+00	1	21	f	1
\.


--
-- Data for Name: comunicate_subject; Type: TABLE DATA; Schema: public; Owner: video
--

COPY public.comunicate_subject (id, name, comment, active) FROM stdin;
21	Accounting		f
22	Afrikaans		f
23	Ancient History		f
24	Anthropology		f
25	Art and Design		f
26	Applied Science		f
27	Arabic		f
28	Archaeology		f
29	Architecture		f
31	Agriculture		f
32	Astronomy		f
33	Astrophysics		f
34	Bengali		f
35	Biblical Hebrew		f
36	Biology		f
37	Business		f
38	Business Studies		f
39	Chemistry		f
40	Citizenship Studies		f
41	Classical Civilisation		f
42	Classical Greek		f
43	Classical Studies		f
44	Communication and Culture		f
45	Computer Science		f
46	Computing		f
47	Criminology		f
48	Critical Thinking		f
49	Civics		f
50	Dance		f
51	Design and Technology		f
52	Design and Textiles		f
53	Digital Media and Design		f
54	Digital Technology		f
55	Divinity		f
56	Drama		f
57	Drama and Theatre		f
58	Dutch		f
59	Economics		f
60	Economics and Business		f
61	Electronics		f
62	Engineering		f
63	English Language		f
64	English Language and Literature		f
65	English Literature		f
66	Environmental Science		f
67	Environmental Studies		f
68	Environmental Technology		f
69	Ethics		f
70	Fashion and Textiles		f
71	Film Studies		f
72	Food Studies		f
73	Food Technology		f
74	French		f
75	Further Mathematics		f
76	General Studies		f
77	Geography		f
78	Geology		f
79	German		f
80	Global Development		f
81	Global Perspectives and Research		f
82	Government and Politics		f
83	Greek		f
84	Gujarati		f
85	Health and Social Care		f
86	Hindi		f
87	Hinduism		f
88	History		f
89	History of Art		f
90	Home Economics		f
91	Human Biology		f
92	Humanities		f
93	ICT		f
94	Information Technology		f
95	International Relations		f
96	Irish		f
97	Islamic Studies		f
98	Italian		f
99	Latin		f
100	Law		f
101	Leisure Studies		f
102	Life and Health Sciences		f
103	language		f
104	Marine Science		f
105	Mathematics		f
106	Media Studies		f
107	Modern Hebrew		f
108	Moving Image Arts		f
109	Music 		f
110	Music Technology		f
111	Mythology		f
112	Modern history		f
113	Nutrition and Food Science		f
114	Punjabi		f
115	Performance Studies		f
116	Performing Arts		f
117	Persian		f
118	Philosophy		f
119	Photography		f
120	Physical Education		f
121	Physical Science		f
122	Physics		f
123	Polish		f
124	Politics		f
125	Portuguese		f
126	Product Design		f
127	Professional Business Services		f
128	Psychology		f
129	Pure Mathematics		f
130	Quantitative Methods		f
131	Quantum physics		f
132	Quantum mechanics		f
133	Religious Studies		f
134	reegan		f
135	Science in Society		f
136	Sociology		f
137	Software Systems Development		f
138	Spanish		f
139	Sports Science		f
140	Statistics		f
141	Systems and Control Technology		f
142	Telugu		f
143	Tamil		f
144	Technology and Design		f
145	Thinking Skills		f
146	Travel and Tourism		f
147	Turkish		f
148	Urdu		f
149	Welsh		f
\.


--
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: public; Owner: video
--

COPY public.django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) FROM stdin;
\.


--
-- Data for Name: django_celery_beat_clockedschedule; Type: TABLE DATA; Schema: public; Owner: video
--

COPY public.django_celery_beat_clockedschedule (id, clocked_time) FROM stdin;
\.


--
-- Data for Name: django_celery_beat_crontabschedule; Type: TABLE DATA; Schema: public; Owner: video
--

COPY public.django_celery_beat_crontabschedule (id, minute, hour, day_of_week, day_of_month, month_of_year, timezone) FROM stdin;
1	*/1	*	*	*	*	UTC
2	0	4	*	*	*	UTC
3	0	4	*	*	*	Europe/Moscow
\.


--
-- Data for Name: django_celery_beat_intervalschedule; Type: TABLE DATA; Schema: public; Owner: video
--

COPY public.django_celery_beat_intervalschedule (id, every, period) FROM stdin;
1	1	days
2	1	minutes
3	1	seconds
4	30	minutes
5	8	hours
6	30	seconds
7	30	seconds
8	2	hours
9	3	hours
10	5	minutes
11	2	minutes
12	3	hours
14	1	minutes
15	15	minutes
13	10	minutes
\.


--
-- Data for Name: django_celery_beat_periodictask; Type: TABLE DATA; Schema: public; Owner: video
--

COPY public.django_celery_beat_periodictask (id, name, task, args, kwargs, queue, exchange, routing_key, expires, enabled, last_run_at, total_run_count, date_changed, description, crontab_id, interval_id, solar_id, one_off, start_time, priority, headers, clocked_id, expire_seconds) FROM stdin;
\.


--
-- Data for Name: django_celery_beat_periodictasks; Type: TABLE DATA; Schema: public; Owner: video
--

COPY public.django_celery_beat_periodictasks (ident, last_update) FROM stdin;
\.


--
-- Data for Name: django_celery_beat_solarschedule; Type: TABLE DATA; Schema: public; Owner: video
--

COPY public.django_celery_beat_solarschedule (id, event, latitude, longitude) FROM stdin;
\.


--
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: public; Owner: video
--

COPY public.django_content_type (id, app_label, model) FROM stdin;
1	admin	logentry
2	auth	group
3	auth	permission
4	auth	user
5	contenttypes	contenttype
6	sessions	session
7	place	photo
8	place	place
9	easy_thumbnails	thumbnaildimensions
10	easy_thumbnails	thumbnail
11	easy_thumbnails	source
12	article	article
13	book	book
14	organisations	organisation
15	sites	site
16	chunks	chunk
17	chunks	group
18	chunks	image
19	chunks	media
20	djangoseo	mymetadatapath
21	djangoseo	mymetadatamodelinstance
22	djangoseo	mymetadatamodel
23	djangoseo	mymetadataview
24	enumeration	enumeration
25	organisations	photo
26	base	countparse
27	base	parsing
28	base	typepage
29	django_celery_beat	solarschedule
30	django_celery_beat	clockedschedule
31	django_celery_beat	periodictasks
32	django_celery_beat	periodictask
33	django_celery_beat	intervalschedule
34	django_celery_beat	crontabschedule
35	main	seo
36	comunicate	language
37	comunicate	subject
38	comunicate	chat
39	comunicate	room
40	comunicate	member
41	lesson	lesson
42	lesson	article
43	account	emailaddress
44	account	emailconfirmation
45	socialaccount	socialaccount
46	socialaccount	socialapp
47	socialaccount	socialtoken
48	authtoken	token
49	authtoken	tokenproxy
50	oauth2_provider	application
51	oauth2_provider	accesstoken
52	oauth2_provider	grant
53	oauth2_provider	refreshtoken
54	oauth2_provider	idtoken
55	social_django	association
56	social_django	code
57	social_django	nonce
58	social_django	usersocialauth
59	social_django	partial
60	video_chat	token
61	video_chat	userprofile
62	bot	tguser
63	config	token
64	config	userprofile
\.


--
-- Data for Name: django_migrations; Type: TABLE DATA; Schema: public; Owner: video
--

COPY public.django_migrations (id, app, name, applied) FROM stdin;
1	contenttypes	0001_initial	2016-10-12 09:09:18.54378+00
2	auth	0001_initial	2016-10-12 09:09:18.819117+00
3	admin	0001_initial	2016-10-12 09:09:18.896939+00
4	admin	0002_logentry_remove_auto_add	2016-10-12 09:09:18.910465+00
5	contenttypes	0002_remove_content_type_name	2016-10-12 09:09:18.941336+00
6	auth	0002_alter_permission_name_max_length	2016-10-12 09:09:18.967893+00
7	auth	0003_alter_user_email_max_length	2016-10-12 09:09:18.978184+00
8	auth	0004_alter_user_username_opts	2016-10-12 09:09:18.987866+00
9	auth	0005_alter_user_last_login_null	2016-10-12 09:09:18.997873+00
10	auth	0006_require_contenttypes_0002	2016-10-12 09:09:18.999223+00
11	auth	0007_alter_validators_add_error_messages	2016-10-12 09:09:19.00883+00
12	auth	0008_alter_user_username_max_length	2016-10-12 09:09:19.032744+00
13	place	0001_initial	2016-10-12 09:09:19.11807+00
14	place	0002_auto_20160831_1220	2016-10-12 09:09:19.130854+00
15	place	0003_auto_20160915_1412	2016-10-12 09:09:19.414761+00
16	sessions	0001_initial	2016-10-12 09:09:19.472332+00
17	article	0001_initial	2016-11-09 14:24:54.118895+00
18	easy_thumbnails	0001_initial	2016-11-09 14:24:54.260891+00
19	easy_thumbnails	0002_thumbnaildimensions	2016-11-09 14:24:54.291142+00
20	place	0004_auto_20161014_1136	2016-11-09 14:24:54.332452+00
21	place	0005_photo_article	2016-11-09 14:24:54.36024+00
22	book	0001_initial	2016-11-10 16:22:58.708882+00
23	organisations	0001_initial	2016-11-26 15:39:23.570213+00
24	organisations	0002_auto_20161128_0742	2016-11-28 09:45:04.521359+00
25	article	0002_auto_20161223_1511	2016-12-23 18:03:29.396145+00
26	chunks	0001_initial	2016-12-23 18:03:29.643383+00
27	place	0006_auto_20161223_1605	2016-12-23 18:03:29.705525+00
28	sites	0001_initial	2016-12-23 18:03:29.719886+00
29	sites	0002_alter_domain_unique	2016-12-23 18:03:29.735499+00
30	djangoseo	0001_initial	2016-12-23 18:37:08.039702+00
31	book	0002_auto_20170117_0540	2017-01-17 05:46:54.7104+00
32	book	0003_auto_20170208_1243	2017-02-09 18:07:47.706317+00
33	enumeration	0001_initial	2017-02-09 18:07:48.085339+00
34	organisations	0003_organisation_place	2017-02-09 18:07:48.131673+00
35	organisations	0004_photo	2017-02-13 15:41:44.335585+00
36	organisations	0005_auto_20170311_2253	2017-03-12 11:37:34.494209+00
37	enumeration	0002_enumeration_main	2017-03-14 09:18:00.126224+00
38	place	0007_place_user	2017-03-14 09:18:00.17888+00
39	enumeration	0003_enumeration_menu_level	2017-04-12 18:57:33.217211+00
40	organisations	0006_organisation_menu_level	2017-04-12 18:57:33.493572+00
41	place	0008_photo_photo_type	2017-04-12 18:57:33.622736+00
42	place	0009_auto_20170507_1603	2017-05-09 11:42:05.134158+00
43	place	0010_auto_20170509_1108	2017-05-09 11:42:05.174572+00
44	place	0011_auto_20170509_1137	2017-05-09 11:42:05.208386+00
45	place	0012_auto_20170609_1038	2017-06-09 11:08:43.908729+00
46	place	0013_auto_20170622_1248	2017-06-22 14:04:57.435469+00
47	article	0003_auto_20180508_1400	2018-05-08 14:38:09.373358+00
48	organisations	0007_auto_20180508_1400	2018-05-08 14:38:09.487493+00
49	article	0004_auto_20181024_0617	2018-11-04 17:30:03.032928+00
50	article	0005_article_site_sighn	2018-11-04 17:30:03.612084+00
51	base	0001_initial	2018-11-04 17:30:03.811363+00
52	base	0002_auto_20181024_0726	2018-11-04 17:30:03.900748+00
53	article	0006_auto_20181105_1812	2018-11-05 18:23:14.928439+00
54	base	0003_auto_20181105_1812	2018-11-05 18:23:15.141072+00
55	article	0007_article_chronology_date	2019-03-05 17:08:12.669512+00
56	base	0004_auto_20190304_1939	2019-03-05 17:08:12.885453+00
57	base	0005_auto_20190304_1944	2019-03-05 17:08:12.917217+00
58	base	0006_auto_20190304_1956	2019-03-05 17:08:12.951716+00
59	django_celery_beat	0001_initial	2019-07-12 10:07:20.45523+00
60	django_celery_beat	0002_auto_20161118_0346	2019-07-12 10:07:20.555797+00
61	django_celery_beat	0003_auto_20161209_0049	2019-07-12 10:07:20.599982+00
62	django_celery_beat	0004_auto_20170221_0000	2019-07-12 10:07:20.614758+00
63	django_celery_beat	0005_add_solarschedule_events_choices	2019-07-12 10:07:20.690355+00
64	django_celery_beat	0006_auto_20180322_0932	2019-07-12 10:07:20.819712+00
65	django_celery_beat	0007_auto_20180521_0826	2019-07-12 10:07:20.934237+00
66	django_celery_beat	0008_auto_20180914_1922	2019-07-12 10:07:20.981718+00
67	django_celery_beat	0006_auto_20180210_1226	2019-07-12 10:07:21.014828+00
68	django_celery_beat	0006_periodictask_priority	2019-07-12 10:07:21.032291+00
69	django_celery_beat	0009_periodictask_headers	2019-07-12 10:07:21.103779+00
70	django_celery_beat	0010_auto_20190429_0326	2019-07-12 10:07:21.604264+00
71	django_celery_beat	0011_auto_20190508_0153	2019-07-12 10:07:21.653901+00
72	admin	0003_logentry_add_action_flag_choices	2021-05-10 20:42:35.356026+00
73	main	0001_initial	2021-05-10 20:42:35.982488+00
74	article	0008_auto_20210423_1751	2021-05-10 20:42:36.356783+00
75	article	0009_auto_20210423_2031	2021-05-10 20:42:36.50533+00
76	auth	0009_alter_user_last_name_max_length	2021-05-10 20:42:36.533349+00
77	auth	0010_alter_group_name_max_length	2021-05-10 20:42:36.555132+00
78	auth	0011_update_proxy_permissions	2021-05-10 20:42:36.597778+00
79	base	0007_auto_20210423_1751	2021-05-10 20:42:36.769232+00
80	place	0014_auto_20210423_1751	2021-05-10 20:42:37.07286+00
81	place	0015_auto_20210423_2031	2021-05-10 20:42:37.105792+00
82	base	0008_auto_20210423_2031	2021-05-10 20:42:37.170638+00
83	base	0009_auto_20210427_2044	2021-05-10 20:42:37.191508+00
84	book	0004_auto_20210423_1751	2021-05-10 20:42:37.342616+00
85	book	0005_book_seo	2021-05-10 20:42:37.47892+00
86	django_celery_beat	0012_periodictask_expire_seconds	2021-05-10 20:42:37.496735+00
87	django_celery_beat	0013_auto_20200609_0727	2021-05-10 20:42:37.505389+00
88	django_celery_beat	0014_remove_clockedschedule_enabled	2021-05-10 20:42:37.512595+00
89	django_celery_beat	0015_edit_solarschedule_events_choices	2021-05-10 20:42:37.51975+00
90	enumeration	0004_auto_20210423_1751	2021-05-10 20:42:37.69799+00
91	enumeration	0005_auto_20210427_2044	2021-05-10 20:42:37.786108+00
92	organisations	0008_auto_20210423_1751	2021-05-10 20:42:38.038686+00
93	organisations	0009_organisation_seo	2021-05-10 20:42:38.077881+00
94	comunicate	0001_initial	2021-05-26 11:38:14.465269+00
95	comunicate	0002_auto_20210430_1121	2021-05-26 11:38:14.688116+00
96	comunicate	0003_auto_20210430_1136	2021-05-26 11:38:14.695199+00
97	comunicate	0004_subject	2021-05-26 11:38:14.731298+00
98	comunicate	0005_auto_20210430_1453	2021-05-26 11:38:14.764263+00
99	comunicate	0006_chat_room	2021-08-21 20:24:38.501483+00
100	comunicate	0007_auto_20210901_2112	2021-09-01 21:12:45.789436+00
101	comunicate	0008_auto_20210907_2001	2021-09-07 20:01:25.816398+00
102	comunicate	0009_auto_20210907_2018	2021-09-07 20:18:37.437188+00
103	comunicate	0010_auto_20210907_2023	2021-09-07 20:23:30.505905+00
104	comunicate	0011_remove_chat_name	2021-09-07 21:54:07.893114+00
105	comunicate	0012_auto_20210913_1936	2021-09-13 19:36:58.444363+00
106	comunicate	0013_chat_shared_room	2021-09-13 19:42:19.030297+00
107	comunicate	0014_auto_20210913_1942	2021-09-13 19:42:19.087478+00
108	comunicate	0015_member	2021-10-16 17:39:40.729284+00
109	comunicate	0016_auto_20211027_2111	2021-10-27 21:12:19.613826+00
110	lesson	0001_initial	2021-11-07 10:35:24.633826+00
111	lesson	0002_article_lesson	2021-11-07 10:40:59.083952+00
112	lesson	0003_auto_20211107_1450	2021-11-07 14:50:58.566264+00
113	lesson	0004_auto_20211125_1921	2021-11-25 19:22:07.521464+00
114	account	0001_initial	2022-04-15 19:20:08.72205+00
115	account	0002_email_max_length	2022-04-15 19:20:08.966093+00
116	authtoken	0001_initial	2022-04-15 19:20:09.004856+00
117	authtoken	0002_auto_20160226_1747	2022-04-15 19:20:09.250803+00
118	authtoken	0003_tokenproxy	2022-04-15 19:20:09.256261+00
119	socialaccount	0001_initial	2022-04-15 19:20:09.408221+00
120	socialaccount	0002_token_max_lengths	2022-04-15 19:20:09.521004+00
121	socialaccount	0003_extra_data_default_dict	2022-04-15 19:20:09.53286+00
122	oauth2_provider	0001_initial	2022-11-19 16:12:31.892951+00
123	oauth2_provider	0002_auto_20190406_1805	2022-11-19 16:12:32.296522+00
124	oauth2_provider	0003_auto_20201211_1314	2022-11-19 16:12:32.323431+00
125	oauth2_provider	0004_auto_20200902_2022	2022-11-19 16:12:32.713761+00
126	oauth2_provider	0005_auto_20211222_2352	2022-11-19 16:12:33.11784+00
127	oauth2_provider	0006_alter_application_client_secret	2022-11-19 16:12:33.178856+00
128	default	0001_initial	2022-11-19 16:12:33.348538+00
129	social_auth	0001_initial	2022-11-19 16:12:33.350261+00
130	default	0002_add_related_name	2022-11-19 16:12:33.41863+00
131	social_auth	0002_add_related_name	2022-11-19 16:12:33.420209+00
132	default	0003_alter_email_max_length	2022-11-19 16:12:33.427394+00
133	social_auth	0003_alter_email_max_length	2022-11-19 16:12:33.42902+00
134	default	0004_auto_20160423_0400	2022-11-19 16:12:33.454057+00
135	social_auth	0004_auto_20160423_0400	2022-11-19 16:12:33.455665+00
136	social_auth	0005_auto_20160727_2333	2022-11-19 16:12:33.470704+00
137	social_django	0006_partial	2022-11-19 16:12:33.502871+00
138	social_django	0007_code_timestamp	2022-11-19 16:12:33.589611+00
139	social_django	0008_partial_timestamp	2022-11-19 16:12:33.652678+00
140	social_django	0009_auto_20191118_0520	2022-11-19 16:12:33.817676+00
141	social_django	0010_uid_db_index	2022-11-19 16:12:33.861296+00
142	social_django	0005_auto_20160727_2333	2022-11-19 16:12:33.866778+00
143	social_django	0002_add_related_name	2022-11-19 16:12:33.868333+00
144	social_django	0004_auto_20160423_0400	2022-11-19 16:12:33.870002+00
145	social_django	0001_initial	2022-11-19 16:12:33.875732+00
146	social_django	0003_alter_email_max_length	2022-11-19 16:12:33.877456+00
147	video_chat	0001_initial	2022-12-15 13:43:50.816618+00
148	video_chat	0002_auto_20221215_1352	2022-12-15 13:52:18.931697+00
149	video_chat	0003_userprofile	2023-01-03 15:50:43.984876+00
150	comunicate	0017_auto_20230108_1727	2023-01-08 17:27:47.24611+00
151	auth	0012_alter_user_first_name_max_length	2023-06-15 18:41:18.61487+00
152	django_celery_beat	0016_alter_crontabschedule_timezone	2023-06-15 18:41:18.624688+00
153	django_celery_beat	0017_alter_crontabschedule_month_of_year	2023-06-15 18:41:18.631006+00
154	django_celery_beat	0018_improve_crontab_helptext	2023-06-15 18:41:18.6404+00
155	base	0010_alter_parsing_created_alter_typepage_created	2023-07-02 13:28:33.757436+00
156	article	0010_article_site_sighn	2023-07-02 16:04:47.322011+00
157	base	0011_alter_parsing_created_alter_typepage_created	2023-07-02 16:04:47.334397+00
158	base	0012_alter_parsing_created_alter_typepage_created	2023-07-04 09:19:19.75583+00
159	article	0011_article_send_to	2023-07-04 09:31:55.351491+00
160	base	0013_alter_parsing_created_alter_typepage_created	2023-07-04 09:31:55.362031+00
161	base	0014_alter_parsing_created_alter_typepage_created	2023-07-04 09:31:55.374696+00
165	base	0015_remove_parsing_news_parsing_html_parsing_site_sighn_and_more	2023-07-25 19:48:13.602011+00
166	base	0016_parsing_parsing_for_choices_alter_parsing_created_and_more	2023-07-27 18:53:42.725374+00
167	bot	0001_initial	2023-10-12 18:39:15.999007+00
168	account	0003_alter_emailaddress_create_unique_verified_email	2023-11-20 17:16:11.521359+00
169	account	0004_alter_emailaddress_drop_unique_email	2023-11-20 17:16:11.54043+00
170	account	0005_emailaddress_idx_upper_email	2023-11-20 17:16:11.553284+00
171	config	0001_initial	2023-11-20 17:16:11.575695+00
172	config	0002_auto_20221215_1352	2023-11-20 17:16:11.585399+00
173	config	0003_userprofile	2023-11-20 17:16:11.616232+00
174	social_django	0011_alter_id_fields	2023-11-20 17:16:11.690397+00
175	social_django	0012_usersocialauth_extra_data_new	2023-11-20 17:16:11.704616+00
176	social_django	0013_migrate_extra_data	2023-11-20 17:16:11.725991+00
177	social_django	0014_remove_usersocialauth_extra_data	2023-11-20 17:16:11.738597+00
178	social_django	0015_rename_extra_data_new_usersocialauth_extra_data	2023-11-20 17:16:11.751298+00
179	socialaccount	0004_app_provider_id_settings	2023-11-20 17:16:11.771305+00
180	socialaccount	0005_socialtoken_nullable_app	2023-11-20 17:16:11.790417+00
181	socialaccount	0006_alter_socialaccount_extra_data	2023-11-20 17:16:11.811706+00
182	comunicate	0018_alter_member_unique_together_remove_room_date_time_and_more	2024-02-03 14:26:26.558244+00
183	config	0004_remove_userprofile_user_delete_token_and_more	2024-02-03 14:26:26.583418+00
184	comunicate	0019_alter_member_user_alter_room_name_alter_room_user	2024-02-14 14:27:14.693524+00
185	comunicate	0020_rename_user_member_social_account_and_more	2024-02-14 14:29:07.151454+00
\.


--
-- Data for Name: django_session; Type: TABLE DATA; Schema: public; Owner: video
--

COPY public.django_session (session_key, session_data, expire_date) FROM stdin;
0x3h78h1dbpnbe8m0a3t8dr2y90xn9lb	.eJxVjDsOwjAQBe_iGllex19K-pzB2l0bHECOFCcV4u4QKQW0b2beSyTc1pq2XpY0ZXEWoMTpdyTkR2k7yXdst1ny3NZlIrkr8qBdjnMuz8vh_h1U7PVbkwUFrLMahqjZsTEUXPRUIGBEQ8UBqQzZ0lUDmujQOO8sBFZWo_Xi_QHzEDdd:1reDTq:p2tXhLFcTlritVh6z5XCm6zwt0NcpMbDO3JQEjIP5NY	2024-03-10 12:19:42.465358+00
\.


--
-- Data for Name: django_site; Type: TABLE DATA; Schema: public; Owner: video
--

COPY public.django_site (id, domain, name) FROM stdin;
1	vokt.ru	vokt.ru
2	video.chat.vokt.ru	video
\.


--
-- Data for Name: djangoseo_mymetadatamodel; Type: TABLE DATA; Schema: public; Owner: video
--

COPY public.djangoseo_mymetadatamodel (id, title, description, keywords, heading, _content_type_id) FROM stdin;
\.


--
-- Data for Name: djangoseo_mymetadatamodelinstance; Type: TABLE DATA; Schema: public; Owner: video
--

COPY public.djangoseo_mymetadatamodelinstance (id, title, description, keywords, heading, _path, _object_id, _content_type_id) FROM stdin;
\.


--
-- Data for Name: djangoseo_mymetadatapath; Type: TABLE DATA; Schema: public; Owner: video
--

COPY public.djangoseo_mymetadatapath (id, title, description, keywords, heading, _path) FROM stdin;
\.


--
-- Data for Name: djangoseo_mymetadataview; Type: TABLE DATA; Schema: public; Owner: video
--

COPY public.djangoseo_mymetadataview (id, title, description, keywords, heading, _view) FROM stdin;
\.


--
-- Data for Name: easy_thumbnails_source; Type: TABLE DATA; Schema: public; Owner: video
--

COPY public.easy_thumbnails_source (id, storage_hash, name, modified) FROM stdin;
\.


--
-- Data for Name: easy_thumbnails_thumbnail; Type: TABLE DATA; Schema: public; Owner: video
--

COPY public.easy_thumbnails_thumbnail (id, storage_hash, name, modified, source_id) FROM stdin;
\.


--
-- Data for Name: easy_thumbnails_thumbnaildimensions; Type: TABLE DATA; Schema: public; Owner: video
--

COPY public.easy_thumbnails_thumbnaildimensions (id, thumbnail_id, width, height) FROM stdin;
\.


--
-- Data for Name: lesson_article; Type: TABLE DATA; Schema: public; Owner: video
--

COPY public.lesson_article (id, title, description, text, answer, active, created, updated, user_id, lesson_id) FROM stdin;
1	Generally, the adjective order in English is	Generally, the adjective order in English is	<p>Generally, the adjective order in English is:</p>\r\n\r\n<ol>\r\n\t<li>Quantity or number.</li>\r\n\t<li>Quality or opinion.</li>\r\n\t<li>Size.</li>\r\n\t<li>Age.</li>\r\n\t<li>Shape.</li>\r\n\t<li>Color.</li>\r\n\t<li>Proper adjective (often nationality, other place of origin, or material)</li>\r\n\t<li>Purpose or qualifier.</li>\r\n</ol>\r\n\r\n<p><span style="color:#a52a2a">Examples</span>:</p>\r\n\r\n<ul>\r\n\t<li>&nbsp;&nbsp;&nbsp;&nbsp;Lovely or beautiful little thin girls</li>\r\n</ul>	gg	t	2021-11-07 14:18:21.046609+00	2021-11-10 21:20:51.724239+00	1	2
3	20 Sentences of Second Conditional	20 Sentences of Second Conditional	<p><img alt="" src="/media/uploads/2021/11/21/20-sentences-of-second-conditional.jpg" style="height:1045px; width:550px" /></p>	\N	t	2021-11-21 06:53:47.388008+00	2021-11-21 06:53:47.388039+00	1	4
4	2nd conditional	2nd conditional	<p><img alt="" src="/media/uploads/2021/11/21/2nd-condictional.jpg" style="height:315px; width:560px" /><img alt="" src="/media/uploads/2021/11/21/2nd-condictional.jpg" style="height:315px; width:560px" /></p>	\N	t	2021-11-21 06:59:09.260042+00	2021-11-21 06:59:09.260073+00	1	4
12	Practice exercise Phrasal verb with GO	Practice exercise	<p><img src="/media/article/11_27_2021_12_15_56.png" alt="" width="1500" height="1500" /></p>\r\n<p>&nbsp;</p>\r\n<p><img src="/admin/media/article/11_27_2021_12_17_48.png" alt="" width="828" height="1474" /></p>	\N	t	2021-11-27 12:18:23.52335+00	2023-01-21 12:34:05.820003+00	1	7
5	Adverbs of place	Adverbs of place	<p><img title="adverbs of place.jpg" src="data:image/jpeg;base64,/9j/4gv4SUNDX1BST0ZJTEUAAQEAAAvoAAAAAAIAAABtbnRyUkdCIFhZWiAH2QADABsAFQAkAB9hY3NwAAAAAAAAAAAAAAAAAAAAAAAAAAEAAAAAAAAAAAAA9tYAAQAAAADTLQAAAAAp+D3er/JVrnhC+uTKgzkNAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABBkZXNjAAABRAAAAHliWFlaAAABwAAAABRiVFJDAAAB1AAACAxkbWRkAAAJ4AAAAIhnWFlaAAAKaAAAABRnVFJDAAAB1AAACAxsdW1pAAAKfAAAABRtZWFzAAAKkAAAACRia3B0AAAKtAAAABRyWFlaAAAKyAAAABRyVFJDAAAB1AAACAx0ZWNoAAAK3AAAAAx2dWVkAAAK6AAAAId3dHB0AAALcAAAABRjcHJ0AAALhAAAADdjaGFkAAALvAAAACxkZXNjAAAAAAAAAB9zUkdCIElFQzYxOTY2LTItMSBibGFjayBzY2FsZWQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAWFlaIAAAAAAAACSgAAAPhAAAts9jdXJ2AAAAAAAABAAAAAAFAAoADwAUABkAHgAjACgALQAyADcAOwBAAEUASgBPAFQAWQBeAGMAaABtAHIAdwB8AIEAhgCLAJAAlQCaAJ8ApACpAK4AsgC3ALwAwQDGAMsA0ADVANsA4ADlAOsA8AD2APsBAQEHAQ0BEwEZAR8BJQErATIBOAE+AUUBTAFSAVkBYAFnAW4BdQF8AYMBiwGSAZoBoQGpAbEBuQHBAckB0QHZAeEB6QHyAfoCAwIMAhQCHQImAi8COAJBAksCVAJdAmcCcQJ6AoQCjgKYAqICrAK2AsECywLVAuAC6wL1AwADCwMWAyEDLQM4A0MDTwNaA2YDcgN+A4oDlgOiA64DugPHA9MD4APsA/kEBgQTBCAELQQ7BEgEVQRjBHEEfgSMBJoEqAS2BMQE0wThBPAE/gUNBRwFKwU6BUkFWAVnBXcFhgWWBaYFtQXFBdUF5QX2BgYGFgYnBjcGSAZZBmoGewaMBp0GrwbABtEG4wb1BwcHGQcrBz0HTwdhB3QHhgeZB6wHvwfSB+UH+AgLCB8IMghGCFoIbgiCCJYIqgi+CNII5wj7CRAJJQk6CU8JZAl5CY8JpAm6Cc8J5Qn7ChEKJwo9ClQKagqBCpgKrgrFCtwK8wsLCyILOQtRC2kLgAuYC7ALyAvhC/kMEgwqDEMMXAx1DI4MpwzADNkM8w0NDSYNQA1aDXQNjg2pDcMN3g34DhMOLg5JDmQOfw6bDrYO0g7uDwkPJQ9BD14Peg+WD7MPzw/sEAkQJhBDEGEQfhCbELkQ1xD1ERMRMRFPEW0RjBGqEckR6BIHEiYSRRJkEoQSoxLDEuMTAxMjE0MTYxODE6QTxRPlFAYUJxRJFGoUixStFM4U8BUSFTQVVhV4FZsVvRXgFgMWJhZJFmwWjxayFtYW+hcdF0EXZReJF64X0hf3GBsYQBhlGIoYrxjVGPoZIBlFGWsZkRm3Gd0aBBoqGlEadxqeGsUa7BsUGzsbYxuKG7Ib2hwCHCocUhx7HKMczBz1HR4dRx1wHZkdwx3sHhYeQB5qHpQevh7pHxMfPh9pH5Qfvx/qIBUgQSBsIJggxCDwIRwhSCF1IaEhziH7IiciVSKCIq8i3SMKIzgjZiOUI8Ij8CQfJE0kfCSrJNolCSU4JWgllyXHJfcmJyZXJocmtyboJxgnSSd6J6sn3CgNKD8ocSiiKNQpBik4KWspnSnQKgIqNSpoKpsqzysCKzYraSudK9EsBSw5LG4soizXLQwtQS12Last4S4WLkwugi63Lu4vJC9aL5Evxy/+MDUwbDCkMNsxEjFKMYIxujHyMioyYzKbMtQzDTNGM38zuDPxNCs0ZTSeNNg1EzVNNYc1wjX9Njc2cjauNuk3JDdgN5w31zgUOFA4jDjIOQU5Qjl/Obw5+To2OnQ6sjrvOy07azuqO+g8JzxlPKQ84z0iPWE9oT3gPiA+YD6gPuA/IT9hP6I/4kAjQGRApkDnQSlBakGsQe5CMEJyQrVC90M6Q31DwEQDREdEikTORRJFVUWaRd5GIkZnRqtG8Ec1R3tHwEgFSEtIkUjXSR1JY0mpSfBKN0p9SsRLDEtTS5pL4kwqTHJMuk0CTUpNk03cTiVObk63TwBPSU+TT91QJ1BxULtRBlFQUZtR5lIxUnxSx1MTU19TqlP2VEJUj1TbVShVdVXCVg9WXFapVvdXRFeSV+BYL1h9WMtZGllpWbhaB1pWWqZa9VtFW5Vb5Vw1XIZc1l0nXXhdyV4aXmxevV8PX2Ffs2AFYFdgqmD8YU9homH1YklinGLwY0Njl2PrZEBklGTpZT1lkmXnZj1mkmboZz1nk2fpaD9olmjsaUNpmmnxakhqn2r3a09rp2v/bFdsr20IbWBtuW4SbmtuxG8eb3hv0XArcIZw4HE6cZVx8HJLcqZzAXNdc7h0FHRwdMx1KHWFdeF2Pnabdvh3VnezeBF4bnjMeSp5iXnnekZ6pXsEe2N7wnwhfIF84X1BfaF+AX5ifsJ/I3+Ef+WAR4CogQqBa4HNgjCCkoL0g1eDuoQdhICE44VHhauGDoZyhteHO4efiASIaYjOiTOJmYn+imSKyoswi5aL/IxjjMqNMY2Yjf+OZo7OjzaPnpAGkG6Q1pE/kaiSEZJ6kuOTTZO2lCCUipT0lV+VyZY0lp+XCpd1l+CYTJi4mSSZkJn8mmia1ZtCm6+cHJyJnPedZJ3SnkCerp8dn4uf+qBpoNihR6G2oiailqMGo3aj5qRWpMelOKWpphqmi6b9p26n4KhSqMSpN6mpqhyqj6sCq3Wr6axcrNCtRK24ri2uoa8Wr4uwALB1sOqxYLHWskuywrM4s660JbSctRO1irYBtnm28Ldot+C4WbjRuUq5wro7urW7LrunvCG8m70VvY++Cr6Evv+/er/1wHDA7MFnwePCX8Lbw1jD1MRRxM7FS8XIxkbGw8dBx7/IPci8yTrJuco4yrfLNsu2zDXMtc01zbXONs62zzfPuNA50LrRPNG+0j/SwdNE08bUSdTL1U7V0dZV1tjXXNfg2GTY6Nls2fHadtr724DcBdyK3RDdlt4c3qLfKd+v4DbgveFE4cziU+Lb42Pj6+Rz5PzlhOYN5pbnH+ep6DLovOlG6dDqW+rl63Dr++yG7RHtnO4o7rTvQO/M8Fjw5fFy8f/yjPMZ86f0NPTC9VD13vZt9vv3ivgZ+Kj5OPnH+lf65/t3/Af8mP0p/br+S/7c/23//2Rlc2MAAAAAAAAALklFQyA2MTk2Ni0yLTEgRGVmYXVsdCBSR0IgQ29sb3VyIFNwYWNlIC0gc1JHQgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABYWVogAAAAAAAAYpkAALeFAAAY2lhZWiAAAAAAAAAAAABQAAAAAAAAbWVhcwAAAAAAAAABAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACWFlaIAAAAAAAAAMWAAADMwAAAqRYWVogAAAAAAAAb6IAADj1AAADkHNpZyAAAAAAQ1JUIGRlc2MAAAAAAAAALVJlZmVyZW5jZSBWaWV3aW5nIENvbmRpdGlvbiBpbiBJRUMgNjE5NjYtMi0xAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABYWVogAAAAAAAA9tYAAQAAAADTLXRleHQAAAAAQ29weXJpZ2h0IEludGVybmF0aW9uYWwgQ29sb3IgQ29uc29ydGl1bSwgMjAwOQAAc2YzMgAAAAAAAQxEAAAF3///8yYAAAeUAAD9j///+6H///2iAAAD2wAAwHX/4AAQSkZJRgABAQAASABIAAD/2wBDAAYGBgYGBgoGBgoPCgoKDxQPDw8PFBkUFBQUFBkeGRkZGRkZHh4eHh4eHh4kJCQkJCQqKioqKi8vLy8vLy8vLy//2wBDAQcICAwLDBULCxUyIhwiMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjL/wgARCAP3ArwDASIAAhEBAxEB/8QAHAABAAIDAQEBAAAAAAAAAAAAAAUGAwQHAgEI/8QAGgEBAAMBAQEAAAAAAAAAAAAAAAECAwQFBv/aAAwDAQACEAMQAAAB6oAA8QeczkVA7vBvmwb2eYhvs98TDbG3rG7JVDyi4omW9DALAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGn5rPJr72pDNlbPmrur6fNNQSWtGhin/UIrbxwsrro5Izn00M83EefvYfdRtfZl7G9AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGHNWcbaNg0rDzaIPTku28NZstR25LJr0O/Si1Z+c3tXjLC0jo8bq0LXL5lOpM1mTytoYbPUuTW3IqV78AsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA1alM+PN3n9bNXe+dKzaOlHRk5tZud9nnZrFS/l+ff1sWel9v11/mG1fd9xx+HfdIjNucmcxoJC9Khb6fZeHbbHfi5t0j82ln7V+WP0KWQiCX4n078+HXeicc6MTrQ3w0d0+qztE4RZKIATHE+wfmk7nduXWktDHkCF0C0seQAPNcLKrVkPnI7hwY6V1Tn/QTY+ffJxLNQbwbMrcvzqfqPi1j5Mdl6Bybq56VnXLc0t0MdZLUgZ4PNYLShpkAq0pBWjzt89UscB2dEpzK98y3wg9a+aFsqr99fduDzvYLLEdOisH3i9/RutY+NNGb0pTfz/Wav3imMHk+4eHawDuxr1B3L2fm7q1AkDtv5w7RxI6Xzf8AQfBC+Uq7+yG7p+Xe4EFEXngxeYvc0DqFB2Ncqs1q9nKlyr9FfnUukN0Stmazcw6CUrsX55sZYuyVe0A1jifqk9UK9kmuXnbeI/oL8+nVeqUm7Dz68n5avFHmTuP5z6FLGXk36f8AzAdML6a/Lp2mmt+jvzN245PceW9/KncpLXOS7/O7wQnYePWw7ACl2yuTXn76uHV3+nt0ea9V53fojdvLI83dl2t+D8DzbNN89uXVnB0i91z1vTh0wv2Q99rVz082vdFpF31+ei8Htw7Tpod2PFJnJ10/OcL2/hx2TjXZOUHeuBXOjnRL7Q+vn5WtUddDqH5i/VPGza6j+femm7zy70kh/wBF/nX9FGp+Yf0/w8ttc0Yg3OtReIxcy6HGFP8A01wzuYw5h+WOoYq4WXmlktBdPz3+ifzudyu1Kuo8+vJ+WrxSbuduB8/Lf6k/Lh0upW6vmfeqc0VbtvC+9nAO+8nnTqdF+20/O3Wq/FHeq1GbpdQV7LuV/wA7e4D0cMfNum1u8e89XjOb09yK3/Xj9UdLaWtj0dCkIep/TfPTm7u07Lv3LrUbxt59dsTXpWuzNbuvBsHfj8+g8ex8xZhgyex8+h5+/QBq/dkPPoefQPn0YsW0ANbFvAABrbI1tkHj2Pn0APH30AHj2Pj6NPcDV2gaO8MeQGnuDX2AAVG3aXPfWlqXbKW2PP2M7MaVXumVLpikXak2nXjkIHeiPO9zpHIes8n7+Dev8JeGUrmhd/j1263vwfBvJz3z715BeAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIaGuUNw7au9XLBpWX+nVnpxFjWpTYvoyN69ISJmPENWFyx3Fr8tXzb0gOnMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAa9J2Gqyt4r1kc+lYlUbzaWPbofjSt/UfzpW8x9Q2crS0TvS1LR1hwO3HaarSu01RtNUbTVG01RtNUbTVG01RtNUbTVG01RtNUbTVG01RtNUbTVG01RtNUbTVG01RtNUbTVG01RtNUbTVG01RtNUbTVG01RtNUbTVG01RtNUbTVS2nz7tTFWLLWvne8PF7AAAPH30mAiQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAJ3fj5D7LycFastN8jqq115XasrWuuV7zDoyk4+fS9OXzN63euVX3vXpTnWbC9/Ue8c2hT4zelw0q7P61sauQOFrpq13a1rb3LpyF1UDcra5uXdIpOyhqsdCVPEXFSpEsim+pi4KLgvHQVOsWF2vF63RS6KbkztbnPr7nORWdG8XRXVJsSketa3XzQ5uEht1H1pW6Klhztc1KumN4yOnITakpgi8l43/ALpw9ovaqxGdugqd5hc8dDk7Nr5PYKToTlbsdJhJGoy/RSxDz9wAJyQj5D7LycHKOr1rzOiqQ/Q3m9HO899XiiTs8ytzKxWtpWpxPQlZ51NWscy6aZzUIzoTWKdYZBjasQdt2+nOqYb4pbmUhfV68+2rurPOugZWN6nHX1pWurEytXNK4LRSPl4XitatvUmt2QxvAxtwa0iIe3omn270rNZjLzE9GcXr3JS1O3LKhU1sSpXq5rRUltVmvz/1hdjyM5pee2uzLVh7EwvWo27tq1vammc0iZnl4REuwv49fVJqmxY3TmHNqABOSEfIfZeTgrVl595XTub1GleDazKZnpa2ILSrNqVvBMSMrS9vatuVPFla4qlmhZyBwvPa9X9dOdk2qVtStanIXFWdSJuKI942lFOkNa2FUvcrUpeeYtqqKzP4a353peNKH1MrXBUfUTbFTmaTnzxUReLir0dC5Kl6la1U+FsVjWLgqiFrU6brMsOfQAAAAAAAAAAAACckI+Q+y8nBzbpNa8vopGS4+vO6KLKWT7CjbNw+SrEnLMrc/lrT81rRPV782itQXRPNZ9VW1Oe9FkLU3pE1XoCs0PJdvWlYqrX7znaJ2d9lbnUpbvfVnT81o9Z2rSyqzRNu3tK1Hbsf2k1qIvny0R1R6BoEDKx1htEdnknNpz50Hz150KSkZqYpWaXmImlLv5rNbWZlNR8271eNHeOXUKyAAAAAAAAAAABOSEfIfZeThrNs0+TSvp55nTAp4QKeECnhAp4QKeECnhAp4QKeECnhAp4QKeECnhAp4QKeECnhAp4QKeECnhAp4QKeECnhAp4QKeECnhAp4QKeECnhAp4QKeECnhAp4QKeECnhAp4QKeECnhAp4QKeECnhAp4QKeHzf8+vo/PDaoADDmwLR7wt2+3ge3iGROPEOTbwT7eB7eMKNl4J9vA9vA9vA9vA9vA9vA9vA9vA9vAyS0LNRzhGGlESsJh4WZhU4czDpzMk1vcMzDiNtEyEzmYUVzMIzMIzMIzMIzMIzMIzMIzMIzMIzbGjszrYR0/TAAAAAAMGfAtRfE78t17wm/zmtqjoyn6VJY0SmHX8p3tmJ+ktWdzYRZ9nx7nUEgAAAAAAAJqFmq84Rz6HOejVXHxdCe1tnPz1astam/vT8zltsURpWFOii7QnDoRlgPUPl2D1NRHqMrAKcoAAAAADZ1tm2thHT9SAAAAAAwZ8C0YL9wGrtCNTUlkNTXkxp4pEaH3eGDOTIAAAAAAAACahZqvOEc+hCTcJh4AZ8DX2Ezq5sgwaMqWwR0wPMVLkY8ewRF7+UkIqAAAAAA2dbZtrYR0/UgAAAAAMGfAtGKb4t13UTdVLPQ4pf/sHXC/qbtlnUGQLdBx8MjoGWjb6bUoOyXVU40vypW1YJkAAAABNQs1XnCOfQhJvl2Pi3ZWbNn5yMk61Nt9X9yei1KejO4IHTVtUFCep2uymZYztyuWGMfQigAAAAADZ1tm2thHT9SAAAAAAwZ8C0S9r9wFYx2r7FKTsW0VOE6PXUSnP+n/E8ysdqIjqnfS1CkLX6Ro0TpPwgp/59WCZAAAAATULNV5wjn0IGehMPA8+inArdk+LxVV6B8nWjS9i+Jomxc/s2q+O2IpTJiaxlKvcNMgV5wAAAAAGzrbNtbCOn6kAAAAABgz4Fox5+X7vYCLr0VuqK3U7Bgmc6q2eK+3hNvbxjRnfMKc7HV4rbPHqpFuYM82AAAAATULNV5wjn0ISbgcPA9vPqnANFO8quWdrK8ox9IaWW9iKAAAAAAAAANnW2ba2EdP1IAAAAADBnwLc68XVbrCbwVctUFGW5By+mjxufMZ497Pg8wli1IZteSiZZ4ueyEZqyeoTv2Ms69C6LRLyj0J0AAAATULNV5wjn0OXdRhMfFrNmM/PVK2xc2k6jkyztj+tWdPFthJmMdsU5QAAAAAAAAGzrbNtbCOn6kAAAAABgz4FowX7gAAAAAAAAI+QEAkAAAABNQs1XnCOfQhJuEw8AM+AAAAAAfD6AAAAAAABs62zbWwjp+pAAAAAAYM+Bav/ACF8267UJvCwczsxnDy8D8RYstY9psfrnthJParGYuInQAAAAAAAABNQs1XnCOfQqtq5zj4ti2YGez89XbFW5vPV6frq0ugMk3mtbR3EY87UTPRsX5JfJG4Sz59LdryhFQAAAAGzrbNtbCOn6kAAAAABgz4FowX7gI6Bt6KwsdaxW/llFc3ZYQacATYAAAAAAAABNQs1XnCOfQhJuEw8AM+BrbKZqcnMp1oczLbs6QXqbRlXNe1loL5PFYzSsAjJMigRUAAAABs62zbWwjp+pAAAAAAYM+BaMRPm3bMCZ84IuMill2apKkwhvCZxD5UyfyJ9mzt84uasor+6mTVSSJlAzyQmQAAAAE1CzVecI59CEm6Zh4UwjpGnAIJMtlr0fO1zRv2MZFA7yZBWN2bTTRi4rYmhEzNlaOjETgjMAAAABs62zbWwjp+pAAAAAAYM+BaneLCt2BNoKHtEDGfrYz+yPg7J4RgjLJrFYnJLfTWvk58KfYvMkiA1bPjIq3wE+uE2AAAAATULNV5wjn0KHfITHw4iXM/PVK24Zv7qeeSnWK+TOVNPnvMjNoTa39eK6GvPRs3+603GRGL7I65YRTiAAAAAbOts21sI6fqQAAAAAGDPgWjBfuANf0jMEgAAAAAAAAAAAAJqFmq84Rz6EJNwmHgBnwDDM5kFOrBFAAABGTaTCoQAAAAAbOts21sI6fqQAAAAAGDPgWpHmV+W65ITeC0t2DjPYsmGNTZRNwAAAAAAAAAAAE1CzVecI59Dn3Qajj4uvN6e5n56s2bAReSv5r9Wfd8wpuetzYiMkTEys2zbOKARaK7G2CdPMzggIzkdvW1CV1JqqRFklYiXrzBGYADZ1tm2thHT9SAAAAAAwZ8C0YL9wGvg30RixbSQJAAAAAAAAAAAATULNV5wjn0ISbhMPADPgae4mYSS2S0JIbZOt7zFNaJny+LUkCsfl2ycHzYK1+T3S6DnCNTbFQiAAGzrbNtbCOn6kAAAAABgz4FoxUPFuu5Cb6Wl4qkZ3KWp+VNrUyYTNqjqIvLT3JsCQAAAAAAAAE1CzVecI59CEm+Y4+Jc1bsmfnoqVIqefD9v22pTpGvPYFOzJtavRyLkr9gjMIoAAAAAAA2dbZtrYR0/UgAAAAAMGfAtD/My/aCYLxPIrzHev31Wpx94+lClLX9PYnQAAAAAAAAABNQs1XnCOfQrtihMPAx5CnAEKp6sy3RGVy7kUvNZ803w0PouCKxk159RkEUAAAAAAAbOts21sI6fqQAAAAAGDPgWjHxfu+gNerRW4qraU/Xn1MgAAAAAAAAAAAJqFmq84Rz6EJNweHgfXz7nwCGm0yxw6Zx8+KehAAAAAAAAAABs62zbWwjp+pAAAAAAYM+BbnHi7rdYTepWbUrkZ/fGXZVib/V7QuE3AAAAAAAAAAATULNV5wjn0OWdThMfFrFnM/PVqyxszt1zHtTvFZdfdtvbxl5gAAAAAAAAADZ1tm2thHT9SAAAAAAwZ8C0YL9wAAAAAAAAAAAAAAACahZqvOEc+hCTcJh4AZ8AAEPMefU2CKgAHn1IIAAAAAANnW2ba2EdP1IAAAAADBnwLQfyu+bdduE3j65ms0Z1G11X2i0a8BDpv2rAapeMvj3OgAAAAAAAAACahZqvOEc+hWrLzXHxbTmr1hz88RatSv3P7Hfti7ZU8yJvZqWxFZ3YrUipvZqbvL4bpSrqqFOUAAAAABs62zbWwjp+pAAAAAAYM+BaMF+4DVq9yRWr700IHQtoidafATYAAAAAAAAABNQs1XnCOfQhJuEw8AM+BBTqb+YCwimSs8nSDxWEiq7NhJquexin3AigRmAAAAAA2dbZtrYR0/UgAAAAAMGfAtGI35btkxM/PMfSYp0j7XvSZ9H+EyaP0idRmgixa8fUkdFRvhaVEyAAAAAAAmoWarzhHPoQk3UMPClGlu0898+1taxeqVPzpLK5OxTNrwmtOlsR0jGIRUAAAAAAABs62zbWwjp+pAAAAAAYM+Bak+bKt2BNlOuMLFYf1I6ymtBXrWIvby4TQ9SewQyViiR+7OBaeE3AAAAAAATULNV5wjn0OfdBhMfDhZoz89WrLGTeC2Ejbph9CWkFpCqbUjGUdaYSbjIK4gAAAAAAANnW2ba2EdP1IAAAAADBnwLRgv3AGH0jI1doBJGySASAAAAAI1EkEgAAJqFmq84Rz6EJNwmHgBnwDBM50NLLexFB8l9EAAAAAAAAAGzrbNtbCOn6kAAAAABgz4Fqb53PNuuXE3gtrD5ikVs6ntXY2NDGRVuhddEpkwYU7sftx5K5I2dTDSmKLJlEeDJIR/pGaH3fiLmJ2AAATULNV5wjn0KFfabj4vyYj5DPz1astam+bfwYJtg3K5Y50+1qb11rUM+AAAAAAAAABs62zbWwjp+pAAAAAAYM+BaMF+4D5WrMiI9IDQwyor0jIEV2T3xVa/0rwiBsH0tWpncIru/JiP8Au+TBZJkgJsAAAmoWarzhHPoQk3CYeAGfA09xM1yQk06QG1KiO1ZtEBGYAAAAAAAADZ1tm2thHT9SAAAAAAwZ8C0Yqfm3XbhN1fnucxTpEJlgy5KZvFlUWTLOp+Qtgm4AAAAAAAACahZqvOEc+hCTfNcfEt6vWHPz2hv1qbWLQh9adbmq+eKWFS99NlVS1xmEZgAAAAAAANnW2ba2EdP1IAAAAADBnwLQbZX7QTW9azZ4pSfFy+oqX2x7hUvdn+J5ndIXfUtQnYAAAAAAAABNQs1XnCOfQrVlhMPBw5inArVlxL+K3a8UzQbJLep2rulcfSKtbdHejIK5AAAAAAAANnW2ba2EdP1IAAAAADBnwLRgv3AEdliNxBToEyVuyRATIAAAAAAAAACahZqvOEc+hCTcJh4AZ8AiZtLMWVARBHSM2K/YEhFAAAAAAAAGzrbNtbCOn6kAAAAABgz4FuceLst1/RN4Lay1SKbOrt+1NLPsapGSmhJRENJ7viZl5UnUEgAAAAAAAJqFmq84Rz6HLOpwePi1mzmfnq1Za7N9TDnmbbw+X3FLSOpt6Zh9/fc3wXmAn6cgV5wAAAAAAGzrbNtbCOn6kAAAAABgz4FowX7gAB8PoAAAAAAAAAAAAE1CzVecI59CEm4TDwAz4AAD4l9fPoEAAAAAAAAAGzrbNtbCOn6kAAAAABgz4FoZVvFuu5Cbue3ehRn0Wm7kQi846rjTcftPyls1qnpHRvVTtiwTYAAAAAAABNQs1XnCOfQrti5jj4tvyVyx5+ejpGozbJ6s9SnbBiz+bb2CVw5s/PCKAAAAAAAAANnW2ba2EdP1IAAAAADBnwLRgv3AV6W20RWIu9lavqXMVbzaxDwlzJ0N8mQSAAAAAAAAmoWarzhHPoQk3CYeAGfAjpFNqpsWNOsD8nyAjEIAAAAAAAAANnW2ba2EdP1IAAAAADBnwLRjR+W7d8TI5nFemNb5M7TQ8wkVblUb7WyTOUJAAAAAAAAATULNV5wjn0ISbqmHgyDV2qcAh0zCqY52t7T+xjtqlOzpIfIjGicxaFfWuiP8xnJCKAAAAAANnW2ba2EdP1IAAAAADBnwLUbxaVuwJtH0i1zcUqEXZdBWM3dySK543Z4q145/0JIToAAAAAAAAAmoWarzhHPoc66LCY+JBTpn56o27Um21Usm9O2j92dWb/ZLH6impiy7iYrzteZvG7m7nRMjPhAAAAAAbOts21sI6fqQAAAAAGDPgWjBfuAMelESL59mTHro3PP3XPG4A0DfCQAAAAAAAE1CzVecI59CEm4TDwAz4BjlkY/KcwiAABpTO6IgAAAAAABs62zbWwjp+pAAAAAAYM+Bao+ffy3XNibwUHORUZSGCJyonqxOekyGlo4TPNxsabunv6pchOoAAAAAAACahZqvOEc+hR7xSMfF2JSMk8/PVqy1qb6Oz6j7dU98gZaM9zXrttK7dqxZ4yr8RtrdEh40kUk9fUEhtQ1rjIK84AAADZ1tm2thHT9SAAAAAAwZ8C0YL9wGpX7WiunCWcRmTfJjPMqNDxJCryMuQE2AAAAAAAATULNV5wjn0ISbhMPADPgaG+mfERNFodMJmN+SaIgZ4IH3NptC/ZkRGKcEfIEUCKgAAANnW2ba2EdP1IAAAAADBnwLRir+LdlrE21q9u7EU29qkbZbFW2U2BUt4n1JkUWXFVsJcvVd1k2PxFYEWdVMRcGHNNwAAAAE1CzVecI59CEm+c4+Ja0BP5+e09ytLepis7tt5zYr8bGc5LUrNN7VkplgjOUFcAAAAAAAAGzrbNtbCOn6kAAAAABgz4FoDzvLdoTMFhnouKQXy6aKK34tn0oM9YvZVvtlJq2GflUQlet+Erf23jXrdzik5pP59mwJAAAATULNV5wjn0KtaYTDwdbZKcCtWXAvX8Vq1La1TxK6FunakM0hXmqNq968V3hXAAAAAAAABs62zbWwjp+pAAAAAAYM+BaMF+4A0K/FbexZZsAAAAAAAAAAAAAmoWarzhHPoQk3CYeAGfAIybSaq2ObZzQim+j5BIRUAAAAAAAABs62zbWwjp+pAAAAAAYM+BbnXi5/LdfsTeCg53YjPFXMscrJ2KEty4TcAAAAAAAAAABNQs1XnCOfQ5b1KBx8WuWbz6z89WrLW5vFy2tgt04NjWlptre9baiunuZ65Np2Sp3Rq4/RTjAAAAAAAbOts21sI6fqQAAAAAGDPgWjBfuA+Po+Po+fQAAAAAAAAAAAATULNV5wjn0ISbhMPADPgfPo+fSXn79Hz56D59Q+fSQQAAAAAAAbOts21sI6fqQAAAAAHz6MTKTiZRiZRiZRiZRiZRiZRiZRiZRiZRiZRiZRiaWQ2WvumJkiCTZRiZRiyiAPGLYRTXbBXXbA12wNdsDXZYxO82CNdsDXbA12wNdsDXbA12wNdsDXbA12wNf1mJCdAAAAAAAAAGluwRMa/KIc7X45bInTffIR0zZokEdZy8BsJ1vNp8vOkSHC7aXaQ5JmOl+ePYy4y8PukrI1egHdeRdC5Sdpzcc9HVdrlMqXnZ5LqnWdvi9uLhscnjTs7kW2dOycoznXI/nsCdi9cUmTpnNbTQTs+XjuA7B74jOnVcNT5yd52+IdvMcTz6ZL5Gc4+HW4+n1061j4vtHcAAAAAAAAAAAAAAcesV/FOsEiOTa/YRgo9/FDherDnezexyq0W0VOB6UOLQ17zmbbt4p1Z6uKdDdKHMsfUaUc/uGDopWK31IcYuN2FSgulDmn3pQqHu2Dntb7MOOTHSxqUbog5lk6SOZbHRRqcv62KJrdEFMqnXhQav2YVTFcBzR0sAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAf//EADUQAAICAQIDBwQCAwACAQUAAAMEAQIFABMSFDIQESAwMzRABhU1UCExIyRgIjZFJSZDRHD/2gAIAQEAAQUC8VrVrEuj1LLE63GJ1xE1xF1BWY1DZY1RsF5/421q1i7dr6tNeKomL6hLXJLa5VbXKLTrkgalMkavS9YHa9NCbraf+KMeoYvabaoApdDCMUWtWkUbobXNO2HukqOnN8Y271VE5Ww4tFoIqO8lpYehnuDVbRaP+HOeA1757wLcMsMbMbx15H3DZEo3x0QHA7JrXnlQRMoUibrsgXEuEGhlsdgRhsVKCQ6GSQTW0Wj/AIUl6ipNrTK4NvscEWZNNn6wEcXKwEOt8tq80zrmWtVOfhE2A2jh3hnrE2Cwv3aOLakBNi//AArF90qg+O2i3ZroNmOO160qZwhNbtK6if8AV3db3dqmQTGC7oraTfapEWE0Hb7tAYg2v70QXBZQs3p/wZibQopM6iIrFid9K2LGhjGqEoTNVZKWl5NOpzLGzJiz2BoUto/sZ7VlcBxa4Qt03jd8fzDI+MY78BvBlsudRmn1C7xRMTHaXPPxfC5Bp03lltNBz9QZDWFdYcp5szEQ39QGsS+TyyNwlqYWp/qfqB/vHm8oW185kwyv9RxM1vUlLZ9/vwr7LtvPcnvspXiLebVoW1baVrNrPSUuvvVg0Mchyd/bSvFpAQxJLqGavQSeMrzDTxAClKbCpvc2rx6KPukBN0PZ/Wmr2aY1iTb6GmH1FdL5NJqxvV+nbRUv3PH6C0ux2cyvvf1omYx4pBkkmZ0V1QFq5RG5D5NFa9ijOrr6dvQa8ZnHTat63ro2RSXmuZx1prat69szEQTL44cjy+OJMTExlWwUViZrNyY/LVEKgR6n+rdX097/ACa9GE9fTprWDfq+muuZisEzGOHqM5jZ0FkDEatatK3zWOpIMmizOpmKxfM44cr5BNqe0097SUdy9rcNay0IASVqD7i4mQhLlv4Kx3VS/lM5jDqDHELNa0DRxh4+mTEvi5TQqrji7yTUdxkZ/wAfZlD8uji0t3G6+mzfw8xyqt72JdDD0Olbv4vpyO8zgOWZwR9l60xWMLWWnc1krmLgVBMnyy9VHsI9ZsGf/IAuShsmlyRMFxcjrEJc9OQU5JrAOWGxnniL0xXLS6zjE2BYExxM9uYyN2T4JUbDWcVos3hsjdc+Xx61g6w2NVYW7J/q3V9Pe/yR6rpa+nQWqG/V9Ndf1GcnGkgoJbOLBXaRNcDd71HTIZArxcCgIlM+mEVMSxZlHL5K7RsAkFi2QDyj2HdlxXsLP/mCO4JibVBB/wAr0csjAxksGcaOMiUBu2lZtIUGCzVr7fT7wPX3cevu49fdx6QNRigjUCOsViHNJep2fUh/4QDy6b4dhzDm2X/qG3cjrE/jj/wf6a9f6jX7iDvI75RyIxePFyeKme+cVkhY+Mm7R5j6dv3O5/8AIY2O99vErOFGmFJPX01H+D6h99j57nvqJW9o0rlXFNY7JgejsNbgDr6ajX1LH/lE905CeLFawH4/sn+rdWPG2Q5MPl2JW+nIia0qMd+r6a63pRoMme/jIAaFdf186SaY/WFjuxrKoW6tcKqOsVlRICyLVXWvpu3+fsL/AGL0iTRrQY7m8x7XUaEGpr/bQaqgvXQwjroUR3ZiseHC9PGvwUmtqOf0n6vY/Yr2U/8AuPTwnq3rM1tmbcxitYn8cf1/pr18qvzKOgXu/J6cYNfTkDtrZDpLILsM5/8AIYv8ho/oa+mrf4/qH3+KHJMg9l1VLtYAZoYWMrdUtgsdhK8Y5iaz9NT/AD9Sz/51rN7ZKOHF6wH4/sn+rdX097/sn+r9X0115Vi7Dv0+Ot3vqT3K/r52k3x+sJPfjXrWommwzkMbr6dqO4dkOsdkANk7Cx3MKT3rSJq2hQeWcnTjTlNnhEk1NRqtBvF4iLlva0mJeUzbo8iMzN+Qb1yDeuQb1yDesSKRhPDcKj/ijc/5Ef600XYX+nQzc+s+DdR7p1i4h7EsLlWJjso0MNotFvpv19Prys39OL95NZbEFoXHuWQZJn1uDCoEX1n/AMhjPyGj+hjsVZ4a5msMy0wR1jCY262s5jSMShnIAPM5Nd2uJxhWD9uYxJNzEuQg1lXIfbw2JJBMr+P7p1gPx/ZP9WieL6e992T/AFeJ4vprrzKRV2kHLIsZCjzsUtwXTcrlBvYxhK+HyolKPZOHhY1LklcpiCgJjH5x5z50Nx4XH3TF2Nx3FSt23rF6y+WllaXpU7IQRBBk1HdqO7uATZCpbiXnRsgUFgOjYguSOG4abYq4tKI0a/eVSnAv2d3d28Ndd0Rq4xk1QQx64a67ojs7o1/XbcASaoEI+zujXdHgtSl4oEI+24RE1VVaviuEJNUCEfbw18PDXXdHg4a67ojUxE6hdeJ1sB7+yVl7arSlO2y4L6oIQ/A0OSBGSKk7WRwIx8n3aCoZmTMJh1NO+R04qtX/AJU9mrlrV1/iYGxj7j1j95rtKTaHwcc/8IUcDIoXir2TEWgoF0TMvlY1SIm9bD7qQGsMRWCK+z0u0VawnuZqOlR07GCbhEqd/wDwrAd4cWv3iLU1P61uXNrurereFtXU1mlpkFNbyeiTW11Z/wBTumZTw5S6qKytKf0M1byyfg1QW7b+v+GZBNtDJNJglD14Ln1EREaKuE8Tj4jRUG71jCm0JG1KBUXX7bhraTlmdVreZCKoaf8ADnW4p/8AyCbrPlWtWkEasTQ62vIQ1DX/AIkwBm0QRR6Fe9Iq7GqHCTw2bXpqzZLatPfaipCarWtI/wCHko41vD1vD1vD1vD0QahJtS9dWtWdRYEa3I1JKa7151WbTqoqzocrCjeHreHreHreHreHreHreHreHreHreHreHreHreHreHreHreHreHreHreHreHreHreHreHreHreHreHreHreHreHreHreHreHreHreHreHreHreHreHreHreHreHreHreHreHreHreHreHreHreHreHreHreHreHreHreHreHreHreHreHreHreHreHreHreHreHreHreHreHreHreH2k/ink8NdcMf8Wv0aJ6f/ACq3Ronp6Yy0iZie+NAduVzw85f7h4nGOVAkds/awSQgSYsyD4TJ4WE2xYA/LmeGBMSdYU3sPwEY4DS7eIaNy4EmJZENixG1WLHnw24uETR7zLTBirtXuXSzUlXAXfD4lujRPTYLAAhCMiWPa70lRMP6x8X5/HluJlQtzMDvzkotEIFMLLtC0JbKmg2NPkjXhlOI39ZJksXYXZQpke9hTGhsMOVtaikKsnVEzdjGY7c+3GpAh86WmMoi1cDrLK6t55aoSbom6kvo1wL2a3bvEHKR2iV5xcU91GbxjV2LiTIUgQEuBerViFYXBy9MvSJXfFtKNK2AFxq3AW4F60tx1Ztc7bArqk4rfdGbWh0p6HaAegm+MZGKBtKqdODHSc0JmTsuJgxS1aBKddWCK13gVhJi/MyKdh/Gx3jVXk5FbWAcRAM6VYm1UQXMOhgtWSNEmgdIm9hLDUGQptLUi+Oxgoot4lujRPTy26SIw6vcBci7ipWMfKAzw/lAFgqa22mKspSrUsr4ilxgNViuTLv5I7tj0JQO+3rJLGsRhhl+jq9qY5SJhbL+zG2wJQKpA40YTziqd8qUVIfF1dbqBtRkym7JIHXho/Sd9qd0RK2nIv1ta5y3GZQfe2QBOeZAWzrwbzax4LotLc9rJUuRRm8srPxNlGAE4LH3dR/EM1IFppmTlY4l3rXIw5avKNBvYx7m7rIhvELWtyMgLyhm7MCKCw9ZOtrrR/WrVi9ccIu9kxl0Ae0FCtq2oO0vgmE6h3bjQrNUw9yWlJIS+nuHl69Okh35LGknZ8S3Ronp/DdXs0AA5CH5bS9jz+kW6NE9PTR+WDE98eKW7VWpbjp4y32hAJvC8+h6XKFjeJoBbFjwMF2A0tx0/ULdGiem6cg9PgYEsyxeuiEInppi0sr1YoWtmHSRLYhJ1bNG6QuMGByy85C3I2g4xndtKw6sVvoZCVeXbLd65CF0sBoqtHrxj+FjbK6S2P8A9hUdIOQaTEshdLcC1IPejRzcRoaSqyY8sklxU9uYUOQxDsjNcDKoic+EZytK3NRqjBpSqNwwOfvyBBOhCnexFcj7KgW7LUOwZE9TgE00bljUbXG0wadGhlKrLN7GsUilzXZu9EnVbsaxmlYZr81bo0T0362iz7ojKujiL3shbTRBUItAudXNRG8sXZEj7On4qmRDReyZft8lxe2S2wteFZJrJcQpKOyyYxzTGI+zGG5sZBMXtM1mMc4KTqjJjIEjH+Fm9xhP9uuM0GDZtsbQrV4H3PeP+oQYAOB5UjIy1DkkvcV/Kj/Hg9oMNyYs2RGRfH+zyHs6e07y1xRft+we3ciy6IwGf9XTbQ2xtBoFnvSuSPyrfvjFWsbHR3GAxRj5i3Ronp64YjsiIjX96iIjUxE+Dujs4Y79RER4+GO/s4a9/Zw179RER4O6J13RGu6PCwMlxX+4GoIcCH2xERp0FzR3RpkNyH7ojsiIjtmIn5y3RonR/wAqv0dkgHOuXHrlx65ceuXHrlx65ceuXHrlx65ceuXHrlx65ceuXHrlx65ceuXHrlx65ceuXHrlx65ceuXHrlx65ceuXHrlx65ceuXHrlx65ceuXHrlx65ceuXHrlx65ceuXHrlx65ceuXHrlx65ceuXHrlx65ceuXHrlx65ceuXHrlx65ceuXHrlx65ceuXHrlx65ceuXHrlx65ceuXHrlx65ceuXHrlx65ceuXHrlx65ceuXHrlx65ceuXHrlx65ceuXHrlx65ceuXHrlx65ceuXHrlx65ceoiIjyCdPFbXFbXFbXFbXFbXFbXFbXFbXFbXFbXFbXFbXFbXFbXFbXFbXFbXFbXFbXFbXFbXFbXFbXFbXFbXFbXFbXFbXFbXFbXFbXFbXFbXFbXFbwHmYpuX1uX1uX1uX1uX1uX1uX1uX1uX1uX1uX1uX1uX1uX1uX1uX1uX1uX1uX1uX1uX1uX1uX1uX1uX1uX1uX1uX1uX1uX1uX1uX0K9pJ55OjwleXCfSjXNR4dykX+Qx0eA56Ljie+Oy9uCix+YD8YPqeeToybt0x4t67lOz+tSGXQJm5hZGzcyo0aT800yYDR6shZIR0rJKPnh37oLd2/jsdGSeIrrHtWbD2Zb2TLF1lCmyNBkcoNS98jAUi1Bja3yZaLu1KuMuQZqo1J6gafbpc7JWQ73B54fU88nQdcTNALhWp2ZM2yoFLICFi+Jc2KOKthWg+Xxd6hsS1WMqK9RZa5hly7FoplomLR8djoOsFmohDBTsy3ssj7Fv2h4mcSZoHK8Fr4ULIJXCO5U0mBSoj/kNifaHAse+OMQwvPD6nnk6PAQIi9myLdsipeogCBUyazEiAEEGVXY1VVekmXCxAx0FT47HR4CDoWtwjJW1a3rA6VpRJUdqDoOkoJzaIiIsipe1a1rUYqCqRJUtqUqOvnh9TzydH6Zjo/QB9TzydHhZycLs+PInIupSe+nx2OjwNscsBNqrY6s97fhbOQJPhh9TzydGZMcIcKc5hdlrRSoVpbSxx+YUVq05pSTwbGHvcNLMsKlbtOLUbt9vPufZbKtytz1q4zb7xWbIXEKgYJoZOesge+/8ABY6MwY4oxZTFX7Mt7L2TFPyxSyw0EsgbMC17okvv0hllh+hKaYAysNg966yBbxWP4jzw+p55OiYi0RWKx2ZUloWriQxVCvKO4npD+WyG4oyuGArr0vzTdSUay1eHHW9nQF2MKJjFbbe3bFL+2XhRSU7p3t8FjomtbRERHblvZEBDCmOksu3qJd1ciFyzIIcQ4edR9zk+KLtPDYBdXiRx83aP8APqeeTo8k6xTu+Lhr2zET8Njo8f96iIjXdE+DujV+LgRXlcHwA+p55Oj4vfHf8AAY6P0AfU88nRMxWImLR2OluFWv3SV0meaXkoonW4OLZG1qmm1Y1ExOu+O/viJsQdNf3qSDiJtWsOlnf0h/nY3B8XnsdE2isRMT2sHqsKlckeq7RoP3x398Ro7cBNExPxg+p55OjMhOYGFCcIuzJexpXK3SxtxXUJGHigWSUwy+MVstkv8Mp158lP9fLUJb7jH+fLEjE0Jh7f48akJkVh1byTSglnMibZUovcKCcY+a+ex0ZgJyxihGEv2Zf09OfkP/l3vdOrhlwQhhr8UPqeeTo8Dw7lUCd8QV0S0VXlsS6yd7YwJ8gETYjltcBlW8mE1tXSJ9rxwS0GrzKOsaE4tYsJArnGwu6WrrLB/wDZyJZJUbXMP0j+I85jo8Di/NAoy8Kq4DkYZGcTZKtsMPDLNwkuSnxQ+p55Oj4g1xiJ8Fjo/QB9TzydH6Zjo/QB9TzydB2BLUAwFmnZkJmqS+Pgi6bB6MHdXWkDa7OhHEbVWA3LJxQa2QUpC7IWa/IY6DshWgRRmp2ZSZqmPoHa33S7i9JA2BiTOLgsZ4NVknhnpfIqDtUgzDT2BrzkFI0Z1cFhkoWnnB9TzydGUSu4PFokTp2ZL2KXtLf+eaxsQUzVYFlKzyuUW4hsJ/53sYIcnDEUy/yGOjJIka1j1bKB7Mt7IXpi/nLAHS+TaiKZAlDrNbwmEsdEcnVqLaxfoofjcWEcKWg6bC5RmF5wfU88nR4GgywvRfKUGonVXV02QnAmXfyKV24YR3E0l+VAmtdearXh/wCQx0eAo6mHRfIhqqry+oGa+REqazF65CLrqbIFV2waEq6vCatlqLK3CooGy69qZGLKL8sHzg+p55Oj9Mx0eOgB0L8gPqeeTo8EzEa3haKcQRxPfHiCcZ6/HY6PDYo66iYn5QfU88nQ24JMabonKdmS9irj0rrZSlR427I1VwNyW5nqCIu4NiVWqNUXcEySGaXYxzlQLqtjboTIVqRZobUfdhaM6MNRP1uX4LHQ26JSFmRtD7MgewFxY1atKRyLwmaGJZmtWLvVi67I2a/dBTBHa0sE29ScjWdUYEQM5HuizAqhG7x280PqeeTofRh0ePRhKnZkvYq4tQi+Wjux7CssrgbPDCvOSyJVrnNzkW1ackfGRx1wsRyyf8P42HZCoqwNnD1jhLzE5e6rxy/BY6HkIc0mpCguzLx/hrMWhn/Jkjf6+RV/zHx8OSBVY4z4msbZ1jweWrHTTh7lgDsgC5HTrLghnGVYZVv5ofU88nR4GAwwGuPPSplJOoZa5KhSmhjIcRgLFHd3H0cs4nVsQRVAFRWFBCVgTFsfapVwXDpRWFYZSqxYarEX+Cx0eAlKlpGPKPSyg1tNLVaEuGq4pRtW4A3FpVaFqFWNYi6tAUhAgtVWjYjH34IU4VqpWknmh9TzydH6Zjo/QB9TzydHgKSgR0vUlP0DHR4L3oOtMipe3lMswt8APqeeToyL0pDxr8u07Ml7HmLK4uzj9hrmqwH5zHRkHpU0i3zYuzKDvcADqN1aa5fV2XVobc5cRHHAxdtzg5ocK7+Q2yvcKzrXKgs1FU3yWlYjboanaoAN2Hw0ac2QAM1easuMQozzNPID6nnk6GVQt0WVCpTsyXsWfwn/AOnifYfOY6GVAtQAA16dliDpOUGClWJ4HciSlVG6zRPJ+yv7ElZthRq7ouUHyK9+cvEzYeT/AKyPsnonk5S46ZClRpsVmy6IN5dVegPJD6nnk6PAUdDUssG4OCvAEIwD+cx0eAwBMVFj1RWKIZqjx6o7FCM8FFQ1JpWaUFQY/tinfSlR1EqEFoVDByhGbRB0LTbpt/bFO8i4i01fHqksEIgV8gPqeeTo/TMdH6APqeeTo8DBoXCk6N2ktRDfz2OjwMHIKa5IlrHcKNf5IfU88nRmGTrBw7R2B9mS9jX/AEpv+ZYOUrUFMo0cLJbqlONyxHTPApcY/ksdGWaODWMOVhftT99l/bno+MRnOBO9GhiZbv8Abw0a7xWO5CJyE+GH1PPJ0XpQlaDoOvZkvYiDVjHI2L9yaGAeRFGI3jWFfIJ8v90W/K/KY6LjoSK1rSO1P32W9B32ZhWLi6/ZppkNv7dNeMKtUK0U5P4YfU88nR4JiJj+tcNe+axaKjHTVh0vrhrGuGvf8pjo8PdETMRbXd367u7W0LvmImNWGO+oiI+GH1PPJ0fpmOj9AH1PPJ0eApKhGM2SbgDh6sa74+cx0eB8911qT31qwSXtf38gPqeeTozVT2BhKnqHszUzyQoiB5X+DPVuTItLUQJ81jozNT2jFVNVbsy3sh+mP8tUMsZDbhF344fU88nR4G14aXC4ytQYmHGjjvOTyg7kp81jo8DYOYXG4cNEwl3YIUWRrBXG/jh9TzydH6Zjo8YlpG18gPqeeTo/TMdHjiYnyO+I+CH1PPJ0FKMNRGEevYyeFgCVaapQjCLVTCve5hjsZtdfQjCPWzitIrat6/JY6CmECKEoWvaRk/MT045gIExlGWtnFq1sYVKCbWNJTCDAmlzyUwgw6cR/gh9TzydGWTK2LEplUH2Zr2Q+6R5X+TMf62Sj/ZyqhrSVOhoeQXEVqIisfJY6MomZnWNWIqDscNsLVMn9vx5t5PEgFIFKwLIY4Ar2tSpsnlB0oFgq1JtfiftWD5TIAFQ3wA+p55OjwGFU4hiyitV0zSd9aWl0Frri5ZtUy/OTZJYgCfKY6PA2uRi/dGl1iAOuu+pRNa4rJLkBDKxZNZdtuzK5uYldsrDKxt8izrF/gB9TzydH6Zjo/QB9TzydHh748ohRhj4zHR4e/wANijrf4ofU88nQy0FSizYW6djCgWtDRXtkZspjBiyICkhkfMXbCNg7IwSbIBERZsLUXyaw5I8AQMg6Fhc7Ilqrt0Yn4jHQy2FWAHGzTsyvsjIgEqsaZU+5g1x04Pua2mv5yEsjg/xA+p55OjIo86PGoSlTtD+Yr3XzOZiOXe/wtNRYhO+G8nLhLmQ3PuOPpWXD91svmq12HVilsq5JS/EY6MghLekVOUF2Zb2U4ytxKucSRTNmWNM/ZqUptZDc5vF7e38QPqeeTo8NFeF1pLeJVExCuL80ACUCUQR5OspHGZVKVzrK7BXE+ZkuNOxDAWL2XTuM3xGOjwNr80Dk3JrCYoW5JmR1UrykJNxUivGe6n+z8QPqeeTo/TMdH6APqeeTo8F70HStq3rvD3e07NF586WaQz5jHR4CEoKv3BPUTFo+SH1PPJ0ZB6Eh49+HadmS9in7S/5o2QkTInjcw05ISNnNcrTfL6504iNv8obnCiAV90A2HbrAXaYLarzR9KOQ1VJqWxBakrNckUt1HLHuR4snEaxst5jHQ+/yekm4bF2Zb2Q7ocByXFXnDjKwzYRKlb4MeRmPjB9TzydDagnBqJiTp2ZCs3Sx5KkTiYLma/mXvfR3VzWUtXecracoTHlLpr8rl+Hk70bcpl4/wuxMqYy1bIp/+eRw0xy6Fq3yGL9Sn5jF/wAGm9bZrzGOhtITcLrjVH2Zb2QlVuBxgg7OjNSzKsMWUOaTYv8Ar4ofU88nR4b4pW1l1gq1hYcMEXGUjKYWtRjFY0wsFqtMaGtrrjufLcfLm+0SGiu+rq2KWmwACXoTGLEIFQK9wrDBMLjhg+PAcgkFwk8xjo8Bw0YH9sX0VQJhxjl9HTGewFhL6lAEm+KH1PPJ0efw1j47HR+gD6nnk6PCfIhAxpJqzUfOY6PAyeqwqzxQNmpTfJD6nnk6Ms4VQWJcM0Psme6IBLiqJ+YVxUxUYbvvVSZNYoD5BuUmDyahHHb44rRrfIY6Mo4ZbWNZIyDsy3sqZMMUx5IK5ustlWOaGKGeYKYzFZuRpO/xA+p55OgohmqIIg17MoaRqix7Ax43vWZx9JIvjGw0WTnmMhiNB/L0LFC4+Fai+Qx0FCI0UpQdezLeyF6an5AAVamWGjUuP9Z6g4d2MXE/ED6nnk6PBYdL9m2PjoOg9EVXLNa1rFB0HrbpxLcwlVMBeZ+Qx0eC1K3jUUpWxAiLoYhi1WlK6tWt4osuOfiB9TzydH6Zjo/QB9TzydHgbNIFwEkobNXh/tyBiht8tjo8Dp7LL0nir2xLPMaIYkZD4gfU88nRmrHqDCWPIezJexT9pf8ANFqU2U25QeZ423nFZWNkGP8AYIVdUmQ3rPLK1Vj47HRmbHiMVJpW7Mt7LIWtVFhO1QMHNbHKCVma2t91itn2qhsHJHNQrgSjC58IPqeeTo8DIt8Cz0qCVoVhytbfeHa2lxnjUfaMRsjw7jao4qS7NbTk/kMdHgykTZPIVtKTUTKgSyujHAZytbfdYvKDdblNkCRyrgWQlJ8IPqeeTo/TMdH6APqeeTo/TMdH6APqeeToveg60JQle1x1mrNZi0Y01B0GUZakOEOhlGWKEoSNynHdpcVomLR8djouSgorat47GmIWDQDxqgOcTLpRFacuEQFRLjr8YPqeeToy6pmQ4dU6w+w5YCFUyfI4g+6pjVQlsGkLZZAVGyTSqmUU/wBZ8F5tZOQ7GKqSlfjsdGWVOfWMXKuDsy/p6c/IsBF9yyIQ11QdBx8YPqeeTo8GRCZgUADEBWKB8AcgpKi55YgLaJgLsFaySxySurAlAxkEqryxavx2OjwNL1ZDS+TDVZYu8UJLPPAIb5AfU88nR+mY6P0AfU88nR+mY6PO46xb4IfU88nQdgS1F2QtU7WyHIeC1kQjDOMDIWYhoE2+6pd92QjsUtAjpaL1+Qx0HZCtURhnp2OscsCmO46hsVRvfHvEMMVmshAmatAsITy5rxUHPHcAvMmGfInZCvoDYGPgB9TzydGTSu4PFo3Tp2NG5ddVgNEsOXcWCXk10Y+3mR4BIssGOmQUlxDRudHEd0fIY6MkjdvWPVsoHsy/p6b/APLI/wDy73unfeOrywATc7lfy6URdwlKUyvEaclttEb88PqeeTo8Dy126f1qilxvNY2x2sihdvRVKFUlR8gVw7S2IBG/8ljo8Bg0OOq+RDCye1dlUtzckxcri1z64HZDCzBTQvaHSKHoxRI0MnVJvCo7x+eH1PPJ0eXMd8AANYfyWOj9AH1PPJ0eC1q0iG1ZnstaK1AajAtCMM1e0bAyl+Mx0eC1q0itq3jjpx+PfHv/AAQ+p55Oh12iQ0XqO07Ml7GAofb8XuQj9wKTVWaNKIOXooq1RsKRg2VrlLGoo1RsX3K97YwlTN/GY6HXqJwo1RsfZlvZIzKxqfljN7ZAObhQNwW1XLFFjDm29FemCiNv5M7m2QTvEVhuA3o9O6y9RYi5iFjyw+p55Oh1Ibo0kRpU7Ml7FfGplVVIW6KA3LKCUsqDEewxnVjfx2I9gl392H4eRQmsvfGY6HEaOQqrRQfZlvZNgkiqZoYyA6ns/VQ8sZKJIwrYVlcT7XWM6++Jy6Xvch/LAfyuV/mjEd+T8wPqeeTo8BhVOKMVTuCAS4/tla2ErUQlgVWCBWgJApQAFgVWCBagLTixca6QVb/GY6PAwCrIojhqBEK5jpjNcSm3cC1F9Lq0WkaFBF0VChCiRCEp0hmuBIYbsJjYkSNKEuvW5/MD6nnk6P0zHR+gD6nnk6PAUtQjjLq6EYR6eRa9aViYmNFJAhgLBxfCY6PAc9FxxlFtENUYRXgo9NM8t2XvQcfED6nnk6Mo6RMeLeI5TsyXsU/5THWFctZlphgLTA2lWSEYXyFivwwS+QG28zdNk9jVO40XJc3yKXOQK33XhG5JcfzZftixX2ZGd5yacfB57HRknSK6x7Vmg9mW9kZ1W6tN1TG3bkaTBciuPJSSAmPkAUyslusvDMR8MPqeeToOATFALhWp2ZL2KrS9FFp5zIY69QHNerOTdvybsjlZbFxx6xfXT8wcCpNEKRnDDYrCI4oZRP8AC/8AwSntGArcKZbmW89joOuFiBCGGnZlvZMq0IpY/MYpj8V30POU/rI+yd/FDJS9fhh9TzydHgvSpKxj0o1ERWDKrsaEAIIKAR4uIZB0pQVRiGLW0OCWx6d78NeESi4JhBOL1AKo9gW1WsVr9vS4v6+Ax0eAg6Fr3d0QuGtHw3kJr0OIa1di9Kkrt04BLhB8QPqeeTo/TMdH6APqeeTo8DJuXBRzIkpSbTT57HR4G2OWBLrY6jJUtNM3sNdW9ir/ABg+p55OjMmOIGFMcouzJexWyPAucdz1raVXXbs8+svK9fmMdGYMccYohir9mW9kdo8qjKNfH0rBR1PZjGQxK2LhE9qb5mMcwzayAWp+3JReFvgh9TzydExExERWOzJexS9pke7mpla7pvy/zWOiYi0f125WO9KMmvtgUL9uAZCoiSK+PuG5sVGUBt49ew11a3lk1Lwz/Xwg+p55Oj9Mx0ePujt7o+MH1PPJ0fpmOj9AH1PP7u/XBXXBXXBXXBXXBXXBXXBXXBXXBXXBXXBXXBXXBXXBXXBXXBXXBXXBXXBXXBXXBXXBXXBXXBXXBXXBXXBXXBXXBXXBXXBXXBXXBXXBXXBXwTWLa2h62h62h62h62h62h62h62h62h62h62h62h62h62h62h62h62h62h62h62h62h62h62h62h62h62h62h62h62h62h62h62h6gdI/UstrqRzIYABgLNNT/Gln1W7fCtaK1WdWb/WGYAvFL0JQTIDWM2svPOq7pSjDSl6Erz6c2CyBnVmQUKUowUCYR63vQdR5BI1ylGCk5FKtBGEeh3VVps8nXX1J6ECCbDYkCwFyvphtx1IL6fIMRQNrM6McK9YfSkfMg2avKWgzAF68+ltywGoueTgVWV7iA2szorawLXeUGQ7ay2qXoSpXlAWlxWLmbWXsU4SC+nyjDoDS7OimECoXFWZM0utqjASXO4qtoJwsV1a1aVpkUSXuSgqjyKRrkJQVOeT2qMrkGBxVmfl5JXm1Mdk+WQxZ7puD/8Aq2Yz68jJlH+eElk+WxuATjYUt9pyrbBWG8s9z2lV6qr/AFAe9mmtgg2SlNgMbiQtpfTxLVnHLxlXMmnRJz6k9uf/ANf+n/YRWqR0ZTlLEJDdYALkc5kps7l8viwJLz/65hcdzGm4vkcxlsZVCD/+vYzFDdUxKnOlALkc59Rfy5lcUJNYeMqzjcIxcSGMQ+5k5blMt9R+6Ux3Iq4ZATxEaSnm8gfeyjFv8ufvJKY1DkQrGpc6NyByWvqM99wGAXpGauRjI5TEBTWk92Pp/E4sby+LV5piA8hmvmZDDMXdyuMkquGRsmu2vVpfEYpgLb2GYI8IdRDzeOI3CeL4cXh8UcTTgcrbJZfGEbnv+oWNZFZguNxS5l0MIkyqUmNZC08BgLObVO0EqxrYfDLHWTOHNV1ikCJK4NJpUxEWpzWXx+8w+vkKrDCQ+Awq5llMnjGobZRzDYzKnthcQuZZLCpNKsFRanNZhFplvMrmZUWXNTE4ZEwQ1Qy2PPGLyEP5pFppgtZkGCTZUtVFr73kMa1VscZ5o+cSZakVP8HI5PGHx4stY7gcrbJZfGS9VUGcgmYxZGrXVzb+jpTTFYVYyquIRaXdaRavmP1hsFaDr4S0MePI4ejt/sbR5GOgh/8A8G//xAA0EQABAwIFBAEDBAEEAgMAAAABAAIRAxIQEyEwMQQzQVEUICIyQEJQYSMFQ1LwccFicJH/2gAIAQMBAT8BwDSdAsoN/Mq6mOAsxn/FTSPhZM/gURHP8Kyndr4TqsCGKVqoWqHtXh321E9haYP8GxtxgKo/9jeE/piGzK4TdTC+C1VCGuhNeiEw3DLP8Gz7WFyo07tf+/8A4uofc7KC6ikabrChIRc+oYJT6Njo8rp+ie8Xu0CMAxhV1h/v+Cf+DQumgMJcjQqVnF1EKp0dZjcxwUq17j9gVOhVLs2yVXpdVV/JqqdLVpfc8Yf7f8FU/FpRAawgFf6ZWa1jg4p1am+jDj44TOnb4Cr0yGyuiqsbSAc5fIpf8gv9SrMdShpw/wBv+CH3UyPWGvIVPpZ/y9QYCo1mOmxBt5DVXIbUIVIdPVFp+0p9MscWk4VNAGfwVN9plVGWnTjCq97+SjWqUtaZX+j1a1V5c86Lq+4UwHCm395RMmT/AAbH/tdwn0i3Xxg5odoVSqOpC2mYR1MnBtL9z+E9938GATwst3pMzG8BWh3LYRoeishyFD2VFv4tTg92pCy3ellu9LLd6WW70st3pZbvSy3ellu9LLd6WW70st3pZbvSy3ellu9LLd6WW70st3pZbvSy3ellu9LLd6WW70st3pZbvSy3ellu9LLd6WW70st3pZbvSy3ellu9LLd6WW71h0vB/i6v5ldMYaVTqGfuWYTEIVeE2sCjVJAICzo5CY64I1Y8IOJenVIMAIPlwhCt7WbpJCZUu0TqkK/+lfrCzQs1B0mE9x1WZHKa+dEXq9ZolCpPCD9JKNSOQmunQp12pV33f0rjosxCpPCFWVB8lMmNUxxMfRV/Mqg25hCNFvhClxqhTF1ybShZX2hqy/7TGWo0p8oMgyiwl8rKCFH2srSCUxkI05lWcqzWVlBFiAhOZKcyU1sJ7D4WWrFZrKy1ZPKjWcMv7bVbwstBsIMhESIwDIj6Kv5ldOYaSr9TKzFer/EIPKzB4WZ6RMLM00QqaarM9rM/pXCLlmaSsz2sz+lmBXnRXrM9oO1hFxDoRqeVmLMRqBZiD5O/V/MrpxLSEac8qxCnHCDdZQYRwVlekacpzZELLRpzostOZJlRpCylZOpRZystBiy05ibN0lW6yspWHUKw6hZasVmsob1X8yqNUM0K+SxfJYvksXyWL5LF8li+SxfJYvksXyWL5LF8li+SxfJYvksXyWL5LF8li+SxfJYvksXyWL5LF8li+SxfJYvksXyWL5LF8li+SxfJYvksXyWL5LF8li+SxfJYvksXyWL5LF8lie64z9DeVAUBaLRQFAUBQFAUBQFAUBQFAUBQFW8Ydc4ipoVmO9oOedAVe72r3e0XPBglZjvazHe1mO9rMd7WY72sx3tZjvazHe1mO9rMd7VJ7r267DeVBmcHamFMwp8KUddut4w6lzR1EvGiqEFxLeF0/camwA3TkoNt/ETqqmkuAnVPaGSWjynBrLjHpPALJbsUe43Ybz9EKFCjbreMOv7uAMKSg9w8oPcNQUHEcFSUXEiCdij3G7DeVdrbg4nwrkHK5F2iu9q5XIGfrreMOpp5le0KoyxxaVRaHPAKNJupHELIMLKltwKZRAeAT/2EKE/iUWQ0O2KPcbsN5xt9oNRboi1W8otVqLUPrreMOu7uFN1rg5NrQ0t9p1a4ao9RIiFnfffCzeAxdQ8Of9vGxR7jdhvOJcpwn6CYRP11vGHXd3AAkwEaJ1g4FpAk7dHuN2G8q3W7B3K8ar/yvKIOAHEoheU36q3jDqamXXuCqPvcXFUXBrwSmNyyXOKby18+FUcTTbrt0e43Ybz+ireMOv7v6Cj3G7DeVJmMIlyB8K5ByDjt1vGHUtaeoh50VQAOIbwqHcaqf7//AAspk2+YWW3QosaXugJ1K25v9hZdO8tHhOEGPro9xuw3nEhWhWiIUeVG3W8Ydf3cAY1TqziIT68/is0xCz3ao1XGZWYZLk5xcZP10e43YbypHGE6wp1UhSpTXSpCuCB+ut4w6xpdWhqIIMFMaXG0LLaZDCrHTCNNwEkLJf6QpOOiFJx0AQpuOv10e43YbyrRM4EaqFBUaaINgoDhBqt0QH11vGHVvLK1zU5xcbiqT7HhybYyXAys0Wf3wqtUGS08o1AahcCr2atCD2k3O5V7TdcdPro9xuw3n9FW8Ydf3cXMc38h9LWlxgbFHuN2G8rWcHcoc71bxh1JaOol/CqFpcbeFTcWuDghSaYcONUxrX2khAttDrVYxoJPtNYw/d4CawMeB/2ExralunlNFNzmhNtLS+FUtu+36KPcbsN5/RVvGHX93AEjUI1HEySjUcTJKkoVHDgq4q93MqSjUedSU15b+JRM6n6KPcbsN5V2tuBMFXKVduVvGHVUzUr2hPYWOLSmkA6q1mYGBqyTpaZWQTEFZP8AaewtMHZo9xuw3nGNVZorUG7lbxh13dxzv8gemVrAI9rP+4O1VOraCE8yZ2aPcbsN5xJQM71bxh1/dwa242hBhM7tHuN2G8q3WcOCuUBru1vGHU1Muvcqj73FyouteCVZZcXIz54j/wBblHuN2G8/oq3jDr+79DnE87lHuN2G8qTMYcmFMK5XHcreMOpa13UQ4qoAHENTW3GAq7NJjjRVmNLnxyjRYHFvpZQMFqNAT/USqsWNt2KPcbsN5xIVvhWqNyt4w6/u4McWmQmvIBCdXc6f7Wa669ZxWcZT6hdA2KPcbsN5U+MCSpUqQpCLoU7Nbxh1rS6tARBBgqm25wanUxFzSnU3N5CbRMw5FpHO1R7jdhvKtEzgVB4RatUAiDqF502a3jDq3lle5qc4uNxVFwa8Epz2kQ8ysxjYj2mFrHTM8qsQ43Dao9xuw3n9FW8Ydf3cSxw5G7R7jdhvK1nD92EnhcStUZ4WsoHhA8IE8r+/preMOps+R/k4VS242cLp+41QHPhqLWC1xX4tfI9bdHuN2G84kSoUKAoCLZUKAoChQPpreMOv7uAMahGq86ys18zKzHTM7dHuN2G8q4TGDj6U8K5Byu263jDqqZqV7QnsLHWlUmhzw0rIIDi4I0SAjQIMSnst12qPcbsN5xt1VqtKtTdut4w64xVkYU3WuDk18B39p9e4clCv996qPu87VHuN2G84z4U4TuVvGHX93BrS42jEiE5sRtUe43YbyrdZwOhx9xuVvGHU1LOouhVHXOLlQ7jU19obHtAaOaNOUP8A48wF+4TzCrnUSNmj3G7Def0Vbxh1/d/QUe43YbypMxg/0pmIVyuVyBnareMOpYHdRa4wqjQ1xAKotDngFMfmEtITC2WNTjJnbo9xuw3nGFarVbpCt263jDr+7g0kGQjW9BCqQQfW5R7jdhvKnBxjCVI3K3jDrQTWgIiNCmtLjaFlAzaVaUaLwAY5WWR+SZRJ1KsdF0bFHuN2G8q0TOBBJUGEAVE8pnvbreMOrqFle5qe8uNxVF9rw4pttOXSmubo6fCDtGEHhEsEx6V7ZLrvH/pGoIlscbFHuN2G8/oq3jDr+7jH0AE8bNHuN2G8rWcDygeAFrKB4QJ0Xo7Vbxh1NnyP8nCqW3GzhdP3Gr8g6p/Scym0QfSfTa1ofHK6mMwwEA1pcwDwsts5cIU2yKccp4aGjT6qPcbsN5xIBUBQoUK0bVbxh1/dwBIMhB5EgLNdFsovcZTnl35LOfESs10Wys10WyiSefqo9xuw3lXCYwMzCuVyuCuVylXK7663jDqqZfXtansLTaVSbc8NKdSGmkaoUCSsiWthGlDL52qPcbsN5UYEGZCtVqDdVarVBVqI+ut4w60kVpCJnUqm614chV/5e0x0t0WYBaPSc8FsbVHuN2G84l2/W8Ydf3cGtLjATqbm6lASYREGNuj3G7DeVbrOH7kTqhzu1vGHUvDOouIVR1zi4Lp+41EC2HiNU/nUeV92uXzKhpJjxqq4AGnnXZo9xuw3n9FW8Ydf3f0FHuN2biriririririririririririririririripwLGnkLKZ6WUz0spnpZTPSymellM9LKZ6WUz0spnpZTPSymellM9LKZ6WUz0spnpZbPX/0T//EADkRAAEDAQUGAwUJAAIDAAAAAAEAAhEDBBASITETFBVAQVIiMFEgMlBxoQUjM0JhgZGxwTTRU3Dh/9oACAECAQE/AbqlRrBieVvj6n/HZP6lbK1O954HyC3Wv/5vosFrbo4FC2lmVobH9Jrg4S34LaLTsvCM3HoqVjLnbS0Zn6LAvCFiC8JTyNEbO6n95Zf4VCu2q3E34HWqikwvcrJRI++qe8U1+aEuyCezCFjTW4lUpJjuhVoYaTt5Z+6BBEj4FX+9rtpdBmnFMHVUqic7FqsgqLwZJVauNGp7DqF81Y/DioH8v9fArPnXqu+SfqmPDWw5B4JgKEalNnvlB4IyKBaEagcIF2lq+Y+BWbKtVb8l1VQKriwk00K1oqZF5VmBDvEmDqoKpi452ofL/fgVT7u0tf0dlc2IIcE0BgwMVpoGg/PQoN2jsAVOmS3woyE5wIyF1l8bn1vX/PgVoo7VhYrLX2jYd7w1uolgOatNCnVjGJVShTp5sGasehVZzDoiFaqhP3FPU/QJjAxoa34HaLOSdrS97+1QtbanhOTvS4OI0RM6qel1W157Ojm7+lQobMScydfgbntb7xW80u4Ktu1X33D+VtalL3agcP1Oab9oj8zD+2a4jT9D/Cd9o9Gs/nJYjV/GqgD0CpPs9MYWELeaXcFvNLuC3ml3BbzS7gt5pdwW80u4LeaXcFvNLuC3ml3BbzS7gt5pdwW80u4LeaXcFvNLuC3ml3BbzS7gt5pdwW80u4LeaXcFvNLuC3ml3BbzS7gt5pdwW80u4LeaXcFvNLuC3ml3BbzS7gt5pdwW80u4LeaXcFvNLuC3ml3BbxS7hd9r++32ZPwax/gNX2q0uqtAVqszQz7saZFCysYHhxkgJ1jIxZ6f6qlhcwTOiZZGNc5jnTkf2QsWIjC7IqtS2bolU7JiAl0E6J1JjLPJ1lU7MHM2jnQnWfBScHaynWEwCwznC3SXBlN0qvZjTAdOSpWbaDIoWaQPFmUbPDQ4lGyOz+cIWMnqn0cLQ4nMqjRYQwnqULNijAVVoFgDkyzzAJzK2GYE9JW6Owz+6dZsEYin2cFxaz1TbNijA7JVKYaA5pkKlsiWtIWxGygjxa/4jSZ4x6QhZDnnl/2nWXDJeU6yFoJ9FtGE+FitAaHw1WikxocR6+xYvwGr7Qq7Oux8JttqQQ8zKdbZxeHMp1qcaYpeiq2zH0zQtZ2jqkao2vMQ3IK0Wg1Y/RU7XhAlskaJ9cuZgPrKp12NohpE5rfXakdZTrcfyjrK3yHBzGwq1faZAQmWrDhy0QrwWmNELRDcICFscCDHRNtEAAjRPqYo/RU6+AARoqdcsAAVSoHZAKjXbIxjRC0wNM4hG0SMxmjaPDhARteeIDrKFow5MCL5YGXb0drtYW1MOHqt56EZZfRPrYgRCqWjHOWZTHYXB1z6+PFI19ixfgNX2mwvqtaFuwLW4D6rdSdDkthmc8lu+RdiT7O2DJjNCyu/MYW7ECXGExmKY6LdSHQ5VLN4jsz1hboSQGn9ELN1xZLZOx7Pqt2OLCChZp905LdfR2q3Z0a56o2dgxZ9At3OY+X1W6n8pnon0YbjBlMotdTxExmm2aThJzQsxyk6oWYmM8ym2VxA9VuxjXOJT6GFsk+fYvwGr7SqYKzXfom2kMgMHr9UbQJGR/lOtQdk4ZfVPqgtwNCdaGvnG3rK3yfe+hhMtWHp9VSqYHYkbUSII6ym2ktJcB1lb1mCB9VStGFuAo1JfjIRtegj+VvAaMLRln9U2vGHLRG1S3PX55J9cEER0H0QtMdPT6KlXg/uq2BtPA319ZW08AYt86x1nX+06uyGuIzzW3aAxxEn/wCoWvIT/aFfxAx0hG0DBgA/6TonLzrF+A1W+xvrEOYuF1lwusuF1lwusuF1lwusuF1lwusuF1lwusuF1lwusuF1lwusuF1lwusuF1lwusuF1lwusuF1lwusuF1lwusuF1lwusuF1lwusuF1lwusuF1lwusuF1lwusuF1lwusuF1lwusuF1lwusuF1lwusqFPZ0wzljc/VSs1KlZqVKlSpUqVKlSgc+aNztUU3W839PIGvNG5+vsz5Y15o3OElFDVQoUIBR5I15o3P1uClSsSnOVKd5A15o3P1vjzRrzRucYKKGq0uOnljXmjc/XkBrzRudqihqgoUXQoHkDXmjc/W+USpUqVPkDXmjc/W+FChQVChR7Y15o3OOdwWinJEqc1IUqfbGvNG5+vIDXmjc7XNFBQhdCgKIK1WVx9ga80bn63ypun2J9oa80bnCSjd1UKFHlDXmjc/W+c5QKlA+UNeaNz9eQGvNG5xgooarTzRrzRufryA15o3O1RuIRUKFCPkDXmjc/W+VKlSpU+QNeaNz9bgoUKPLGvNG5xzuGqlSLj5Q15o3P15Aa80bna5oput2XmDXmjc/W+VKnyxrzRucJKKChQoUeUNeaNz9bgpRKnNHyhrzRufryA15o3OMFFDX2T5I15o3P15Aa80bnaooaoZodPMGvNG5+t8qfMGvNG5+t8XQoUKPIGvNG5xg3BaezPkDXmjc/XkBrzRudrmim6r9VAUdU7W6Oijoj7Q15o3P19iVN0qVPtjXmjc4SbgoUKFGXlDXmjc/W4KbpU+UNeaNz9b480a80bnHNFDW/5XHyRrzRufryA15yFChQoUKFChQoUKFChQo/9E//xABFEAABAwIDBAYGCAUDAgcAAAABAAIDERIEITETIkFREDJAYXGBFCAjQnKhMDNQUmCRscFDYnOC0QXh8CSDFURTY3CS8f/aAAgBAQAGPwL1quNAvZgvWQa35rOT8gF9a5ZSu+S64PiFvx1+EqlbTyOX4OucaBUhFB94/wCF7Q3u/MrJtg/m/wAL2khPhks218V9W38l9W1btW+BXs5K/EqTMy7swqwOy5HMKyTcd+C88ydAtpOfLgFV3s2/P/ZezFEXONAE5kPXAqLsqpku60OfYe7hVYmGWQ3R5h2h0UDNsfaMqagLaSFpdcWiuVaFPfLu7M0PEKrc1cN13MK2cbvPgqO3o/mFc3MH8Ec3HQKrt+Ry2kub/kE0NFz3mjQnOxr2Na/q04FPjxTrnuG4TxH/AOpkvUe3IuJur5J0b3OeHG7zVXsDjSmaYQ2mz6qDoTaWknnqnYcNubI7ec3Wh7kdk22qLo3bjcqc0bfMK6PNnLkr2ZxnUfuFc3MH8DF7tAtrJ1naD9le/N5+Xd0R4iEVdEdOYK2LIXNrq540HctpbvUpVe0dny4q6OPL+Y0XuBe581c6Oo/lKo12fI5FWVtTMOBZT3tMu5BkYIboDTLo2jeode7vVh6j/kfwNZ7sf6r0h2nu/wCegR4g2NJ67f0WzfRzR76veaAKjPZs58St1XfydGaAfIK00WbC5Ve26Lx3lVhqHKkpEdKV/moiNDw8FRGE9U6eCsf1mZH8COk5JsPvP1P6q0aBP2Obh+q2XBx6rsya6nuVBk1q2pyp1Wf5RbLunoELGtApRdbosibcs+j0lm7zYeKZKOGYW3JAFCWj9lVVHWbmEyUdV2R89PV2GHpkM6oX2045Ko4+o5otyPJSNnNQBy+kc4cAvdHkpHTmtD9NU6BWYMUbzOpTfTGgh3/OCbKzRwr0+7+StiaHHuCpMwDxFFTEx072raRmoOiIFv5KTbmttKdgji/u/JPk5bo/dEsFTyQ9La6OnFuiMxdeNGlbOIVazNyIkFx4IyzGpPqiwUqM1SMefBXP35FRgy5cEM6tkOfcVs9mS129X3QVsto27lXokh/LzTZOY9SXEDSvy6I3cRuny6KTvAPJWRSb3I5J/iVM52gavrmo7B4fTl0ej3jafdVSrTJXwzVsUgry06LJpA08ihEyS5zshRbOWTe/NPkidc0tPRO+Q0AIVu1+RV7DUHotllFeSptaeIKuYag+pU5Khlr4ZqglA8clUKaDaASW6cVcOChdiJbCPc702KPJrdOkr+0qRrhmBUeXRLAfdzHmipvAKrsgqGWvhmuv8iqwPDvDoueaAKm0r4BWxSZ8jl0VOQVDJXwzVIZATy9R38oATT97P80XHgtsHBzX52nv70d23ZZEeCcXDN+dCjJJmT60fgtjg4iBzotrijT9VRgoByRjiic1vhmjIN11N7u5oixtlNf3qo3k1NE13MU/JOZ91x/z0yP4kUHmsQ46yZDy6JcOfiCfPyGXii95qSvSbiJTm2ncjdqpgfuqSH7pQYdJBRFx0Cnx7vLzRwsRpG3XvKe6YXNYNE5kWTdQjHKavj/RH4Qmui69ck1t95cKnxWJ5f7dDo5HERNzIHEp0AzGoXorjuP08U3Dwmhfqe5N9L6vfpVFojDTwIXo5rY75H1DCw+yZl4oulFWsFaKsQo14rRCB59m/LwKlxhHtAOhuJkBvDufL1Cv7SpHO4ig8+iWc+/kPJFTeAUeHB3aVTKRtNRmSENiKXCtFG9h4hF78gM1U5MHVajipm3Z0bVMxMLbTWhomPf1hkfJGGM+yb81JLM24NyAKfHHkAahe067Mj0zu7/2TB/KFdS7hTxTQYnNGuZ3Qn2auPHvW0xLnOceSF0Jce8phhj2dOmgzWlvim4aRpNo1XUK6hXUK6hUuWV2h706SLD/APTE5mvzpyW5ooz/ADfspR4dMWGHxFRxd2ali5FRng7d/NAc3Doh8FIP5ipfhTMSPe3SmyN1aaraM/jAAeau960vKqVJe0uvpohMxpblTNFv3mo/CFCP5gttMXV0yKkih0oTn0Sn+ZD4AoSPvhMxTcw3I9FGOub90q1u48e70vdyB6Jj4KE+KqFI7mzoHxH1CrcEbX08FWd1fFyuxT69zVYwUARU3gFfjQ2nehB/p8XcK/4TX4w1kkFVH8QTqe8QOiPz/VBk4qAaqTZC0NaaU6HMewuuNckZ2C0EDVSs5tr0zjvP6JvgEYs2hhzKtuebW+8v7h02OXFdVZBZKN3j6sviE6A4q2EE1YRveFU1zdCMlH8X7KXy6XNgFxBoPJe98kJccDc7j4IOHBMnbzB6IfBP+IqX4VI0ajeHl0YbAHRhT4xxaR0TMeAdCuo38k7Dtise2ufgj8IUPxdD/hPRMzvBX9oUI76/kjBI0vPJbbCGwnO06LZzttKZIzUHpczmKItPBTN8FC3uKDBxUreTegfEfUK/tPqFTeAUl2jTaPJVd7rSQo/hUfxBOI90g9EfdX9VK9hoQ05rEtmNxAoOiVrgCQ5dRv5J0ccVjm9Mg50KZ3Cn5KVjGi1z65q6ZoG7wTqcKFE2LJui2jmZDVZq6toVarPUaprYm1DV1F1F1F1E+7Uu/RSViiDM/HxTRrkmN8SpHc3fp0STfdCkxLuGX59F41jNeh2Efw3f3CMUwoQvQoY9o73e5EO1Uvw9EkXCuXgn4k+7kOh2IwzbmOzIHBbWlRo4L/p2ufIdBROxOIykk4I/CFD8XQ/4SpH1tp1fFHaM1yIPFGYjN3AI4mcUe7QcghioBVwFCEMPjWndyqmRwA7prUps0jaRtNc+PqHFYYXB3WAVZsmuFCrouqBQIYrEi0N6oU3w9A+I+oV/afUKm8AnS09m81qhMBdwIX/iD47WaAdyD+RqpGOjLW0pnxqjldHwcEYMR1a1BRwf+ntLy/U0QiPWObkZYG3RnPLgrnCrXZOCMeDa58jshkjJN9ZJ8ulj+e6nxcjX8+ksPFHCOjq4bq3+K9ofJOEaz6HPTXHU5rJWyRfNUbk/kVs3wZ8M0GKhbXxPQ9/BuX5JgOup8/W0WSpI0O8V7Nob4LT199jT4hezYG+A6dPUo8A+K9mwN8B0+0YHeIVWxtHl62+wO8Qtxgb4Dp09XT1dFkqFVEba+HRWxtfDpqY2/kqMAHh078bT5L2bQ3w9QhuozHkmTjQ5Hz9T0kDJ2TlZBrzW0lNBzKbDAK55lVGS1WzGgTPBWYnMfeXB7Sr4Mxy4oOnzbEcvHpL+SZB945/v+BXRHqvzH7rZv6zOm12YKc5+8NR3K0brOSAcaDmqbQKm1C3XXJvw9FYz5IBu4TkXcAgxmg6bR1Wfr/sjiD72nh+BacRmD3raNyezUfsr2rNUhyb9/wDwjBhxVp6zjn/+q7C73cdVR4pTghdFqKhfVfNEsFByTfBUCvxG43lxWzY2+Llx/wB1dhDUfcP/ADJW9V3IrZx9c/JbBvVHW/53qn4G20XWGo5rbQ+Y/wCcVtpzu/c/yvabrPu/5VB0UlaHL2Uhb80WbVtD3LORqEbpTQcsl7JgHf03t3X8wti9odINHDh/hbNm886k/qVY38EbWHJ3HvV7dyVvNWTbjvkfornGgVIMh97/AArIPNyo3zPP8Fb2RGhGqpIL28x+4VYH1byOYXtWFvhmFuPB9Wl1TyGa9kyne7/C9oTI/l/sqzbrfujVWtFB+CKVWq1Wq1V3VdzC3Hh/jkvax/Kq3XlvmQspz/8AZZzH/wCy+/8AmVRjKeOS9rJ5NyVI8lqtVqtVqtVqtVqtVqtVqtVqtVqtVqtVqtVqtVqtVqtVqtVqtVqtVqtVqtVqtVqtVqtVqtVqtVqtVqtVqtVqtVqtVqtVqtVqtVqtVqtVqtek/RaLT8Gn8MHoMTWgtBzVR0PwxAo2vreiUFOfl65lpVXTMDWEVHS+Ue6FtXCmfYzKRVMc0dZwH0hdyW3jbma0CBlFHcR6oi/lLlA/K2U0PcnS8lc8UcDQhSQgbrOPepA4Usdb6xt1UNzRSStfJOiwjRRmrnI4fENtkGeWh6HTyDq107k2WlLvoSnSngFLI9w2hNR5Il2sSdiHylueVFK2U71DVS4WZ1aaVU2KJOzZoE6XEYjZcgponuuLBkUaykNaUY4nWnn5JjhIXsdrVNje4sip7qBw+IuZxa7XoZhYDQv4oYiOYupqo8XWgPuoPLy68DLkqsNDUIYkzG6lQFLtM3NFF7I0dnqi92KrNyBQm985VQxDZjtDnRMY7KV2pCE0OJvfxCbJ94VTQH7NnvHimPwkxc6uYrVRxRvLQ5uahdE9xD3WkErZ4p7mR03aJ4imvicMs8wnEnfbuealEpq+KoUGHL7TJm5xW1ws5LhwJrVQNicWiRqsDi7xV9TkVGxhJ3xrmjiWyv2jc9VDGHbPa5l3ILa4Sclw1BNaoPHFNwbXFraXOoodk91jnjIlFlctnosO0HI1T455dnHHwGVU2GGXaRv86FSNx0jmEHdGgonsfLe33SNaJ0wJqWnisPHGd+XKqM0Ejr255nVYV0RtMhTcRFI4kHOp16L3NqaUREQps94LDRD398+AU0Z0eL0+c/xXEqe55DA85BTYdzi5rBcKoy4ue0nRoNKKeC++wVa5MxMsjstAnSYqawV3Wg0T8LftWUqCgQNMgnS0oNTRHHTZXCjR3dEoJIoXHJNfU7w+hKZBE0muZWdfzUmHodnIKVTsO6IvqcqJ75m0qCm4iAGpFDRbJ2rhn5p0WIw+05GikfJE2OoNKDNOvBG9xRmiYXU/wmN2ZYxutUKxCWHwzTH4WJ0TRrXoZioBUs4IYdkJbXWqbAwXFtNFGD91HxCEOyJJG6e5StcN9/BGNoIdXRGCPDnaUzdRbKlHtNaFDDNidtBlVMc/elbqmshwgv41GSDch4KKSRpfENQEBhYSGtIJNKKJ4GVpUForR6LcRHfCdKCqM0DDHFSmfErZgezc4PPktm0ezltLvJR4iNt+z1b3IMwsG+fvNyCw5pkAehwYKnIqNzGO64yopA3M0WHxDG3GMZtQZhYN8/ebkFRNxjG3ClrgFBawhl4zKGKtLmFtpooJQwhgrqnyPjvjkzqBWhVYorIhxIzTo8dFdnukCuSmcGljH9VpT8KWODmtPBYeRg34s6IwwROvdlmNFhI2ithzVGiuY6S08UXSimyFgTJoRU5t/NMj5BT3ClZCp6jdc0Cq2GJiJpo4CtVK90YYCN0UzTGuFE6HERXCtWuAqnSGMRs93LPoN7C8cgh0SMIoSXIQOaWlnP6E9k2TTTNMiPuinbIi00sdd9jnoM1K0QPrvxD4y23gg/mK/QOk1tFU2XS4dgfC3VmqkZSmzNOglzLKGmfqulpW1B/MV+yymRQ9eQ0RJlMjeNVFh4ja546x4BCUT7VvvApkG02TC2t3NU2m1iI14p9kmzjYaZalTNlNbRVr0zESS5fdUzpDU1TZBNa6mTeCE1N8m3zW1GKueM6ZUUUjDZtDm7kmvgm2zPer0Pgeagi5qLXH2bqhvksWa7jG2jxTXCazLdARxD83jLzW19K9prTKibiI8nVoU/FTyX5dXvW1dirXnOmVFc7rDIp0keoQkw2J2j+R0UeGhyfJx5Lb7UyNHWBUUeHdQSNTI2ybTa5b3BRXSmRshoap0DJNkyPU8Sm4d0m1bJoeIU3tDu0r3rENjfs23Zkap+EldfQVBU8hdvNJohPti00qAE2f+I7d80ZhMXOAzHBMe81JCk8EJdtaQ3IDRCWEVkKMvpW+OCgliyc8hekCYvLcy06LDnDGm1QxG1MgHWBTMNC6y4XF3cmO221Y40NdQvRon2gtr4KOJ8hkZJzT4XzbEN0HNObM69vuu7eVFiWiuyOfgiyDeJ17lDiHtvjAo4IMwkQkeUIsVGNlTJ3eh6DXZ03+SkgxG6C65pU7mj2QbunmovBTfEUA76wDqptRVwdeQqsjBf8AdpxUd8IsPXHJMP8Ap1dpXhpTojxbNW5fmoJmjejNT5p1es5pcfNR/CnsZrdVVdGL/u0zqm7mzq4G0J8bdV7aMCQainFXbPZXcEXRtvPJXQbsvAN5rD4uQXWij16Nhd9z/ksO3kwhYbxKw/8AUTzi2VZJmHIDCRAtbq9Stky2lLVifjT/AOmsT8RTPgTCwVcx11E5sYJeRpTRR+Ck8EPg/ZR2VArvU5Jwwrb3kLCuGdHBGGCrpH5UWEyrYvRsNvOf8kyaZt8Vtp7k2PCRNkJ17kf6aw3mnRY9gbTqu5hSbCuw4V5p1ld007cejToyCzWSz9SvRWnRkPXrTprTprToyHqZrL1rYXWO4IwljWXZF1U2Me6KepkFHs/deCqqB7dGHNZdGXTn9gH/AOLqD6LVarVarVarVarVarVarVarVarVarVarVarVarVarVarVarVarVarX1MlqtVqtVqtVqtVqtVqtVqtVqtVqtVqtVqtVqtVqtVqtVqtVqtUAT2ZuHed53Q80pY631tnXePDt21k0VRx6S7kE2alK9nHYW7Mbzk7ajeb6mJxnEnc8GpkvMZqaPCgDfJLnJ2ExQG0bnUcQnxYO0NjyLnc16JjALiKtcOKmw56rKUUWHHVeCSmULL6G3wQ21L+NO0tbFq5XPGYNOl3iEyRncvSaNDBnbxovSuYyCMsgaWkdUahNldoK/qtsyxoOYanTP3SzrBbaK1jToCnh4tkj1CrEGimpKdhoLW2DMle3pd3dgHYdnMKhWQig6XW9Z+6PNCJk4A5WqXAyat3gponGji8nNOkjzaxlCVNhpDa8PrmotlnsgbipxIaXgUUOzNbQQoHOyFpVW59ptlFVs4hQdLvEJni1SfConD3aFF9w3hkqN4Z/NNkuFKLFSNH1hJCZvAWihWJxDeq7IL+4ovbJZI3iCnbQ3WupXn2AdlaZG1tzHRt7d/SqtdGKVqrIW2hVmYCVbC0NXtmXUTSxgFmipM26iEcYoB22yQVCseKgIsdoVswN3SiubGKrZsFArtmFQK90YqrWigVkYoFfIwEqxgoB2AfhgdmEFtRlceVfoHSxdYUTSeI7aZqVor6UI1CdhaaCtfWhaz33UPZB2FuxNATmQn7Y3AHInpLjoFPM7rTGo8tExx1GR8lI10xaxrzpqp8BI8utG67jmnRzHfiNDVYnEteRU7ngEJmHfeAPNOklO/FUHyV0zi5ziDn4r0gzuDw2oA0QxZ61PmtqcZ7Wletl+SOIBo/mPFMxc8p52jSifJLiNkwGjWg0UmEe/ahmbXd3Y2CMlrTqQqzZ0OR6XeIUc/wDDmADvFSfAnQbXZRx+RJTcOJdqx48aIulnLG8AMlLhnP2gbmCpodoWsa7zWGY03ODsiV6S2ZznN1B0WHxYJsd1h4pkMJo6R1MlTsA7DR2ao0UHTsWdaU2oC9/5qXB+6d5qn/qFT/CE8xf+ZbTzTYeQTcARuxSF/kpMGzTEFp/ynNbwoj8H7IRs62vzQ20Ya8ai1PMLbWnhSnFRj+UJ8GPYK1ycRqE70VlKcaU7HRwqqDpd4hbJ3FqdtRvNZb+SkdimVjkzBpVAYdgrzAUv/iH9ldKKUxtsaWigWK+JYdzBcQ7RGCAEyPypTRejcQ35oSyfwW2+fYR2qJ7vqo8/P1606c+2ZLP1Ko2dbgrX9Ymp7CPsa3j9pDsNTkqtNemSVmoCGIbK05XW0TZqUqrS4V8ei0uFVhg00q9ZmiyKtrmqErfcAslUuFFVxoFhdm7Jz+HRNjToTa3wCsuFeXYauNFUdJldwW02giroKL0TFdbUEcVTis1HFlvnPuVRn2YdhbsRUA5gJ+1FATkD0y+Ca2Ky0ty50WyYLLMnJ0Y3ncxU/Nbau8Mh+aaZW3PcKl3HNYTU2O816dNnaaNZyTmcJ21816Z7j3mNE8IG/Mpwl33nXUlSsb1Wu3a8k58+8A4gDkjBJ9XC3qrDGHIOfpwTiOs7dHmthF17fmmwYhlk4+9rXx7CwxguaNQFSbLPIdMQ4X9GGpqv+2sL8Sg3frDvd6siFB2YdlkjjFSQmxDCmrRStVM159pNWq9Ebh6O0u4eKOFl3XGqGHOHLnNyurksK6lS11XUXpGGbcyTrtH6qOfDiskZTYmj2jd7zT5JxSSR1Snw7AvJdUOHFTbcULnVRbKLTcV6ZAzaNcKOCgmdHYxrtOPiVHB7sW+UTELncAhD6OWGvWPBU7IY9DwWzlgLyOIXpmKyOjW8k3FwtvytIUMzo7WtdpxUU8LbtmdFc9lncezD7FfK3rSa/aY/DA7DtJjQK+E1HTKW5GiZKJpGucK6p2BxRucM2u5hUldnyXsXVpwThGa2GhToGnfbqEMPXfIrREufoaeauhdWnarpTSq2kRqOlxaaZhN8FI2uVicHupZqqROzHBWyHPkFtmO108U1jnVl4q0u05K9hq0qsTi5grmU3f62ise7PuzQkjNQfpx2FuzO81O2p3ndMvgovhCZb7jM1iJ35vvosO+PIvqCpAerM278lFjnf+Yc4H9lPieDdwLESkbweQpWtyBZXtTXRat4K15zJr0u8Qm+Cl+BTucK20osO9ursin4qNm0a/XmFI+PkfzUZ7kRhIL289AphpvnJP8A7k19M3aqScM2kcmZpqEJItD9OOyvhbkXBCISsAGWiLq3vdq4p0+CcBf1muXpWLcHPGQA0CYYja5v6FNw8Zo5lKHwQjObtSpS412jrk7FV3S2nbjE/QrZRSNLRpVOe918j9Sp3QOtc2ngV6Ti3AlugCOycwtOleCdE41L63HxWxe5piH5owwvbZXU6hSNcbrjVOw7jma/NNidqEQxzC06V1C2da8T9OPt90zes/XtI7LUrrj81tnnd5qo9e+I1GnbqOcAqjtQ7DfJx0CLo8qag9Mvgo3vjFS0JzGCgFEx8nEAADitm+J8Z71sWNMj+TUWULHt1adUXMFKGhBUkbNY0/DAGrBUlGNrXSPuJo1XMypqDwRihY6Ut1t4I2ZFuoOoVI2Oe6pyHcmVBLn6NGq2EjHRPOgdx7GL8yeAW0j6fZ9ZxoF7UXuOpKbAw+yl4clJEBQxpuGpVzs0Y4mOlLdbUSzhqCtxjnHkEI2sc95FaBXWllNQ5ExRve0cQtu07qv2L7PvUXpBO5SqAMT2h2hP0w7CG1tLdERW5ztemXwUb3A1IHFOHgo7HWvZQtKGExjQHEZOGhWJMBYDfnchip3M6tN1YlvBzdoFh5Hfxmb3jqpcWdZXH8k48bysXTuTjAWdY1u1qpMROW7491TO43lHYlt1mVyifM5ns3Vyr2MEG1zVYDUnM9LJPuvQcNCoGN9zMpk3uy7pU+L/ALW+SrCWUqa11Uk0xG/91SO43lelYUi6lCDxU+7a9goQmbEx2071LJOQc7sk6S1jWFvHWibEcqj90yHFgFrsg8fTDsroSaXINbingBejPeT/ADJgjkMZZxC9InkMrxkO5ekYeQxPOtOK2kszpD8k1xdbahHW23RNhbo0UWzBuzqpZ612nBOlwspiu1GoRMkpkJ5pwBrc65Nla4xyN0cEHzYhz6cNOzljxUFWw4hzW8kSN5ztXFbMmnGqETeCL8NKY7tRwRL5DITzRYDWpqi+KYsrw1Rb1i/rE8VTDzFjeWqdDK4vu1JWxfO4x8k3DteRb7wTX4iUyWaD6YfhgdlMkmQCD26H7GveaAK0O1+jZUVvdTsA7CC0Vc5OvFHN6ZfBMmaKkNC9JiiGzGeepTZm+99gNawVc7mryKEZHpBYK2uqQgG0y90prWi578gFtMQxpZxt4JkrBcHH5ITyRAR/NekRxjZDnrRelHq0qtvs22628aJmKjFWk5raNzJ0XpX8tVh5ZBncDQLbyRAR8uK22tdBzW2ljbZxA1CZOzMOI/JbSVgZFSvetphmNs4XcUSRa5poR9COw7OYKyEdMvgh8LV/Z+yZ5/YAEo0WziGXSA80rohPHuy1ypxWGllyFKeaeD72QWGa7UOan+SP9P8AZNpw/wAoPbiX0I5p2GjNwoaKCJ38Npu/RN/03iJKHwUH9QKTwWHdoBSpW9iXlp71FG3MBwT2t1LSmls7204Dgn2vLy451+hHZTHJmCvR3Dc5LZ8KUQiiFGj7EtlbVXtbn3qyUVCvDakc0BIK0NUY5MwVs/dpRbJo3Qq2+VUGMFAE58YoXar0gN30NoK2moRjfoVsiKt0our5VQjeMm6dF5bQnlkrIhQfQj8MDsrpiK2ouaKEahNwlM3NrX7FAZEZPBFjcO4luqbNZYS6lD2odhbscrjqn7bO3Q9MvgsPjR1JGhr1H/TXocL9mAKuco4nS7aOXLPUFbs2zZ3ap2DlftBbcCpcLC+1uWfJBkj7zz7WxsRtrxV0vA0r6mK8Qm0++EcTtsxnbwTcQ3V9KeaMwxNXjOnBNxMe6TRekzyZUrYjMZtkPdAUkMpq6M68+xjsNrxUd6tjFo7umXwTIX6OYEyKbrRNLVtMY2sUg15FNEAaX8KJ7Me6kYG4OC/6YUZYsT4DtlsguHerWig9TFeITfjCl+FRForbQ0VxtHchshRuVFbzC2WMaGyN1qneiDx7GOy0KoFdTNUcKrcaB4LfaD4qoCupn9g1A1WYqqFUCusFfBUIr0b7QVQZdjH4YHZTK/Rq2sNsTDpdqhhMa0Bx6rhofsUyx6oHmn4f3Q2vRl2gdhbsq213qJ21rbXdr0nxCaBpRYVw1vUMbXWXNOYUM+GqKutOevb2WVLONF7bnlXpd4hN8FJ8Cnjc4hmVQOKiEPUlyI7QOyuh5oQ4mB7i3Krc6puJxDNmyPqtOqgkA3Q05qKwVpIPsN0XNCKaB5c3LLin4ucWufoOQU72MvGVaapkxYWRxc+J7QPt+Wev1naR9v5fQZ9hHYb5XWhXxOuHS6Y8EJsRM5l2Ya3KiZh537SOTqk6p0TXVc3UJrXuoXaKkzw1XxOuCLnSDI080HszB7XdK61Xxmo9Q4oO9i19lOj2rrd4q+M3BXOeKVoto9wDSrY3glVlcGqkTwSrpXWhYcxOu9p2EdhbsdWnRO22ruHT/cE0jksK0a3qLEe7LuFE+7hx8yppzAZnF1K8k+XZGKN7dO9YmSQXWvyqrW5DtbXRZ28FbJqTXpfJx4L0Yu3iOR1Qrq3IoyubVxPFTws6tK0U0jxcbyM0In9SNlQEMQwWvY4aJjpW3yHqilSoHCIxePFWSZiNtQFBIwUJeB2EdldE/Ry2UdkrBpVel4xwLx1QNArGdYZhHa5yPNXJ78Ja5khqWnmicTaBwDVO5/8AEdUfYETfcBq7omp9W/MeKpHa6vulPmnNZJNVJf7zqpuKwxF4yIPFN9KtbG01oOKZisPQlopQqKeW0WHQJuKw1LhkQeKjllIFruqOwj8MD7AukNo+whG47ztB2YdhvlKvi4dI2w0UmHINjW1Gatz3jpqVsjcxx0uFEcL74FU3DO6z9EwP1eaBGKjnuGtoqiYuGoKc01q020TZ35B+g4oMbVrrhk7JAycdANSi0Nc0jg4U7MDLxW0i06XeIRnYSxwFa1TZpuVSq2vt+9TJbSu7rVe9b96mSwvmhh9XHlw7KOwgNNHNTrjVzvUm+AfsnX+4zdTH+8Hiiw+L77Heamxzf4D2geWqYR1YmXeZT4sHFdb1iTQLEbUBrqCtFipKZh1FC1+gbUeKY7iHBRz4cjaRaVRw87NnK3szXMNHNVhNScz0u8Qh7R9aVzNQjLP7mRTnCICMt4nOiZ30QYOrRQbHrUNE7/1a79deyjsz8XXrClE3EQv2creKbJjJdpZoAKBGGtK8UcK43XVqfFOudc53FPlwstm01BFU+YvvvHFTSVrtTVNkY6yRmhVcRNc4aZZIPw8tlOFKhHEzv2khFOWXajDWlVY7E7vcEcKOqVsHzbmmQzQwkmYpRbL0j2fhmopq/VIYmJ1p97v7KPwwOyl7zQBB7cwVsa75FaeowP8A4htH04wvvEV7FfIaBfWhXDQ9qHYQ6lxdoiaWubr0y+Ci+EJn9NHDNjvNMqIYbFx7Mu6qbBCzaSu4LDx4iPZuEgPcmxsbfI/RqazGR2B+QINUyO264J82Mjst0HNCaSABp71HNKyhcaEV0V0kWzipUEovwkNzBxJpVOytezrNRkItoaKbD0psqZp8MENz2up3UT4ZmWSM1COHwke0LOsToml7CxwZQg/TAAXOcr6UIyPS7xCaKx1p3IbGO8lNZio7Q/IEFNhibfI9Oui3hpnkU8NjqC/PPTsw7DZJw0Ksj46k9MobyUZbwFFVmYjZmn/01hPEo3+8zJYZlc7wVFv7OrcigJsQ5wBroFhvAqh4kUQw8rowziWnVQj/ANwKUN1tKjt4BYmRnVyHmnt4h5WKc01GSxP9RSf01iWO616baa0Zn9ML8iOIWzj6XeITTs26clHDDS6Q6lQ7WW/fGVKIPa6yRmhCfhcRQuZxCmH/ALh7MOzFzbmV+6VbCKI4n3yKJkrtY9ENpq3QjVA5kg3VrmrZRWiDnOe+3SpTMQ7rM0QAFWXb9OSIw4rJ7tta1UTMXW5tD0FzS5ldQ05LZwigRk3m3a0Oqc+IUup8k8x++alHE+8RRbU1a7m00TZIxQtFOxbKTQrV/wCabG/3dDxQLrnEGtSVeSWu5tKOz1OpOq2wqDWuR7MPsDIfbA7M3DurU8eXRIXCljrfsQyuzAQdzUkIGcfah2Fux1cdU7bZlvHpqViMV7zzVv8AamSceKxDjoJCjNHIIWe6KVT8JifrGcRxCkjjcGhjjvfspMJic3MzrzUmwkETGG0ZVqpNu+thtpTtTWxZXcVdJqDSvS7xCAtfpyWIkHGie2B+zYw0rrUp2ExGbgKg81LDEQ0Nd1lHhIyDK4ZuTDM/aRvNNKU7KOw2StuCsibaOktb1pN0IMbiXADhRS4FxrTeCxTBxe4IQTODHx5EFS4tn1dLQeaxH9Qqb4Aprpzh3lxq22oRGGffxceNe1WytuCsjFB0u8Qm+AWJ8lLDi8nXVGdMkfR83gc6rE/Go5MR9W4UqhmCa5b3ZR2UF4Bt06NraLuaNgpXNXSRtcVa0UCNgpVF4G8eKdFJhzIa1ubxUmLkZs78g3t1rxUdBeBQnVe0aHeKpG0N8ES0UrqrXCoVWRgHso/DA7K+ZurQmSH3hVNwlN0tr6kAjNLn0P2CZWipCDufqEEDZU869EUIO6Wmo7KOwt2Vba71E7a1tru16ZfBRfCEz+mnQNkLGlgrRRNicSyXIgoYIOLWNbc6nFYe15LC8ZHgUzCGTZMIq4qOTBTXZ0c2tclDFC624FENc51efaWWVs40XtueVeXS7xCYWGhq1OxG1cZW51TZmamlaIS4eQnnmnNrlYpA9xEcWVAomFxcKGlU6GaTZxx+VSmR4aS+OTUVrTsY7K+H7wQw+LjcHMyyGqOOkbY2lrQU91MtmsKQNCUMZaXRuba6nBQOjjcI2vGZUeNDNo0Cjgg2KEkn+XRYdwGVD25waK5hMAHFqkA+6oi5hOWdFHLhGFtOuaUCc6mVil2jSY5cwQopiwtbQ0qnTvZfHJ50KDYoz40p2MfhgfhgdhukNo71dGbh3eo58J9lCQHIOGhU7pXUG0OqujcHDuXtXhviro3Bw7lWMhw7ls6i7Witkka0+KqM+03SG0d6uaajpMp8ltJZywngF6HijdXquUWHc+ked2aZh4H7rnZ58EfRzUHvr2cdhbsc7TonbbK7h0ulPuhPjnkF8tSUGnWPJTSyi6jyADonQxZMeytFNipxebqCvBRiHdbMMwpsL7r99qxX+o8qtb5K6TDPlc/MutrVSMc1zWXbl3LtLHRC63grZdSa06YuV/RhqaqJtoo4GqgtaBV4CtjFo7uzjsrYYtHO3vBUsH5KR7B7KQfNSPY0PD3Hdr807G4rJxFAOQTzhmbWOQ1pWlF6ZiwG2ijWhMnwv1jcvIoYZ3LPzWwbGJmDqmtFXENDTyHbTEVszGJKcao4vFHf0AHBRTAbrQaqLZjqvqe0D8MD8DWE5nh2Idh2kxoFfCaj1JMVEd3DkD/K23u0qhLGatKLoTWhopG3fVdZUuPjTJMa4/WdVGWQ0aEHt0ParpTSq2kRqOkvb1jkFdiXuc896GEe69j+rVbCu/Sqax5zfkEyNrsgd/Jbe7cHFbNpNTpUIm47S3ThRWyHM8FA+I1FpXtTqqRnMcD2AdhaIzRzU7aHedy6Xy8gjBIyQl9bt3mjA/WM08li8PxjO7/cnwv0dGH/AJap+KxOkhLin24akZbqSP0Ubx1owHDyWGgZ/Gzd4BUHamujObeatcakmvTG7gH9GHaNRmv+2sL8SwvijGzI6psGKjsfw5J39NYiR3WBooi0UuBqpXMj2haKCppRRzujDLdc+wDsrYwaNuq5UTsS07jxmO9CZrqNyuHOia6J1pGXkV6LoKUXo0krQylMtSmQPzoKFSyjNrN1vbzE/QrZxStLeFyM8rr5HcU3E4d1r25ZqOeaQFzTpwTHxOtfGahDfAkryyTJcU5vs9A1HE1yLaI4jCOAv1BTMTLJcRr/ALL0nDOtfoa6FXTvbbyaOwDtlFsohQfbY7Lc40AVBK38+kuPBCWPQ9F0Trh6j4W9aPXtlzzQK5pqFs673L6D0f3qV7EOwhzhUnQIlotLdR0y+CDpQ0GzXjVM2vz5IuwsBkYPe0T5GcjUck2OCIyluq2jcuBCdLDHYATkrsNC5549y2jcuBCd6NC6RjdXLEyN0NO0CouLltG5cx0u8QvRHdV4uYpPgWwiYZJOS2ErDHJrRPjcLHR6hPkijLgDQd6DNmXAu61egwwRmVzdUHULSGUIK2ETDJJyC2E7DE86d6ETGl8h4BCHExmIu0QjcK1FUdpGY6c/pB2ENeaEaFFrTUnU9MvgmOczMt1WJirds6hpTDDO1reVqnL33l9TpRM81iv6hUni5M8/1WNDfvFNp31WLLdKjtAqbSOK2bM+/pd4hMlj68QBCMo4xrECN4Ye8VyTMRNLdZ3UR9G67W79OSaYerRf3HoxAPWvQp9xYm7rV+Swwb1rlNdrbkomjrX5KCvI/SjsroX6OVu2kt5VWyiFAiYJXxA8GnJOiqTdqTqhCzMBSFp+sNSjA0mhr80IWZgKRzT9YalExvfG12rWnJOdF71MvDthifkCg3knTR13uC2oJY8cWraOkc895TqZl5qSU7Zk0ca0W0je4CtbeHRtmOdG462oSsrUCnitqCWP5tW1JL383IPqWvHvBbZ7nSPGhcmYgnNn0o/DA7KZX6NVSHgc6K+J1w+hueaAKo6HSHO0VQlaKB3PtBlk0C3rm+IRm1AzyQkGV3QzKt7rei55oOyjsLdkN5ydtdW9MvgovhC2UeTJW1onwYOjRHq480MJjKb3VcFNh5tYzl4J+HPU93yXozOoxtXKSKENFjjvHkn4XFUvZnUcQpBhy1jYzTPUret03/8AZAylpbZugIy1YKZ2o4poo60/mF6XlfbVMm3WxcuJT34dzGNaaUKG063GnYWti1dxVzxmDTpd4hbFpvcW0A71n12NTcQRVzgPzKErnModctFAX5uv4L0h4bZxag9pGzNPFf8AUFp5U7IOw7OYVCshFB0y+CjvkaKNHFHGN+rYLW96xGHlNrr65qBsW9ss3FNxXB7S0rC4zi11Xf3KXFn+K75BYn+opP6adisPNs3jiD+qdLJ1qfug+Ihzmx1p5L0rE4l1TwBoneDl/b+6i+EJ2Nwk2zdrkcimSyakdhtlFVs4hQdLvEIOYKPaKghOk425+KiHA258k2OfFNcwcNKqD+oFJ4Jvg1Cw1y7IOyljxUFfVNVG5BVmYHKkLQ1WzNuAWyeKt5IMjFAE7ZilxqVtqb5yqr3RCqsplyRdEwNqtoIhVbFrdw8FsLdzkg1ugV+yFezWSCoVEYw3ddqExsLasac2jkjFFhnXHm2lFHHNvFn6oseKgrZ03dKI7JtteyD8MDsr5qVtQe3DCh/mQLxQ8R9imalaLaywbncUJGaHofI3UBMkfqR2cdhbscgTmQnbUkgHInpl8FGzYSGg1ATTtTEzjTVRRxTGVkmRBNaKOCB1t7UWmR0lfvduYIyWtOpCrNnnkel3iFZsHAEUqtpEbg1q2suKpIc8nZBSl/WaCCmSN1oAFeZ37T5J760kjOdO5NfH1pKAIyv6zAR5pm0NXHPPsQ7DQ5qjcumXwUXwhRHE19H4+Kw5wkdGh3WpSqh+A9vo4VVB0upzCtbc51NKJ8LsnPzogzERhr25GrVIYW2tIPCiY1nWABXHafdpxR2wzkNSPFMwjtIHOP8AhOwIG7K8OVOxD8BV7MPwwOwZrRaLRaLRaLRaLRaLRaLRaLRaLRaLRaLRaLRaLRaLRaLRaLRaLRaLT1KFaLRaLRaLRaLRaLRaLRaLRaLRaLRaLRaLRaLRaLRaLRaLRaLRVA+yQ7EOtBXpJdSOlaraQOuHRVFuHfcR2MuOgR9Hddbr9mAzuDAeaEkZq08U5kTw4t1ogJ3hpPNbHaC/ktpK61vNB7DUFFolFW6+SJgeH05IQOeA92gW0lda3mVfC64K95oBzWzjlaStpKbW80JDK20raROub3K2eQNKbdK3e0zUXxJjMQ61lgz5IjDSbQE5lWSytBRew1BHBTPlcGgN4r2Dw6ivmcGjvW1Erbea9IvGz+8mlsg3jQeKuneGjvW12rbTlVbcuFnNbbats0qtu14s5o7B4dRBkzw0nmti+VodyQ27w2qD4zUFWTSBpQj2jbjoEGTPDSeamjjcCWtNVO+VwaMtUdg8PpyV8zg0d6pDIHFDbvDK81s2PBcBWipPIGq+FwcO7oueaAKxkrSVfIbQOasjlaSjJIaNHFbbats0qtsx4LBxREEgcR2x8XvajxU0L+szqeajklyZKrz9Wz9Ao8fFlwPjwWHghzL8yO9Swu+sZk3zT8TIPrcvJOgflG7L/CfjmaMcKHlyUGHgzuoT4lMgb7oTMN7gFac0BhsK+JzeKum62QNe4p08pN2dvcp28A2qe7Eu/mKZHEd0558FF8S/7YTqfeKkZ/qMJfdx/cL/AKLqfunNm6rRVCGM5XU8ivRXGjQQ0JssBOtCCvL90MW59BG7IL0dxoLrR3AJjonEtdzTfganSyuPEN7k7DyOOzbvEDiU2CM5XU8iowPu/umTscS6tHVRxsz3GS2o8AsSf/TzH5KSSd5y/MkpkFbrXtoo/h/dSuLrnSMzT9tW1o4L0dhyqW+SImaXsjNLQmTYHDvhLVhpDkXNqjjHOue5lafNPnxcTsQSg/DxuZE80LT0R4YdWlyjlvdeKHuTMFWjcvmhPA41ac6p0kmtKfkU98zjkaNonYV7iIxm4DjRMiiOVw/I9tLsO2rH5+Cibhm1dFl5ImUUkec0+B3vBbbEstDNPFEwN9m81ry5psTNGiiZLhxV7cj4J+HlFHyZn9lt8Uy2zTxTXwE7PLjl3puIw/1jE2Omzp72i2A9pJl5rZSto7PJSuxDLQQtp/pjw7la4VCZ6U++V1CVG2BtxBXowb7SwCidHMLHFxUkDht2ycf+aJ7ZOu/gpHTstBCGKDPZ3DNbfDPbtOLa0KE2PfoaNahFEKuI/dGOdtpuXpuBzJzy1BTZZ952gbkhhg32loFFspm2uqVI+dloIQxQZ7O4ZqOSFlwA/dCOBtzrgvR3N37CKKaPFMtvR9DzB4qOaUX5hznKN8DLgAnMGpbRS+kMtupRelWezurXyXp+AzdqQmukOwaP+aKLYNvtrVNjf90Apz8CL2ORnxj7WfdTXwk7LLjl3oPi+sZ81HE+4RtIrnwQxOG+sbwTYcRUNHNOweHFxoiydtpuUkkzLWkH9U3EsZWMFuf2bt8FLs16TjJdq4Z/QbaN1kib6ZiLmtQiZk1uQ/8Agf8A/8QALBABAAICAQMCBgIDAQEBAAAAAQARITFBUWFxEIEgQFCRofCxwTDR4fFgcP/aAAgBAQABPyH4qFg5cTU7gFH3amlnva/qOwngf0x5vuH+of8AeP6i+P2vFTX3f/Q/7n5Bq/P/AMcjEG1nsSH+P9pRKLq/wB/RMuHU2+z/AHLPsVB/v8wD+WX+Wf8AlIir7BHjeQn4kD+ypmx6v/V+ICfvj49oGG3Q6fD/APFhOxO2P8LX8Dqzfv5f695Xhtt5fLDJAtXpGQOABA4fEzTXKlVbydeIpiHSobH5jpGWtFUGu+ZmJAOuBfMPAzEdmbE4hZgPJG37xs0zCBXH+XSbt+9/tPzA6GQJ/wDEEKNHrf8AIjnYOOex0CCXbv6f9ztSwbfPQhSaAbLo6ZJR+YHwnFahFyu7U9NAlgca2qtdlVWZi3C2TRC1dwWVcTSYXj14ZkSLokz9nSFoMF+0M2DZfmf7myAYSY7eTn4du066CHH6ZIHShYn/AMModBHBXiDjoJXeu3o6PS4tj2ik8y9BwrM2e5lWDB1KJhwFoZ+wzHjKuw/AtiuvyP8AZKuv2H9ygOncfs1Gsb9wMurV1Jd3D2pmCdziD59L7eY9X9Osrdljuf0/z/8ADYn3u/8AzOhDB/n3cRusbintJP8ATPMFS4Q77J1jYG0szq/ou38zP5u3a+WXnyfiWiclXmdFSmX8QWu8al9paGwO3X3jqmkvkv8AuLqAuYlDVrhjz4Fpq+DmIC2Rh5mF/wBXtEaX7x0ff/4TnhGDq8fmDi5f7HDI0FBLsi6B2cSnGgPhQ4E0SFq/zDGBk38938QGkHTFdZQqs7a6qYBQdpd7bl5C6EVW4Q4Gb4lpsl/o0zZIVn8jL9FgcDlPWKjqiVv9Bx7wWWnxbPv8I7RWq8v/ACYOzmHDnmEswLPgpbiB4PmCDDQAc/5Nh7p7ERp8Uk7ZBQHHb/MyegtY2MTQVwF8zUhrzymhbD39FSYNBq/rc5VdSr/Msp+i/wBofrXvV7MOgPaIeo0/rcMzAwAbv5Dvgq/h+WXDr/c/pKxmY6mIXTVt3mz+4Yvi5dG8y2wjTy7D23D7M/8AvP48m7EvxMu2VLXQuKJp1cr3miccrSCkw46+xxDCQ8NPLAJxTpXSedSjWWuA/tnsXwX6dgG/DI+zLi2F+efVQW6Idl5L6Wo9L2bH3MPRn7lH7EqCVoFPi5+q6xuqCr2uf+3O8m21fp/MBnrFBoCNRo6X+EutPSy/Ppjd92Zhf0UB3KwAbAaeag1qCnj0Frelwan5ZNH3qFgZwmT0YAnhl/Erz2B/UPE1kyfAicBtZSkjpf4S1Yel/lAS2PJLYDQvy/5DOxWTXbaGlVEJqhQ9N3ifkTZ+2pYwW/Ry9HJsZ2P0T8tn7XvHjAbWK0F2v8I7VzzKrxOr0fE3laJdnvjCJb6yH7+jhwNrM9o6X+E7ghYfs/Bdf1Llo7t964WtFftACYOhH/tHd2rF15QQKxZ9eYj1u1jllSpU8huV6VmjPY/0Ev4DmuXnpOz6hAOp/Z/qWJICm8q+3MrqG+JevuRtExfGM953A/uX/bP2Sv8A69ama9xYSr2D+X/Pp2yr+B/qDzX3jBHzPtWH1fVWDhPKGfMLOBJ+YivLXjj8S9GV79kwshb7TqBp+nQl441X3PYglcEtWwYLqI4vp7zEQYvVafTOp991wS/1b73ZiROt1539Gqw+YMEJQjSPRmbi2nQZ/MVAdQ2dnmAyqhr7S5W0uOCPGorJXDhoOfgo1pQORywVObNW6gwGLNDpli9sHk0kb/kjbWMa9Ehk0iOzHru8T8ibP21HmpUurh6E3Qjufpn5bP2veEYDvOrdQYRNAVs7wuI3joblcJqe4uSINTU9iPkVeA794f7UZGNsJR09vkYud2N16pUoWqORy/1ADVMgW7g3XFPA5JZlbX9ej60dAvwZ2P8A4JvVKAcuEwxrhanYgAVgzlauYhVFuH9xC6uu/GpiKFEKz9pXpS18MyhYPu+0tfcA5J/6RP8A3yf++T/2iIBS58AuK/vWXxpDgiAOFamHkH3UXun+P+evcC/4D+5e2wvk5Z0IWvDkl+NLb9d4wP8A5n0Nft2w9g/kn4b+ZX/A9w1+Jo4A9pdrP5AfxEJFLyCWfiI2RcwwewYcXCxAadlxOPX8J6ZNqWnhNAo9pYlZWzdegdUg/EJd1/kYv7QYSNDx9H0qKr3T26SglK3+x19UB277EVW3mF9r/MLwz/EQNhmOxwP3r4c7vE/IibJbd6QGFneqA6M/a4GMegOJ+Wz9r3lkwYK2+DmU1Ss+fioyFSnJxU/UdYD/ANZ/T0Orzb8o6jHbq3vLedGYGvRF1lU6R5oCtsEc4IfY/wDfU/sMiO1/ao0j0rCnFxWREo73YzT+2/TQhNEMuIcyvePXR85/mA1R2MQGCp3Dh8OXS/omhnxMByOhmsmPhNT9ZTd8fw+uKU2TXXEqDCKs+ybU1Z7Qtefuz0d/t2x2v6XPw38yoVn7no32e/C3+CdFYfaIjTsjKLisvrP/ACk4bxApFW/TP4P0/adPQOsPvCK69P7oAeLvGUqH3ICqfMD0r5Do8S/zg6PhjX0H177X3ExkKph+E/5h9kv8TMasD3nZlPtXw53eJ+RNn7a9dk/LZ+17xSWUPQwlBr94YP7n7fefqOs/7kBX9+nsw/KKofA4Qme0bVOr49Cpocg4T/kQL/ElejW0FVdbPW9+P3Cv6n70aQoTQrR2P9RLqMczcUu34jMuqDqTe3RyQwg7DUQOhmLiuDMHCtVgamcwkpBXL3Z3P3J3P3J3P3J3P3JR2kn8JeWs8tBfy5gymAz1lnRH9H9wX2r9geguc6eeIA+tHvk+mArD7HDOxGEzfw5IvH7Z7kKWy7d3Wa7C35hS3o/mJZTDaYW8mSUt09536MkRyC5x0hpJTwI5hB5EGPwFsN58wLJLp4+n7TpH7QaXX/WcYrQHcM6HqyUcEMphTb7nmY4UPaGkhAKaEzR1IjRkFXsRph1sLGg+C/8AytoeodIsJ0nk6NSgamx5fabb4Xa9Wfrd52Pgju8TQOYEd/tXrsmh8sCW/rcEbaUaF2MqLqdCxlp+HmHV47xCtl9ky18cBtTM5bOQV36MvWHQF08jMYD8ACbp/cGJ+f1Hte0YsFQ359oXAqOFwDKos6Dj16flX8n9yi7pfH/d+ul0o+8RRmKctc1G7i0xLDu4bYKprIX0YyDbH4ieV4zLbcXrqxufq8rE11TBD0dH8TCX9LMxnPHU8YltbDPly/mCVvlTPnNQAKIa+n+5/mJuQ9zJ9EHDAaFeiDhnYQ0FQSgdlwuvCAnYQ0CvRfKDANCvVa3ewzK+CD0WygyvIPUAwYla70FxS0uwev4IhiNu7H4lLa7DFLa7B6IOGdhADBj4OwgWQD4OwhoKgNFj1lKzqB6flZSABRN4Y9bvcwmi9lerdo9wz8KwPg/sUrKf0qdp9n4K5KBunR/pgHZ1de0s8XLufEYthg/75h2WW4djQvJBRj2+Yqo64ydK6jz1j/vHHb0s9CWNhEmX/n1JPNMHV4INTLc+xle//wAKJG73dIrteJ7nD6vQoUjLYdbvwf6ufboc+ZfknPRAjVx+5n8Sf/YvrLmz+I/uo7Zok5WmVYWWv5+rxBwr1Avuv3/b7xfC/H/vf/woUceJQQjapPyvPECaXZyPRigvAI5b11z4/wBwLnAwz18vxA7qG/4ks/SysMBeAsnDANuLY3ron3PMCWroiBb90/1PZCuP694apT3wdjldtTbo7xP/AH2iC7+l1/1KvqT7dPMACmA/+GeoxeD/AL6TZS8Pi6/gSskLR39Op7ahlF4zt/XEBDQcHp72Zv7ynogqmhR5zBw3E1/icWeBYpAVUT8swrM+5+/rex969+p5iTngmjueV25lh9v9x4IO8q8r1f8A4hko4XHn37wL9mWfc5IqBZ+g/wBf4kIg2s+722+H9sVFzmyHl5Zs0uU2u/8A8UKPsSQl7Nrh+mRFckFjvH9LP4n2arz9vgUMuIxY+RfiY+r+uH+4RvGNvsNT7yxl5f8AUMmDQf8AxCtvh/8A+QPIj79feaEuxX3MTBJ5/wBHowgiXI/76Rbm1HSL1xfYtmVx9D+W/wCJQgvvGXy//D//AP8A/wD/AP8A/wD/AP8A/wD/AP8A/wD/AP8A/wD/AP8A/wD/AP8A/wD/AP8A/wD/AP4iWeiUnT/DQ7i+x9oDof8AxbXxfT8X/wCW2+fT8X0DAYXPeAWYcnoFtjJvD8Q6xp5cvj2g2Ad2bDATn8+pg22AwmJKKO3yb0qoUd4KRcj3/wAhqaFzREWupqX3M8D8IuVsXtrglLXYD7JWBbodViwDROEh5Ky+VoSi2K6HxMMbI0PWVWdFw21ENVpjV9CoUwvYOpHUPQXw6IMGgXXx7fPp+LO2ZeeJtKUJf/qFnAo+DJHaPp3JlUpo8mYsZZJXr/kr7hyY+3iaWjjal6Z3qmZq51lVWOAQDrDeaPUvyjnZa9mG7jh+HpydN5NEtKQq6zEAYAeF5zBZCBfwixm6GoUKKrVH9ssdZJ6wJAXUdCJQLlJ95cTb1/tKNHDbGe8d0wegJvfDPdwj+CDh7d1U6EoYiLUkuMRh0/3M+AyrvmIVI9oLzaRWGRuTzFdXvdlRGcJse+o7PObIbh57l1Bj/VXXRgoCW7dy1gSKHGXklwJcLtbcTkpS4PtBYCH8Ig5rmFGBphH7zJCUwXtcG6UNjP8AphnDd0XcfUJcvDiNExhLPmIkO4W3mjSQ0F6CJ8zKtHCzhb3oVejiMFKbdhyyiLbZCm7I7XMV3qB0qZxaDZcoUHJ2ZnSQBxTC0pngLhYAce24+9ey8Q2yZNW92BodsIQVRToGYYL8lK/qWn2g4o6+YmWwYQdZjXLPLXIwMInwEbwLQ2wYb9g90dM3sBFaIB0cwth4OPj2+fT8WUiCsDHQldnazFiNmKzJhuZJcLqjbWxaxajuOAIqXx/qCZUp84zSLZWjiAwqRXMMVXgrgh7yrrCbFxDtlflOuMQsI6QIrTD6cgrDeGyWRgX0xHVI0X1uA/SCx9BYb1jktFYULTfYl58ngpcXKcX+2YQsZGC4HEFa/wDJu0qDo8e0oEc82DqDWntG2F6l56pP6qWhG2IIrc1jmLqUVrglIiZ4TzcPLqgV4CZe31xY/wBy8O1FYm8+fkUMyxloDm5TthlDBj0UMTQdmFoJessc+IPSmAeZgBo5kSGMsZaA7w09BxFSdeldYClsTVvaJ1iM2jE5+BFLjfYjpfPBA2QOaae0tpJo5juujk5q3cG71Ht2m3ackpqW2Atu2LMHODxLTnqM8zR6aiij7w6PZecuSXEzFroYBPES/CwLNkVWOhGo8ut1JLfHB0rXMe8qbHyyypeyEPE0nCGj7voGCJOvudBWDEdS/wDpg42RBO6aUOePj2+fT8X5RwBobe0ci0LHb5wCFNXzX0Xb59PxfS1tLGts72l/GgAdX57zH1VU8/4MZ5FPEw9sV8hZ1WdmYXU53v0T0lByDn4bZwLqY+qqnn6Tt8+n4swHKxeDlieBVTrOyVb5rgIVnoFF08lT7gPTpbLY0R1R9txnSs1yHeUeL4VuOTtEeD4Gzv5jRnMvsJHyUUgwrAykQY3iXRg2bb2lWrVu75aJHBmupXpSbfs9SalDZ50wPDMe5XFYo6w0RL5wJYCn9LNaXo68/eFk9uFAtVBxA0ODszGEVr1ZBGvV27zTElqt1xxC1m9tQ3CZSHZvSWqKcn5+0YfkFNus1hDD+IZ3pKx4LhwxOqw61KxuPA48zcNIMnoSnHF+4wynXSo/W6ljXPmGGClOmVXL1pFDyrxM0hFn6XeDdiItAwMGSOu26WXII3eqewQLeTnDZqL8QELHNdJysGHWQ34l0DFc09OkJDmPjsuYEcNDZyVFNJU1fcneHEV7xI1iqFQ91spWB7id/ndvn0/Fj9jaTdouzVwHC+ZpPlLo6xJndUgHVijj1Lro7QV9WcvHcsdQo4RlElohSqz7T8TC0/pZLlwZi2tf3AfaULx9pmG9XX2RScSc59o1thsv3Lv0ohar8CWnBfzZ/O9QT8FBQssHWmYwiZu8EsIrohmb45Z5MwgEtKrYpxbLr06sK6G3ycxqNXUR9kQhp2y+ZkRTMDQ7z/zOCCN+OgHspFg8keTMUJT2l2q428QsW52fzC/u8T8R/EuxGOqljCYjdwzfj0f97v6OOogtsmHjXEFTqq6in7YvcHEpQHW2X1ha7O4hzguoDvaXhw7zHJGG7GmK9hlSA6w5/wBZgfd/jOlJA/cINs6H+pcCQGRZz85t8+n4voaQL9NPHiIChcAoUdphSPmUVU1KKqpnwLOfSmkX19NOHj0Qdyhwzt6AGpwq+sQd+ilgvr60UC+vppw8SjcolDuIKFkAUKIo2mpRKLuUar0T0nK/qGmM2mOaJq7CN79KKqaYPEHQZnjBA2gWwcy2wZwC5vDNPDxKNyjcwpHzAAogBr5zb59BZnT/AOW/n9VLpJ5p5p5p5p5p5p5p5p5p5p5p5p5p5p5p5p5p5p5p5p5p5p5p5p5p5p5p5p5p5p5p5p5p5p5p5p5p5p5p5p5p5p5p5p5p5p5p5p5p5p5p5p5p5p5p5p5p5p5p5p5p5p5p5p5p5p5p5p5p5p5p5p5p5p5p5p5p5p5p5p5p5p5p5JQig/wpGmJ3k7yd5O8neTvJ3k7yd5O8neTvJ3k7yd5O8neTvJ3k7yd5O8neTvJ3k7yd5O8neTvJ3k7yd5O8nefAKXTc7v7zu/vO7+87v7zu/vO7+87v7zu/vO7+87v7zu/vO7+87v7zu/vO7+87v7zu/vO7+87v7zu/vO7+87v7zu/vO7+87v7zu/vO7+87v7zu/vO7+87v7zu/vEgJ8hu+J0WDjBer9OYt2u6+JERAstlPmdfn4U1la13hHqF+uA7uftOtjxvT9He6PxK0LoqUeCmTTfqoLdE6pbAPN/cGGdOLRHYOsP8AAppg25bZX0E7ekDFRtK6sygLvRi54Jka333hAtex+Y1+ZU4zNvQiipeVp9f0HWEsLi30qKNsH+RhAFgPceIlnd4ASaS1flCdQWBtPMAHJOgk8lOqkt7iuKZom8Jeh7RAaZLdr0JrYjvVPoD3TqU3iddeer59XB+gfxAM84HcvNaWdR3UH+SaWdoneCmrg2kKYWMB3VwaL4hBLq+Lg9m8mrpiY2Fb1zL5A6nzGvzAFMa4SGKP6/oOs/edJ+f/AIgkXleJpmpBtU1UAsj+AitEDedURkFGeoZmrAgtVU6tdnWprhradrJ5mWIx6Rz9Ae74VQm8nD6cUx7tRgwl73cq47EqVXPP4lbI9JWpLR5jLCxXi9w+caXxOBBw+Y1+fhfnfhhc9KjxBAsqTtCp6HYqUzILCHRLVu/H2gI6DQTPQb4g4g4o1Ks77olzJ29YEFrB9Ae76Pr8/UXu+LGT7P8AgESBAXnbG3oH8fMa/Pw9CsVrcUi2roROM+b4nNA0nH0R7pfRVfiSygLmdy/XEGNvtAvzp3AK97CNleZZ9r4CZ0YJoQhta+1S1HCB10fMwrApu+GEVOW7vSB5wT0OE4wjWQNSodun3VLX8eAFXpFeyKcGSkqpqHZXMtGZFujlmKkGyq6/Ja/MtjD3HS47ZwE2nr+g6xwuOwVNz8b/AFFNSF0iHeWUmq0SO850lJXy8w+ANbbT3hezYn2B0ITi05LjcY5pbg8SvdUW4hJowdg5gotdfQHulcgPDKpA4PUlpQ/uV56OKkTO1iMKWSLd++IwLGn7Zx7rfPMse8QwxCMc96QALCJ7J6SEzoHWowYynyv7R1Dgn8BDaP2ITUG3IneKmKLNT2+S1+ZUoOjmA0UHB6haP0YG4iuzWITMWH2EPNVXQPJFLCLih7xNF50L4JetUIVjrXeF/b1jYNiDmqnKA1D1XMzyAPCCxgxf5fQXu+Gh36UXcANSi7gFFPLfj6UXfMou4g4fSqwRRsL61KvD6aWPmABRj5LX5+He/SjcQFJcwAqOguUblEolDQzK690fdOTO879Be75bnjlXPyOvz9Re6XzAcsrkDyeqI1eI7SeZXvVzZpYTuSlv0Iv0aiOmy4skUtO9Shqtq4sgKbricY7OYmAF0Rake6EEFqyUqE1dkrU6izpHjbDrpNb9PBn8wRfyGvzKoB1cQCyx9c95odXpAjFllY7xuJjwwnOnZFwIF1Ml1grge0Hb6M90vgutztiLTHczu16/v94wFSUvV3xcaKyEuR5bg84nEtwzyCt9hKlBXHLKABh8qqvzBtQdBV6950lU+H6wZT/HSiDmK0j2uLqr8alryvOQUvDuDo6mZ0z1wXEsCM7SVkj/AN1GNQAwe8/3LREy2FbZ8g1+ZbGHuOtRCTaybD12Gwv02+p9pyj9nxL6GX8E6rvX0Z7vh1AGIh4Gw1iEjL1NCwekRsTeCq6jxmyCDKuzWliwq3CNXBSlVxnogurkOjOASrznbL2ldrpAVlyRVurEHw+0+I21Gp6S8Sju/MpTn1g5h1+Pu8QRomTFwYnZXK8IaNqP8+vz8I2PIuiQjglfhjgAyhmkYKcXueWVwHVl8je8kZcs77n0Z7vlbzlLT26fJa/P1F7vo+vz9Re6dCG8zprz1PPq+SG5BUFsY29pQcMAUrvhl+xL3K20T2Y7dNHVMvI5ehFJonZN5zZTdOhF1btwnt8zr8wDTacrDVn9XoLYeYrZ6P4iGKgxxxA0qh7teYuubEU/md5XRb+Ik1aOnhL1wNKrX4jC67UWvtMQY2S5eidmNxA5PEPPXpPLdA081OFAn597oPEKtOm5U4qYNFev7/efvukqLkfz/wBw78g3wECbSPJCwr71t/cS5jwpYNvlD29w1aEXghu0SDrj5nX5lzjMW7x7cvK0eo/c5iFzk/xMgcA/iHaVluLIa9M0ckqwYocXSIzoziZKQFYvl7y50VEUj/cbYMPAxqfq9pVi3qezLdcgvDjap5+fe74WoKAWGP8AoiWpbby9xmwXdC+pUTXtSwGPbLehmIOXTdYdWWKnKw3VkV0irFMnN/M6/Pw65GmBsLkchN4Bf+udYXmbDTDhGrVJeeGlv7IOS+7nRCC0tUO0sBaQHJMsdh8nMAA75qHHG3XmMLBsWj2glm2p1X597vo+vz/gFRwfZ9Je74QLKCf+KiEYfLfiAWpLPjT0pbdz5jX5+JO39FCAWWP0p7oSvbUG2WJJ1sHr+/3jaDi27mHM4e8uIdgFVoljALK4feYj/aV0d5xn0ahNlvcCS2Oym+fEcs6UZ4mGCFXR1Y0vTrYU4LvWPdHqyqNSRWwUKra5eJVn9mtwvlAD8H5LX5iIXRhkJV0jsfW/YG7Vwx19HcdwRwuUYoqkee5LpAOGg7zhNhYPeWBi6wEZyF0StA5ZidIeQesNGpUCkmgF08e01rRXqVuHdjO5SPMbX5rHzz3TNirW5l9GWjHr+/3lleznCO0V/M+65EqXYTcUh4ZAq81ria9iYEv7wK2Pfv8A7NMRhQfaWPDUGEysYKPWveFDjiF+xFakhAmTnMDTyK+0oJAdqVzVSuP1Ye/yWvzCOJDspmbSWd/VDhEsVmwsZTOkPanRQ+7xL73+OGg6xLl3qMlpMWqzzAkMgXxM2pwcEvZPdFMPWqy3vCYCqa+My2EzSXBGP4D0SGAJ33ufPPd8NBoashxioKNS0cxaMtNwidUcnklYBWJQOxLAjUsjyI2pKqcD2Iq5YNGzpF+7rRxxOuAEYNs7ONwktV8KiI12QI9aj1d/QFdCZHu+6e0QE9GGYHdAD5r5LX5+GmM6SFeM114lonugzOlEA4Yr1nPqy987ou067V6HgmRT/vgh7elPa5ZtZewmXT+3iU8SgXvyMzBAlFQx3Zev5GB3ctzK6gL6/PPd9H1+fqL3fCo1O1jL2dnh+g6/PwuxbywbVcCxB/xhJ6mPF/QHuhK20XoqUYtLrTfr+/3lC2Dus0RKGWDxdahv0C66fP6/MPDsPQBLux8GvWq5LriWlngZK7SpZ6M5fOxuKjBz3ZxGpVLLxHrHPfGefVUSMSwc+JqcfYycLCL7COBjQDKp7B3ePvK9jb4LhyxS7Yj1hGmurk6lq0ZS0SrZvnVmKVBDOHmNiL7G6T2EQz8y90yEA2JsY4oW2rlfX9/vP2vUhrL9ru/P6/MRp0Jhg+ht3fU0r5uZS7T6sPkwkuhf+xiSxR1Zp6B+0/e7npDZPBfEWAemRC5Fhznf8ws8HDvpG1s+DmECOIftd5lNXU4xuE9H5sUkVWmj1MzVoB9pxphYym/QGhz7fMvd8InU6SZhSBlwanAdJ21OBADfz+vz8NMxNdoPsjSlqLyvwwfwhZalcpU8kBrlEtsywdtQ7jVU5iuQDwQfacHZB6Vck2Z5n9vBENm95F6WnJnEBtau5Uu0sIMVWvR6jYu2+06rd8y930fX5+ovd8NBoboigm0ku58C+ga/Pwjzk3wmlwS9QqUd58fSnulo9R6YTPMW9+PX9/vC/wBRtwxippX9yqjV6/BculjKn2E00zRye6zefj3Kzhbt1x3YvI7zxfzWvzAFhdR24jzK0Or4cWGyf9kRvLE16RzGKh6xUktsb9olG3p+SEFNJAwFde0ua6lLRyxQKurw4fob3RQd+BZKtR4Fev7/AHmugPDWGUG2XqF1FdUwd0HiEeF52kjFC1FH11M1zOnNOcpc/R9vm9fmVc9AXAIg0GPhx+s7z8xLzUv1FZiEWDKuz2gG6WNYlh2/3I6YaZS4aMKsEXn7/Q3u+FENjwwAUUE4N31mVaDo5n4+QTF1PRcHABCtcQBAC28/N6/PxDgB2esOoh3iBRYwAooJ+eVLlOQ6Po5dD1Bg9IOh9De76Pr8/UXuihlgjk9VlorZRfvUiCwe/QgmBuvntfmKGX4KFKoZ7sR/YGM2qw63j0EyV/SHuldUMH4e0rkEKfl6gI0kzQ6NfaeIc+5HH0TsrNxMlEVNH57X5ikQXTq4uVSwadq9f0HWfiP4n4L+oAkWRWmPaLKS5LZf0h7vhYbScPRNQeh0KgIzzu4erE4ojTBuBT3LRdHz2vz8NFtIx5NQ6RZGykbrR6JHVa3grZMn5DV+kD3fR9fn/A0SlKOlfSXu+j6/P+AC1Z2/wOwq/oL3SnweWVFdU9c+9cHV4jt7iIOo9TH0jAIeWECMmj1Y0JHjn7SgLqEN0Lv9I7FOx6nzWvzKSg9YVC/J8Am1JcE5YhSdIOTtRcqTXklGKz3G4f1EXm52W1zKfB6xgJccyhjuRIYAuvoL3TZBzbF3FnBTa6r1CtNFkejUV9p1b39rJ0YPu8f1BztGTYZ01gcZgizQqqniGFSAyC1tqHQAwB81r8zedYbVuKF4w49e1CvJ1BW2Q+56S26T7X/IMBkvLBMCLHSHH+4bcAbBCTHojcNVQHCVmRh2I6YKjgU9pUTKjVwArhWL+gvd8OuOpnEsG6QlHSVGpKBh3qS7YixNs0TVdkRnX2lvuyhGsN0+b1+fhJFfj2p2iLVPtnhLeilU09RgxEaaDpCSuc10YNYfACAWyHaonIGYxJR2eCPPLLN0uAErBU8Qct8v0F7vo+vz9Re74VDbO+QR1/hqPRq3q/L6/PxAdPwgpPcPoz3TGcLQG2MlcqRwnrko8aU34lRwAtvHMHFGQzR7Q4WmZniJzDwadowjVfRLE2nBeYGNRNSIw26BSS6Tjpap07RIngWXtDSK9Vq6x023QLTsQLj26D5XX5jsN6huX0vTuPq0oa/2SmFfZuIbTmPbmJ5h3McWw7FQZuslZYQg0wcbzkF+76K90a6bZeswGNpdaK+FA7PRl7f7lU855ltX+B+mZH3wGW4v9YwKjdZQ/ubFCB2R7WqnoKw7W4e6LaFUPOYMwijVuU6RdbE6nyuvzCwcTOkZZ2PZx6/oOsfjEDYeIQ83oN12jOdPaN1B0vA+LgBBoK4qCnQ3erqJ7PvT/X0V7viXMQ8Wqr/UVQBRsJ0SZUxecOrBetonQktNvJMVSyGlYNEEkFe4dSLrRC7XyxGOJ1qWnvs/7gu2jGh646sskRSrE3C2hQ7D5XX5+HkDDe9TLbCsI1LewMvN9YZZkwoRwLLkVWJj3hRqmMaelxjqlsPNxOnsVg/RXu+j6/P1F7vh3puscGnY9oiGcByr4D0XSdf861Hb8V/l1+fhTlPl9FMrYWPwWP0Z7pkVVDRiIq4w4z6/v95+y6T8l/ce5TesvXoQlZdobGb4xLUB1Y7XCDaOzATDaL+XtA/14cvoy8WGSt3we8NYtQ3lxK4N95ergsg8pR5vmIM8NZo6kpRVRrHSFC2UnDMwU1u9RmVYLbuWSO2qjlZ1wnLK6k5vYFDtHcK6l27f5dfmAO9jVEo20o9f0HWMRgPdBlHUBgPLA7W5JnvM88ILQB1jKcrofmR4t27Xu8/Rnugi8tYbI/tVXsPqEVv9cxtLpXZJwrFHV/8AZ+M/qfsu0UE8dtZzUCh3wwc5xmGlmAhkgG39bmbvEuje2W6gZxp0IRwQZt7VfadiY+TcZkmwaYAp2h0gmkaJPzU/Gf1Ec2161AwLZThz/l1+YDX0dkzjrau19f0HWbXqN0lHysaCZH7VR7o0v7hdSUyKPWGeaPow93xKXyigi3Nbdr5YFbyecVL3brq9Zbwm5UJiYvbZJq3pBPPBMJ4Zf8Fh0EluFaZYzM2EGC2c8YYeAjWxe5zZ19ARXEF7QXV/3MynNNRF+aQnHhLsrzzzBDefzip0ZdyHmJnU23e16v8Al1+fhWWvS7T/AN7ACU5mFd5aUEFxxx4gU0KwGobQ8ote8UXawoF7n0Z7vkBFgPy+vz9Re74g7VNjVtX6ABuiuh8/r8/CLbQKO8ETQuNWKLXTf0p7poko5VUNcidF36g2IMsdZ+G6TqWK8jDMKKL7RExUyDXLBdNvB1IRDJZb2BGvK4OKMV0HsEdblVZdAGet/M6/M3HWXbUVD98ev6DrG/iBJWoXZwwBrTVoIIVVKp4itRZGjglrgzGAOag0bnsT9Fe6LyLwyhronrubD3twTiaCib7i3qO5sCI9yJwRRrmcHQ6xiaRfrO0x8TXgPmbinqC5fmdfmUMB1hoJ6D1/QdZ+u6T82MVOlUFe8XmZClhP2/MPpzeRT7Sl3ApZzxz9Fe74W2VXYun0uOBKxzXmYJ3Woq2dyoJmAxBoMTBO+2irZQXDprmNBR4Tl1ldBAHflr5nX5+Gr9uHMACjRDB7YMs4fOi4NSOyoqGndDbFIm4czujYPor3fR9fn6i93whwNgHUJukVXeAwbfm8/BSRn3D5zX5+EVyAp7sVbgP3+DoIJ9CpA51HP0V7pdULJ+HtLtIi35V6/v8Aefsuk/Jf3GlAc1dukyu2neTmLNFxp7IAV9PeThhaUHaa4LiZ7FLCucy0lmXocvmpjlFb38xr8wmQV+TvL9ZNu3q/QdZ/p2BF6I3UY4DpLOQ7dg5lJ/0b35GZmYcccRT9VWrZ3OLoM4ledGByu8SbcEyPob3Tc1g9cuVcX3gzYo2ANRt5c0e8zCxZrHEZwlpONTgSI2xjKnVK+Okwo25bXWoo7A4+SxDCL2sc/M6/MocPwPQWg8x3ymAO0IC2+PaAs6AGTvUI72uUHSZmYc8cRoWKm8yhi6rNdXpHeRloYE73PjHv9De76Pr8/UXu+j6/P1F7oqKPKqVaryr+BBtKnK7mUkLPeB33SncWCuFjd9VTudBXKtQ1avM7Pdma8TtTBgQ4HSfMa/Mrp6iqCRNpPUvLTA6rDWYoeCVwIWGLhI8itMnDKL48Lo+/SZm9nRZ9He6VTuy9cQzmK6a59dG6MSF5TeF1LpW1/HEvY84B3rrOLIvQws9s8gHaG7YB6s5qZ5j/AKE3Op4JhjarlDowPKsBTbj5jX5mBZYR35jHDSdHryOC/TIepfiLVcArbncdyYKOOkq52UK+jvd8J5YFrqhARpCtITXMa4HaPjKvR7Bg2Fd8wKZymKWAFjXXdYLm1ZrBCu3Xz2l7oYbK73FN74W8d35jX5+FEavI9EgDVgafeLJLOEMx/uUTzKvB9Ie76Pr8/UXu+j6/P+dECarZfoT3Tp83dmAWw9T4HP4+OHMFy5fbq5xFgyojueYCKcjgPecVD1PylT9ddD7zdqKzT8ieH5nX5hrow2sIWP1c1ae6yvz6aoHYisx2tjtPs49pTYX3ZVoYDo/ekRimxFQ4JzAvxKn5KQN2cLZ/16xBxpdAyvtEHrYKfoD3Q2CbB03KGlTGhXqCPa88Qs4pFrY5GCru2oqjlezAliOa9RFLNp5svBLAEEEo6xv/AOv10lktztbPzADQY+Z1+ZRdmK5DFGcytHrYfpXBEs0zu+rx+nofs+ID3P8AUXoBBesTPt77Xifgv6gv3T3wQyyVTlzAigtAdpX62GFsfoD3fCM8BZyHEAFNEJ+U+buhPE90XESDqXmDC+CHpWpRBMhewMaoeGMExfX97fmtfn4SdjUQ8UZCNqvPAOhDBCtwSN2i0YB0JSZYDqOKfazIdI8cKzc97lJ2p5iDXT4e8MoZ6TOiF17FGQRpoXme79Ae7/ITPSVBlJb67+a1+fqL3fChFsLqWoV7PXQmFfaW5cd9otFso2Bqzr8CyuAx6/L6/PwvyFtcEEiTSQZA0LeVf4Ehdv4a+hPdHyNoImRVJ6/v94+uwuBg/mWC4l1+CZofWRaunWXfmNNqtMvtzcwGfyyibRUbGEAvp6kLBRdXHZfLKRrGxsSZsrQNfbrLT7XnDz8vr8wRXSNSkqhpOH1/UdY7fAbyZJ+N/qVtIWjAHdlgYFFkTsy7+sjx1mPOZfexaFS9R/56cFcLQQ7aDYDKWkrcAHdlbmgHJ4Mz7FOl3h9pKtj7xHdoV14K7xtcWDl8490QI2w4jIm2nr+/3mP3lCjdbl0p6tSpWsGKWs9bhSArVF0wBT+rYKI1D970hCnl/kgGxx/Zi6uPuXGZHge/y+vzLptMS6jZtXL6/oOsxR+0ZknHPX2cXL4bHkQNBWAOaEAbd/wTErhDp1m/0kdR78RtQUqygPAqKdVftiUA3D24ldgjugBF/wDb517vhvYAprcGWoxw1AFF/PmKnDbICufs3kVF2bOXeYiZtu4uNuOldwuzZy7zmKidtLTxuoUuHAjoPl2vz8LxNBs7QwNCpQAcOgvpDWD11Mxw5RqL7QxN5xGcgjOh7SwD2h29Gm2RVcdK0Vt5cveGy/F1PvEOv7rTxEroq0w9tILrxFoVIHGfnXu+j6/P1F7vh1f1tRR3gYysbqn+FsTeXUBNY5H0ANCIN4i7AXW3yevz8NnHa7xKUB5wzNE3UsgioF079HDMZtVX6Ul9Zx9Fe6IQKpboqEYLzJpv1/f7wgHJ/VPsLIH9JpRrP8AgYNN4IyqTiSs9Sr+li77RbR5BbqVXBcQNFcsE6lTTDTv32hCmsmgvLZUClWUUdYucc7SvtcDICVxD+CDF3WpXFVWtHL/qOAORta6y7CKdi/kNfmV+MjbOo/MXlafX9B1mhrqK4TJ8hnjtB+Ug4gqYgAW0obMLxXtME0lOwe8ylnp5Oo0pKMKff6I906uNOqLPf1/f7y70tCL1EOJfHlB7KjpYxgzITR2n9/8AAY/qKw3PtvEOytfgn5ufjP6m47XhKdEGjLLvW0CAABzjomIIq0Q9KJ+37+ixR/YgMYVbjuQHKvPyGvzBdOa6kFUfj1/QdYKc3GNhLr6Hg3HkGAP8sFjUaFqatlMUP2u8CqcLGwcDh+iPd8IUsZGLWfahwgNBFhAcu/vK3R3ULgRYPWUwrjoxDKawRZDrHLB0Gi3Ko0ZWWcexroqNyMWpQ531/UTgLL4Z3OHY101AyoKDtFruV2+2oAKMB8hr8/CvO/DAANGId8ydSUnbecOJX/dFA7rlRwo29E5OSGKXlp2Kl+Plr6I930fX5+ovd8PEruow6dj0SxRhyU/QNfn4WKvQV5mJvZTaI31nZ6Y1XpFMs6/R3ul2Gq3O0q+d/MPX9/vNUfNF8Q801nBX34ijzcqo1XZHoZba61L9Au+HzuvzLMgaGelxSzSibT1/QdYtNknkCt4hXQ56v/sJpPgB2Ki12jHNcwBLfmGUHmXhwelSmHnmF/8AiMPWNbt3DpWxvdMEenJZyc5+hPdKUgeGVyAcHqLp/dzl/sSsxUcLryqYjmIoX/IW/wDTPz2vzK4B0YAKKD1agv8A2QJEVKPLUpTlHo6H4g62oyKe0wj5H5Kgu2JOtTaFRzXAXQyQVLmMfgNR2wAA0fQnu+NBwyg0Sjfz2vz8VHoo2g+uTDPpRKN/Q3u+j6/P1FoFfRySSSSSSSSSSSSSSSSSKpsnYzsZ2M7GdjOxnYzsZ2M7GdjOxnYzsZ2M7GdjOxnYzsZ2M7GdjOxnYzsZ2M7GdjOxnYzsYjQE+k2sDRhc+0RAJk9GV+W1Z19EBWiUZDbhMe/yeBMLfBFzpNE35+mOlChg0BbBzNGI61M6VAgUZrBzzFwg2obY9iaYOZBQcG0KAN+ibpRO2LBFuFPl9WRAPeVRLg3ovfiLBhtTVXI3uoQE3MBUbh3MCFb6xPz38TJMl7WmZcU93pOz47iKblORBeEtVG5YC+wOZ3T3DqqtNuYjZ+A6RMM3e+iXKLVpiZRs56Sq9UemmNxsNLcxymd3wRUK7BM7Vw5QOp8mAdCr3Da6yana9q5i1xOTd6mUG4cpQpcnFjDxmZqusAXKLanfxaqPDbgc/aPDhPVEt5AcDEBG8Ln7Tuhpej4m8uCEnnAXuNT7SqJdKdF1f3gY91cTMxDs56R3vxfB5mwJgb+cEwwPZSxlHYfZXs5liBaV5Fq/uQLZGv8Aj+7M/ARDoyomts3tD73EWUtPP/luEsTY39/3Yquzk9HKgSviAf8AEo6aI8Qe01AUPd5Y4LpIcliX5dbZ3xKzIXQLhuWWClHFP+y/94nuRSzhocudeIuVMArcp+W/iaP1yQqCp/qITdxyp8nNx1oBsd4c95ftmgatupcdX7BpgTw5V7YMFYYu+80xyyBq5TLGlE/KEfitJtZP3HUgF6IBx3So+VlXAS0OuzoaYHbE190Qhx1Cl3LLDLeDSfiAHIK+9v8AUM6Gan5CLx9Ydmkl8MrzUHpGn/cKcSwqtZcAnzKWXHrjnmD/AHBdwfadoBtgjotRl2zcCM+kaLoXr/UE7sBaH/T6XYrMOrdfiXAltxZM6mBXqdL5ynpBZu+SKhZueuC4ASICwNbj3+DFWVH8y3fVvfRfnbp7E2ZO/wDcx9iTrGfEA6BonYIOzwzDUFzG9JddErG0BxRB7TUFx1/4i5mfYf8ASYcA3RteOkylWiYDqI0OCq1Zsp6kUrl5FW85b9ojfnuCw5m47+9MJJy06xCHVcAPCXqYDDkXWaCP7bXXTvAtr3KVLQoDTsMyqPWdNdzULHyo4xQRVRYWnXtG4oXM0E3xKGNhpO8WW0Mbc7cTVZonKgTXajpKZKBHuB5hpoOio69JYOGl2JHOUVeY4ijGzr2iOELmaCMaHFsivoJXamYHvejdSvYqUpkpHUpe49KpOLGNk3wb5+0YhTrZ17wPLQDvUUEq2GavpB2PNlNYQvuOpvXOxnKANUFfdcu3o4DpKDOA9qYB373HFmNRoDdrftwTKXKJQHURvwwA6HSbmPgqj15miQptV1qnqSgae6h5a3KkhU7t2y6YprtR0iJAg2dEZQibnG/pry9W6bxfROIQvDDuatf8BnFqWrGv7iKXUC1/MJWhR2P/AMH/AP/aAAwDAQACAAMAAAAQ88763gwb3888888888888888888888888888888888888kQ5yH7mt08888888888888888888888888888888888phow9rygXe/8888888888888888888888888888888883H0FsXmjpGx8sV48Y80w48wY808844pUstZRww0808882Fb5JNUmBYc8oRJJ5oltp5hNBhc899p889BBBZ91IVh8wG9Ao099ooE89pBZwZ4BJUkJop8s9pp0818FFxtRIJc8J8Mz9WS6cods8cMcc88M8c888s88scc88888MMscc888pQpyUFthyg98888888888888888888888888888888888Oqcdcd4nc88888888888888888888888888888888885zSAy1zUhzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz0+d888sM888888888888888888888888888888888888888Kj+3w008U3xz44464599Ow6yy2rXx+zd3w74WFg8888+OcsteufMtBcddM9OPeve/u8LOvstsM/dPu/vt/f88884r2w1C63+/Y9wzxw53Tp8xOzwww/+8888888888888889mHmRKJarD7z7NtCC6DFNXXSd8kTZU88888888888888Zyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy2d888qyy22yyzyyyyyyyyyyf8AKwwShiAwwwwwwwwwx/PPPPPPK0gAcSPUaQAAAAAAAAPvPogOfYaLCQwwwwww1fPPPPPPKwAAcsskoAAAAAAAAAPvKA0IYokUQQwwwwww1fPPPPPPKxgCqyxhDjiigAAAAAPvLC0WjhCjjwwwwwww1fPPPPPPK4AAwtE1zhu4gAAAAAPvKYR5pnLtogwwwwww1fPPPPPPKxAAghAzCRC3yAAAAAPvKSQRwDgwwwwwwwww1fPPPPPPKwgMWfTdhfTCqQAAAAPvKIgOqqAwwwwwwwww1fPPPPPPKwAAAAAAAAAIQAAAAAPvKAwwwwwxwwwwwwww1fPPPPPPK6gKVWYHAAAAAAAAAAPvPggGr00g0wAwwwww1fPPPPPPKwAMsgo0wAAAAAAAAAPvKA0MaEU8cIAwwwww1fPPPPPPKxgLzxxliwzggAAAAAPvKiAkjyyCyCgwwwww1fPPPPPPK0wLPbUYJbaYgAAAAAPvKAUFRJZzEpgwwwww1fPPPPPPKwADQAAAAAAAAAAAAAPvKAxAgwwwwAQwwwww1fPPPPPPK8gOBgAAAAAAAAAAAAPvLAg6DQnOeMMXowww1fPPPPPPKwAIwQAAAAAAAAAAAAPvKAwcwoYgQkc0gQww1fPPPPPPKxwLIijiQAAAAAAAAAPvLSwETzzygwwwwwww1fPPPPPPKwQJuE8wAAAAAAAAAAPvOYQ3/wA0aIMMMMMMMNXzzzzzzyswAc4gAAAAAAAAAAAD7ygsUI0MMMMMMMMMMNXzzzzzzyvICc0oAAAAAAAAAAAD7yiIXb0MMMMMMMMMMNXzzzzzzysAAAAAAAAAAAAAAAAD7ygMMOsMMMkMMMMMMNXzzzzzzyvoCfFjSgAAAAAAAAAD7y4IAF2q3ogMMMMMMNXzzzzzzysABOKIMAAAAAAAAAAD7ygMJNMCBOMMMMMMMNXzzzzzzys4B8oII8IoAAAAAAAD7y4FeoJYAMMMMMMMMNXzzzzzzyvIBaHlE3wIAAAAAAAD7zkMVwYOkMMMMMMMMNXzzzzzzysAAkkAkAAAAAAUAAAD7ygMQYMUMMMMMMMMMNXzzzzzzysYC+DnVVQGChHEAAAD7yqoAGVgMMMMMMMMMNXzzzzzzysACJILOPLIMPKMAAAD7ygNECCMMMMMMMMMMNXzzzzzzysMB448ssAAAAAAAAAD7ykNBkA8IMMMMMMMMNXzzzzzzyuEDttOIQAAAAAAAAAD7zqAbXDiEMMMMMMMMNXzzzzzzysAAYoAIAAAAAAAAAAD7ygMEgMgoMMMMMMMMNXzzzzzzyu4C7WXuSEAAAAAAAAD7y6IDXKu2oMMMMMMMNXzzzzzzysAAAQAAAAAAAAAAAAD7ygMMM0kMMMMMMMMMNXzzzzzzyu4AzEXdgsAAAAAAAAD7zyIQr0IMMMMMMMMMNXzzzzzzysAAJPMPIEAAAAAAAAD7ygNBMAEMMMMMMMMMNXzzzzzzys4AIE4M0AAAAAAAAAD7y8gU8kBRYsMMMMMMNXzzzzzzysIDISSRkAAAAAAAAAD7zgJUC0CunkMMMMMMNXzzzzzzysAAsgkkEUAAAAAAAAD7ygMEwMMMUMMMMMMMNXzzzzzzyvoC0WnmUAAAAAAAAAD7ywIA/cKNL+/sMMMMNXzzzzzzysACPPKKKMAAAAAAAAD7ygNFIHMLHDAAMMMMNXzzzzzzysYAs4IIta6cMgAAAAD7ygoIqc9cMMMMMMMMNXzzzzzzytMDO/l3amVCqEAAAAD7yuANi0icMMMMMMMMNXzzzzzzysAAoAAAAAAAAAAAAAD7ygMAkUgMMMMMMMMMNXzzzzzzysYDVyoAAAAAAAAAAAD7zyoTtS/xMMMMMMMMNXzzzzzzysADACAAAAAAAAAAAAD7ygNMFGFOEMMMMMMMNXzzzzzzyz333333333333333333z73333z3333333333zzzzzzzzzzygzBBASAxhBAhHQCBxQhgSX3REjTyQiTzzzzzzzzzzzzzwxyxwxzyzGyzyxQxxwzzwwwwxwwwywzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz/xAArEQEAAgEBCAIBBQEBAQAAAAABABEhMTBBUWFxkaGxEIHBIEDR4fBQ8XD/2gAIAQMBAT8Q+GabZwXyMs3perOV7/1N8R0iM7uWjEVDP/FayaGrD0M8sZylpYhfpAnl3n/EEKMia0HnnMA3fru5Ol8teUayY6owaeWIrb2EzfEPsLwYiNP/AAlxVwfmX7fhfIX6iGoWl7i+d1pzzdnCMlzyjaxqBXqtZeMuHkWdpYSne/j/AFRIDYaM5k0Dc8mv/Cx6pgEir302A2FojnU/9dKl8f5biHD+ozWWYz/mKsNyZK95ie43BVHmAYQ43fGvS/j/AIWXLMAPVOLLyGeOcnKoGQbvLW7+pQ4KLScNJoTPonCmA2HOFOM/9IhQq2aI7n4MPm/j9ybHnFX9fDRElL8gf93jq0GM6zzTNwK33KH8FtR7v+4yxpTWPjprXq/8KjnMamkWBGSpngDrpFKIOGL4PvE8t9wJnSDCOCeWO28/4ZhrrxMAb4v5+BqbmPh/vuKzI/GDQ9ukuAFBof8ADwguc12lnN2m/C5GIu5+8TnHeb3s5lNcvFPxFrT9Tmu05rtOa7Tmu05rtOa7Tmu05rtOa7Tmu05rtOa7Tmu05rtOa7Tmu05rtOa7Tmu05rtOa7Tmu05rtOa7Tmu05rtOa7Tmu05rtOa7Tmu05rtOa7Tmu3x5X6aP+N5kJDGZNckWMUL3gJhl8VrEArXSVRBT7iA4E/MylS41YNZTzSoi3Grio0JcC0FUXxlhVjWAlFMu5Mf7SLF3BqF0D/dIInS4k1NNcx0Bg3waTcEclar8wm7x9ym0MGIilrfU0G66/wATIp59xwcG4ltT/ERIUkqAf9iXojweLhnmbv8AqJoxnO/hMIGv4gIDfzlZuP6jVv8ACPB4fpmQatlgRVSl3BpBLigvXERkaQxbyd8O+dZcUoOsCnwqNA0VKqB3VANV4qZJTumSrcvZaxCC9W43Cef+YsJe+/6lqo1cvLxlpb1ILVf8ZiJVuUG+u6Nlzi7hVw41hiTp/tYGqXdUdZZ/u4VXH8B1/uUtG6OVjnPmXhuXinBLj4XkOhX6YbnGXKNUHm4kGzP8zQYy7pmgVxyULi90d0vfKLQv+4NL3xLbmmImsXKgorFx3NX1OlQGQiC6Zx5m/axrMmmLq4Po3p7lcPXxA6isXL7SnWOAXiAFCyriM0LUQXRgglxg3yl6YurhUB+wnVyJZfDdwg6cnbETJZ8do5Y3KTgVpEH0rJc0146Z+prSBFR3VAAnQqXRFNKwTCa+y4VrYam88jhFqzOPEs1a1+IVcVV3pnvEEb0V7xteePmWHQr1/EL5DhU1OSo41e6tPULQcUGnKWJNDXojm1VPLMcEvffm4OrfGYGs7eHMepOpOpOpOpOpOpOpOpOpOpOpOpOpOpOpOpOpOpOpOpOpOpOpOpOpOpOpOpOpOpOpOpOpOpOpOpOpOpOpOpOpLE7/ANGnOVOVEOsQNVOVOVOROVOVOVOVOVOVOVOVOVOVAGHxTUME5vvHBCvNjx3eDtC7x0yznOb7zm+85vvOb7zm+85vvOb7zm+85vvOb7xxK1N/PYac3tj43RN4YJpOLYITOLiyzvhtItaoYgQ1uE8o9y0JbBvhiCTVI46VRygGQlmS6/8AYMWuGc1i/ePqGAbHVusbJSQKq8Zvje8dh5B72Gn81EOpKVVSlVUpVVtb0HwisczSzLClnXOsUUC84/bExJesGIQ2HkHvYacr8Wk1R0UawFCBq5dhwhwJzk3cShexnFqsiItJoHqS4bJJybBPqay81dZ0110uswCWKy4fcJsqad0wUUbN+oXX3ujIOt+Nh5B72Gn8oW4prOkbA4wGuU0i9ZZrwqCpH1LK5QUVsZ1c4Hxh26bjtlnhApDdVrjhpX5hLdkDXBVaFcoUPCqvqogDVTeW8+Ig3GD/AHXYeQe9hp/NDVQCX8FlOHxcuULlCGxjdTgfBossEIFNQ/2fqUw4GHZ+Qe9hpyvxDfvSN0B4gaV3RGzWJYQ3Zm4U1l9BxgZEzcMp+pjYbCcSuiIC1mmfKQKpCkbsqAaChGprSVWs1MBhL321jps/IPew09mAabG9B+w8g97DTm5sfCKHhE0cYIaeNRWv9iYC79rFqVDMWG9xm/cT3DvcXsjib1L3aXUBDFMW2b91Vj71l1NDxA1d6diAAcAd7jmLVqhbfTH5ljSuv6/IPew0/kVuYK+ACMVoNreg+EQNYiVF60BfWPhwBdZ0pzFa6NNDWoopDOuN/HrHEtUe2kLoCvEiDXf1+Qe9hpzNln4VgmKGYrgXRgXNwQcYM0M3lwHJsav1qEdHSQXqML4qF5MNa1mZlM1f1rNOSJoMJINI+5BBEGD9fkHvYac3Dn4vslxSqm6LAsXkMxuRFAcIxQmktU4Sq2tjX0pAjlbWAhuiWzUgU78Z/q4/T9g3/BCSOSs99PMAxTxFE4Qrrhdlimn29I8Qy1p05HHrL1BStU3nRH+f1+Qe9hp/N5r9jeg+AVojFIdf0gDzsPIPew05We8fCpJwlgC7v2EcG6EQDW5DItIzYtoeIXXMZWOWpjeVG0lrW/THPXOsKoajNuDpMeMjfMqz1Ur9lV7Md5fEFoxvwP8AUpAXeaEK+98LAFIaKb/POZm7+jyD3sNP5QdZRr+wvQfCllMLMU5wIyznMQXH1YvXMM5euvOZCyyBFDFBNnOJWh0iKy39HkHvYacr8T8mFLvjC5glauobWE66TWCJYBZ/uEwYpp1d5fGLYFmscfuCATbW/X7IamtGrnXhpn1EexvIPew05W/4b2YaF74pKxKa63tppc4EW8vxTB0rwVGAMlvFSgK1N5b/ABLxunOGmW+X227HyD3sNP5EagaP2F6D4cdRxMS3F7XyD3sNOcf8LasTJ4pKkGm3nEl0SwSrgaOEgiwpKMmbTSM69Phroxxuq2nkHvYaf7S9B+hm+U7Y2nkHvYac3Vj4TKgkm64vdLKA20YrFGYuVhviDrMepqLcqzc+43Jpnlr/AHCAuFv8Ewso3jF4/ECFXJuLhqr0nLWdeuw8g97DT+RW5SmKUBulcO2vQfG9VARowiwDqQ1mbvOvCVogUWVuzr3mMgYKqsU7oCgAaVsPIPew05S8s/BnBK6LmU4xLVmK7hjDKXV7OoHbRHQUky7VtQ2aLrSm+7Ka4uYdTC9i5QU12XkHvYac6j8FarjGhprGVN35gEvjrHxZvZUA1lOezR5MgR2uWaB4k35o0HBv1DWbuYDi9PvfMD8OLeNXfGWn13cOX8ctl5B72Gn83ul/Fmn61DX9d6D4BWiazHU2vkHvYacrPu+Pwlu53xC7b5aqPCKTtGGjLszpEyvWLWesoCdZagnf+uYCLoRJByTyj3KKx1108RoBSOl1ZprnrKDDvb6z9/ez8g97DT+Q1EppUQ6kKqqBYCXlgCBNhAmwlNJmuv13oPhEJSRQTsiwtsgaKt12fkHvYac638MY1Ru00YBrEFrECoVtYprpGGuTSMUgihRjuRI2WFpvD15gxi9ehV2xQWsdE2XkHvYacou/htZhWjhcMAuCFAOEKJ/e1ktKaIqtsNzcjBBWj8jLpoXdeP8AcpQwYSuelS4UmuLsvIPew0/mlwB0+Cym2vQfBHUYlNPxjZilO8vZeQe9hpzi/hFrwlWdWJVm65u3GJWMO8gAUbSqnagxKYKvdHV3E9xLfVX0x4iOIDlu6P466T0781zqXfQtVrb9XVxm4Gt+rzdj5B72Gn+0vQfsPIPew05urHwtA1FJQJxvgrp6S+6VL2kWoEMywMG+abTKgqpSgKouEyqq6xV5z1l/Zeuuz8g97DT+aWsruZWsSkEYrFQK2l6D4FPkjogC6p/7j6gy3KPP87TyD3sNOUur+KdyyU1uDaMvNbWrTbRESFMI6rKwvTOnDWv8TOFa5+ozcIvIEQvT/d4GkhS3XAm+lcdh5B72GnOo/FgiArUxBKBppBp5JZLbWDawER6rA0gQoBcIVe8rOMSy5KpWbukgUceLqvaK2Fq0urvdc4BNKzrhXeVPCM3elPLYeQe9hp/Khr8WXUvdArBL2l6D4BWiWyV+i0psfIPew052/grUN0WuAmQvDUVy5zILrBVZ1/jaWJwC54AnlHuVUcQ9bM/Z5uWiLyvN2l8KrdAruMcPzndyiQ0ZmUALP0fXSFPgXdutX0qXdoXed5fSiE5kl39v6vIPew0/nKJFsVAGkpKY5QBsNpeg+DTZIEjXWcmTKHXX6jdq5yFp9Tkz8cOkBqwggLT9XkHvYac638JUMMM6wDBCyBY4qE03UpryuV4bGLayER6pB0ooQ4hJpS6nHSVYQtQt4TLMts11zioQ6iuOlfzsvIPew05S7r5AsUmsUjxlNt0FQcJbPOLVcHVcqioECinYV6aaIiszDa3Iw6DKgnTeQgd5W8Yvrp9VLZZu/eblv3Yqfda9tl5B72Gn8gNfsb0HwA12G7jkj6jkN8dnu2fkHvYactr/AAi4O6bprDaXjt4xcAMQSFDunlHuGFHT7465x2zLBF0U0AZ3VuiURuy8X0+tYKwU+ytOlh3nO/2VjyvbY+Qe9hp/tL0H7DyD3sBrScycycycycycycycycycycycycycycycycyKdX4dsl6TkexOR7E5HsTkexOR7E5HsTkexOR7E5HsTkexOR7E5HsTkexOR7E5HsQJsPY/+E//xAArEQACAQIEBAYDAQEAAAAAAAABEQAhMTBBUXEQYZHhIIGhsdHxQMHwUHD/2gAIAQIBAT8Q4I4A5yyKGwO/Xyl3yRnuveZzfzeX43Qhe3zCgN579Q7wEMwcx/iiQOQ/Y8v7mAonLybD+HvB/P7SfzeUreg0isS5hg5Q66hvlO2h/hpOogZg6H/DtxD1OQjar6IyA/uUTQIGQCICo0EDIh0Tr/LlC8+KDa3Ua7j+zg0dg1H+EDew/sP7QxP9+4sPDANUlrAAGBBNiIWUwhUXCAQoI64puqP8KuciHQdoxACa2IGSEBCQj36TJ9M4gIBLhp90Fe3+FWWpdQYCwJEYAcpU6qKI/UIFJuYNtCEIKGcqEBJI4WrL3/4QnlhtxbrQcGsgIgyCghABVUfsf3KLfO2EppZRt3gBQ14FppIbUPz/AMIbxG4OhFoU1GgHPXzgDlLazKEtLcSlHkQ6gZ1/rwl4QBtf1Cf19QcVBT/DBzoegaH+/RBRarl+tff34XZHWMdaOBQv7XMT+oMxbotew/wx7ENyp9sPmKpgWIAEecFJpYA6n7nyhX6AOtJ/O+YRteYe75h82BHqfsRPYbj1rWfbD5n2w+Z9sPmfbD5n2w+Z9sPmfbD5n2w+Z9sPmfbD5n2w+Z9sPmfbD5n2w+Z9sPmfbD5n2w+Z9sPmfbD5n2w+Z9sPmfbD5n2w+Z9sPmfbD5n2w+Z9sPmfbD5n2w+Z9sPmfbD5n2w+Z9sPmffCAuohFXL9+HKf+MbbpLgxC9YKGse4wK9aQZmwVof3CbUACDrkW8KahRhGj0NjzUCeIEsdCPrDhiICQUcrhXcp1vIg+YMaxWKtfiFanIVBYWQy5uCAIEqxNYEACiHyXtnBapZBFfO45wgMSJRuEs+Y5wLWKliKjkYouHkSBucvWCyAJAWjz8pT3YBsVXnrygIIG3mc9qjrFwFxIFCQUUycmYLPMS5kX8pe0ID5Af3nyj9eCSLEJBnelpWQi7ggscjA0yBgLKuepUBnOvYJXpCKpVUIpJ3s1l5NwQFdWRVdDYryiR0DDLogSfILeAMmEwyEiA7cxGlB5Ko+4dWhQOlWH1IPlAEsw8gNHufKVrRQd62PUwglUCqASSjFNrxFSlkasMfxtD4vUQivI5rO3KC4hNAMmTpnSgiQAUDVnmpRmIUDkj4PQy5JD5lGwGeRyI2iqCAoln+EV7dmVbo4US1JMs5aCwcCLEqQRlVfEAkKiAGc7l3fOEWCAIVJPmTeJYrlWnzA8WZuZgORAiqjKhhKwCSPRltD8k6hNfPKIJyBZzbuOQ5QQBEOZPvF2c21KvprW8MFsL3+YxTJCNSt1rziQKJ5670HSVlzZU71GddoYRIsXqT+5dYSJHmACPSDQsSd2ER0gwJAc2esz3AQ3kiqekEAlwJnkQRbWs5ek2Vo1qvqVa7TqVStBlCOm2Z3BBGxcCiIDN2yQr8gaQ6CxJ6r4gJBYhOCtFlavW8DBpfVwEQXkC6sQLhQqAkHoCP3EiOsLPUDImcuC9PWE1cWC8ex/fg9DM00fMoYJJY1SABrTKuUBwUSZaOV6JvygylACTXPJJvkoUNBCmdWGFT3tnBVBAhmRYb9ZZAqIF6kbA05ymy67yvVIcmYUTKx/wB7wDyhIE+ZAW79jCoAcgOjavtEgFnVQncW5xgKKk0b9HTWET8yhQBN3cJVJIIahTBCSda5Bl0dpUAQFCReqbypbOXqUUVsndJqqcXANCOtGnlm6QCSQchrs7uEpQCbJ0JeotQ1g1sGqO98wIIgEwZeg0cIUcMlU1GwKHMwoJgkQBW4K09YMcQxCrYJCshUQgMAgwK19EHlWBAqyBVpPRQ6s0Cq2OhSPNHH9DCCg6uhYPpG1gDMalALgBWpCyMAdydcwbBbbwySCQAvkLBavVWtDpEMHoF1zcTCFlQVkArcoCZTcnmMjenrBCSzryF6gtreKyDGY1BoRBuoueZK6kysaIdBJXrFBpQupHyFPkxmxN0KPsaS4irTPR3lkIgO5lEJAoIfuCiNLiywTsLaKNnkkbt/MpgGhXZE1quagWVEBryVtyiQGTX/ACsKcCjAs2Cd+sMEABLUsLUb0F9YEQsX6AfqAWWCqhAWWqKrylTQCSdqsOnxClRZvmxqF55QihLAVCAOjHLkQ4nIvYIfrGpFQAmw5gKh84RERQ643oYdwUCrNvr2m317Tb69pt9e02+vabfXtNvr2m317Tb69pt9e02+vabfXtNvr2m317Tb69pt9e02+vabfXtNvr2m317Tb69pt9e02+vabfXtNvr2m317Tb69pt9e02+vabfXtNvr2m317Tb69pt9e02+vabfXtNvr2m317Tb69pt9e02+vabfXtCEiyB4Bf8QRFEbWAlG1jawkKONrG1jaxtY2sbWNrG1jaxtYZK4AvM+B/BEA5hAmksQKlIlYShkDOEAMgQgBlQjJgWsAX/AB04zAQgJFo4zbAtYAvHVcDHHHHHHHhKDARUFgDCFSI6ljECCjK7GEIPAtYAvxUUIiiiiixESLgUEQuIaFwmQCEzTAtYAvxf4aAdBN/AgiuHawBeKr4G/iHEYCowmXDQEwC4wZGESGHawBf/ABFawBeZ8M+DjjxQAqYABpLUzRLRKGEBmkKMSoRDfx2sAX4qKKKL8BEzNGMlHhMy5wllnx2sAXj4uPiDHHhAJUQhUMAZQiGxjWhIB8IFARjeO1gC8XFcFAIoBFSDBGDiEklmEi4FUDE3QBahYi5cAgC5iFvx2sAX/HRBF/CA6DAtYAvKvgfwRCSOkJFiACjABRIgSBUQQErpAAIAEgBEQIhxHTwWsAX/ABkCrQkaxruOAhYxmMxwkc4CRaPwWsAXjquBMcceMpIgIowRBApyzlm+EIrBtYAvxUUUX4KtQkS8GJpCWcG1gC/F/hoBlQB4trAF4qvhnwGOqMJlw0BiqJhedliWsAX/ACESTfEtYAvM/A48YAKmAAUIAypSaggkwi1EoRCLioLAtYAvxUUUX4CBRYgIUhMx248eFsC1gC/gfBxwnEASohCoYDKhoYhIXgNowhYVrAF4s+B4LgsUYOISSWYaAmEEiYhQIFuGCWMK1gC/46IIvi2sAXlXwz8FfA8EkkdJYlzSEUGWBph2sAX8Sii4KLgsFAqPrG1jXw7WALx5cDwcceKMkQEUYLIBlpJjx7QphWsAX4qKKKDHJIgwI42Z0J4VrAF/xkAyhxIhCwrWALxVfDP8FUYTLlqAoBRUIm11M63UvwbWAL/4itYAvHXgeDjjxQAqYCKEFgDCqBiUQlnDtYAv4FFFFjoFVEOkQHTEtYAvxJ4vGA1HABlCLkYjCYAMbOAzWMngWsAXiz8C4DFGaISyzDRcCqcBFC46AgxgOMXeUIJjAtYAv+Wlg2sAXlXwN4/BpiEkjpLEu4IAjCAEXIgGAIjogBiAAA8VrAF+Ki8CxkCqiAkRko0JJvHSjJRko/FawBePLi44444448EZIhCKMBkCENIDMrAUNzCtYAv4FFFFFFFFgiVPAkQYNcBYiBcoQIWFawBfi/w0AyhCQgDpCEVh2sAXirwzhgxyHhMuWoRSoUN7SsUZ5QQBTPBtYAv/AIitYLjjjjjjjjjjjjj4OPiQN4mkTSJpE0iaRNImkTSJpE0iaRNImkTSJpE0/wCE/wD/xAAsEAEAAQMCBAYCAwEBAQAAAAABEQAhMUFRYXGhsRAgQIGR8NHhMMHxUGBw/9oACAEBAAE/EPMrzEUB7tTVfp2Tn2mmXbEuij3qZgtgOqdaCz8R7Cv7K/sdSQDYu7phI1uPhKGmkwKnLB9lrNz/AMaKBZRAHNqYFuC/G0vOBwaJjtZycNB7FDipqT1PUUcVPXsoaL9I37+UCR9TlUwEcE6kUy6XDnwqdKKm00b3XRpYrVL5YIe8ONXK640nAvPuW2rG9iJf43kw8P8AxcXW2zX+g1WxUYrKTmTQGeI32Co9kYNPsPnkps04ht0lfdpOznIAZWgKZP8AFhUlnZQosU0WhkkYFobTUFAUk6SIwTGKAkOxZ2TBlCVaaDZnIgUF0EsFRikk/AA10JpbWipkIkiNygAV2xfoXPegmLoFL2g3fO2zTA5tWy7n8ONB/IQkR1H/AMRkwwcrd2GV/ur3hgC60D8Iy3pUDODU0LrurvAtUkMdXAlXQF1qRqgFeAIdRd9iomT6TbacjK2IpMSGsY2JARIWvFrVC+IBmMNJyvQ4egdpgJZffNGMEksFSkDCLo0u7PzxBnsObJeggsWsPHG+BcBpUANyk3hZZc8aG8y5ZMlaAwDNPzGJJJpCXEfho1M7lcd9aNcjTaucSddsa7GdL5D+JcBHCf8Ahr94mMuwGqtg3pTyS9Ifn7urLinjI7VzsJq6t9vDJapxaE8GKEwSMzglMqRBFta1MsSdIS4pw1oJOU3RSBZaTgifyUUnPLR1/wCih13g9xLtUXKSp/A9K1C9ri9An2mgmcwsaRCaiZKT7LRsLKhlnEOM0QpotALBxPHM+Am4lDhtGycNM70yvFvtpub8c3/ho9ZQdilvYvy8KljmV22OZhwc6uM4WnehOS60QhRSXWEslRZI2xAwGefSL0ThJVAFGKzaRBbk4v0VPgMqzxCXX3r6IZ1MZj2ouGd4FGahTKTeyBilBQwgjxFZKUQgOIQkIG1zo0diNVkEIjcGo0ze7RQQZAgO8xtRszhBU9kuhpyICEcI6UB6q77GW7SOEbVLiYm3+tniP/hCUPc5sPdBTXVIWYfyY4pRaiAYAsFIYHwymU1MZsl6MYXSWiam2MZ51Vx/ynS2EkacboNhxmsSaleN+VIMTvN2Co5BuZBI5AnlRuiEWHSl5y4s0bgFUSwarYDitLqasZD801N6IZb4j8U2vZoA3nSw/NqbJriCiYTPJN6ulLEo7oRBNv6q38QGHSadSl56L+xJ71Z1C9MicSzhL5QIYidX0CEjD5q4hbFG/B1RSvwibiSeReEevYQM0hAzaVhoTj+SAq82JRJoOOIfkWomx6oKXA/mH4wjAF1eVXKUISYkGwOllouExJUZDANmaTpZbzAmHieCAZBaJ6KBfo0jwo2cGWDCgzVhc8pE0J50KI4m9HJqfaWpES1OECAjmB4qk9ZA0VLBfHoI9wJ4GOgfas1hOdZuo9qmowzRsE7VM3yuoyZI20UTpXBOaSAWW3GJplB2oQw98F7UW+FmMp0OE3L0jld1wNA0DQpaxreKiaUIy2gpx+lFde5qrrLTqdpNzXfgXqUt6A9oeLegL1XsBvrPP2Kt0IPwkycYaDLmYivOJkl0s1amYwzm0Tnh4ABYTgja9wOVJnicIsPZHxcvASuwU+129CCf2g8NWAzP5RD4HmkkMx7wKe9B+mBAAn2r77dRKn2wCSvI8CpliGdkieceH28TRyvT1AEq2Ap8LQwA8wnWhFkUleQSfbwQloUCDhjjRK7oMrF4jrSt4QsewY96nLbOSzk4JqPggDx4BLK1DQGYgPmhRO8kxDgnhn4IifMLHvR8iWJB+WFCPiViHBPIeM5QAG6tKM2ID5CUSwQgPkBRxDSJInBKdClYlbYOKphYInEZKPIREK0JRspaMzUOM2KwHFu+HVu1dQ719tvQTSUFzIR4xDw8HSDN0BEOEy96+q3r6DegtRlAAbq2Kag2YL5CdaA3/Uh8xSLNy4nMye54GLyRgOK2pRNGpfyEUpTbA8gEn28ArjKIANVaYFZCjfIh9qEPWJn5AL7eSWMF82X3KgRdf3B0ig0VLGYE1E8KCtiIMrI0qHxhMCEsahyNAUKWicRhP7ipgLcU7GhQYaAFiihRzg/JkpzV+jVrSNNYG9rq0+Vsmzzzh15VZjszFfYutOl6XKyiTskmD5p53fkHGPURSaGsdgShlumc0KyLPlV9AE8aEoz3MB0oh2cOUR4vuw0uAr7Uc0obljHPtpIs1KhkLnWstbY62D5aSYZ6VWo1CQCpJkSyl2baU0iWebm6/WiilgdRi0faAJ6u7+RVkhiafuke9LVJNsCVoj5Ft0VMeyHvTGZKURak1LRq3oU6UMt4FNYBqO4bcibnABipaARzMk4kItfXbNWtAGr9w0J0ci4kcwuJvTlrY9BKzp4L/wAShdXUcAtELDJE5JjUx7UIcFI2NcbQEeMVbuOcFiFoqb7FcaCyPdtGc2mKURnY0XTCScjZrIb+E2gcEwjvbyO8ZStbO8Tg2q0wUZTRI1i7RfXEYCsBoNn3p9R8izI2AtkxrSGJQlKA2Y8IQSgFw2LePVu1dQ719tvQjK1NxID5l4eCApR6AynCYe1fVb19BvRezDMDITuAY40g5uikqobXsUbjttkCQ0GJik3MWNhhNxGiyploEtKfJAwaDuWr8UpikUBZYbLLBOIpgmsQZDAIBIzTH5q5SgXFImnDvlILZ3icMRegV6YyKkbKARzpjgapEBE5tMFC+MoWz7pniPjvr0V/qhIwT4FTYa8BIgK2C+Wgln7LAogpM4mkSyq8FV4IIjakEqWKfeXaiwsvBLyR0UiIFgEkTAxeoeBdpsVfBNBAdzsCX5isujEAipa8JjxcgfABR1KMgbKDoiy+9WtGEmW80ZMDi8VBZYghB2CsXk9h+HUH+kx8TCN0I4VjLR94zLTmkLfeYSruQcsQdlN5AJ5CdzwMpqn5ajEwP4fgPk4a++nmk9qUuGDipO1KYMBuh6EiryQVmQDyAUkUlS6rdp0aJMQa0pvTohqCqlNudBNyfuCvrtmg9kW+Gf6pNMiABggV3q4L6QijkDQ8AFybkW96ILMinNkD8gaaevHeCynCVH28JuOQfbV7Gl4ZRxc1XR1PFx4CvFGkTyqV4tTNfpX0SNdncl/3SZQoHZLlZzn/AJPD7Lc8erdq6h3pCIIBspJL7VazYLHkAY9qM2qUkHgu8cj3qOOV4AGCvqt6+g3pHHzCvrDCvanbQCUUYAT7SvKk6IMZEYKLHIsV9HtpOoUOZl6eDiTnNoG4cFABAheL4qU7MQCoLGs0s3aH44IAAEMpUsOTDKKbWqfmAcY/Eu7esigGwp0VhP8A3bOdmUazzERMJui2tJHMXPihiuiqJ85LGxxph8tDtFBBG2veSobgbA+CKdQAtYq0PxbPlCFvB6ppE6YAk2UzDFphijRiQCLgS3KryZv+KzOh7Sf34sotGYN29si1FYpmjDQYBGmwlOBBm2VJV0B3Ggw/Cx4Ak0R8FQBav1+A/vqE5IOZJ7+ByrMt5h9lKgbEbOLApEcJCOiUDkkCgWMScq+5/wBVOXBlZgYAk2tFfXbPi36ff4PmYBwQ/qoTY+qomVDi0BfaoGh3hGYVXEzZoGYeVamAv1HKkuWZXDcLJypPEPjUUE5Jai5PhFP54P7omZYnRGGt9+7NI1nsKj+qOCFRqqCtcO2nh9luePVu1dQ719tv4uka+q3r6DejJV4tlYHFJaakU60TJ7CpCOo++vo9tE7K/wAiTwGUZU/t/Q0gFDIZAT3oMrEAjNDYLY08LymFBB1N1TzAJ+pUnC1kgLIHOieICoC+N3Wf6i+d3qU+0KQQCAER1UVcJwrnCoX4UXkoJyJ6TQmAiX9lQJWmCw5341IJNUqNTZ0zTpwDLkXogYbZEGliyubzOKRmxcIWJSI53najiWCFgvGSNHpSQHokC2xdLgHzX3D+6+4f3X3D+6+gf3SwrkZwNPEaTRxCVzMIRuxaVQwMCCAXjjQjZb+A91Onhxy7qPh1UHAs92Ke0so1eT4OvgyYhOf3Q+1cR8UvoUl3dAvSk0glyxomo6NSt0DUZZsiEFmZONGApT4G/WoCJv1BAkbJSmR9nPZBimteLvNRyAPfweI4aX3gLqb2xSyAYqt0nUSSai7QtgZabsw7TNP5sMhsnYrTSKeQX8TR3VyfD6ffSbpqFs0PAWY34VO6qtIBkGIxok1J/cDMAgNWCt/hNC67Gy2hzpB+9ICDVJhMxEUP50owAeGQtJQGlJAuRBm+X2pYXkGUkDkmJcR5JeRSpfkGVli40lC1wrI6iyX51BlgqyKuq62NqRrYfGlLQMg3WipF7uyuI+KCEkfmPHq3asgZacaWwn5vF0jWROVxqICbnOinFDTcUaIzE5KENcryBvCIOKRWkOU8oyJkrMMu1HbIgdVD/VLzIyAEsAt+6eF5JnYEcGs22qb+V/AABeGJEqKDx1Iyl99VgCgWj1i4kEHAAKfZSAUN0C8NHbNYRLgAMgnVabLTyNSoIRLlY2CriqMrhJ3Vl8Ydy5xPx0k7BeFPb4gqnlUIaZCEjKWQhqXzrUM5VfKRa7iY/wBqw6i1wuBtxaJ4NjhAtJLjHtRBJekpKdD8UbhLMDaAo5Pw6gIL8WkkkU7omguxhYZCaup3BPkOdIVhqLAvAZOV+FDEPyUbYkQb06+Zj3vHumhSpmQtZsAuURQIIAgCmltg8j14e1DTF3Zmes+AEAjo0VBchHgBBI6Nf5BWIJbEVE7MBnqNJjbknfgKku/AU9JS2I8EZZug0dBDgR4pELKh8pTEm3Q+QPBOWboNDgBNQPBBIaHhBsWrhYsD4Rrj6xD5DxTlLfuA1BytQvagAgsHl4s8Q+UriTxD5DwAgkdGv8goeEGxbyf4BScs3APJ/gFTEEtiKUizASPs1G7mQNnnFQREWqWb3D+YoEIBgKQEEjo1crUyln4rgloB8AeLx0yvfKVryfdA8gkTHzaB7496dC0Ls5J9mdhfIGIgGSzO7PYpmBicDk15450717gHA6ccVlAdpIbI6mMWIrQbLAZ486mbkAGTnpOKgPuQYlp7HelVYkXWp0xYDFpDRxL86gJgLJAeTonyU9YVjopmPmowGFdhIXac8428Q5lxBLBzYKbK2bVvnLe9ABB/4S22xafgrJweFWuzRy/fLPEfE1KFwEcjRQgAxmYDMM5WPOgFXFJccWvLFQKYO4rCKEkdDNISzzdf1SF7Z4xV3OCNaAH6Xrrms2d13OP7L0MFxFUdsXQMceMARW1Wbquqt18c4Cr0g7B+XCgRQgN0Sz7/AIR/4VEw7gDHs4eDUinms7zbJcmmPcCWFkNEaeMAlWwHGkXYY9wmee201JjsaU2hN+PMN9KkfNdGBdVh5WqA4kVsZEaeER84Qnpirg73H91OZ0szCDXnUhcLvRJaMAlXYCtQZbfZNHO/CokBmQIXPA844mnd5Q1Bao+6HIUmQDJ4eJoOKSoJopnM9l8dmrwGgAoJJvK5P5OEupR0QEAYA/8ADXDomCHTk1ezakgz9rJGeAF5NqWw3iVNIl30J7s1D6piD4zBwvNcUYQ0AQBsHgcGGEWchc9mrIqExSQEMJ3aKsQMowzmVKSM+tKVkBQEOKl8RWJJhhLzUvXxjVRBbXA44XsihjBKJWGHeQXiigK496gwwaaByNWpCG6vLym7+v8AxC0Jy2I02bPlJRkTYIEJ01jRHk0PB0Ct773Q8/4r4BUgHu1JGvZOA4nYjYalLOppcuThzzgq7deQ3i7GDB/4oQHZDkupuMjtUjPzhPcTmScqEg0FeQp9gYNqiVeveLHvTMjsMOaufHkHlAatqaD258c+tSxG1r+yT8iltK5kj43MnvUMkm45HYOHyoPTwCA/8QxFJmJe1fcP4r7h/FfcP4r7h/FOZkzOvYR7DV1F6fExXwUw2PYPsyehVsQ2I/soVpwe/utKOoDuKlwNudySjge+HwHQqHXucPdyvajQFkFzEJXm19w/ivuH8V9w/ivuH8V9w/ivuH8V9w/ivuH8V9w/ivuH8V9w/ivuH8V9w/ivuH8V9w/ivuH8V9w/ivuH8V9w/ivuH8V9w/ivuH8V9w/ivuH8V9w/ivuH8V9w/ivuH8V9w/ivuH8V9w/ivuH8V9w/ivuH8V9w/ivuH8V9w/ivuH8V9w/ivuH8V9w/ivuH8V9w/ivuH8V9w/ivuH8V9w/ivuH8V9w/ivuH8V9w/ivuH8V9w/ivuH8V9w/ivuH8V9w/ivuH8V9w/ivuH8V9w/ivuH8V9w/ivuH8V9w/ivuH8V9w/ivuH8V9w/ihWAX3H8UCcRuJ4KXCOPj+FsAPOsg+YrBzkH/i0Isyh08Ot/8Aluo9jw634I0Uq+zDFqaGGIajc8COCpUogm8a+ZgXJl1sZ84lwUzA4J4FERpp3S2tK0Xv4jifxCm8Vf3c2IXH0YtABglUF3BTZtBOAnKRF7fyTEqFGYCab31LyiBbZSgmloMmxl7+WPqE2YJwO9lZ0KYU3o3LYM5MPEqHw4NhQBbi1L/fSOYvhrUGXJQW2jNZbE1fAs6+Y1jLEQWH3qHcw0zYMm0Gb1GR5AtYbGozp0p4kvShO1RsVslwnVzTImAmUJTPt5+o9jw63Wu8I3Vh80GtCza7ZZyelOpGnVB6NvarKitsAJsSAEnOnMMRysCIxOaSVqyszhdFD7U9+7KnZjZYTzal5UESNbEljG7TUgrbwDOWEkeNbJqgIW8jAY51qd3aClzjrS5wuVmEgipMMjmhncesTLkmGCNKnYRKpd4UFtzwb1DQshADpeVc08MEllLMKiTa96CrDZvsssjIWqd6lMGFiS78KFbAlKF3Kl5lJIdpRLBM70hSBORAi8dKZ5TzicvKlyZMxM4g2tqxTM/DNxSW6B806aoGQuBlmL4jSpQMtTGVGLLJO14pHo8DSzArINmaFOCM2UuezU1t2/tCYJ1vUxuWH286VfMXPAVUMWEDpQlcJjzGtEY6TLsgTZkqaYMifNmCmBGaszSJbzD0pLIxVV3W+Wi1ipFdwLhvByqdkh6CCI660pdFNsErGJjDpTaUmUJCxwoWQFAcQuomm1JOVXgELug6VdtkdOgmkL2KCasZMAstFWpViDIMJDrrWHgvITQLWl4UwDQU4UvyEtHMEyYrTDrjxEYmoxOmiARJhpDoG8RKXgo/Ag8C5CvDTxYASchLTrLSwhK9hIHOL0JnupTqDrLZKcSatO4HW5erz3pi3BLXKde4SsIDJhhmJoMcYhVeTFQG4mjTqF5JhOTLRfxgJdMeytKbJSe4c2aRjgjmz+7QS2hO1j+1ZHWWutwvARBT1zlyhKTyakLTaBYLGWpq5grlZd1rST7yiT22pYXox0SIHCjK1kWVLmBWsTU8+YHsmxU4J4AUtplW1QIdPl2Z4mumaJAAKJZIUyOprRQ8KcJfPOo9jw63V+zakaChGVWgmiiQAWLxaiSScwEzAiSYasYg1hhEjCIwcSpx1QYoMGKnHuci4FDdJ7Vx2HAGI9iCmZWJJ6WUbOdyruQxsMPiNacuxBEkEw0XwcoEAIRMTHGnxzIUiUUqF4IAo8hRdJEN7xuIVKDO8FxScWtA+CAI+QiQDXUTNLT1CYbJQgm970UbNkSwlBpLS4zoIRDCNfcbtW54uqNlAbg4mrcE2kWCA11im9JiGFKAxk+aK/SIiwyoi5LBRTJqLQrF9xs70Ud4lAFhhhMcYptpIol0QQLA53q+agB0Q6EXvK2pEGGIwWLg0JxV4RZovAMmKy0VaYYADUlYpdsqVBbBdGhNz0LxGMFOhDZbpDf5q72sBZyuA+5qHdMoxRIuL/0qcAdUiTMuCY1o6CBAOsgcptTsQAPJXJzU0a9w0jSxOh4RozlKgLBrajrLNBAzDmzR3xAFWzAVdVuSQkwOUvan4gAeSupzSpRIFkFjQ0Knd8DJlIDWKUhu5/IA3gJlqWFwgpmUNMUxe46G5HIBc0hxiAKyJmGWktqT28bw40iuS4pg1R3pgKNyAhJhxM0UcezC2Bat8VJ7NKNgOtip9nkx7ElrFRuAOIWksYJnNNZd4VQXMFWg7HgY88qhDQCqVKQmZOG1AAUpVmE22ZrXI3mF+tOJ8xiTUnJRA5iEUgYcMUr9hGXZLxmoMmwJJeO9oKAUSGiTkGjHmxt4WkSkssBO8gwO3hM97RHkNpYGmKiLeZLY9qz8qEExCpsGd6lk/XkLJz5+o9jw636SAORVI5KIG+YFESesHoX6s0Ea/wDF6j2PDrfgEQ2It0BmHeraRFDaSfOtIMmECBJMM7UQfFhEBj+AUNbETCYmhFjK9MTpPoJSCLSA3gb2o1q6XNd4i2PCcdIE4AkLPlOHORibhm+9EHxYRAY/5PUex4dbopUD6QtDhNNbUmTYRTF7JULMmJBLpNpbxUXWY2RF1JG1TwWjCUYBYLU2FMJ4HOCkLzjNSVYKwo5pcMBruof6Ekg7St1fFOkA1FoItRDbGZA4hTLvVo4hYSheUXilvMSISUAzyqGS0a2Wh7z7FW6FKYWpq4eC2BwhYWD37Uk9ZCIlkpbW9CyG0+INe/8AtEPoAtgturWyGGhMCnV5VOiIC3M29JpMrWYQUBM2ohA2CFBBZgmKkRIcTkgLLxqC2pxahwS9Q7YXEkkFHCZpSuAWTDVCmRHViBric6xyoYegS6RLMUN+QAQG9uqYcaJOTRWYsjaZik/E7xC4YYo0wEKNSMA4VJaYVaEsh0mguVY4LILLBi1MUFCGQwjgzNJrGTEVCGM5p+KxOQiAtpRyzCbSRPVyagWI+LprHtMUtNVtkTwXQ8KddNLKy7V0LsoImXQyNQqF2s7zWAIoK8JirqWt3mewobhzLJsuC0eIgQ0gF4UsK1YCrD5FqIGqAKQsdDpWAf4y2DBLTsD4kYMsVLA0BBNhusE0XUUgR8yJpTXTuEbuo5UgVhKJ2YG2/reo9jw63S7UXOxlDhFTRooQYVSEMwRShSxSIWhwnpR6NkdYlS0VksgAKzYWRtUK9W5B0DqmKYhUCdPkm5FTyAwlUif6UUBIarMMtqKxJxxVARaIymjpX81ckN7JKYylHv8ALYnWpxvgaTdJqvnZozJ+xtTEHg1kFTVQfDUAxBqxUvuhSeXi4qe0UEFs0Ox1f7pDmTFCEUqTIYW1dalR8SRKyZvKZo4JGO6APeIoHKYahFt5ag7kmXYAOFNKMYs9AjNtKFtQiVaIIL7UqetuYF4cFZpqiiQAIqkIxTjXJzmFIqExJ8FFYSY/opHjjnUyMTPaiMgxxpE8tJ8oUpGQycWiIkTb1othbL7KhEMvfoENzTWVqiXYOMNJHvAXKTSIE86+lxaKmXYdlBg1hSQKUTYuaIvzq4NoB8aIgTTvlIcvyiSKFeECS2N5BFNSkAywCDXWKQgbgQaKzCIirg1qUDCB9zRSSu+Ol14UlpaJPtpChYm+FGxETQJ7wm1RVsFOGydaM9KEMPbrNuHrOo9jw634FIRlAE86giNKlmRzAJ+KjkDoklQrbQg6UFBdgPegIQjbSgBAQVpiHSKWLbFAk9/AhAewn5pBIbjSysuYBPx4AQJONIIBKQSRbagAgsFAQAOFKyciJ+aAgCcfAEwcIJ+fEgI2gn5pJs1LMzmAT8UoiCTWlEUFMUwIDGKZETRJKiWDQIKBCXBS5QCoBOa0AnerbBDkigAgxUwsBbEmqNGjzu4ZZBe5SvqYXLBn3pAQJKgiNKAgANqlyZzAJpxy60hNMcaRjZECX3peh3WEEIg1xTKkyQCedIBCR0aAgV2B2q5YTvSiIJMNBQXYD3oEIBoVgATt6zqPY8GGSr7f+WKNdXHweCTZpLeNBtX1v6r639V9b+q+t/VfW/qvrf1X1v6r639V9b+q+t/VfW/qvrf1X1v6r639V9b+q+t/VfW/qvrf1X1v6r639V9b+q+t/VfW/qvrf1X1v6r639V9b+q+t/VfW/qvrf1X1v6r639V9b+q+t/VfW/qvrf1X1v6r639V9b+q+t/VfW/qvrf1X1v6r639V9b+q+t/VfW/qvrf1X1v6r639V9b+q+t/VfW/qvrf1X1v6r639V9b+q+t/VfW/qvrf1X1v6r639V9b+q+t/VfW/qvrf1X1v6r639V9b+q+t/VfW/qvrf1X1v6r639V9b+q+t/VfW/qvrf1X1v6r639V9b+q+t/VfW/qg2YfvRo2AH8K4qkuc6/1mv8AWa/1mv8AWa/1mv8AWa/1mv8AWa/1mv8AWa/1mv8AWa/1mv8AWa/1mv8AWa/1mv8AWa/1mv8AWa/1mv8AWa/1mv8AWa/1mv8AWa/1mv8AWa/1mv8AWa/1mv8AWa/1mv8AWaJf7nyNEikjGjX+mr/TV/pq/wBNX+mr/TV/pq/01f6av9NX+mr/AE1f6av9NX+mr/TV/pq/01f6av8ATV/pq/01f6av9NX+mr/TV/pq/wBNX+mr/TV/pq/01PpiyKxh9B1B38xvFtEpMSaS+Ctf30GCOXmYfZ4iZQzHpTPk6D2fKxoUMJZUFqw7Ackk8ZxwDvCYqLONbJDNtvT9Y9n0HUHemKLBkQKxqs2o4AROAlFtEjxcvASroFCUWemTZ5lvajgyI4P7gonhfRmYAZsngVd2Dwtrw8z6UXsJR3AG1Q2HSIRMkOGz8UNdvJEgLLN87UA8yEyEIZtikCBO2DoHLCcUBDDII2bQN8ekM+ToPZoGBqHIIEBu0d6cbLAibZv4/e7ackUwmVdFy9rUqSlyTu3IzGKYrrVOH5Z5UpAAyqXG4oMpUvKsDKwAc21CBKHGXJ0Kcq3u8qDLHPSsn4TI2FTE+1HSQNqJhODDQmHQqSrDfEJd6XGJowDGwTmtyYXNgw3F29B1j2fQdQd6fDnhdFbiXGngWcipum74tYyhmZsY+VQuP4zJFbt1vTE5YiCBIaFxoE4OoSQybKRcoQWfpNwOHPShrB0LMSTnE+9GhuiUCEi03Pmn5LxggGF9/iiSm6a4IOGJvFFXL0gljLzoAk4QRi2T0hnydB7NFDbKFQ5hL0NC4gvK5Vbr4/e7fE5J9RizZI96jdFoDAAC8znahNQKDKtel6zN+QLCRMyUJJAK6MDjim2cRCZhkeU0oE8Ko2U6fNfU71gIKCjQzc+zQ8QQkET0DrHs+g6g7+XMQSrOp4Li0azkbUpWBkdcIyTzqYHsocu6t33oIRIHJDaUKUzvGQ3Xi5fen7ZCSA2kRjhQFfQIj1p1WnmYtyZZhISjpijGCZ7+kM+ToPZ8q8HFxSMmKS6yWYHD4oibTMKslGERUJ4EOlMXebwsTmBUPap1bw4JZc8WnsrZYkTyMdKPieAQAaBTpdJRIXdBChTpAoByKucJoJct6RXoXR5oSfehshA4D0HWPZ9B1B3/AOGZ8nQez/wesez6DqDv5gfJCcTdrBFi96ESTzugSACAGzwrLkLmhfSGfJ0Hs+WOJUE7lRm9ICbeyrTayVCQGuTMWiOO/mvTDYZssbZ9J1j2fQdQd6V2zYkAskxN/igwLLpSNxmLcp8VvkW2BLUzGTi4Kw9xOVMtnivecyGpxle8zaTC0N6gM61YIL5kkeZT4lwlBUVfc9qlbaWCZbMQs7xTwhzIkWuNlqbMicqa5dWQpK4CioKF8ERUBrk7oEdWC661EgCDRzeUkxRZhSDBnNjT+qQgaxKIkjEneo56DZLC66l13oXiCw1NuzShMghAJEWUk6/zGfJ0Hs1KPAhFEQhi1+NNQF01mXWG0+P3u2n7Icwo92fmr0Kazwj0xBaFMZeMzliGjFOJcBgIvJlNKnOWbAjOaR1dXxHJktM3y5aJ/tTV3GM4kMQO1LvIcrRIpwvfgUyhwoDdCcyr4IAlyxq+g6x7PoOoO9KMkhEeY0XxkEA9jxLBSsNFnot71Z4gR5NYAsVOt3TVgvLvDflUAE3aIgYb3tR3LbdEhPOHrQPBBx0Xe6tZ9jnISPzf3qEO8C0nuS/KrcTQ2EUGHWdWCIHXp2PcmKhIFRpJDEXS0wS6mBG+yc1hRJvxpL/3sQgDRbgC8VbFBYi/8xnydB7NMFPRB8NATGgCAOAeIGF2eWioi9uiFezRyhKzeQLzD3qx0AQ8lmP8rB2IEm8InFqUKgbo8CScda3XUExyNVAZEkUVgPMKSPeKiqg1IEyEcKO0orgiJ7lPONyjkeaC/oesez6DqDv5VAgUxw8NBJxOtSEAm9qggCXWgiHGyUxyEHhppxa0sECmGgIJHRqCIoAggNCpCG8J+aQIJHRoAIKCgU0A96FCA0LfzGfJ0Hs+VAQJOPhAwCXWo4g6JJQMI7BFPCEMSTFQMAnegFQBc0oikxih0IsoXfemyVqLAxtLzo9EsIzKb6weh6x7PoOoO/puETKWbxmP5TPk6D2f+D1j2fQdQd6QXZKAHNaFYBKI8k8QJS6kgyGHnRzkwKRGBrFSRZiwJDHCiDXpHwWazcoomay+EzQ+tiwDQxk50yJjEgS7E5aXMsBCrZDFDMQCWZZvGYrPEMQWNjWjTZgc+Fo4RLiMjQWHgiF2mYmuFfkHy1Pk2rs2MoSlAqgM0UvMufeTnbrXsm/pJn+Qz5Og9mmxnlQfLQMRJEZE4PjaJEBlMDnRNpgliuScNAf23gV8bx2rgcm4mN4zFMFGAoK8N6EALISJEKe+sVxmwhPk9N1j2fQdQd6mEzJKEuhmL/NPC0lAgbjA25x4/Q4KMomAEoRNhFL1nfSzoM3Z09qY+ARbGygSZ9qnoVm6ZXsNI2TKwrhm0TagzCDYn5KKdLAJmVk/PrtR05tnX97fKrf4LpAJym/tUm2UbH6V+Kx5PCilJkQ0C5REPRKAkIbnKpszerobCSq9KnZcyIgud89KuEHTVCwcSN6XGyAZbVuRLUz4sLXCs81Q4yiC5XF5dJ/kM+ToPZqEaCQoohDNrTpUvA9NZppLePFL2i8NABBgoxmSX3f7R0neus96MDo9zgQm/aj4CWCW7lvPpusez6DqDv5UcRpwKyMXo21EVIiU296mzAnZUAeazTvroDk3Jdb4M0peKDUmttJD2oFCxACxHLj8Uclk58SXcSNWmTg5Aocfnek7JMCU7/CFCaseI0BO8KU1IauRgd33qOREQsJrkUK+D7oVeCWKAWXglUQ23qPpC4GIgfB1pMC4SSgowFoCiDXcdJ4vT5qY8JEFtOlqLA8YiN5l2aVZkBO8Gf4zPk6D2fKu7kfyU8HDQYLCGzhbMVKTS11quJu/NSH2ViM2T7pScBYaWBX0xAU+Rw7EBieVSM9AwwMLGJ29N1j2fQdQd/Sx/WVngNh/MZ8nQez/AMHrHs+g6g7/APDM+ToPZ/nEbnousez6DqDvUVM8JlVsBdaeBJwImwbnjbrgREuFkpUMZKAnLThNFztMh43GOzRZCyGubwKe9EkL5hFuwMcal4iCkOlw2zRPykJIMapDkxVxXCC2zdYgxq0WEiRHlIJY3LUPBEGFHFXPSmfJ0Hs0dNkAFBmAvQ8LGTRMiNx8Y8hZEcdSkUSrq8lIMMk2DqjFItMKMiZBa5NpqPZpQi3gFqFNmXAjdAwc6AREnVqQSLQ71MNMDEy1jRTC3RHUZlEVEY1D/PESrrMVw5gMEbUoQEQhhQIjJ3qSBJKoOQxRQMc4MOb2f5+sez6DqDvQXhjwIhJ0SLUhIIvISi+qz4/Q4K+p2U7o3Q0kgPwoygMCXGCcbe1AAKMxFaWM56FNDaO0c9ippBOmD+0OlZHAeGn7h80BxppkVYnE6tHisfBldHz8+lM+ToPZoOFiLAiGR3KAc4KmwADri/ijAYXppGpEj7KRcI1wagjeAkQCxvFHCiOjAX9loXJ92DFmpbT+qOPCGEpMm/GoKkswJy1rjjXNqJpYtlJTin2V0fcogoAQrIBfABaoUUIsGSHJ9tUGIzhKSxDWf5+sez6DqDv5b40QQMjeL6UbN4MggyXaOavVjMGxQvJgaveXavzRVgVAbmJut2kC9eyYAkGg7hMIMza971d/oKFyt74goX1dOymzIXvTAIlmIS7aItv6Uz5Og9nyi7LoMmycRuVcSsfbFhx708ukkRMYBsUdWQAwAkc6WaeRU9J8t9b0yo5BBdDVHGp7FEIEIYbFGNo3CSgvYBaDmc2LKAsvOigq+soBPFN2KtsISQhBkm1EVSTOGU2mN6YeoKRgixjSg01gMaqDQ/n6x7PoOoO//DM+ToPZ/gC4MNVHRBp6nrHs+g6g7+VGIkqsAcWvqn91DTaRIugi6Zaf2TJuJJ5lglqbqpBNwhD0hnydB7Pmk8OrPhaBixEZH3PVdY9n0HUHehgW0MhdzYDVoOykBI3MWR38focFIAw0ii7mi6ODwEd6tzRSVEBvUtqXOIibFBvisWnSW8mxRqAzDBuaJSBOIFgzCmtGyZfCLkmC2k1owwLKNkZTN9qVamS1mEwTpQ+xTo2iVpATSmymJ4FB/wAsSBKPVmnFIXCgnGtKgGuYE3NIwzR77RQAzAwvD+Yz5Og9mp3H2koZVbBR6MMHBox4umkHuyeYFuNWQUKxUXi9immxXrDOJ4nw8KMB+NsZiBbWq5xMhCm903inrTCBXaTd4VboZ3BkpgjZiQxYUB0ooxnaNMtsUsoUwATySHNJPKjcc3MsU9QoEOwJmTaoeoygAbwsxWi8AkyYAbraKT7kEzm5MMk8f5usez6DqDvSfmkOFyES1mlTGmGxgBfd8focFJkPYJKCwDarUahyCUCQGMkCMm1Luc7Iym2jZ/FTLaLSjYlopK4slBxPA8dKuIkOiRkOafiphLlb2C8YQovbgu+g7/FA7BQLoREvCkNxYBuF7tN8w0tTKsbUEX0xYiHAdaOJnutFwnaVoT80jA2IMyvWhoSBckkLzkP5jPk6D2avPQISZBJPZpq2wmBhFjQA8SkTw3Ef7oaxAMI3Ka/Zjob3+D5ow1xbSOT0okiArcC6c2Ka8lK7sl8VKu8IbiOCiumuBIQciaQiGC2iHRt+6vMM2ZQyjtE1KEgS7isYmc0gRKbCQIG5opjWyJM2tKYpv4aG8gjHtRvXsRsBtfj3/m6x7PoOoO/leJOylLzMPKhaCNEDAXqYU2otcJjhVkj4UEBfs4oq+IpzMGrSRzJCNqlppCYZInIya1s0Xr25ToyufWniAlywGGTJSjqILLGV4rei3kWCwtBO1PHFqAI4s6zU4aRCZCw1BE0sbhgsTN6W/MlIiy1WstZEtByVG3Ua3UOH8xnydB7PlGZMDUpFoqxFspIpK/8ANnhcCiCSySh8aU54jLIUZV5tWXmMNqQcUBBhMJBoFjN6RQZBIiy3SoLu2woRIwqU0F7yE8L4pNu0hJZk4rKIg7GIwEWKxQQUbAbobUig5lAVxte5V/fkL0IMp/N1j2fQdQd/+GZ8nQez/wAHrHs+g6g7+WyFZCwYwXqENbxEhJZ9aZ8nQez5RXhKoCl3qFcGxCkfxwgAZBLUz6DrHs+g6g7050l3AJVi7kgpiEi7BKEm42ZPH6HBVhrWWWoiHWrtOJQSUDBqaxSoLuZRZPZPVmfJ0Hs0elJJYDSJVpzeBMjAiTwfFshw6IzbWKPCKYA+xbblT7x6Yl3XYmo7sDYTaWbP29IhAszAyBrGKFE8LXLOieEcKGXK1AWkYIucKkgB4ht7ptS8AzW8TOJi8dKK8ezlKMRqJFTi6ohElbXwU5ERDMJQfJFBjAo7oA3mOtIGEA1SB0T7VNUIMwMgoJCCUprLZjWhIQssKkDWCk52LZFxM6l8Wo7uQyGokCwc6FGeWzDs7fw9Y9n0HUHeiCJxI3A0vQTPK4vDTx+hweFnE4a9f6tGfJ0Hs0lItXwTknZqApqpZRyrq+KHbQMJGQmlbNLQIt5DMb+1ae6kCc7XFN5X6lRIg1jNS3hg5GVvaiMvgmRRYBGxT8ZqIjjaAEuNrRilQqFEpwuWsKECe7bm4xDTqqUO/fyu/FEvAEOAUZm2dlMWx8kpAcTTjSdcKWzdiIp+6fIwEGS16I6QA1VWpFSkfALaEkNalxLSETXde8/w9Y9n0HUHfy5SrJJM5L0LiCyGFuGbRQBFjK2WTnFXAFuOSrdly+rM+ToPZ8p1TTKytxLlImfPtSFirpFNh3EuPEp3qpZq4CxUIQr6RibVbpuBSYZLl8lKZ8jJ02TnFBtEG0hyMzMzTcPJfhU0dAsYwVJT0qVm82FsTtUUwYZupCxMTFGG07iRibZqJfomSSZyXoeIHBQIBmrsc5l081ISRCFbhseDnUmKLiKKUARkCqu6sq/w9Y9n0HUHf/hmfJ0Hs/8AB6x7PoOoO/lWJO2hbxEvOpjDjFJuMmRp2sgzIBNozp6wz5Og9nyr80rgKcNqbdYGJbibU3u6BCm9ozFqMep6x7PoOoO9SgJhEoCQNBd+FJ1jgQsFUmYjr4/Q4KtBgRgUS9/bjSLymJhGpk6kyvxjTc+eFSIgNgsXyGabrmQF1jgG0VE5wcBJBGOdQ0eyBYyh1V1avUHBIKpJwLemM+ToPZotBhrrCA6ZmlBX0kRAbxqTHk+o40yiBQ3YgVEmOOw1DeDVzQN0rGC14F6kgK8tCUB03pi6FsYli5OpFNSEMnSDiyaWjdxxIlbs0GbbhhZwWm3o+sez6DqDvWZNpE9mgWLBB8Hj9DgonMk4BDiN6YoHa3JfCUWokiKC+jHWpZCdzErcIjWpqrK1CSwlzQltTikgEXJNp4V95t6cZ8nQezV3OTAE73owzwMByDyfUca+/wBqfZ7UlENMijoaQqskTtS6avTcSYJnDf5qMWJZ+xT+ti0zZGYbWpgaQcCCQHKOHo+sez6DqDv5TymhCROI0fACACAOBXDtFqzac00U8iB9mp275/pCiwDhDDlJURGFAJsHbhUDIQQFGJcvpzPk6D2fNjmEAPM60MMGYAkmt6bgBCJInEouAEAEAcClpJd/7iKx7GwJbFmgAgoMmwXD5KJjOAA+D0fWPZ9B1B3/AOGZ8nQez/wesez6DqDvQCkBq0AII6njL71uBocXBQ+5gB0pYEh9qu5xBZ0hw/3aKkmJvSqJZAzHqjPk6D2aBlANW1CJJc8bEB0ZLA2rK2SN0msTNoyXS+9SXvjNHQBuM+o6x7PoOoO9BzIZKpFyF0M9KT0ckjhkDcG3v4szC5wle4UEQCg2BFDCCIMwy1oN4YmgbKETQqq0GTMubM+qM+ToPZq1YAF0yBpGKQgxMyETe4TMeP3u2vr9nhuiK3ywAKXBLIZqdnqADCTzPUdY9n0HUHfyiCIWwqV85oD4MOAM7x9KDIyoudDTR9ijl8tKJAXBml34NoEysaHqjPk6D2fLBIMtwNMuElFtQIisGcY51Y2jNcCeNijxmGMggdYclEjphynDnHx6jrHs+g6g7/8ADM+ToPZ/gHcxEZgC76nrHs+g6g7/APDM+ToPZ/ggWxSpOn8ERBJBLEux6HrHs+g6g70vVESxd0N3lQqdolwmiZHn4lTDnBYe7QZwEciluxepCo4J2hdbwe81GSiLVxNTaHXO0fNBiORso3hLFX6/Csw7JkedPA/fZBmGVNYo9pB8JhPTGfJ0Hs0dZUCsvALtYgyvJ5IwUitpEGPflTByLROVWBVSu3MBLQOziWb7OzT4skyrpICba0akhVABJG8lahxzFyGJ9qAY5TheRl9qJIkpkhvDDFC3RgXl2DL7UBWuS42yNz0PWPZ9B1B3oXQ0NZEWW0ka1HO8CIA3UtLOniVko8kv9xSgC3DaEUdBQRmEtEEu66YifrFTBZztqfH9VJaJDMImNv6oidJtyLhZe/WipF0ShItLATRqRgIANAPTGfJ0Hs0CcIiJCGSYNL0I6WmkMBE4m148RFYD+L+GaYyJvV27YwTSKp3ak2XnCnaSwIaAOONE1EYyNBplRohcAJlQbS70FFSjNF4xaelLpAMJFwxnekV8LILA2L60hH2Fi22TrRURoEpJeMa9KZ6MmAESQtJv6HrHs+g6g7+UcpTBk2TiNygj3EmmDjHvQlziLTqTlv8Atpxk5IBt9JJpyYepFcE8u9Bdlhm2VFo+6Uw1AQCV1OGhVyLZUspzazf05nydB7Pljz36wugGutQ4+IrSaoN0ycF3pU9kigmsKMkSUG1jorAq4/ludibF6vUEzw6mtFuKaisStgqcejChOHRvSzpFdZjMi4/FMvZ/jmfulLwj1YrKKZII9D1j2fQdQd/+GZ8nQez/AMHrHs+g6g7+XDxzr/SKBlCcP4Ro6FYHAejM+ToPZ8qgSsFJQC8HyyCVmzHMcvTdY9n0HUHegToJJTYO9NXEDyOJx0fE8XPjtk5E4peW/nFflLloAiwldAwVYxwrNfFPdOtWWwuIRum8cqBetg4ol0mGKh+lKitXgTeoRX5Uk3bGKAqvmPiNOAtWrYibjdihl0OROhuNadQtSt1g5Kewlc2gM0LajNSYEmz6Ez5Og9mpMoQ8qMsWsUJayoSAaJv4v7KGzGmgBHDpRiF1bWqK7cLSu5hNRKixILtZ2pOQs46MzPKhMN3OpjO3tThzJHcSodasAN3p6XrHs+g6g70N9hZqBCMXMENIBYc4pQE3csvl8gVJQwJuTjdRAAjbBZkPiaGJrjgWnlNFOYFYUjqSpiR3DDPQj7VGO84sYsKrR7U2iHkltcWJkoqyq8oGNpgo1BRWMkxvY+KRuinYSkHZisCFAsEjs2pY0rnxd2ufn0JnydB7NAjBhPIaYRqPWBFpQAE6AeP3u2rPIGIhJKMUyoKHQYRGF5Dahc4mXIACJi4ULMEnzfwUSUEgKhtQISDosH6pGgBCvU3/AB6XrHs+g6g7+Z/EMzLJM9lRYlU6ki9EOEVAOhq+1S/BBTKkYtyqCgbWLm8S4trpTnM0wDi4uKj/ALvg5ji0aftIZZRGIdALFHlMIQjeyyzmrJAmkhyDamaJhARmykhaXFTQ3JXcyjhN6iOAAjMwHoTPk6D2fLGUoEbJTi1REglLIREzOKGhAZEry4zWF9QQCJDBrvWHMSymQF4RosoIREPw21moRYEUsUZp47ebibkPHl6XrHs+g6g7/wDDM+ToPZ/4PWPZ9B1B38prhS8B7UDEA2FEjR/zL2R1208jF4YIwtWUt/OymwwIibLMzbb+Ez5Og9nywgnHESwda+4/ii1EAwiSPkMBmPTdY9n0HUHeg5NkuBKrwmonZAsolCPs28focFfa7K+y3rB1rq+eWQBdaIItY/yTvS/A4odT2fijaGEH0JDUclORIpExlNBROcwVeNjnRSOvWZYMjVrpV7zRt6fAzanBkDpaWmhohSCQoBoJiplyTcRNYklot8gNa0a1sLJ52ow0DlQy4QvMG9GRQAkx6kWikoaFVYHhCswFI0iN0LCfHyUWoRca6HVq8KPyBLK1Q2f4TPk6D2aB7IQUCwrFI88TIMTZ2R8fvdtKbMUWdh1mpOgPDhlLBVpf0UeCkYcaoYVUO3IhlbxoQ3ppWRkrEATAvx9N1j2fQdQd6nzJBhcMTJDqNX1dBKFjFgNvFxitAyyF0KMmjY3HCPxUwBh6QRtO9Put6Al1ipOwERusRIfDSiIWTskEpoTVuWagGQLLzHvV2AFELQkNRAGGvuFEhG7yB5CbAM1N+/A+82JoekxBvYEoJWAB9aVPoSPNGbve9Yv1cQiYfZoShudKM/FT7mRIxIw8ygS/e9ELeZfmj3WhOTWHl+aY8SZORDxh/hM+ToPZpN5i0gOS8iUdC4oSjV8fvdtJTJyhMDOKnrUXIkSxq3q4cWyISRF40oRHLeIaNQabgInBEyaNykDtTNsfj03WPZ9B1B38qCQ3GlADl15zaGKjAnLMvdLtAFFbQjTe1Xp7tBMJk1xRC3TyqDtU6SLbASdBFRxilpe4YpKsDMiRi2KxoQYQlMmuabDT2iLqbX1qYiFBzybWc0r9hELqBXmBhpBIbjTWllGZmgcETGVd03Wpspyu5zIb6w01kFW0EENOO9XnKvl22xVrXNoQ43tU3EkqRxhm1FAUN3FN+JP8JnydB7PlHxAWUMqS9BEABUZKQJCCJMptegKDRzuSTjcFQfLUNKYY51LIWVGm6poAzwZmQ4+m6x7PoOoO/wDPmzXHEAA+jM+ToPZ/4PWPZ9B1B382PecXsXGePLwvWplZwLOvqzPk6D2fKscLDKqDNATABOyTTxLETkL+q6x7PoOoO9IUHhlkTANpeNQLhglgNkLSR4tDDUdAutN6WpqYxzuUaLIftl0mlnklsCWoQxQjUSP9UlqIShWzRufNEpbvAMBwWCVd6Cj4eG6DmVZKPHK3QOVOduSpbERaBtHpTPk6D2aFeERBRAgmTW9FuVwEBgZjEk3jx+9200itIksBa9QIuAwZyaVOm8jEzA2Aoo+LuNr4DeiJjp2NgSLrCq1xbsKYNzDBQPsPDCEjJ6XrHs+g6g70hLJTkkwmzxKJEzMMSurqvPxZy2Jl2vaT3oVuTKl0J0mgvwDiIBIOInw0x8c9ECh4iieTJMTmjKLDKALk3CO3isdkxBETYzGV6COlpdwFhJ09KZ8nQezR90SBw7jkrD4VwHj97tr6jZX3nGtcQA1CEA7/AOU9Xg4xpqqZrrneihzCIHKSoQuVCwkJanQLXf0vWPZ9B1B38stMES3icPgoiQMEdmqKAA7w0jKxlag8Onfc60KJYGA5BUAl2OkZWMtWzMwTDAuWKmq7zi0r/NEbPAMCJwpt1fSmfJ0Hs+V/A6TAxizQIwCAMAVDLmEIcS60WGnoEclvSdOzBLnGak+agHcYy1mxwIH2ai8lwInJ09L1j2fQdQd/+GZ8nQez/wAHrHs+g6g7+W97UMmQvHOguHTAQKE0gmh3xDYXiLbeRRUFwZYkueoM+ToPZ8pxgUsWDSgYhVDgnyILwU26y0Tz08GxwSEBYznQ9L1j2fQdQd6akQTRiMkuSmayeIFcMU3jHv4/Q4K+12V9lvWX1QGzEmlK3dqZrfrjgC8ysxgiBiJGlzrV3I8UBdLwmlInbTwuB0TFAg87CkMEph9+rCLY2SjjR/RoniEKbTr6Qz5Og9mrhpGRpghpGKdoREyUDe6TMeP3u2kUBAinujSpsZYctzoYYpCH/PNmU6KLKXCNcM350haFCbdN0YoRCFubKpyelA3bNy4uOsOKdPKmoAsi8E0LEFSCSItyfR9Y9n0HUHekBCSNACCA0PHIQKtND81wYnvYJrFYnN3JyjTX5o6DQE102TilL9EUsyTFOOvJIDChyOtJyDlppMZAC60zZgkSmAsxPSnU1EBk5BYM1GYkUi2Tg9KZ8nQezSCCR0aALHjPkLArjoVJB4QpF1imGKICVeCrofGBRuV40p+RATGywregLBgRbpsnFDGCljdIxzipGdZBAbG5bDV8+060ubfmjhrKphm68uLej6x7PoOoO/8AwzPk6D2f+D1j2fQdQd/4JHHqzPk6D2f4FC7UiSUI3GfT9Y9n0HUHes8SCA92gWPBB8nkDkjaRuXLHClygm3BJQM+ZDORP9VJE2IAn2owBhCnkN60T3ABzjFL2gQADJbWkw5kUXO1RRZB0w+5p70VM5RInBPSGfJ0Hs1evkQBO16KscrEeSeNlNPYo/PChoIF4W4N7tW0n9ZNE3s+5RAZYYJIUbYw70S+QcgVuU6KW4mkWxoyw3v6frHs+g6g71NgAzCEgSbKf3TzZ5kWBFRhZ6ePUVahY92CmEvnmrW0gaicR4wu/i3tRetRgJQrS3opL9wF0NMPzUNKU6DBWwxWOeMYiwwMHWtDyFoTgfPSg6QcvEScGDrU+JDBScjg4a0s5rYJWx2t8+kM+ToPZoZICa8wgDnEUHEzJmKBDGrE+MhMPt4f3REEYoaTL9z9NJ5w+RrBq0ZWxSTvRpwpcmNgEusHp+sez6DqDv5QXsziSlzm+21R2qE4AjajZDzANNqzODWm78QALqBcsjRO7JwKMu9u9PcpIF8wto/qKPOZaG5wJu0PpwZJBqhaX5oOCLFqD3rUxgoEEzA2TS6BfEYnAWZx6Qz5Og9nyyvsB8k/nhULtx2QWEOvxWWhgtqO/wC6EEyACFGLZc0wle5CDlvn1HWPZ9B1B3/4ZnydB7P/AAesez6DqDv/AMMz5Og9n+WSYm/gVilYQGUMsei6x7PoOoO9QRywsqbAXaUnRBCJsjc8hBZpkpsWznhUtC4hLwBwqPAVQTDDI3ISpHiPCQOfRowCxj7tZxpRGaKBn0MVfjAAqWiyxMkTUKA2S8LBdZp2Ve0Rgks3PSmfJ0Hs1FKKCFBmA2ouNhJZEyI3HxOAATje9i9CTzdUuhwptVV5cm59o+KuJoYrF8TOKU9GkLPMYzrSHJsjgRDF6THoiUU4INSOxQDEm5IbUFtjkmFhJaJxrT/QJFR7oYOdBAhWLIxYjca0DMzyQlpXyEqGbw6eg6x7PoOoO9KzCjAAEUmG1qXVUUUAxdiW/jnndO7sPlKVGwUakZvBENOCZwMXLIcalQBkB8D7w+9SJAD2R3+KRARk3EIHM6HGpSmCIkhytkpjV1hbvrajw4o5G/L4oMoEBsFj0pnydB7NHBRRoEDIw3KFtuiIACc4z4nLF7Yhz8USeQkTZqF2hB0lN/lRnyd66z3oiYLz01mbSKmHg0RbaDCwi/Sf1X2G9AtEZTdLbYKhEpSIAS8YoGFoEkLyGZav46ImwgCxp6DrHs+g6g7+Ux3HKdMj++FGRgADYKEVJM2IgaadWoP3VmbJEEXAL1J5l2AXLfb0AUY4mUZRzKf4QdQQhsGJgmKctTY9zOlLdR6YhUHtHz6Yz5Og9nyz9iyZEuJxGjV7ou0LDjm0nI0kjYOhSGegVfoxzovAiEG8O65Wk4IqypiRjlRgznUxYmN+M0/ojIFxdXaU8YSb7hL7Raj10DSxoiiqAiEIIDQJvNDYVXQsTFxoDYWUXpabhGbeg6x7PoOoO/8AJK7KUMMJFmmoIQVS5Kt30xnydB7P/B6x7PoOoO/lM40rgG6tFLYAzdaESS4+EzluthLTjqazDcjJzKBEgLrSZnGsgMnkYEUDAHCHXHozPk6D2fLlImwOa1mjOxH3KXBMRILVMx/AiJOCMHHf0XWPZ9B1B3pg7lIlCVXQKM6BCYnCNpHx+hwVFKsNisl2VJ9AayJrKdIxwoQ7BQjyi3oNMITBXIVJ1q0ntAXQvBT7QgdZH2pTjCC4ysxrUVlCA8uQwXgKkpHxdhpkCskFMxShlAQGDBE0Rt6Mz5Og9mpkBIsAMq3pvlqhgnOonj9rtpfUwuZ9NTj4BzAKUiWqWKSSUibkGaX0xBM7ba36qQemERgZLBUs/FDK3EbvgB8MouBly1FWm8DWTiXs0LqJQbklinMSlTgA1oQK4eNy0KQBMAP2hhqY3YvUMDC6rUHZAiYJM4Ma/wAnWPZ9B1B3p0gLupsiORoDTgAmMAGA8focFBOJfoXZR0pwT0mZK06wEnOpLZiJYpHIzTwjaAU0EuaQQEqY1agPQID58MEIBJY1clWxLCbsFPKMHuuZ9opyAjcHVjj6Mz5Og9mlycwg2ciNSDGpmDBggseP3u2rkiIygPQn2qz7YtmI9mpjZUK0olIC1GK0DkBIUXelQ01RfhbsdKHiHcEEB4zmlY1JengByxJ5uY9pmghhQ5HZ4xFOLptLOURwxS2GUZCy9qe5Es2SispYefJOlH6g19yQ/wAvWPZ9B1B38rySecDW0zQtI0cGkBijJTrZU5U5XjS8PQiTsJaix+VZmMuLcKsAbQykrMAa03jtrdoIC16h0KDCEMQBbSraGkMpKmINabZuUYF0IC16JdoABzpJRPKhRYJa0yzLLn0ZnydB7PldrCQyKkyNX4gGdgilrCUkiQuE5N6F4EJgbO9EL6JNgMC000nEGo6WAg2rUEADrsJv/VK8sJfERMcPARqx8wm9T7hExKVm7QAYZQNt1IBRCINtBRupiKTZ3KDiEfBLE0PSNokSzadf5esez6DqDv8A8Mz5Og9n/g9Y9n0HUHfyv4lppYxYoatwybHWRbUSZLSYdkyPB/hvLwXA0utECOAuI3E8AY0DKBMBvUJjAIF4ue385nydB7PlYMIDGVVBBWMLxx8iahQNGZMR80K0pGAO54AByTmGGfBCToLglxdoRBLj6TrHs+g6g70bgYciBbarNqLmMjgJRbRI8focFHeJOJI2U7CXZwZZDS6+alwuPS7BtFLPaewZkR1t9mrV0qvNRdnS/GgGMNEKoX661ZE1knRB0yU19RFnIATLDLtUb1rEiLx7lOrjFcwXYaDIjGTcicGrNS3GyBhBNsWeNQmUkLICZb4qZYLXAI8ySkYvqKO1WYzDbSzmJcJxTDlmv1sxNJItMbhG8TpP8hnydB7NGDjQYAggN71IocCLAiGmb+P3u2kMRk4UDTRvWExEbhLIuQxR81Rsg6GtXOWUDHW4a1C/qBBCgG96CTAEV6L6neoHUUGaShxERTwngsG93t6TrHs+g6g70rDPAZETUS41Em7YVVurd8focFFgaRIgskzPCou5iiSZThd6VPBE2WuLnR96KuBjIuJFpsfNEaRsazvm05jIiaZe0FGtfm70f2+K+z4197vRwwSNJu0uZs/3WrDYIgRbxCaFusRkWRezmnSIYb8BvTw18OmsywAd+NGqJIU14lyvXSo/viECykhxiff+Qz5Og9mhZplSicEvRcLkG7lVuvHx+920DM8iEKSZnvRoJb4aB8s+9BL0FiwzabY96LQ0KzUutcB2Y2iuidlCIoxG1qxucEoJaQx6TrHs+g6g7+UUh5YTZoPPG8uipQFRgAAcAo0ZwFANoQxT68aG7zctbf3E4SUyPgkYYfEUZU4xgzSpRH1OVoz4OpHBSQnkhBd0EH4r4jowREYiNKKWUsrmYutuFXkrBwHcwH2oDOAcj+6a69tkmN80Oc42AEBTdtsrN2Zu7KMGAgCwBofyGfJ0Hs+V+Di4lGSijAgGgFopJ/ophOZppzUIdO3Dar6ApE3cNHJkpGAycsUcQsYSZqxkchMBERTyVCcrxjM49J1j2fQdQd/+GZ8nQez/AMHrHs+g6g7+VSJIc4m4Z96HMuMJVxhooq4GCFydYfWGfJ0Hs+U2RA2BlGb0NggPFNYqEgbaw78Twb0T6TCFrNHMSYQK8D0/WPZ9B1B3plWMkACyTA3+KGyQUqyNxyFuU+P0OCl11OIiJU3HSsYpYYBBei8lBxAqgwiYz3qVmgmy7CQLVECMpLZcOC39SZ8nQezU5syFIiEMWvGtS0D0UmusNp8fvdtHr3dxgwE4ob7WhdiE0uuVHf0Rrcu6a0YbEqQt7hoHgpcaa8iWgt0fCwnk6ZqdLAuUouN1fiU9MbXAfB9kpjqRUrOcWSpvU82Il2E9F1j2fQdQd6bW5ICPMaEYJAAex4sIS7OSigCE7KorFECzxHRjrU6p1cJMEgsDPGljGMunqpnydB7NLmHRE+Gj4LAAgPbxK0QtibRoHoTpAiUiJp85rdpWU2nuocVZbYJEUzTH8jVQixvpSydTtTkOMLFFOOW6gxiInjUhQq0BEPGKvrWWEt3MnOn9EuMDnq9qMmAANg9F1j2fQdQd/KgkJI0AEFAwiOjQYAAxbFXLLmvqjPk6D2fKg2aAuAeA5YaoLSCQ4oAILFad4oJ+fCBkLutXLL7+j6x7PoOoO/8AwzPk6D2f+D1j2fQRak2rhK4SuErhK4SuErhK4SuErhK4SuErhK4SuErhK4SuErhK4SuErhK4SuErhK4SuErhK4SuErhK4SuErhK4TyRNEzDv/wAHu7u7u7u7u7u7u7u7u7u7uG4YJ/yZjtPcAmLHSgLgMgaTDe+0TQKWykEGiMI8zwVmAVeBUIijtkxNhr6NMZJswErbhU6+FhrowNn/AJjtxKQKExUqlCyB1KVjsSi5S/uNB11UhQYkoezYhRokITUxTPNBMArB8tSP2PIOpTKvUVzDBg1p6cA1MnE86BngozTEHGGnrSCIBWA92rr9ZZJkoXWSIBxWjiCgYK5on2pzqhYgrB8tF0I3SVjAXY5VYbSNJJpweDQKTkaYbwShxaEQ4Mi8xJ72q9ivLipzlB+SKknUPCQIGkEU3K8uSc4mPemgZKEIcJUKBpA52l72tiG8MMcavdqBIl2DK8ClZBsiCwIwkxaSsJtN2M6udqsBd11qwzqUIZkKCXYMvtQ0NikQsnK4xo0TewtzIGeK0dMTgkZAypwKAMiwSLMrh4N6zR9tw3hhjjiio0LQhYE96egQZAuBcDzSlzTt3cQJY4xQ8Pl8o4JRtvjSJxITHvQy2OV2W86UFGJaETEnvQoHlyuEMa2qfeWQTNt8vCtHFcjLczWjVoEnY3eVGXCWJDeUMe1MKpM0WZjlND8s7LChdpkq6RIxm8JY9qgp7CBh2TI8/A1+SsA4ranSngJbCwPtWITSB7tFHNF0WxAn2oyiTaAxet8BYo3hqt8FTMEiBBm6I96utlTYbwwpxLesJAyPa6Hvc96WRUyCuXu/Y1MUG1iw9ubaiwbU0kg9b3anx0RnZBHsUTKDL5dLJ8VOVFnEO8+KhZQd0iyv9CrxsNgzvlhedQGtBhCfO6lffJEn5TzqIuEhq39xlpBmjN1j3QIOLQTKJhGdxm40Cj3xgAQ6oDU0aW1Byms7tKnSA250k5lH6giBUAHQTppVyBjwMSdlJ8SQs2gMdm201NkNAyuguTmowwmRHJffs2p9M0YkAuxdpE0Ey3Z/EielNZlJggQMSr2qyG0lMUFi5HWui1dxXimxmnAyYpJrmoAVBuwvOk2mi4FM2ASF0t7+Ga7oG8AKhGZW5bFFZJigPGxNMhQFyZdjMT0pbYIWywUqFZcjUFpLm7mpypRIVFEuJuphlplwSPsoaahAqKCyoYCHSmW5Q5mAmiTXGeHOVGftCQBrOXiaU1Kl4aCvAGjSQS6j5gg9ykLpXRDeImJXUvFNsaQDEZRa2o7lOhRkhA0h2aRwl5oQJytoaztWMgZYDpYYKEThCs2WMXBceCkOaoFQneB1ppIGLgIEJjTNTKIE6q7NUED90cQTA3oIAhHpSKHX5gBcUL8ahyvUglYzuEcKEbXuEo1lRrUElgEwROU9bCTVQG5ijAzYa1EgwaCoDdQkQfmsw2gLbTFt33qAeGE6N/YYoRdOAPYkLgl+KjKKICeQs2ZbFQoqPARU9WJUFLjKhd3oJ4yMKeMkmA+7SqQqcAJJOE+6UOiViDxCa6s6M0R4mG2TIlhJic0osg4AtLPYCHamtaRUFUWCpRpqSLGFxS9LVLZA5JZdKhkgYYui4b3HWubPTN4xg0I2o4wGIgYzca0xQL0TNImY03po04lAAWKZMVct4CU7ArJGkQN6KkosSRuGF30pa4yiUnFzpTqithBFiZsm1Rps0kiJKYRa2iVDFEe0VMLBm7SHrLoLDcsGCn7Odo3ANyaVOgVQBsgwJq4zUaNYUjlYIFYN6XurSlKJMxYN6aAsuiwCGRS9Tl3JJYsXOlJjI2EEWJmzwoI7rCESl0cUphlKFgLdDWpSM6k4aRi8mtMmh6EmQO9Bdl0+bwVP8abaJuAkIuimFjlUVdOCGbFxpXFZLCYHzRzAMkuSydykJGjCTDEznhUASiQAJJCADJM5rEMTCZXuU2LTY4UtQiQwLZMpmNKaKUacoVYqMEGSQ6p0I1IzFXcRAAkLgy0PqHiHiE11vozQnGUYTe7RG40hUQCRUkGRGC9K+RL4KlYDiZ9qh5Ut7ltQw506AU7C4S9rstJePUjKAbk0pv5zSVQsK4KDyCAAjKFm3L/mIJDcaVi6SE8i85EpTZ4gw5FwhsB/BA0gjYyxCAtJVsi8JGw2i7w1GrPYBB/8H//Z" alt="" width="700" height="1015" />Adverbs of place can refer to distances. For example:</p>\r\n<p>Nearby, far away, miles apart.</p>\r\n<p>An adverb of place can indicate an object's position in relation to another object. For example:</p>\r\n<p>Below, between, above, behind, through, around and so forth.</p>\r\n<p>&nbsp;</p>	\N	t	2021-11-25 17:19:23.592761+00	2021-11-25 20:47:49.859204+00	1	5
15	an auxiliary verb	Basic English Grammar: What is an auxiliary verb?	<p><iframe src="https://www.youtube.com/embed/nmVN-5oOGy0" width="560" height="314" allowfullscreen="allowfullscreen"></iframe></p>	\N	t	2021-11-27 15:37:52.553703+00	2021-11-27 15:37:52.553733+00	1	8
6	English slang	English slang	<p><iframe src="https://www.youtube.com/embed/WhVde2qZveI" width="560" height="314" allowfullscreen="allowfullscreen"></iframe></p>	English slang	t	2021-11-26 06:15:58.042452+00	2021-11-26 20:34:08.89961+00	1	6
7	English slang	English slang	<p><iframe src="https://www.youtube.com/embed/whpmEpDxJA8" width="560" height="314" allowfullscreen="allowfullscreen"></iframe></p>	\N	t	2021-11-27 07:12:21.490845+00	2021-11-27 07:12:21.490877+00	1	6
8	Top 20 Expressions Americans Use Frequently	Top 20 Expressions Americans Use Frequently	<p><iframe src="https://www.youtube.com/embed/Vgzmci2cAJc" width="560" height="314" allowfullscreen="allowfullscreen"></iframe></p>\r\n<p>Do you know what I love about English? There are so many fun expressions. Especially American slang and idioms. They really make the difference between someone who learned school English and someone who speaks real everyday English.</p>\r\n<p>In this post, you&rsquo;ll learn twenty American slang phrases and words so you&rsquo;ll sound more natural when you speak English. I will explain seven American slang phrases in detail.</p>\r\n<p>&nbsp;</p>\r\n<div id="fws_61a1d8f666320" class="wpb_row vc_row-fluid vc_row standard_section   " data-midnight="dark" data-bg-mobile-hidden="">\r\n<div class="col span_12 dark left">\r\n<div class="vc_col-sm-12 wpb_column column_container col no-extra-padding instance-0" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">\r\n<div class="wpb_wrapper">\r\n<div class="wpb_text_column wpb_content_element ">\r\n<div class="wpb_wrapper">\r\n<h2 id="section-1">Overview of the Top 20 American Slang Expressions</h2>\r\n<p>Explained in detail:</p>\r\n</div>\r\n</div>\r\n<div class="wpb_raw_code wpb_content_element wpb_raw_html">\r\n<div class="wpb_wrapper">\r\n<table class="align-left">\r\n<tbody>\r\n<tr>\r\n<th>AMERICAN SLANG PHRASE OR WORD</th>\r\n<th>MEANING</th>\r\n</tr>\r\n<tr>\r\n<td>1. to pig out</td>\r\n<td>to eat a lot of food in a messy way</td>\r\n</tr>\r\n<tr>\r\n<td>2. to screw up something</td>\r\n<td>to mess things up</td>\r\n</tr>\r\n<tr>\r\n<td>3. to take a raincheck</td>\r\n<td>can't do something right now, but want to do it later</td>\r\n</tr>\r\n<tr>\r\n<td>4. to jack up the prices</td>\r\n<td>to suddenly increase prices</td>\r\n</tr>\r\n<tr>\r\n<td>5. to drive someone up the wall</td>\r\n<td>to make someone crazy</td>\r\n</tr>\r\n<tr>\r\n<td>6. to ride shotgun</td>\r\n<td>to be in the passenger seat</td>\r\n</tr>\r\n<tr>\r\n<td>7. a couch potato</td>\r\n<td>someone who always stays inside</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</div>\r\n</div>\r\n<div class="wpb_raw_code wpb_content_element wpb_raw_html">\r\n<div class="wpb_wrapper">\r\n<table class="align-left">\r\n<tbody>\r\n<tr>\r\n<th>AMERICAN SLANG PHRASE OR WORD</th>\r\n<th>MEANING</th>\r\n</tr>\r\n<tr>\r\n<td>8. a party animal</td>\r\n<td>someone who loves or really enjoys parties</td>\r\n</tr>\r\n<tr>\r\n<td>9. it's no biggie</td>\r\n<td>it's not a (big) problem</td>\r\n</tr>\r\n<tr>\r\n<td>10. a bummer</td>\r\n<td>a disappointment</td>\r\n</tr>\r\n<tr>\r\n<td>11. something sucks/to suck at something</td>\r\n<td>something is bad/to be bad at something</td>\r\n</tr>\r\n<tr>\r\n<td>12. a chicken</td>\r\n<td>a coward</td>\r\n</tr>\r\n<tr>\r\n<td>13. booze</td>\r\n<td>alcohol</td>\r\n</tr>\r\n<tr>\r\n<td>14. bae</td>\r\n<td>a beloved person like a partner or best friend</td>\r\n</tr>\r\n<tr>\r\n<td>15. bro</td>\r\n<td>a friend (often male)</td>\r\n</tr>\r\n<tr>\r\n<td>16. to have a crush on someone</td>\r\n<td>to be attracted to someone in a romantic way</td>\r\n</tr>\r\n<tr>\r\n<td>17. to dump someone</td>\r\n<td>to end a romantic relationship with someone</td>\r\n</tr>\r\n<tr>\r\n<td>18. something is sick</td>\r\n<td>something is awesome/really good</td>\r\n</tr>\r\n<tr>\r\n<td>19. an epic fail</td>\r\n<td>a huge failure or complete disaster</td>\r\n</tr>\r\n<tr>\r\n<td>20. to be hyped</td>\r\n<td>to be extremely excited about/looking forward to something</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n<div id="fws_61a1d8f66695e" class="wpb_row vc_row-fluid vc_row standard_section   " data-midnight="dark" data-bg-mobile-hidden="">\r\n<div class="row-bg-wrap">\r\n<div class="row-bg   ">&nbsp;</div>\r\n</div>\r\n<div class="col span_12 dark left">\r\n<div class="vc_col-sm-12 wpb_column column_container col no-extra-padding instance-1" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">\r\n<div class="wpb_wrapper">\r\n<div class="wpb_text_column wpb_content_element ">\r\n<div class="wpb_wrapper">\r\n<div id="link_to_video"></div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n<div class="vc_col-sm-10 wpb_column column_container col no-extra-padding instance-2" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">\r\n<div class="wpb_wrapper">&nbsp;</div>\r\n</div>\r\n<div class="vc_col-sm-1 wpb_column column_container col no-extra-padding instance-3" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">\r\n<div class="wpb_wrapper">&nbsp;</div>\r\n</div>\r\n</div>\r\n</div>\r\n<div id="fws_61a1d8f666b90" class="wpb_row vc_row-fluid vc_row standard_section   " data-midnight="dark" data-bg-mobile-hidden="">\r\n<div class="row-bg-wrap">\r\n<div class="row-bg   ">&nbsp;</div>\r\n</div>\r\n<div class="col span_12 dark left">\r\n<div class="vc_col-sm-12 wpb_column column_container col no-extra-padding instance-4" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">\r\n<div class="wpb_wrapper">\r\n<div class="wpb_text_column wpb_content_element ">\r\n<div class="wpb_wrapper">\r\n<h2 id="section-2">Definition and Meaning of &ldquo;Slang&rdquo;</h2>\r\n<p>What does it mean when we talk about &ldquo;slang&rdquo;? Let me give you a definition and show you what the word means.</p>\r\n<p><strong>Slang is a very informal kind of language that is usually only spoken and not written</strong>. It is especially used by particular groups of people.</p>\r\n<p>For example, there is:</p>\r\n<ul>\r\n<li>youth/teen slang</li>\r\n<li>internet slang</li>\r\n<li>army slang</li>\r\n<li>hip-hop/rap slang</li>\r\n<li>slang used by different ethnic groups</li>\r\n<li>and much more&hellip;</li>\r\n</ul>\r\n<p><strong>Slang is often used unconsciously to linguistically separate its group from other groups.</strong>&nbsp;Especially teenagers use slang expressions to differentiate from the old-fashioned way their parents speak.</p>\r\n<p>&nbsp;</p>\r\n<h2 id="section-3">American Slang Conversation</h2>\r\n<p>Already interested in what an American slang conversation looks like? Great, let me show you an example:</p>\r\n<p>&ldquo;Yesterday I was&nbsp;<strong>riding shotgun</strong>&nbsp;with my best friend to Josh&rsquo;s party.&rdquo;</p>\r\n<p>&ldquo;So sad that I had to&nbsp;<strong>take a raincheck</strong>&nbsp;because of my driver&rsquo;s license test today.&rdquo;</p>\r\n<p>&ldquo;For sure. The party was really&nbsp;<strong>sick</strong>.&rdquo;</p>\r\n<p>&ldquo;What a&nbsp;<strong>bummer</strong>&hellip; Tell me everything!&rdquo;</p>\r\n<p>&ldquo;You know that<strong>&nbsp;I have a crush on</strong>&nbsp;Josh. So I was really&nbsp;<strong>hyped</strong>&nbsp;for the party. I always thought he was a bit shy, but it turns out he is a&nbsp;<strong>party animal</strong>.&rdquo;</p>\r\n<p>&ldquo;It&rsquo;s better than being such a&nbsp;<strong>couch potato</strong>&nbsp;like your little brother.&rdquo;</p>\r\n<p>&ldquo;Absolutely, he&nbsp;<strong>sucks at</strong>&nbsp;everything except his video games. He sometimes really drives me&nbsp;<strong>up the wall!</strong>&rdquo;</p>\r\n<p>&ldquo;At least he is good at&nbsp;<strong>pigging out</strong>&nbsp;on chips and snacks.&rdquo;</p>\r\n<p>You didn&rsquo;t understand much? Don&rsquo;t worry, I will explain every one of these slang expressions and many more later on. For most of them, you will get a lot of examples to show you how to use them.</p>\r\n<hr />\r\n<h2 id="section-4">American Slang Words and Phrases and Their Meaning</h2>\r\n<p>Now let&rsquo;s take a closer look at the Top 20 American slang words and phrases &ndash; and especially their meanings.</p>\r\n<h3>1. to pig out (verb; to eat a lot of food in a messy way)</h3>\r\n<p>Pigs aren&rsquo;t clean animals, and when they eat, it&rsquo;s not pretty. That&rsquo;s why Americans use the expression &ldquo;to pig out&rdquo; when someone eats a lot of food, generally in an unrefined, messy way.</p>\r\n<p>If your friend says &ldquo;We pigged out at the buffet&rdquo; they didn&rsquo;t eat a lot of pork, they just ate a lot of food.</p>\r\n<p>Examples:</p>\r\n<p>1. While watching a movie, I&nbsp;<strong>pigged out</strong>&nbsp;on the chips and delicious snacks.<br />2. My grandmother&rsquo;s cooking is incredibly delicious. I always&nbsp;<strong>pig out</strong>&nbsp;on her food.<br />3. At the all-you-can-eat-buffet, I&nbsp;<strong>pigged out</strong>&nbsp;until my friend had to carry me out.</p>\r\n<h3>2. to screw up something (verb; to mess up everything)</h3>\r\n<p>If your colleague tells you &ldquo;We really screwed up the project,&rdquo; they&rsquo;re not talking about a do-it-yourself project.</p>\r\n<p>It means everything went wrong, they made a lot of mistakes, and the project was a disaster. Basically, if someone tells you they&rsquo;ve screwed up, it&rsquo;s never a good thing.</p>\r\n<p>Examples:</p>\r\n<p>1. I&nbsp;<strong>screwed up</strong>&nbsp;the presentation.<br />2. With his green suit, my uncle&nbsp;<strong>screwed up</strong>&nbsp;the whole wedding.<br />3. Your little sister is really nice, but she always&nbsp;<strong>screws up</strong>&nbsp;everything.</p>\r\n<h3>3. to take a raincheck (phrase; can&rsquo;t do something right now, but want to do it later)</h3>\r\n<p>If a friend wants you to do something with them and you&rsquo;re not available at that moment, you can say &ldquo;Can I take a raincheck?&rdquo; It means &ldquo;I can&rsquo;t now, but I&rsquo;d like to another time.&rdquo;</p>\r\n<p>The expression comes from baseball! If the weather is bad, spectators can get a ticket to come see the game later when the weather is better.</p>\r\n<p>If you want even more everyday expressions and lots of real, modern English vocabulary that Americans use every day, be sure to get my selection of 10 of my best lessons to boost your vocabulary in English.&nbsp;<a href="https://christinarebuffet.com/youtube-top-sst-episodes/">Click this link to get access to the lessons immediately.</a></p>\r\n<p>Examples:</p>\r\n<p>1. Sorry, I have to work tomorrow. Can I&nbsp;<strong>take a raincheck</strong>&nbsp;on this dinner?<br />2. I sadly have to&nbsp;<strong>take a raincheck</strong>&nbsp;on the beer. My wife wants me to come home early.<br />3. He was too busy at work, so he had to&nbsp;<strong>take a raincheck</strong>&nbsp;on our movie night.</p>\r\n<h3>4. to jack up the prices (phrase; to suddenly increase prices)</h3>\r\n<p>In the summer months, hotels and airlines increase their prices because everyone wants to make a booking. During winter vacation, ski resorts jack up their prices.</p>\r\n<p>So, &ldquo;to jack up prices&rdquo;&nbsp; means to suddenly increase prices. This expression has a negative connotation. You&rsquo;re not happy and you think the price increase is unfair.</p>\r\n<p>Example:</p>\r\n<p>1. What?! The hotel only costs $40 a night in February! And it&rsquo;s $100 in July. They really&nbsp;<strong>jack up their prices.</strong>&nbsp;Those thieves!</p>\r\n<h3>5. to drive someone up the wall (phrase; to make someone crazy)</h3>\r\n<p>If you&rsquo;ve ever been in a room with 10 hyperactive children, you might tell your friend &ldquo;These kids are driving me up the wall.&rdquo;</p>\r\n<p>It means they&rsquo;re making you crazy and irritated. Imagine that you feel crazy enough to try to drive your car up the wall of a building. That&rsquo;s the emotion this expression conveys.</p>\r\n<p>Examples:</p>\r\n<p>1. My stepmother really&nbsp;<strong>drives me up the wall</strong>!<br />2. At the office, my colleague works in a completely unorganized manner. This&nbsp;<strong>drives me up the wall.<br /></strong>3. The hot weather these days&nbsp;<strong>drives me up the wall</strong>.</p>\r\n<h3>6. to ride shotgun (phrase; to be in the passenger seat)</h3>\r\n<p>This expression comes from the old American West, when the person sitting next to the driver of a stagecoach needed a shotgun to defend against attackers.</p>\r\n<p>Today, we say the person in the passenger seat in a car is riding shotgun. So, riding shotgun is less violent than you might imagine. Except if it&rsquo;s two teenagers fighting over who has the privilege of riding shotgun&hellip;</p>\r\n<p>Examples:</p>\r\n<p>1. With your long legs you have to&nbsp;<strong>ride shotgun</strong>.<br />2. I&rsquo;m driving. You are&nbsp;<strong>riding shotgun</strong>.<br />3. I&rsquo;m<strong>&nbsp;riding shotgun</strong>&nbsp;with my best friend.</p>\r\n<h3>7. a couch potato (noun; someone who always stays inside)</h3>\r\n<p>This one is funny if you literally imagine a big fat potato sitting on a couch. It&rsquo;s not very nice to call someone &ldquo;a couch potato&rdquo; because it means they just sit on a couch all day and watch TV, look at their smartphone, or play video games.</p>\r\n<p>It&rsquo;s certainly not good to have the physique of a potato!</p>\r\n<p>Don&rsquo;t be a couch potato, but do watch today&rsquo;s lesson on American slang!</p>\r\n<p>Example:</p>\r\n<p>1. My little brother is always at home and watches his series. He is such a&nbsp;<strong>couch potato</strong>.<br />2. We are going out for a walk. Come with us, don&rsquo;t be a&nbsp;<strong>couch potato</strong>!<br />3. During the corona quarantine, we were all&nbsp;<strong>couch potatoes</strong>.</p>\r\n<h3>8. a party animal (noun; someone who loves parties or really enjoys them)</h3>\r\n<h3>9. it&rsquo;s no biggie (phrase; it&rsquo;s not a (big) problem)</h3>\r\n<h3>10. a bummer (noun; a disappointment)</h3>\r\n<h3>11. something sucks/to suck at something (verb; something is bad/to be bad at something)</h3>\r\n<h3>12. a chicken (noun; a coward)</h3>\r\n<h3>13. booze (noun; alcohol)</h3>\r\n<h3>14. bae (noun; short for &ldquo;before anyone else,&rdquo; a beloved person like a partner or best friend, mostly used by women)</h3>\r\n<h3>15. bro (noun; short for &ldquo;brother,&rdquo; a friend (often male), mostly used by man)</h3>\r\n<h3>16. to have a crush on someone (phrase; to be attracted to someone in a romantic way)</h3>\r\n<h3>17. to dump someone (verb; to end a romantic relationship with someone)</h3>\r\n<h3>18. something is sick (adjective; something is awesome/really good)</h3>\r\n<h3>19. an epic fail (phrase; a huge failure or completely disaster)</h3>\r\n<h3>20. to be hyped (adjective; to be extremely excited about/looking forward to something)</h3>\r\n<hr />\r\n<h2 id="section-5">American Slang phrases and words in British English</h2>\r\n<p>Although Americans and the British speak the same language,<strong>&nbsp;there are American slang words and phrases which are at least confusing for British.</strong>&nbsp;Sometimes they don&rsquo;t understand them at all.</p>\r\n<p>A common example of this phenomenon is the word&nbsp;<strong>&ldquo;pants.&rdquo;</strong>&nbsp;When hearing the word &ldquo;pants,&rdquo; Americans are expecting long pants, whereas the British are expecting underwear.</p>\r\n<p>One of my examples is&nbsp;<strong>&ldquo;to ride shotgun.&rdquo;</strong></p>\r\n<p>If an American would say to his British friend: &ldquo;I&rsquo;m driving. You are riding shotgun.&rdquo; his friend would most likely look at him confused and be completely irritated.</p>\r\n<p>So, if your British friend doesn&rsquo;t understand you, make sure you haven&rsquo;t used an American slang word or phrase.</p>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>	\N	t	2021-11-27 07:18:19.167631+00	2021-11-27 07:18:19.167659+00	1	6
9	Slang is similar to idioms	Slang is similar to idioms	<p>Peace of cake means as easy as to eat a slice of cake</p>\r\n<p>An idiom is a phrase or expression that typically presents a figurative, non-literal meaning attached to<br />the phrase; but some phrases become figurative idioms while retaining the literal meaning of the<br />phrase.</p>\r\n<p><img src="../../../../../media/article/11_27_2021_09_28_31.png" alt="" width="750" height="1087" /></p>	\N	t	2021-11-27 09:26:45.142875+00	2021-11-27 09:29:10.279849+00	1	6
10	British slang is little different	British slang is little different	<p><iframe src="https://www.youtube.com/embed/1XLEC52dwnE" width="560" height="314" allowfullscreen="allowfullscreen"></iframe></p>	\N	t	2021-11-27 09:32:17.625468+00	2021-11-27 09:32:17.6255+00	1	6
11	Phrasal verb with GO	Phrasal verb with GO	<p><img src="../../../../media/article/11_27_2021_10_37_35.png" alt="" /></p>\r\n<p>&nbsp;</p>\r\n<p>go ahead. proceed (after obtaining permission) "Can I ask you a question?" "Go ahead."</p>\r\n<p>go along with. something. accept something, agree with someone. ...</p>\r\n<p>go away. leave, disappear. ...</p>\r\n<p>go back. start doing something again. ...</p>\r\n<p>go back on. something. ...</p>\r\n<p>go for something. choose. ...</p>\r\n<p>go in for. something. ...</p>\r\n<p>go off. explode.</p>	\N	f	2021-11-27 10:39:08.085144+00	2021-11-27 10:39:08.085174+00	1	7
13	present perfect	present perfect	<p>The present perfect is used to indicate a link between the present and the past. The time of the action is before now but not specified, and we are often more interested in the result than in the action itself.</p>\r\n<p><img src="../../../../../media/article/11_27_2021_13_16_28.png" alt="" width="707" height="1000" /></p>	\N	t	2021-11-27 13:13:28.560817+00	2021-11-27 13:16:37.019292+00	1	8
14	We use auxiliary verbs with present perfect	We use auxiliary verbs with present perfect	<p>We use auxiliary verbs with present perfect</p>\r\n<p><img src="../../../../../media/article/11_27_2021_14_51_44.png" alt="" width="600" height="870" /></p>\r\n<p>&nbsp;</p>\r\n<p><img src="../../../../../media/article/11_27_2021_15_29_13.png" alt="" width="228" height="221" /></p>	\N	t	2021-11-27 15:36:24.239953+00	2021-11-27 16:14:57.002025+00	1	8
16	Synonyms, Antonyms, and Homonyms	Synonyms, Antonyms, and Homonyms List	<h2>What are Synonyms?</h2>\r\n<p>A synonym is a word, a morpheme, or a phrase that means precisely or nearly the same as the particular word. The synonym of any particular word can be used in the place of that word because the meaning of both the words are the same that is why we can substitute them from one another. Synonyms are the correlated, similar meaning words that are used randomly one in place of the ot</p>\r\n<p><img src="../../../../../media/article/11_27_2021_16_06_26.png" alt="" width="1024" height="695" /></p>	\N	t	2021-11-27 16:06:53.475696+00	2021-11-27 16:18:07.066397+00	1	\N
17	Direct and Inderect Speech	Reported speech	<p>Reported speech&nbsp;</p>\r\n<p>is when we tell someone what another person said. To do this, we can use direct speech or indirect<br />speech.</p>\r\n<p>direct speech: 'I work in a bank,' said Daniel.</p>\r\n<p>indirect speech: Daniel said that he worked in a<br />bank.</p>\r\n<p><iframe src="https://www.youtube.com/embed/ZGdt9apUpqg" width="560" height="314" allowfullscreen="allowfullscreen"></iframe></p>\r\n<p><img title="direct_indirect_speech.jpg" src="../../../../../media/article/11_27_2021_16_25_53.png" alt="" width="828" height="1288" /></p>\r\n<p>&nbsp;</p>	\N	t	2021-11-27 16:25:28.417357+00	2021-11-27 16:32:10.030081+00	1	9
18	reported speech	reported speech	<p><img title="reported speech.jpg" src="/media/article/11_27_2021_16_32_36.png" alt="" width="1000" height="1413" /></p>	\N	t	2021-11-27 16:32:50.120686+00	2021-12-03 21:55:12.195299+00	1	9
19	question words	question words	<p>question words</p>\r\n<p><img title="question_words.jpg" src="../../../../../media/article/11_27_2021_17_26_22.png" alt="" width="700" height="1020" /></p>	\N	t	2021-11-27 16:34:20.1502+00	2021-11-27 17:27:43.759685+00	1	10
20	5 W’s create sentences	5 W’s create sentences	<p style="padding-left: 40px;"><img style="float: left;" title="5 W&rsquo;s-create sentences.jpg" src="../../../../media/article/11_27_2021_17_29_01.png" alt="" width="263" height="350" />The Five Ws, Five Ws and one H, or the Six Ws are questions whose answers are considered basic in<br />information-gathering. They include Who, What, When Where, and Why. The 5 Ws are often<br />mentioned in journalism (cf. news style), research, and police investigations.</p>	\N	t	2021-11-27 17:30:06.92215+00	2021-11-27 17:30:06.922184+00	1	10
21	Basic Information who what when where and why?? Also how?	Basic Information who what when where and why?? Also how?	<p><img title="question_words_1.jpg" src="../../../../../media/article/11_27_2021_17_33_16.png" alt="" width="1000" height="1291" /></p>	\N	t	2021-11-27 17:30:40.491101+00	2021-11-27 17:33:20.109452+00	1	10
26	10 phrasal verbs with meanig	10 phrasal verbs with meanig	<p><img title="10_phrasal_verbs.jpg" src="../../../../../media/article/11_27_2021_19_15_32.png" alt="" width="964" height="1096" /></p>\r\n<p><img title="onother-other-the_other_1.jpg" src="../../../../../media/article/11_27_2021_19_21_13.png" alt="" width="860" height="1217" /></p>	\N	t	2021-11-27 19:15:16.58186+00	2021-11-27 19:21:32.643368+00	1	12
22	Third conditional	Third conditional	<h3><strong>When do we use the third conditional?</strong></h3>\r\n<p>The third conditional is used to express the past consequence of an unrealistic action or situation in the past.</p>\r\n<p>For example,</p>\r\n<ul>\r\n<li><em>If he had studied harder, he would have passed the exam.</em></li>\r\n</ul>\r\n<p>The first action (studying hard) did not happen. But in the case that he happened, the consequence was passing the exam. The third conditional is very similar to the second conditional. But while the second conditional refers to something unrealistic now or in the future, the third conditional refers to something unrealistic in the past.</p>\r\n<p>We often use the third conditional to express regrets &ndash; describing things we are sorry happened or didn&rsquo;t happen. For example,</p>\r\n<ul>\r\n<li><em>If my alarm had gone off, I wouldn&rsquo;t have been late to work.</em></li>\r\n<li><em>If there hadn&rsquo;t been so much traffic we wouldn&rsquo;t have missed our flight.</em></li>\r\n</ul>\r\n<h3><strong>How do we create the third conditional?</strong></h3>\r\n<p>To make a sentence in the third conditional, we use,</p>\r\n<p><strong>If + past perfect, would/wouldn&rsquo;t have + past participle.</strong></p>\r\n<ul>\r\n<li><em>If you had told me about the meeting, I would have come.</em></li>\r\n<li><em>If you had told me about the meeting, I wouldn&rsquo;t have missed it.</em></li>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<p>As with all conditionals, you can also invert this structure:</p>\r\n<p><strong>Would have + past participle if + past perfect.</strong></p>\r\n<ul>\r\n<li><em>I&rsquo;d have come to the meeting if you&rsquo;d told me about it.</em></li>\r\n<li><em>I wouldn&rsquo;t have missed the meeting if you&rsquo;d told me about it.</em></li>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<p>The word&nbsp;<em>would</em>&nbsp;is often contracted to&nbsp;<em>&lsquo;d&nbsp;</em>by native speakers. It&rsquo;s also acceptable to use this in informal writing. And in speech it&rsquo;s common to contract&nbsp;<em>have&nbsp;</em>to&nbsp;<em>&lsquo;ve&nbsp;</em>in the third conditional. For example,</p>\r\n<ul>\r\n<li><em>I&rsquo;d&rsquo;ve come to the meeting if you&rsquo;d told me.</em></li>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<p>However, we can&rsquo;t write this, even in an informal context. It&rsquo;s useful to be aware of it though, so you can identify it when native speakers say it.</p>\r\n<p>As an alternative to&nbsp;<em>would,&nbsp;</em>we can complete the second part of a third conditional sentence with&nbsp;<em>could.</em>&nbsp;For example,</p>\r\n<ul>\r\n<li><em>If I&rsquo;d stayed at university, I could have got a masters degree.</em></li>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<p>Here are some other examples of the third conditional:</p>\r\n<ul>\r\n<li><em>He&rsquo;d have got the job if he hadn&rsquo;t been so nervous in the interview.</em></li>\r\n<li><em>What would you have done if you&rsquo;d been me?</em></li>\r\n<li><em>If it hadn&rsquo;t been snowing heavily, we&rsquo;d have carried on skiing.</em></li>\r\n<li><em>The company would have survived if there hadn&rsquo;t been a recession.</em></li>\r\n<li><em>Would you have accepted the offer if we&rsquo;d reduced the price?</em></li>\r\n<li><em>If you hadn&rsquo;t invited me out, I&rsquo;d have stayed in all day.</em></li>\r\n<li><em>She wouldn&rsquo;t have given you a fine if you&rsquo;d apologized.</em></li>\r\n<li><em>If they hadn&rsquo;t won that match, the club would have fired the manager.</em></li>\r\n<li><em>They could have stayed here if they hadn&rsquo;t found any accommodation.</em></li>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<h3><strong>Mixing the second and the third conditionals</strong></h3>\r\n<p>It&rsquo;s possible to combine the second and the third conditionals. There are two ways of doing that. We can either,</p>\r\n<h4><strong>Describe the present consequence of a past situation</strong></h4>\r\n<p><strong>If + past simple, would have + past participle</strong></p>\r\n<ul>\r\n<li><em>If I were adventurous, I&rsquo;d have gone backpacking after university.</em></li>\r\n</ul>\r\n<h4><strong>Describe the past consequence of a present situation</strong></h4>\r\n<p><strong>If + past perfect, would + verb</strong></p>\r\n<ul>\r\n<li><em>If we hadn&rsquo;t missed the flight, we&rsquo;d be in our hotel by now.</em></li>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<p>Here are some more examples,</p>\r\n<ul>\r\n<li><em>If I&rsquo;d studied for a year in the U.S, my English would be fluent now.</em></li>\r\n<li><em>The roads wouldn&rsquo;t be so icy if it hadn&rsquo;t rained so much last night.</em></li>\r\n<li><em>If she weren&rsquo;t so shy, she&rsquo;d have gone to the party on her own.</em></li>\r\n<li><em>The fans would be miserable now if their team had been relegated.</em></li>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<p>The third conditional has quite a tricky structure that takes some time and practice to become familiar with. So why not start now by doing a fun quiz.</p>\r\n<p>&nbsp;</p>	\N	t	2021-11-27 17:34:31.561074+00	2021-12-06 13:58:14.06887+00	1	11
30	200 common phrasal verbs	200 common phrasal verbs	<p><strong>ask</strong>&nbsp;<em>somebody</em>&nbsp;<strong>out</strong><br />invite on a date<br />Brian&nbsp;<strong>asked</strong>&nbsp;Judy&nbsp;<strong>out</strong>&nbsp;to dinner and a movie.</p>\r\n<p><strong>ask around</strong><br />ask many people the same question<br />I&nbsp;<strong>asked around</strong>&nbsp;but nobody has seen my wallet.</p>\r\n<p><strong>add up to</strong>&nbsp;<em>something</em><br />equal<br />Your purchases&nbsp;<strong>add up to</strong>&nbsp;$205.32.</p>\r\n<p><strong>back</strong>&nbsp;<em>something</em>&nbsp;<strong>up</strong><br />reverse<br />You'll have to&nbsp;<strong>back up</strong>&nbsp;your car so that I can get out.</p>\r\n<p><strong>back</strong>&nbsp;<em>somebody</em>&nbsp;<strong>up</strong><br />support<br />My wife&nbsp;<strong>backed</strong>&nbsp;me&nbsp;<strong>up</strong>&nbsp;over my decision to quit my job.</p>\r\n<p><strong>blow up</strong><br />explode<br />The racing car&nbsp;<strong>blew up</strong>&nbsp;after it crashed into the fence.</p>\r\n<p><strong>blow</strong>&nbsp;<em>something</em>&nbsp;<strong>up</strong><br />add air<br />We have to&nbsp;<strong>blow</strong>&nbsp;50 balloons&nbsp;<strong>up</strong>&nbsp;for the party.</p>\r\n<p><strong>break down</strong><br />stop functioning (vehicle, machine)<br />Our car&nbsp;<strong>broke down</strong>&nbsp;at the side of the highway in the snowstorm.</p>\r\n<p><strong>break down</strong><br />get upset<br />The woman&nbsp;<strong>broke down</strong>&nbsp;when the police told her that her son had died.</p>\r\n<p><strong>break</strong>&nbsp;<em>something</em>&nbsp;<strong>down</strong><br />divide into smaller parts<br />Our teacher&nbsp;<strong>broke</strong>&nbsp;the final project&nbsp;<strong>down</strong>&nbsp;into three separate parts.</p>\r\n<p><strong>break in</strong><br />force entry to a building<br />Somebody&nbsp;<strong>broke in</strong>&nbsp;last night and stole our stereo.</p>\r\n<p><strong>break into</strong>&nbsp;<em>something</em><br />enter forcibly<br />The firemen had to&nbsp;<strong>break into</strong>&nbsp;the room to rescue the children.</p>\r\n<p><strong>break</strong>&nbsp;<em>something</em>&nbsp;<strong>in</strong><br />wear something a few times so that it doesn't look/feel new<br />I need to&nbsp;<strong>break</strong>&nbsp;these shoes&nbsp;<strong>in</strong>&nbsp;before we run next week.</p>\r\n<p><strong>break in</strong><br />interrupt<br />The TV station&nbsp;<strong>broke in</strong>&nbsp;to report the news of the president's death.</p>\r\n<p><strong>break up</strong><br />end a relationship<br />My boyfriend and I&nbsp;<strong>broke up</strong>&nbsp;before I moved to America.</p>\r\n<p><strong>break up</strong><br />start laughing (informal)<br />The kids just&nbsp;<strong>broke up</strong>&nbsp;as soon as the clown started talking.</p>\r\n<p><strong>break out</strong><br />escape<br />The prisoners&nbsp;<strong>broke out</strong>&nbsp;of jail when the guards weren't looking.</p>\r\n<p><strong>break out in</strong>&nbsp;<em>something</em><br />develop a skin condition<br />I&nbsp;<strong>broke out in</strong>&nbsp;a rash after our camping trip.</p>\r\n<p><strong>bring</strong>&nbsp;<em>somebody</em>&nbsp;<strong>down</strong><br />make unhappy<br />This sad music is&nbsp;<strong>bringing</strong>&nbsp;me&nbsp;<strong>down</strong>.</p>\r\n<p><strong>bring</strong>&nbsp;<em>somebody</em>&nbsp;<strong>up</strong><br />raise a child<br />My grandparents&nbsp;<strong>brought</strong>&nbsp;me&nbsp;<strong>up</strong>&nbsp;after my parents died.</p>\r\n<p><strong>bring</strong>&nbsp;<em>something</em>&nbsp;<strong>up</strong><br />start talking about a subject<br />My mother walks out of the room when my father&nbsp;<strong>brings up</strong>&nbsp;sports.</p>\r\n<p><strong>bring</strong>&nbsp;<em>something</em>&nbsp;<strong>up</strong><br />vomit<br />He drank so much that he&nbsp;<strong>brought</strong>&nbsp;his dinner&nbsp;<strong>up</strong>&nbsp;in the toilet.</p>\r\n<p><strong>call around</strong><br />phone many different places/people<br />We&nbsp;<strong>called around</strong>&nbsp;but we weren't able to find the car part we needed.</p>\r\n<p><strong>call</strong>&nbsp;<em>somebody</em>&nbsp;<strong>back</strong><br />return a phone call<br />I&nbsp;<strong>called</strong>&nbsp;the company&nbsp;<strong>back</strong>&nbsp;but the offices were closed for the weekend.</p>\r\n<p><strong>call</strong>&nbsp;<em>something</em>&nbsp;<strong>off</strong><br />cancel<br />Jason&nbsp;<strong>called</strong>&nbsp;the wedding&nbsp;<strong>off</strong>&nbsp;because he wasn't in love with his fianc&eacute;.</p>\r\n<p><strong>call on</strong>&nbsp;<em>somebody</em><br />ask for an answer or opinion<br />The professor&nbsp;<strong>called on</strong>&nbsp;me for question 1.</p>\r\n<p><strong>call on</strong>&nbsp;<em>somebody</em><br />visit somebody<br />We&nbsp;<strong>called on</strong>&nbsp;you last night but you weren't home.</p>\r\n<p><strong>call</strong>&nbsp;<em>somebody</em>&nbsp;<strong>up</strong><br />phone<br />Give me your phone number and I will&nbsp;<strong>call</strong>&nbsp;you&nbsp;<strong>up</strong>&nbsp;when we are in town.</p>\r\n<p><strong>calm down</strong><br />relax after being angry<br />You are still mad. You need to&nbsp;<strong>calm down</strong>&nbsp;before you drive the car.</p>\r\n<p>not&nbsp;<strong>care for</strong>&nbsp;<em>somebody/ something</em><br />not like (formal)<br />I don't&nbsp;<strong>care for</strong>&nbsp;his behaviour.</p>\r\n<p><strong>catch up</strong><br />get to the same point as somebody else<br />You'll have to run faster than that if you want to&nbsp;<strong>catch up</strong>&nbsp;with Marty.</p>\r\n<p><strong>check in</strong><br />arrive and register at a hotel or airport<br />We will get the hotel keys when we&nbsp;<strong>check in</strong>.</p>\r\n<p><strong>check out</strong><br />leave a hotel<br />You have to&nbsp;<strong>check out</strong>&nbsp;of the hotel before 11:00 AM.</p>\r\n<p><strong>check</strong>&nbsp;<em>somebody/ something</em>&nbsp;<strong>out</strong><br />look at carefully, investigate<br />The company&nbsp;<strong>checks out</strong>&nbsp;all new employees.</p>\r\n<p><strong>check out</strong>&nbsp;<em>somebody/ something</em><br />look at (informal)<br /><strong>Check out</strong>&nbsp;the crazy hair on that guy!</p>\r\n<p><strong>cheer up</strong><br />become happier<br />She&nbsp;<strong>cheered up</strong>&nbsp;when she heard the good news.</p>\r\n<p><strong>cheer</strong>&nbsp;<em>somebody</em>&nbsp;<strong>up</strong><br />make happier<br />I brought you some flowers to&nbsp;<strong>cheer</strong>&nbsp;you&nbsp;<strong>up</strong>.</p>\r\n<p><strong>chip in</strong><br />help<br />If everyone&nbsp;<strong>chips in</strong>&nbsp;we can get the kitchen painted by noon.</p>\r\n<p><strong>clean</strong>&nbsp;<em>something</em>&nbsp;<strong>up</strong><br />tidy, clean<br />Please&nbsp;<strong>clean up</strong>&nbsp;your bedroom before you go outside.</p>\r\n<p><strong>come across</strong>&nbsp;<em>something</em><br />find unexpectedly<br />I&nbsp;<strong>came across</strong>&nbsp;these old photos when I was tidying the closet.</p>\r\n<p><strong>come apart</strong><br />separate<br />The top and bottom&nbsp;<strong>come apart</strong>&nbsp;if you pull hard enough.</p>\r\n<p><strong>come down with</strong>&nbsp;<em>something</em><br />become sick<br />My nephew&nbsp;<strong>came down with</strong>&nbsp;chicken pox this weekend.</p>\r\n<p><strong>come forward</strong><br />volunteer for a task or to give evidence<br />The woman&nbsp;<strong>came forward</strong>&nbsp;with her husband's finger prints.</p>\r\n<p><strong>come from</strong>&nbsp;some place<br />originate in<br />The art of origami&nbsp;<strong>comes from</strong>&nbsp;Asia.</p>\r\n<p><strong>count on</strong>&nbsp;<em>somebody/ something</em><br />rely on<br />I am&nbsp;<strong>counting on</strong>&nbsp;you to make dinner while I am out.</p>\r\n<p><strong>cross</strong>&nbsp;<em>something</em>&nbsp;<strong>out</strong><br />draw a line through<br />Please&nbsp;<strong>cross out</strong>&nbsp;your old address and write your new one.</p>\r\n<p><strong>cut back on</strong>&nbsp;<em>something</em><br />consume less<br />My doctor wants me to&nbsp;<strong>cut back on</strong>&nbsp;sweets and fatty foods.</p>\r\n<p><strong>cut</strong>&nbsp;<em>something</em>&nbsp;<strong>down</strong><br />make something fall to the ground<br />We had to&nbsp;<strong>cut</strong>&nbsp;the old tree in our yard&nbsp;<strong>down</strong>&nbsp;after the storm.</p>\r\n<p><strong>cut in</strong><br />interrupt<br />Your father&nbsp;<strong>cut in</strong>&nbsp;while I was dancing with your uncle.</p>\r\n<p><strong>cut in</strong><br />pull in too closely in front of another vehicle<br />The bus driver got angry when that car&nbsp;<strong>cut in</strong>.</p>\r\n<p><strong>cut in</strong><br />start operating (of an engine or electrical device)<br />The air conditioner&nbsp;<strong>cuts in</strong>&nbsp;when the temperature gets to 22&deg;C.</p>\r\n<p><strong>cut</strong>&nbsp;<em>something</em>&nbsp;<strong>off</strong><br />remove with something sharp<br />The doctors&nbsp;<strong>cut off</strong>&nbsp;his leg because it was severely injured.</p>\r\n<p><strong>cut</strong>&nbsp;<em>something</em>&nbsp;<strong>off</strong><br />stop providing<br />The phone company&nbsp;<strong>cut off</strong>&nbsp;our phone because we didn't pay the bill.</p>\r\n<p><strong>cut</strong>&nbsp;<em>somebody</em>&nbsp;<strong>off</strong><br />take out of a will<br />My grandparents&nbsp;<strong>cut</strong>&nbsp;my father&nbsp;<strong>off</strong>&nbsp;when he remarried.</p>\r\n<p><strong>cut</strong>&nbsp;<em>something</em>&nbsp;<strong>out</strong><br />remove part of something (usually with scissors and paper)<br />I&nbsp;<strong>cut</strong>&nbsp;this ad&nbsp;<strong>out</strong>&nbsp;of the newspaper.</p>\r\n<p><strong>do</strong>&nbsp;<em>somebody/ something</em>&nbsp;<strong>over</strong><br />beat up, ransack (BrE, informal)<br />He's lucky to be alive. His shop was&nbsp;<strong>done over</strong>&nbsp;by a street gang.</p>\r\n<p><strong>do</strong>&nbsp;<em>something</em>&nbsp;<strong>over</strong><br />do again (AmE)<br />My teacher wants me to&nbsp;<strong>do</strong>&nbsp;my essay&nbsp;<strong>over</strong>&nbsp;because she doesn't like my topic.</p>\r\n<p><strong>do away with</strong>&nbsp;<em>something</em><br />discard<br />It's time to&nbsp;<strong>do away with</strong>&nbsp;all of these old tax records.</p>\r\n<p><strong>do</strong>&nbsp;<em>something</em>&nbsp;<strong>up</strong><br />fasten, close<br /><strong>Do</strong>&nbsp;your coat&nbsp;<strong>up</strong>&nbsp;before you go outside. It's snowing!</p>\r\n<p><strong>dress up</strong><br />wear nice clothing<br />It's a fancy restaurant so we have to&nbsp;<strong>dress up</strong>.</p>\r\n<p><strong>drop back</strong><br />move back in a position/group<br />Andrea&nbsp;<strong>dropped back</strong>&nbsp;to third place when she fell off her bike.</p>\r\n<p><strong>drop in/ by/ over</strong><br />come without an appointment<br />I might&nbsp;<strong>drop in/by/over</strong>&nbsp;for tea sometime this week.</p>\r\n<p><strong>drop</strong>&nbsp;<em>somebody/ something</em>&nbsp;<strong>off</strong><br />take somebody/ something somewhere and leave them/it there<br />I have to&nbsp;<strong>drop</strong>&nbsp;my sister&nbsp;<strong>off</strong>&nbsp;at work before I come over.</p>\r\n<p><strong>drop out</strong><br />quit a class, school etc<br />I&nbsp;<strong>dropped out</strong>&nbsp;of Science because it was too difficult.</p>\r\n<p><strong>eat out</strong><br />eat at a restaurant<br />I don't feel like cooking tonight. Let's&nbsp;<strong>eat out</strong>.</p>\r\n<p><strong>end up</strong><br />eventually reach/do/decide<br />We&nbsp;<strong>ended up</strong>&nbsp;renting a movie instead of going to the theatre.</p>\r\n<p><strong>fall apart</strong><br />break into pieces<br />My new dress&nbsp;<strong>fell apart</strong>&nbsp;in the washing machine.</p>\r\n<p><strong>fall down</strong><br />fall to the ground<br />The picture that you hung up last night&nbsp;<strong>fell down</strong>&nbsp;this morning.</p>\r\n<p><strong>fall out</strong><br />separate from an interior<br />The money must have&nbsp;<strong>fallen out</strong>&nbsp;of my pocket.</p>\r\n<p><strong>fall out</strong><br />(of hair, teeth) become loose and unattached<br />His hair started to&nbsp;<strong>fall out</strong>&nbsp;when he was only 35.</p>\r\n<p><strong>figure</strong>&nbsp;<em>something</em>&nbsp;<strong>out</strong><br />understand, find the answer<br />I need to&nbsp;<strong>figure out</strong>&nbsp;how to fit the piano and the bookshelf in this room.</p>\r\n<p><strong>fill</strong>&nbsp;<em>something</em>&nbsp;<strong>in</strong><br />to write information in blanks, as on a form (BrE)<br />Please&nbsp;<strong>fill in</strong>&nbsp;the form with your name, address, and phone number.</p>\r\n<p><strong>fill</strong>&nbsp;<em>something</em>&nbsp;<strong>out</strong><br />to write information in blanks, as on a form (AmE)<br />The form must be&nbsp;<strong>filled out</strong>&nbsp;in capital letters.</p>\r\n<p><strong>fill</strong>&nbsp;<em>something</em>&nbsp;<strong>up</strong><br />fill to the top<br />I always&nbsp;<strong>fill</strong>&nbsp;the water jug&nbsp;<strong>up</strong>&nbsp;when it is empty.</p>\r\n<p><strong>find out</strong><br />discover<br />We don't know where he lives. How can we&nbsp;<strong>find out</strong>?</p>\r\n<p><strong>find</strong>&nbsp;<em>something</em>&nbsp;<strong>out</strong><br />discover<br />We tried to keep the time of the party a secret, but Samantha&nbsp;<strong>found</strong>&nbsp;it&nbsp;<strong>out</strong>.</p>\r\n<p><strong>get</strong>&nbsp;<em>something</em>&nbsp;<strong>across/ over</strong><br />communicate, make understandable<br />I tried to&nbsp;<strong>get</strong>&nbsp;my point&nbsp;<strong>across/over</strong>&nbsp;to the judge but she wouldn't listen.</p>\r\n<p><strong>get along/on</strong><br />like each other<br />I was surprised how well my new girlfriend and my sister&nbsp;<strong>got along/on</strong>.</p>\r\n<p><strong>get around</strong><br />have mobility<br />My grandfather can&nbsp;<strong>get around</strong>&nbsp;fine in his new wheelchair.</p>\r\n<p><strong>get away</strong><br />go on a vacation<br />We worked so hard this year that we had to&nbsp;<strong>get away</strong>&nbsp;for a week.</p>\r\n<p><strong>get away with</strong>&nbsp;<em>something</em><br />do without being noticed or punished<br />Jason always&nbsp;<strong>gets away with</strong>&nbsp;cheating in his maths tests.</p>\r\n<p><strong>get back</strong><br />return<br />We&nbsp;<strong>got back</strong>&nbsp;from our vacation last week.</p>\r\n<p><strong>get</strong>&nbsp;<em>something</em>&nbsp;<strong>back</strong><br />receive something you had before<br />Liz finally&nbsp;<strong>got</strong>&nbsp;her Science notes&nbsp;<strong>back</strong>&nbsp;from my room-mate.</p>\r\n<p><strong>get back at</strong>&nbsp;<em>somebody</em><br />retaliate, take revenge<br />My sister&nbsp;<strong>got back at</strong>&nbsp;me for stealing her shoes. She stole my favourite hat.</p>\r\n<p><strong>get back into</strong>&nbsp;<em>something</em><br />become interested in something again<br />I finally&nbsp;<strong>got back into</strong>&nbsp;my novel and finished it.</p>\r\n<p><strong>get on</strong>&nbsp;<em>something</em><br />step onto a vehicle<br />We're going to freeze out here if you don't let us&nbsp;<strong>get on</strong>&nbsp;the bus.</p>\r\n<p><strong>get over</strong>&nbsp;<em>something</em><br />recover from an illness, loss, difficulty<br />I just&nbsp;<strong>got over</strong>&nbsp;the flu and now my sister has it.</p>\r\n<p><strong>get over</strong>&nbsp;<em>something</em><br />overcome a problem<br />The company will have to close if it can't&nbsp;<strong>get over</strong>&nbsp;the new regulations.</p>\r\n<p><strong>get round to</strong>&nbsp;<em>something</em><br />finally find time to do (AmE:&nbsp;<strong>get around to</strong>&nbsp;<em>something</em>)<br />I don't know when I am going to&nbsp;<strong>get round to</strong>&nbsp;writing the thank you cards.</p>\r\n<p><strong>get together</strong><br />meet (usually for social reasons)<br />Let's&nbsp;<strong>get together</strong>&nbsp;for a BBQ this weekend.</p>\r\n<p><strong>get up</strong><br />get out of bed<br />I&nbsp;<strong>got up</strong>&nbsp;early today to study for my exam.</p>\r\n<p><strong>get up</strong><br />stand<br />You should&nbsp;<strong>get up</strong>&nbsp;and give the elderly man your seat.</p>\r\n<p><strong>give</strong>&nbsp;<em>somebody</em>&nbsp;<strong>away</strong><br />reveal hidden information about somebody<br />His wife&nbsp;<strong>gave</strong>&nbsp;him&nbsp;<strong>away</strong>&nbsp;to the police.</p>\r\n<p><strong>give</strong>&nbsp;<em>somebody</em>&nbsp;<strong>away</strong><br />take the bride to the altar<br />My father&nbsp;<strong>gave</strong>&nbsp;me&nbsp;<strong>away</strong>&nbsp;at my wedding.</p>\r\n<p><strong>give</strong>&nbsp;<em>something</em>&nbsp;<strong>away</strong><br />ruin a secret<br />My little sister&nbsp;<strong>gave</strong>&nbsp;the surprise party&nbsp;<strong>away</strong>&nbsp;by accident.</p>\r\n<p><strong>give</strong>&nbsp;<em>something</em>&nbsp;<strong>away</strong><br />give something to somebody for free<br />The library was&nbsp;<strong>giving away</strong>&nbsp;old books on Friday.</p>\r\n<p><strong>give</strong>&nbsp;<em>something</em>&nbsp;<strong>back</strong><br />return a borrowed item<br />I have to&nbsp;<strong>give</strong>&nbsp;these skates&nbsp;<strong>back</strong>&nbsp;to Franz before his hockey game.</p>\r\n<p><strong>give in</strong><br />reluctantly stop fighting or arguing<br />My boyfriend didn't want to go to the ballet, but he finally&nbsp;<strong>gave in</strong>.</p>\r\n<p><strong>give</strong>&nbsp;<em>something</em>&nbsp;<strong>out</strong><br />give to many people (usually at no cost)<br />They were&nbsp;<strong>giving out</strong>&nbsp;free perfume samples at the department store.</p>\r\n<p><strong>give</strong>&nbsp;<em>something</em>&nbsp;<strong>up</strong><br />quit a habit<br />I am&nbsp;<strong>giving up</strong>&nbsp;smoking as of January 1st.</p>\r\n<p><strong>give up</strong><br />stop trying<br />My maths homework was too difficult so I&nbsp;<strong>gave up</strong>.</p>\r\n<p><strong>go after</strong>&nbsp;<em>somebody</em><br />follow somebody<br />My brother tried to&nbsp;<strong>go after</strong>&nbsp;the thief in his car.</p>\r\n<p><strong>go after</strong>&nbsp;<em>something</em><br />try to achieve something<br />I&nbsp;<strong>went after</strong>&nbsp;my dream and now I am a published writer.</p>\r\n<p><strong>go against</strong>&nbsp;<em>somebody</em><br />compete, oppose<br />We are&nbsp;<strong>going against</strong>&nbsp;the best soccer team in the city tonight.</p>\r\n<p><strong>go ahead</strong><br />start, proceed<br />Please&nbsp;<strong>go ahead</strong>&nbsp;and eat before the food gets cold.</p>\r\n<p><strong>go back</strong><br />return to a place<br />I have to&nbsp;<strong>go back</strong>&nbsp;home and get my lunch.</p>\r\n<p><strong>go out</strong><br />leave home to go on a social event<br />We're&nbsp;<strong>going out</strong>&nbsp;for dinner tonight.</p>\r\n<p><strong>go out with</strong>&nbsp;<em>somebody</em><br />date<br />Jesse has been&nbsp;<strong>going out with</strong>&nbsp;Luke since they met last winter.</p>\r\n<p><strong>go over</strong>&nbsp;<em>something</em><br />review<br />Please&nbsp;<strong>go over</strong>&nbsp;your answers before you submit your test.</p>\r\n<p><strong>go over</strong><br />visit somebody nearby<br />I haven't seen Tina for a long time. I think I'll&nbsp;<strong>go over</strong>&nbsp;for an hour or two.</p>\r\n<p><strong>go without</strong>&nbsp;<em>something</em><br />suffer lack or deprivation<br />When I was young, we&nbsp;<strong>went without</strong>&nbsp;winter boots.</p>\r\n<p><strong>grow apart</strong><br />stop being friends over time<br />My best friend and I&nbsp;<strong>grew apart</strong>&nbsp;after she changed schools.</p>\r\n<p><strong>grow back</strong><br />regrow<br />My roses&nbsp;<strong>grew back</strong>&nbsp;this summer.</p>\r\n<p><strong>grow into</strong>&nbsp;<em>something</em><br />grow big enough to fit<br />This bike is too big for him now, but he should&nbsp;<strong>grow into</strong>&nbsp;it by next year.</p>\r\n<p><strong>grow out of</strong>&nbsp;<em>something</em><br />get too big for<br />Elizabeth needs a new pair of shoes because she has&nbsp;<strong>grown out of</strong>&nbsp;her old ones.</p>\r\n<p><strong>grow up</strong><br />become an adult<br />When Jack&nbsp;<strong>grows up</strong>&nbsp;he wants to be a fireman.</p>\r\n<p><strong>hand</strong>&nbsp;<em>something</em>&nbsp;<strong>down</strong><br />give something used to somebody else<br />I&nbsp;<strong>handed</strong>&nbsp;my old comic books&nbsp;<strong>down</strong>&nbsp;to my little cousin.</p>\r\n<p><strong>hand</strong>&nbsp;<em>something</em>&nbsp;<strong>in</strong><br />submit<br />I have to&nbsp;<strong>hand in</strong>&nbsp;my essay by Friday.</p>\r\n<p><strong>hand</strong>&nbsp;<em>something</em>&nbsp;<strong>out</strong><br />to distribute to a group of people<br />We will&nbsp;<strong>hand out</strong>&nbsp;the invitations at the door.</p>\r\n<p><strong>hand</strong>&nbsp;<em>something</em>&nbsp;<strong>over</strong><br />give (usually unwillingly)<br />The police asked the man to&nbsp;<strong>hand over</strong>&nbsp;his wallet and his weapons.</p>\r\n<p><strong>hang in</strong><br />stay positive (informal)<br /><strong>Hang in</strong>&nbsp;there. I'm sure you'll find a job very soon.</p>\r\n<p><strong>hang on</strong><br />wait a short time (informal)<br /><strong>Hang on</strong>&nbsp;while I grab my coat and shoes!</p>\r\n<p><strong>hang out</strong><br />spend time relaxing (informal)<br />Instead of going to the party we are just going to&nbsp;<strong>hang out</strong>&nbsp;at my place.</p>\r\n<p><strong>hang up</strong><br />end a phone call<br />He didn't say goodbye before he&nbsp;<strong>hung up</strong>.</p>\r\n<p><strong>hold</strong>&nbsp;<em>somebody/ something</em>&nbsp;<strong>back</strong><br />prevent from doing/going<br />I had to&nbsp;<strong>hold</strong>&nbsp;my dog&nbsp;<strong>back</strong>&nbsp;because there was a cat in the park.</p>\r\n<p><strong>hold</strong>&nbsp;<em>something</em>&nbsp;<strong>back</strong><br />hide an emotion<br />Jamie&nbsp;<strong>held back</strong>&nbsp;his tears at his grandfather's funeral.</p>\r\n<p><strong>hold on</strong><br />wait a short time<br />Please&nbsp;<strong>hold on</strong>&nbsp;while I transfer you to the Sales Department.</p>\r\n<p><strong>hold onto</strong>&nbsp;<em>somebody/ something</em><br />hold firmly using your hands or arms<br /><strong>Hold onto</strong>&nbsp;your hat because it's very windy outside.</p>\r\n<p><strong>hold</strong>&nbsp;<em>somebody/ something</em>&nbsp;<strong>up</strong><br />rob<br />A man in a black mask&nbsp;<strong>held</strong>&nbsp;the bank&nbsp;<strong>up</strong>&nbsp;this morning.</p>\r\n<p><strong>keep on doing</strong>&nbsp;<em>something</em><br />continue doing<br /><strong>Keep on</strong>&nbsp;stirring until the liquid comes to a boil.</p>\r\n<p><strong>keep</strong>&nbsp;<em>something</em>&nbsp;<strong>from</strong>&nbsp;<em>somebody</em><br />not tell<br />We&nbsp;<strong>kept</strong>&nbsp;our relationship&nbsp;<strong>from</strong>&nbsp;our parents for two years.</p>\r\n<p><strong>keep</strong>&nbsp;<em>somebody/ something</em>&nbsp;<strong>out</strong><br />stop from entering<br />Try to&nbsp;<strong>keep</strong>&nbsp;the wet dog&nbsp;<strong>out</strong>&nbsp;of the living room.</p>\r\n<p><strong>keep</strong>&nbsp;<em>something</em>&nbsp;<strong>up</strong><br />continue at the same rate<br />If you&nbsp;<strong>keep</strong>&nbsp;those results&nbsp;<strong>up</strong>&nbsp;you will get into a great college.</p>\r\n<p><strong>let</strong>&nbsp;<em>somebody</em>&nbsp;<strong>down</strong><br />fail to support or help, disappoint<br />I need you to be on time. Don't&nbsp;<strong>let</strong>&nbsp;me&nbsp;<strong>down</strong>&nbsp;this time.</p>\r\n<p><strong>let</strong>&nbsp;<em>somebody</em>&nbsp;<strong>in</strong><br />allow to enter<br />Can you&nbsp;<strong>let</strong>&nbsp;the cat&nbsp;<strong>in</strong>&nbsp;before you go to school?</p>\r\n<p><strong>log in</strong>&nbsp;(or&nbsp;<strong>on</strong>)<br />sign in (to a website, database etc)<br />I can't&nbsp;<strong>log in</strong>&nbsp;to Facebook because I've forgotten my password.</p>\r\n<p><strong>log out</strong>&nbsp;(or&nbsp;<strong>off</strong>)<br />sign out (of a website, database etc)<br />If you don't&nbsp;<strong>log off</strong>&nbsp;somebody could get into your account.</p>\r\n<p><strong>look after</strong>&nbsp;<em>somebody/ something</em><br />take care of<br />I have to&nbsp;<strong>look after</strong>&nbsp;my sick grandmother.</p>\r\n<p><strong>look down on</strong>&nbsp;<em>somebody</em><br />think less of, consider inferior<br />Ever since we stole that chocolate bar your dad has&nbsp;<strong>looked down on</strong>&nbsp;me.</p>\r\n<p><strong>look for</strong>&nbsp;<em>somebody/ something</em><br />try to find<br />I'm&nbsp;<strong>looking for</strong>&nbsp;a red dress for the wedding.</p>\r\n<p><strong>look forward to</strong>&nbsp;<em>something</em><br />be excited about the future<br />I'm&nbsp;<strong>looking forward to</strong>&nbsp;the Christmas break.</p>\r\n<p><strong>look into</strong>&nbsp;<em>something</em><br />investigate<br />We are going to&nbsp;<strong>look into</strong>&nbsp;the price of snowboards today.</p>\r\n<p><strong>look out</strong><br />be careful, vigilant, and take notice<br />Look out! That car's going to hit you!</p>\r\n<p><strong>look out for</strong>&nbsp;<em>somebody/ something</em><br />be especially vigilant for<br />Don't forget to&nbsp;<strong>look out for</strong>&nbsp;snakes on the hiking trail.</p>\r\n<p><strong>look</strong>&nbsp;<em>something</em>&nbsp;<strong>over</strong><br />check, examine<br />Can you&nbsp;<strong>look over</strong>&nbsp;my essay for spelling mistakes?</p>\r\n<p><strong>look</strong>&nbsp;<em>something</em>&nbsp;<strong>up</strong><br />search and find information in a reference book or database<br />We can&nbsp;<strong>look</strong>&nbsp;her phone number&nbsp;<strong>up</strong>&nbsp;on the Internet.</p>\r\n<p><strong>look up to</strong>&nbsp;<em>somebody</em><br />have a lot of respect for<br />My little sister has always&nbsp;<strong>looked up</strong>&nbsp;to me.</p>\r\n<p><strong>make</strong>&nbsp;<em>something</em>&nbsp;<strong>up</strong><br />invent, lie about something<br />Josie&nbsp;<strong>made up</strong>&nbsp;a story about why we were late.</p>\r\n<p><strong>make up</strong><br />forgive each other<br />We were angry last night, but we&nbsp;<strong>made up</strong>&nbsp;at breakfast.</p>\r\n<p><strong>make</strong>&nbsp;<em>somebody</em>&nbsp;<strong>up</strong><br />apply cosmetics to<br />My sisters&nbsp;<strong>made</strong>&nbsp;me&nbsp;<strong>up</strong>&nbsp;for my graduation party.</p>\r\n<p><strong>mix</strong>&nbsp;<em>something</em>&nbsp;<strong>up</strong><br />confuse two or more things<br />I&nbsp;<strong>mixed up</strong>&nbsp;the twins' names again!</p>\r\n<p><strong>pass away</strong><br />die<br />His uncle&nbsp;<strong>passed away</strong>&nbsp;last night after a long illness.</p>\r\n<p><strong>pass out</strong><br />faint<br />It was so hot in the church that an elderly lady&nbsp;<strong>passed out</strong>.</p>\r\n<p><strong>pass</strong>&nbsp;<em>something</em>&nbsp;<strong>out</strong><br />give the same thing to many people<br />The professor&nbsp;<strong>passed</strong>&nbsp;the textbooks&nbsp;<strong>out</strong>&nbsp;before class.</p>\r\n<p><strong>pass</strong>&nbsp;<em>something</em>&nbsp;<strong>up</strong><br />decline (usually something good)<br />I&nbsp;<strong>passed up</strong>&nbsp;the job because I am afraid of change.</p>\r\n<p><strong>pay</strong>&nbsp;<em>somebody</em>&nbsp;<strong>back</strong><br />return owed money<br />Thanks for buying my ticket. I'll&nbsp;<strong>pay</strong>&nbsp;you&nbsp;<strong>back</strong>&nbsp;on Friday.</p>\r\n<p><strong>pay for</strong>&nbsp;<em>something</em><br />be punished for doing something bad<br />That bully will&nbsp;<strong>pay for</strong>&nbsp;being mean to my little brother.</p>\r\n<p><strong>pick</strong>&nbsp;<em>something</em>&nbsp;<strong>out</strong><br />choose<br />I&nbsp;<strong>picked out</strong>&nbsp;three sweaters for you to try on.</p>\r\n<p><strong>point</strong>&nbsp;<em>somebody/ something</em>&nbsp;<strong>out</strong><br />indicate with your finger<br />I'll&nbsp;<strong>point</strong>&nbsp;my boyfriend&nbsp;<strong>out</strong>&nbsp;when he runs by.</p>\r\n<p><strong>put</strong>&nbsp;<em>something</em>&nbsp;<strong>down</strong><br />put what you are holding on a surface or floor<br />You can&nbsp;<strong>put</strong>&nbsp;the groceries&nbsp;<strong>down</strong>&nbsp;on the kitchen counter.</p>\r\n<p><strong>put</strong>&nbsp;<em>somebody</em>&nbsp;<strong>down</strong><br />insult, make somebody feel stupid<br />The students&nbsp;<strong>put</strong>&nbsp;the substitute teacher&nbsp;<strong>down</strong>&nbsp;because his pants were too short.</p>\r\n<p><strong>put</strong>&nbsp;<em>something</em>&nbsp;<strong>off</strong><br />postpone<br />We are&nbsp;<strong>putting off</strong>&nbsp;our trip until January because of the hurricane.</p>\r\n<p><strong>put</strong>&nbsp;<em>something</em>&nbsp;<strong>out</strong><br />extinguish<br />The neighbours&nbsp;<strong>put</strong>&nbsp;the fire&nbsp;<strong>out</strong>&nbsp;before the firemen arrived.</p>\r\n<p><strong>put</strong>&nbsp;<em>something</em>&nbsp;<strong>together</strong><br />assemble<br />I have to&nbsp;<strong>put</strong>&nbsp;the crib&nbsp;<strong>together</strong>&nbsp;before the baby arrives.</p>\r\n<p><strong>put up with</strong>&nbsp;<em>somebody/ something</em><br />tolerate<br />I don't think I can&nbsp;<strong>put up with</strong>&nbsp;three small children in the car.</p>\r\n<p><strong>put</strong>&nbsp;<em>something</em>&nbsp;<strong>on</strong><br />put clothing/ accessories on your body<br />Don't forget to&nbsp;<strong>put on</strong>&nbsp;your new earrings for the party.</p>\r\n<p><strong>run into</strong>&nbsp;<em>somebody/ something</em><br />meet unexpectedly<br />I&nbsp;<strong>ran into</strong>&nbsp;an old school-friend at the mall.</p>\r\n<p><strong>run over</strong>&nbsp;<em>somebody/ something</em><br />drive a vehicle over a person or thing<br />I accidentally&nbsp;<strong>ran over</strong>&nbsp;your bicycle in the driveway.</p>\r\n<p><strong>run over/ through</strong>&nbsp;<em>something</em><br />rehearse, review<br />Let's&nbsp;<strong>run over/through</strong>&nbsp;these lines one more time before the show.</p>\r\n<p><strong>run away</strong><br />leave unexpectedly, escape<br />The child&nbsp;<strong>ran away</strong>&nbsp;from home and has been missing for three days.</p>\r\n<p><strong>run out</strong><br />have none left<br />We&nbsp;<strong>ran out</strong>&nbsp;of shampoo so I had to wash my hair with soap.</p>\r\n<p><strong>send</strong>&nbsp;<em>something</em>&nbsp;<strong>back</strong><br />return (usually by mail)<br />My letter got&nbsp;<strong>sent back</strong>&nbsp;to me because I used the wrong stamp.</p>\r\n<p><strong>set</strong>&nbsp;<em>something</em>&nbsp;<strong>up</strong><br />arrange, organize<br />Our boss&nbsp;<strong>set</strong>&nbsp;a meeting&nbsp;<strong>up</strong>&nbsp;with the president of the company.</p>\r\n<p><strong>set</strong>&nbsp;<em>somebody</em>&nbsp;<strong>up</strong><br />trick, trap<br />The police&nbsp;<strong>set up</strong>&nbsp;the car thief by using a hidden camera.</p>\r\n<p><strong>shop around</strong><br />compare prices<br />I want to&nbsp;<strong>shop around</strong>&nbsp;a little before I decide on these boots.</p>\r\n<p><strong>show off</strong><br />act extra special for people watching (usually boastfully)<br />He always&nbsp;<strong>shows off</strong>&nbsp;on his skateboard</p>\r\n<p><strong>sleep over</strong><br />stay somewhere for the night (informal)<br />You should&nbsp;<strong>sleep over</strong>&nbsp;tonight if the weather is too bad to drive home.</p>\r\n<p><strong>sort</strong>&nbsp;<em>something</em>&nbsp;<strong>out</strong><br />organize, resolve a problem<br />We need to&nbsp;<strong>sort</strong>&nbsp;the bills&nbsp;<strong>out</strong>&nbsp;before the first of the month.</p>\r\n<p><strong>stick to</strong>&nbsp;<em>something</em><br />continue doing something, limit yourself to one particular thing<br />You will lose weight if you&nbsp;<strong>stick to</strong>&nbsp;the diet.</p>\r\n<p><strong>switch</strong>&nbsp;<em>something</em>&nbsp;<strong>off</strong><br />stop the energy flow, turn off<br />The light's too bright. Could you&nbsp;<strong>switch</strong>&nbsp;it&nbsp;<strong>off</strong>.</p>\r\n<p><strong>switch</strong>&nbsp;<em>something</em>&nbsp;<strong>on</strong><br />start the energy flow, turn on<br />We heard the news as soon as we&nbsp;<strong>switched on</strong>&nbsp;the car radio.</p>\r\n<p><strong>take after</strong>&nbsp;<em>somebody</em><br />resemble a family member<br />I&nbsp;<strong>take after</strong>&nbsp;my mother. We are both impatient.</p>\r\n<p><strong>take</strong>&nbsp;<em>something</em>&nbsp;<strong>apart</strong><br />purposely break into pieces<br />He&nbsp;<strong>took</strong>&nbsp;the car brakes&nbsp;<strong>apart</strong>&nbsp;and found the problem.</p>\r\n<p><strong>take</strong>&nbsp;<em>something</em>&nbsp;<strong>back</strong><br />return an item<br />I have to&nbsp;<strong>take</strong>&nbsp;our new TV&nbsp;<strong>back</strong>&nbsp;because it doesn't work.</p>\r\n<p><strong>take off</strong><br />start to fly<br />My plane&nbsp;<strong>takes off</strong>&nbsp;in five minutes.</p>\r\n<p><strong>take</strong>&nbsp;<em>something</em>&nbsp;<strong>off</strong><br />remove something (usually clothing)<br /><strong>Take off</strong>&nbsp;your socks and shoes and come in the lake!</p>\r\n<p><strong>take</strong>&nbsp;<em>something</em>&nbsp;<strong>out</strong><br />remove from a place or thing<br />Can you&nbsp;<strong>take</strong>&nbsp;the garbage&nbsp;<strong>out</strong>&nbsp;to the street for me?</p>\r\n<p><strong>take</strong>&nbsp;<em>somebody</em>&nbsp;<strong>out</strong><br />pay for somebody to go somewhere with you<br />My grandparents&nbsp;<strong>took</strong>&nbsp;us&nbsp;<strong>out</strong>&nbsp;for dinner and a movie.</p>\r\n<p><strong>tear</strong>&nbsp;<em>something</em>&nbsp;<strong>up</strong><br />rip into pieces<br />I&nbsp;<strong>tore up</strong>&nbsp;my ex-boyfriend's letters and gave them back to him.</p>\r\n<p><strong>think back</strong><br />remember (often + to, sometimes + on)<br />When I&nbsp;<strong>think back</strong>&nbsp;on my youth, I wish I had studied harder.</p>\r\n<p><strong>think</strong>&nbsp;<em>something</em>&nbsp;<strong>over</strong><br />consider<br />I'll have to&nbsp;<strong>think</strong>&nbsp;this job offer&nbsp;<strong>over</strong>&nbsp;before I make my final decision.</p>\r\n<p><strong>throw</strong>&nbsp;<em>something</em>&nbsp;<strong>away</strong><br />dispose of<br />We&nbsp;<strong>threw</strong>&nbsp;our old furniture&nbsp;<strong>away</strong>&nbsp;when we won the lottery.</p>\r\n<p><strong>turn</strong>&nbsp;<em>something</em>&nbsp;<strong>down</strong><br />decrease the volume or strength (heat, light etc)<br />Please&nbsp;<strong>turn</strong>&nbsp;the TV&nbsp;<strong>down</strong>&nbsp;while the guests are here.</p>\r\n<p><strong>turn</strong>&nbsp;<em>something</em>&nbsp;<strong>down</strong><br />refuse<br />I&nbsp;<strong>turned</strong>&nbsp;the job&nbsp;<strong>down</strong>&nbsp;because I don't want to move.</p>\r\n<p><strong>turn</strong>&nbsp;<em>something</em>&nbsp;<strong>off</strong><br />stop the energy flow, switch off<br />Your mother wants you to&nbsp;<strong>turn</strong>&nbsp;the TV&nbsp;<strong>off</strong>&nbsp;and come for dinner.</p>\r\n<p><strong>turn</strong>&nbsp;<em>something</em>&nbsp;<strong>on</strong><br />start the energy, switch on<br />It's too dark in here. Let's&nbsp;<strong>turn</strong>&nbsp;some lights&nbsp;<strong>on</strong>.</p>\r\n<p><strong>turn</strong>&nbsp;<em>something</em>&nbsp;<strong>up</strong><br />increase the volume or strength (heat, light etc)<br />Can you&nbsp;<strong>turn</strong>&nbsp;the music&nbsp;<strong>up</strong>? This is my favourite song.</p>\r\n<p><strong>turn up</strong><br />appear suddenly<br />Our cat&nbsp;<strong>turned up</strong>&nbsp;after we put posters up all over the neighbourhood.</p>\r\n<p><strong>try</strong>&nbsp;<em>something</em>&nbsp;<strong>on</strong><br />sample clothing<br />I'm going to&nbsp;<strong>try</strong>&nbsp;these jeans&nbsp;<strong>on</strong>, but I don't think they will fit.</p>\r\n<p><strong>try</strong>&nbsp;<em>something</em>&nbsp;<strong>out</strong><br />test<br />I am going to&nbsp;<strong>try</strong>&nbsp;this new brand of detergent&nbsp;<strong>out</strong>.</p>\r\n<p><strong>use</strong>&nbsp;<em>something</em>&nbsp;<strong>up</strong><br />finish the supply<br />The kids&nbsp;<strong>used</strong>&nbsp;all of the toothpaste&nbsp;<strong>up</strong>&nbsp;so we need to buy some more.</p>\r\n<p><strong>wake up</strong><br />stop sleeping<br />We have to&nbsp;<strong>wake up</strong>&nbsp;early for work on Monday.</p>\r\n<p><strong>warm</strong>&nbsp;<em>somebody/ something</em>&nbsp;<strong>up</strong><br />increase the temperature<br />You can&nbsp;<strong>warm</strong>&nbsp;your feet&nbsp;<strong>up</strong>&nbsp;in front of the fireplace.</p>\r\n<p><strong>warm up</strong><br />prepare body for exercise<br />I always&nbsp;<strong>warm up</strong>&nbsp;by doing sit-ups before I go for a run.</p>\r\n<p><strong>wear off</strong><br />fade away<br />Most of my make-up&nbsp;<strong>wore off</strong>&nbsp;before I got to the party.</p>\r\n<p><strong>work out</strong><br />exercise<br />I&nbsp;<strong>work out</strong>&nbsp;at the gym three times a week.</p>\r\n<p><strong>work out</strong><br />be successful<br />Our plan&nbsp;<strong>worked out</strong>&nbsp;fine.</p>\r\n<p><strong>work</strong>&nbsp;<em>something</em>&nbsp;<strong>out</strong><br />make a calculation<br />We have to&nbsp;<strong>work out</strong>&nbsp;the total cost before we buy the house.</p>	\N	t	2021-11-28 20:59:38.143527+00	2021-11-28 20:59:38.143556+00	1	12
29	Take is used	phrasal verbs with Take	<p><img src="/media/article/11_27_2021_19_53_09.png" alt="" width="964" height="1259" /></p>\r\n<p>Take is used to talk about the amount of time you need in order to go somewhere or do something. It<br />must be used with an expression of time: It takes (me) at least an hour to get home from work.<br />Learning languages &ldquo;Take time &ldquo;</p>	\N	f	2021-11-27 19:53:15.445452+00	2021-12-07 13:44:22.32524+00	1	15
24	Grammar another other	Grammar another other	<p><img title="onother-other-the_other.jpg" src="../../../../../media/article/11_27_2021_19_02_15.png" alt="" width="1500" height="1200" /></p>\r\n<p>The words another and other mean the same thing, except that another is used with a singular noun and other is used with uncountable and plural nouns: She's going to the cinema with another friend. She's going to the cinema with other friends</p>\r\n<p>&nbsp;</p>	\N	t	2021-11-27 19:01:51.790338+00	2021-11-27 19:09:44.40856+00	1	13
28	Day 28 phrasal verbs with Take	phrasal verbs with Take	<table id="t2-hover">\r\n<thead>\r\n<tr>\r\n<th>Phrasal Verb</th>\r\n<th>Meaning</th>\r\n<th>Example</th>\r\n</tr>\r\n</thead>\r\n<tbody>\r\n<tr>\r\n<td><strong>Take after</strong></td>\r\n<td>Resemble in appearance or character</td>\r\n<td>Jamie really&nbsp;<strong>takes after</strong>&nbsp;his father.</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Take apart</strong></td>\r\n<td>Dismantle or separate the components</td>\r\n<td>The technician has to&nbsp;<strong>take</strong>&nbsp;the machine&nbsp;<strong>apart&nbsp;</strong>to repair it.</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Take away</strong></td>\r\n<td>Cause something to disappear</td>\r\n<td>The doctor gave me tablets to&nbsp;<strong>take away</strong>&nbsp;the pain.</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Take away</strong></td>\r\n<td>Buy food at a restaurant and carry it elsewhere to eat it.</td>\r\n<td>Two beef curries to&nbsp;<strong>take away</strong>&nbsp;please.</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Take back</strong></td>\r\n<td>Agree to receive back/ be returned.</td>\r\n<td>We will&nbsp;<strong>take back</strong>&nbsp;the goods only if you can produce the receipt.</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Take back</strong></td>\r\n<td>Retract or withdraw something said</td>\r\n<td>I&nbsp;<strong>take back</strong>&nbsp;what I said about cheating. I didn't mean it.</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Take care of</strong></td>\r\n<td>Look after</td>\r\n<td>I'll&nbsp;<strong>take care of</strong>&nbsp;your plants while you're away.</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Take off</strong></td>\r\n<td>Leave the ground (a plane)</td>\r\n<td>The plane&nbsp;<strong>took off</strong>&nbsp;at 7 o'clock.</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Take off</strong></td>\r\n<td>Remove something</td>\r\n<td>May I t<strong>ake off</strong>&nbsp;my jacket? It's warm in here.</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Take in</strong></td>\r\n<td>Allow to stay in one's home</td>\r\n<td>The old lady next door is always&nbsp;<strong>taking in</strong>&nbsp;stray cats and dogs!</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Take in</strong></td>\r\n<td>Note with your eyes and register.</td>\r\n<td>Amanda&nbsp;<strong>took in&nbsp;</strong>every detail of her rival's outfit.</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Take in</strong></td>\r\n<td>Understand what one sees, hears or reads; realise what is happening.</td>\r\n<td>The man immediately&nbsp;<strong>took in&nbsp;</strong>the scene and called the police.</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Take on</strong></td>\r\n<td>Hire or engage staff</td>\r\n<td>Business is good so the company is&nbsp;<strong>taking on</strong>&nbsp;extra staff.</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Take out</strong></td>\r\n<td>Remove or cause to disappear</td>\r\n<td>Try this. It should&nbsp;<strong>take out&nbsp;</strong>the stain.</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Take out</strong></td>\r\n<td>Extract from somewhere</td>\r\n<td>Sam&nbsp;<strong>took out</strong>&nbsp;a pen to note down the supplier's address.</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Take out</strong></td>\r\n<td>Invite somone to dinner,&nbsp; the cinema, etc.</td>\r\n<td>Her boyfriend&nbsp;<strong>took&nbsp;</strong>her&nbsp;<strong>out</strong>&nbsp;for a meal on her birthday.</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Take out</strong></td>\r\n<td>Obtain a service or document (insurance, loan, etc.)</td>\r\n<td>Many homeowners&nbsp;<strong>take out</strong>&nbsp;a mortgage when they buy property.</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Take to</strong></td>\r\n<td>Begin to like somebody or something.</td>\r\n<td>My parents&nbsp;<strong>took to&nbsp;</strong>James immediately.&nbsp;</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Take to</strong></td>\r\n<td>Make a new habit of something</td>\r\n<td>He's&nbsp;<strong>taken to</strong>&nbsp;walking in the park every morning.</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Take up</strong></td>\r\n<td>Fill or occupy space or time.</td>\r\n<td>There's not much space here. The big table&nbsp;<strong>takes up</strong>&nbsp;too much room.</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Take up</strong></td>\r\n<td>Adopt as a hobby or pastime.</td>\r\n<td>My father&nbsp;<strong>took up</strong>&nbsp;golf when he retired.</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Take up</strong></td>\r\n<td>Start something e.g. a job.</td>\r\n<td>While writing his first book Tony&nbsp;<strong>took up</strong>&nbsp;a job as a teacher.</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Take up</strong></td>\r\n<td>Make something shorter</td>\r\n<td>That skirt is too long for you. It will need to be&nbsp;<strong>taken up</strong>.</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Take up</strong></td>\r\n<td>Continue something interrupted</td>\r\n<td>She&nbsp;<strong>took up</strong>&nbsp;the story where Bill had left off.</td>\r\n</tr>\r\n</tbody>\r\n</table>	\N	t	2021-11-27 19:50:44.668225+00	2021-11-27 19:50:44.668256+00	1	15
23	100 Phrasal verbs	Phrasal verbs	<p>1. Call off = cancel&nbsp;</p>\r\n<p>2. Turn down = reject</p>\r\n<p>3. Bring up = mention&nbsp;</p>\r\n<p>4. Come up= arise/ produce&nbsp;</p>\r\n<p>5. Hand over = relinquish / give a chance&nbsp;</p>\r\n<p>6. Take over= take control /responsibility&nbsp;</p>\r\n<p>7. Take up= require&nbsp;</p>\r\n<p>8. Get on= continue / have a good relationship&nbsp;</p>\r\n<p>9. Talk over = discuss / interrupt&nbsp;</p>\r\n<p>10. Use up = exhaust / use completely&nbsp;</p>\r\n<p>11. Look forward to = await&nbsp;</p>\r\n<p>12. Go on = continue&nbsp;</p>\r\n<p>13. Catch up = discuss latest news&nbsp;</p>\r\n<p>14. Fill in = complete&nbsp;</p>\r\n<p>15. Hand in = submit&nbsp;</p>\r\n<p>16. Look up= find/search&nbsp;</p>\r\n<p>17. Look into = check/ investigate&nbsp;</p>\r\n<p>18. Figure out = understand / solve</p>\r\n<p>19. Go over = review&nbsp;</p>\r\n<p>20. Show up = arrive&nbsp;</p>\r\n<p>21. Ring up= call</p>\r\n<p>22. Go back = return to a place&nbsp;</p>\r\n<p>23. Pick out= choose</p>\r\n<p>24. chip in = help&nbsp;</p>\r\n<p>25. Break in on = interrupt&nbsp;</p>\r\n<p>26. Come apart = separate&nbsp;</p>\r\n<p>27. Go ahead = start / proceed&nbsp;</p>\r\n<p>28. Cut in = interrupt&nbsp;</p>\r\n<p>29. Own up= confess&nbsp;</p>\r\n<p>30. Figure out = discover&nbsp;</p>\r\n<p>31. Get back = return&nbsp;</p>\r\n<p>32. Get away = escape&nbsp;</p>\r\n<p>33. Work out= exercise&nbsp;</p>\r\n<p>34. Hang in = stay positive&nbsp;</p>\r\n<p>35. Put down = insult&nbsp;</p>\r\n<p>36. Pass out = faint&nbsp;</p>\r\n<p>37. Leave out = omit/ skip</p>\r\n<p>38. Show off = boast / brag&nbsp;</p>\r\n<p>39. Peter out = finish / come to an end gradually&nbsp;</p>\r\n<p>40. Lay off= dismiss&nbsp;</p>\r\n<p>41. Take on = employ( someone)&nbsp;</p>\r\n<p>42. Cross out = delete / cancel / erase&nbsp;</p>\r\n<p>43. Sort out = solve&nbsp;</p>\r\n<p>44. Make out = understand / hear&nbsp;</p>\r\n<p>45. Abide by = follow ( a rule / decision / instruction)</p>\r\n<p>46. Pile up = accumulate&nbsp;</p>\r\n<p>47. Pig put = eat a lot&nbsp;</p>\r\n<p>48. Pick up = collect</p>\r\n<p>49. mix up = confuse&nbsp;</p>\r\n<p>50. Make of = understand / have an opinion&nbsp;</p>\r\n<p>51. Opt for = choose&nbsp;</p>\r\n<p>52. Pass back = return&nbsp;</p>\r\n<p>53. Patch up = fix/ make things better&nbsp;</p>\r\n<p>54. Plump for = choose&nbsp;</p>\r\n<p>55. Polish off = finish / consume&nbsp;</p>\r\n<p>56. Decide upon = choose / select&nbsp;</p>\r\n<p>57. Die down = decrease&nbsp;</p>\r\n<p>58. Get along = leave&nbsp;</p>\r\n<p>59. Hook up = meet ( someone)&nbsp;</p>\r\n<p>60. Jack up= increase sharply&nbsp;</p>\r\n<p>61. Kick about = discuss&nbsp;</p>\r\n<p>62. Talk about = discuss&nbsp;</p>\r\n<p>63. Kick out = expel&nbsp;</p>\r\n<p>64. Lay on = organise/ supply&nbsp;</p>\r\n<p>65. Link up = connect /join&nbsp;</p>\r\n<p>66. Make after = chase</p>\r\n<p>67. Make away with = steal&nbsp;</p>\r\n<p>68. Big up = exaggerate the importance</p>\r\n<p>69. Blow up = explode&nbsp;</p>\r\n<p>70. Book in = check in at a hotel&nbsp;</p>\r\n<p>71. Call up= telephone&nbsp;</p>\r\n<p>72. Cap off = finish / complete&nbsp;</p>\r\n<p>73. Care for = like</p>\r\n<p>74. carry off = win/succeed&nbsp;</p>\r\n<p>75. Carry on = continue&nbsp;</p>\r\n<p>76. Add on = include</p>\r\n<p>77. Ask over = invite&nbsp;</p>\r\n<p>78. Back away = retreat / go backwards&nbsp;</p>\r\n<p>79. Back off = retreat&nbsp;</p>\r\n<p>80. Bag out = criticize</p>\r\n<p>81. Bull up = confuse / complicate</p>\r\n<p>82. Bear on = influence / affect&nbsp;</p>\r\n<p>83. Give up = quit / stop trying&nbsp;</p>\r\n<p>84. Keep on = continue&nbsp;</p>\r\n<p>85. Put off = postpone&nbsp;</p>\r\n<p>86. Turn up = appear suddenly&nbsp;</p>\r\n<p>87. Take after = resemble&nbsp;</p>\r\n<p>88. Bring up = raise (children)&nbsp;</p>\r\n<p>89. Fill out = complete a form&nbsp;</p>\r\n<p>90. Drop out of = leave school&nbsp;</p>\r\n<p>91. Do over = repeat a job / task&nbsp;</p>\r\n<p>92. Fill up = fill to capacity&nbsp;</p>\r\n<p>93. Look over = examine / check&nbsp;</p>\r\n<p>94. Put away = save / store&nbsp;</p>\r\n<p>95. Put out = extinguish</p>\r\n<p>96. Set up = arrange/ begin&nbsp;</p>\r\n<p>97. Throw away = discard&nbsp;</p>\r\n<p>98. Cut down on= curtail ( expenses)&nbsp;</p>\r\n<p>99. Try out = test</p>\r\n<p>100. Turn off = repulse.</p>	\N	t	2021-11-27 17:41:03.429894+00	2021-12-12 20:21:39.979292+00	1	12
25	OTHER (adjective)	OTHER (adjective)	<div class="header dheader">\r\n<h2 class="title dtitle"><em class="ti">Other</em></h2>\r\n</div>\r\n<p class="p dp"><em class="ti">Other</em>&nbsp;means &lsquo;additional or extra&rsquo;, or &lsquo;alternative&rsquo;, or &lsquo;different types of&rsquo;.</p>\r\n<div class="section dsection">\r\n<div class="header dheader">\r\n<h3 class="title dtitle"><em class="ti">Other</em>&nbsp;as a determiner</h3>\r\n</div>\r\n<p class="p dp"><span class="target dtarget">We can use&nbsp;<em class="ti">other</em> with singular uncountable nouns and with plural nouns:<br /><em class="ti">The embassy website has general information about visas.&nbsp;<span class="tb">Other</span>&nbsp;travel information can be obtained by calling the freephone number.</em>&nbsp;(additional&nbsp;or extra information)<br /></span></p>\r\n<p class="p dp"><em class="ti">Some music calms people;&nbsp;<span class="tb">other</span>&nbsp;music has the opposite effect.</em>&nbsp;(different types of music)</p>\r\n<p class="p dp"><em class="ti">What&nbsp;<span class="tb">other</span>&nbsp;books by Charles Dickens have you read, apart from &lsquo;Oliver Twist&rsquo;?</em>&nbsp;(additional or extra books)</p>\r\n<p class="p dp"><em class="ti">This one&rsquo;s too big. Do you have it in&nbsp;<span class="tb">other</span>&nbsp;sizes?</em>&nbsp;(alternative sizes)</p>\r\n<p class="p dp">If we use&nbsp;<em class="ti">other</em> before a singular countable noun, we must use another determiner before it:<em class="ti">I don&rsquo;t like the red one. I prefer&nbsp;<span class="tb">the</span>&nbsp;<span class="tb">other</span>&nbsp;colour.</em></p>\r\n<p class="p dp">Not:&nbsp;<span class="tdl">I prefer other colour</span>.</p>\r\n<p class="p dp"><em class="ti">Jeremy is at university;&nbsp;<span class="tb">our</span>&nbsp;<span class="tb">other</span>&nbsp;son is still at school.</em></p>\r\n<p class="p dp"><em class="ti">He got 100% in the final examination.&nbsp;<span class="tb">No</span>&nbsp;<span class="tb">other</span>&nbsp;student has ever achieved that.</em></p>\r\n<p class="p dp"><em class="ti">There&rsquo;s&nbsp;<span class="tb">one</span>&nbsp;<span class="tb">other</span>&nbsp;thing we need to discuss before we finish.</em></p>\r\n<div class="panel dpnl dpnl-w pad-indent">\r\n<div class="panel-head dpanel-head"><span class="panel-title dpanel-title">Warning:</span></div>\r\n<div class="panel-body dpnl-b dpnl-b-wh pad-indent">\r\n<p class="p dp"><span class="ti">Other </span><em class="ti">as a determiner does not have a plural form:</em></p>\r\n<p class="p dp"><span class="ti">Mandy </span><em class="ti">and Charlotte stayed behind. The&nbsp;<span class="tb">other</span>&nbsp;girls went home.</em></p>\r\nNot:&nbsp;<span class="tdl">The others girls</span> &hellip;</div>\r\n<div class="panel-body dpnl-b dpnl-b-wh pad-indent">Not:&nbsp;<span class="tdl">I prefer other colour</span>.\r\n<blockquote class="hbq">\r\n<p class="p dp"><em class="ti">Jeremy is at university;&nbsp;<span class="tb">our</span>&nbsp;<span class="tb">other</span>&nbsp;son is still at school.</em></p>\r\n</blockquote>\r\n<blockquote class="hbq">\r\n<p class="p dp"><em class="ti">He got 100% in the final examination.&nbsp;<span class="tb">No</span>&nbsp;<span class="tb">other</span>&nbsp;student has ever achieved that.</em></p>\r\n</blockquote>\r\n<blockquote class="hbq lmb-20">\r\n<p class="p dp"><em class="ti">There&rsquo;s&nbsp;<span class="tb">one</span>&nbsp;<span class="tb">other</span>&nbsp;thing we need to discuss before we finish.</em></p>\r\n</blockquote>\r\n<div class="panel dpnl dpnl-w pad-indent">\r\n<div class="panel-head dpanel-head"><span class="panel-title dpanel-title">Warning:</span></div>\r\n<div class="panel-body dpnl-b dpnl-b-wh pad-indent">\r\n<blockquote class="hbq lmt-15">\r\n<p class="p dp"><em class="ti">Other as a determiner does not have a plural form:</em></p>\r\n</blockquote>\r\n<blockquote class="hbq">\r\n<p class="p dp"><em class="ti">Mandy and Charlotte stayed behind. The&nbsp;<span class="tb">other</span>&nbsp;girls went home.</em></p>\r\n</blockquote>\r\n<blockquote class="hbq">\r\n<p class="p dp">Not:&nbsp;<span class="tdl">The others girls</span>&nbsp;&hellip;</p>\r\n</blockquote>\r\n</div>\r\n</div>\r\n<div class="section dsection dsection-f">\r\n<blockquote class="hbq lmt-15">\r\n<p class="p dp"><em class="ti">Joel and Karen are here, but where are&nbsp;<span class="tb">the other</span>&nbsp;kids?</em>&nbsp;(the remaining people in a group)</p>\r\n</blockquote>\r\n<blockquote class="hbq lmb-20">\r\n<p class="p dp"><em class="ti">Where are&nbsp;<span class="tb">the other</span>&nbsp;two dinner plates? I can only find four.</em>&nbsp;(the remaining things in a set &ndash; here six plates)</p>\r\n</blockquote>\r\n</div>\r\n<div class="x lpb-5 lmb-10 lbb">\r\n<h1 class="hfl hlh32 fs30 lmt--5 lmb-5"><em class="ti">Other</em>,&nbsp;<em class="ti">others</em>,&nbsp;<em class="ti">the other</em>&nbsp;or&nbsp;<em class="ti">another</em><span class="span dspan pad">?</span></h1>\r\n<div class="hfr lpt-5">\r\n<div class="pr hdib i i-facebook lp-5 lmr-10">&nbsp;</div>\r\n&nbsp;\r\n<div class="pr hdib i i-vk lp-5 lmr-10">&nbsp;</div>\r\n</div>\r\n</div>\r\n<div class="breadcrumb fs12 ti lmb-25"><a href="https://dictionary.cambridge.org/ru/%D0%B3%D1%80%D0%B0%D0%BC%D0%BC%D0%B0%D1%82%D0%B8%D0%BA%D0%B0/british-grammar/grammar">Grammar</a>&nbsp;&gt;&nbsp;<a href="https://dictionary.cambridge.org/ru/%D0%B3%D1%80%D0%B0%D0%BC%D0%BC%D0%B0%D1%82%D0%B8%D0%BA%D0%B0/british-grammar/easily-confused-words">Easily confused words</a>&nbsp;&gt;&nbsp;<em class="ti">Other</em>,&nbsp;<em class="ti">others</em>,&nbsp;<em class="ti">the other</em>&nbsp;or&nbsp;<em class="ti">another</em><span class="span dspan pad">?</span></div>\r\n<article class="di">\r\n<div class="share-pad">\r\n<div class="header dheader">\r\n<div class="fs12 had lmb-25">из&nbsp;<a href="https://dictionary.cambridge.org/ru/%D0%B3%D1%80%D0%B0%D0%BC%D0%BC%D0%B0%D1%82%D0%B8%D0%BA%D0%B0/%D0%B1%D1%80%D0%B8%D1%82%D0%B0%D0%BD%D1%81%D0%BA%D0%B0%D1%8F-%D0%B3%D1%80%D0%B0%D0%BC%D0%BC%D0%B0%D1%82%D0%B8%D0%BA%D0%B0/">English Grammar Today</a></div>\r\n</div>\r\n<div class="section dsection pr dsection-t">\r\n<div id="other-others-the-other-or-another__3" class="cid"></div>\r\n<div class="header dheader">\r\n<h2 class="title dtitle"><em class="ti">Other</em></h2>\r\n</div>\r\n<p class="p dp"><em class="ti">Other</em>&nbsp;means &lsquo;additional or extra&rsquo;, or &lsquo;alternative&rsquo;, or &lsquo;different types of&rsquo;.</p>\r\n<div class="section dsection">\r\n<div class="header dheader">\r\n<h3 class="title dtitle"><em class="ti">Other</em>&nbsp;as a determiner</h3>\r\n</div>\r\n<p class="p dp"><span class="target dtarget">We can use&nbsp;<em class="ti">other</em>&nbsp;with singular uncountable nouns and with plural nouns:</span></p>\r\n<blockquote class="hbq lmt-15">\r\n<p class="p dp"><em class="ti">The embassy website has general information about visas.&nbsp;<span class="tb">Other</span>&nbsp;travel information can be obtained by calling the freephone number.</em>&nbsp;(additional&nbsp;<span class="target dtarget">or extra information)</span></p>\r\n</blockquote>\r\n<blockquote class="hbq">\r\n<p class="p dp"><em class="ti">Some music calms people;&nbsp;<span class="tb">other</span>&nbsp;music has the opposite effect.</em>&nbsp;(different types of music)</p>\r\n</blockquote>\r\n<blockquote class="hbq">\r\n<p class="p dp"><em class="ti">What&nbsp;<span class="tb">other</span>&nbsp;books by Charles Dickens have you read, apart from &lsquo;Oliver Twist&rsquo;?</em>&nbsp;(additional or extra books)</p>\r\n</blockquote>\r\n<blockquote class="hbq lmb-20">\r\n<p class="p dp"><em class="ti">This one&rsquo;s too big. Do you have it in&nbsp;<span class="tb">other</span>&nbsp;sizes?</em>&nbsp;(alternative sizes)</p>\r\n</blockquote>\r\n<p class="p dp">If we use&nbsp;<em class="ti">other</em>&nbsp;before a singular countable noun, we must use another determiner before it:</p>\r\n<blockquote class="hbq lmt-15">\r\n<p class="p dp"><em class="ti">I don&rsquo;t like the red one. I prefer&nbsp;<span class="tb">the</span>&nbsp;<span class="tb">other</span>&nbsp;colour.</em></p>\r\n</blockquote>\r\n<blockquote class="hbq">\r\n<p class="p dp">Not:&nbsp;<span class="tdl">I prefer other colour</span>.</p>\r\n</blockquote>\r\n<blockquote class="hbq">\r\n<p class="p dp"><em class="ti">Jeremy is at university;&nbsp;<span class="tb">our</span>&nbsp;<span class="tb">other</span>&nbsp;son is still at school.</em></p>\r\n</blockquote>\r\n<blockquote class="hbq">\r\n<p class="p dp"><em class="ti">He got 100% in the final examination.&nbsp;<span class="tb">No</span>&nbsp;<span class="tb">other</span>&nbsp;student has ever achieved that.</em></p>\r\n</blockquote>\r\n<blockquote class="hbq lmb-20">\r\n<p class="p dp"><em class="ti">There&rsquo;s&nbsp;<span class="tb">one</span>&nbsp;<span class="tb">other</span>&nbsp;thing we need to discuss before we finish.</em></p>\r\n</blockquote>\r\n<div class="panel dpnl dpnl-w pad-indent">\r\n<div class="panel-head dpanel-head"><span class="panel-title dpanel-title">Warning:</span></div>\r\n<div class="panel-body dpnl-b dpnl-b-wh pad-indent">\r\n<blockquote class="hbq lmt-15">\r\n<p class="p dp"><em class="ti">Other as a determiner does not have a plural form:</em></p>\r\n</blockquote>\r\n<blockquote class="hbq">\r\n<p class="p dp"><em class="ti">Mandy and Charlotte stayed behind. The&nbsp;<span class="tb">other</span>&nbsp;girls went home.</em></p>\r\n</blockquote>\r\n<blockquote class="hbq">\r\n<p class="p dp">Not:&nbsp;<span class="tdl">The others girls</span>&nbsp;&hellip;</p>\r\n</blockquote>\r\n</div>\r\n</div>\r\n<div class="nav dnav">\r\n<p class="p dp">See also:</p>\r\n<ul class="ul dul">\r\n<li class="li dli">\r\n<p class="p dp"><a class="Ref" href="https://dictionary.cambridge.org/ru/%D0%B3%D1%80%D0%B0%D0%BC%D0%BC%D0%B0%D1%82%D0%B8%D0%BA%D0%B0/%D0%B1%D1%80%D0%B8%D1%82%D0%B0%D0%BD%D1%81%D0%BA%D0%B0%D1%8F-%D0%B3%D1%80%D0%B0%D0%BC%D0%BC%D0%B0%D1%82%D0%B8%D0%BA%D0%B0/determiners-the-my-some-this">Determiners (<em class="ti">the, my</em>,&nbsp;<em class="ti">some</em>,&nbsp;<em class="ti">this</em>)</a></p>\r\n</li>\r\n</ul>\r\n</div>\r\n</div>\r\n<div class="section dsection">\r\n<div class="header dheader">\r\n<h3 class="title dtitle"><em class="ti">Other</em>&nbsp;as a pronoun</h3>\r\n</div>\r\n<p class="p dp"><span class="target dtarget">We&nbsp;can use&nbsp;<em class="ti">other</em>&nbsp;as a pronoun. As a pronoun,&nbsp;<em class="ti">other</em>&nbsp;has a&nbsp;plural form,&nbsp;<em class="ti">others</em>:</span></p>\r\n<blockquote class="hbq lmt-15">\r\n<p class="p dp"><em class="ti">We have to solve this problem, more than any&nbsp;<span class="tb">other</span>, today.</em></p>\r\n</blockquote>\r\n<blockquote class="hbq lmb-20">\r\n<p class="p dp"><em class="ti">I&rsquo;ll attach two photos to this email and I&rsquo;ll send&nbsp;<span class="tb"><span class="target dtarget">others</span>&nbsp;tomorrow.</span></em></p>\r\n</blockquote>\r\n</div>\r\n</div>\r\n<div class="section dsection pr">\r\n<div id="other-others-the-other-or-another__29" class="cid"></div>\r\n<div class="header dheader">\r\n<h2 class="title dtitle"><em class="ti">The other</em></h2>\r\n</div>\r\n<div class="section dsection dsection-f">\r\n<div class="header dheader">\r\n<h3 class="title dtitle"><em class="ti">The other</em>&nbsp;as a determiner</h3>\r\n</div>\r\n<p class="p dp"><em class="ti">The other</em>&nbsp;with a singular noun means the second of two things or people, or the&nbsp;<span class="target dtarget">opposite of a set of two:</span></p>\r\n<blockquote class="hbq lmt-15">\r\n<p class="p dp"><em class="ti">This computer here is new.&nbsp;<span class="tb">The other</span>&nbsp;computer is about five years old.</em></p>\r\n</blockquote>\r\n<blockquote class="hbq lmb-20">\r\n<div class="utterance dutterance">\r\n<div class="speaker hfl tb lmr-10 lmb-5 lmt--1 dspeaker">A:</div>\r\n<p class="p dp"><em class="ti">D&rsquo;you&nbsp;<span class="target dtarget">know the Indian restaurant in Palmer Street?</span></em></p>\r\n</div>\r\n<div class="utterance dutterance">\r\n<div class="speaker hfl tb lmr-10 lmb-5 lmt--1 dspeaker">B:</div>\r\n<p class="p dp"><em class="ti">Yes</em>.</p>\r\n</div>\r\n<div class="utterance dutterance">\r\n<div class="speaker hfl tb lmr-10 lmb-5 lmt--1 dspeaker">A:</div>\r\n<p class="p dp"><em class="ti">Well, the gift shop is on</em>&nbsp;<span class="tb"><em class="ti">the</em></span>&nbsp;<span class="tb"><em class="ti">other</em></span>&nbsp;<em class="ti">side of the street, directly opposite</em>. (the opposite side)</p>\r\n</div>\r\n</blockquote>\r\n<p class="p dp"><em class="ti">The other</em>&nbsp;with a plural noun means the remaining people or things in a group or set:</p>\r\n<blockquote class="hbq lmt-15">\r\n<p class="p dp"><em class="ti">Joel and Karen are here, but where are&nbsp;<span class="tb">the other</span>&nbsp;kids?</em>&nbsp;(the remaining people in a group)</p>\r\n</blockquote>\r\n<blockquote class="hbq lmb-20">\r\n<p class="p dp"><em class="ti">Where are&nbsp;<span class="tb">the other</span>&nbsp;two dinner plates? I can only find four.</em>&nbsp;(the remaining things in a set &ndash; here six plates)</p>\r\n</blockquote>\r\n</div>\r\n<div id="ad_contentslot_1" class="am-default_moreslots contentslot" data-google-query-id="CMOgk_utz_QCFQJOGQodWNQATw">\r\n<div id="google_ads_iframe_/23202586/cdo_mpuslot_0__container__"><iframe id="google_ads_iframe_/23202586/cdo_mpuslot_0" tabindex="0" title="3rd party ad content" role="region" src="https://e2d7d5a297451c6d5df7f0f2db095a43.safeframe.googlesyndication.com/safeframe/1-0-38/html/container.html" name="" width="336" height="280" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" sandbox="allow-forms allow-popups allow-popups-to-escape-sandbox allow-same-origin allow-scripts allow-top-navigation-by-user-activation" data-is-safeframe="true" aria-label="Advertisement" data-google-container-id="6" data-load-complete="true" data-mce-fragment="1"></iframe></div>\r\n</div>\r\n<div class="section dsection">\r\n<div class="header dheader">\r\n<h3 class="title dtitle"><em class="ti">The other</em>&nbsp;as a pronoun</h3>\r\n</div>\r\n<p class="p dp"><span class="target dtarget">We can use&nbsp;<em class="ti">the other</em>&nbsp;as a pronoun, especially to refer back to something which has been mentioned already in the sentence:</span></p>\r\n<blockquote class="hbq lmt-15">\r\n<p class="p dp"><em class="ti">He had his hat in one hand and a bunch of flowers in&nbsp;<span class="tb">the other</span>.</em></p>\r\n</blockquote>\r\n<blockquote class="hbq lmb-20">\r\n<p class="p dp"><em class="ti">She has two kittens, one is black and&nbsp;<span class="tb">the other</span>&nbsp;is all white.</em></p>\r\n</blockquote>\r\n</div>\r\n</div>\r\n<div class="section dsection pr">\r\n<div id="other-others-the-other-or-another__47" class="cid"></div>\r\n<div class="header dheader">\r\n<h2 class="title dtitle">Another</h2>\r\n</div>\r\n<p class="p dp"><span class="target dtarget">When we use the indefinite article&nbsp;<em class="ti">an</em>&nbsp;before&nbsp;<em class="ti">other</em>, we write it as one word:&nbsp;<em class="ti">another</em>.&nbsp;<em class="ti">Another</em>&nbsp;means &lsquo;one more&rsquo; or &lsquo;an additional or extra&rsquo;, or &lsquo;an alternative or different&rsquo;.</span></p>\r\n<div class="section dsection">\r\n<div class="header dheader">\r\n<h3 class="title dtitle"><em class="ti">Another</em>&nbsp;as a determiner</h3>\r\n</div>\r\n<p class="p dp"><span class="target dtarget">We use&nbsp;<em class="ti">another</em>&nbsp;with singular nouns:</span></p>\r\n<blockquote class="hbq lmt-15">\r\n<p class="p dp"><em class="ti">Would you like&nbsp;<span class="tb">another</span>&nbsp;cup of coffee?</em></p>\r\n</blockquote>\r\n<blockquote class="hbq">\r\n<p class="p dp"><em class="ti">You&rsquo;ve met Linda, but I have&nbsp;<span class="tb">another</span>&nbsp;sister who you haven&rsquo;t met, called Margaret.</em></p>\r\n</blockquote>\r\n<blockquote class="hbq lmb-20">\r\n<p class="p dp"><em class="ti">I don&rsquo;t like this place. Is there&nbsp;<span class="tb">another</span>&nbsp;caf&eacute; around here we could go to?</em>&nbsp;(alternative or different)</p>\r\n</blockquote>\r\n</div>\r\n<div class="section dsection">\r\n<div class="header dheader">\r\n<h3 class="title dtitle"><em class="ti">Another</em>&nbsp;as a pronoun</h3>\r\n</div>\r\n<p class="p dp"><span class="target dtarget">We&nbsp;can use&nbsp;<em class="ti">another</em> as a pronoun:</span></p>\r\n<p class="p dp"><em class="ti">The applications are examined by one committee, then passed on to&nbsp;<span class="tb"><span class="target dtarget">another</span>.</span></em></p>\r\n<div class="x lpb-5 lmb-10 lbb">\r\n<h1 class="hfl hlh32 fs30 lmt--5 lmb-5"><em class="ti">Other</em>,&nbsp;<em class="ti">others</em>,&nbsp;<em class="ti">the other</em>&nbsp;or&nbsp;<em class="ti">another</em><span class="span dspan pad">?</span></h1>\r\n<div class="hfr lpt-5">\r\n<div class="pr hdib i i-facebook lp-5 lmr-10">&nbsp;</div>\r\n&nbsp;\r\n<div class="pr hdib i i-vk lp-5 lmr-10">&nbsp;</div>\r\n</div>\r\n</div>\r\n<div class="breadcrumb fs12 ti lmb-25"><a href="https://dictionary.cambridge.org/ru/%D0%B3%D1%80%D0%B0%D0%BC%D0%BC%D0%B0%D1%82%D0%B8%D0%BA%D0%B0/british-grammar/grammar">Grammar</a>&nbsp;&gt;&nbsp;<a href="https://dictionary.cambridge.org/ru/%D0%B3%D1%80%D0%B0%D0%BC%D0%BC%D0%B0%D1%82%D0%B8%D0%BA%D0%B0/british-grammar/easily-confused-words">Easily confused words</a>&nbsp;&gt;&nbsp;<em class="ti">Other</em>,&nbsp;<em class="ti">others</em>,&nbsp;<em class="ti">the other</em>&nbsp;or&nbsp;<em class="ti">another</em><span class="span dspan pad">?</span></div>\r\n<article class="di">\r\n<div class="share-pad">\r\n<div class="header dheader">\r\n<div class="fs12 had lmb-25">из&nbsp;<a href="https://dictionary.cambridge.org/ru/%D0%B3%D1%80%D0%B0%D0%BC%D0%BC%D0%B0%D1%82%D0%B8%D0%BA%D0%B0/%D0%B1%D1%80%D0%B8%D1%82%D0%B0%D0%BD%D1%81%D0%BA%D0%B0%D1%8F-%D0%B3%D1%80%D0%B0%D0%BC%D0%BC%D0%B0%D1%82%D0%B8%D0%BA%D0%B0/">English Grammar Today</a></div>\r\n</div>\r\n<div class="section dsection pr dsection-t">\r\n<div id="other-others-the-other-or-another__3" class="cid"></div>\r\n<div class="header dheader">\r\n<h2 class="title dtitle"><em class="ti">Other</em></h2>\r\n</div>\r\n<p class="p dp"><em class="ti">Other</em>&nbsp;means &lsquo;additional or extra&rsquo;, or &lsquo;alternative&rsquo;, or &lsquo;different types of&rsquo;.</p>\r\n<div class="section dsection">\r\n<div class="header dheader">\r\n<h3 class="title dtitle"><em class="ti">Other</em>&nbsp;as a determiner</h3>\r\n</div>\r\n<p class="p dp"><span class="target dtarget">We can use&nbsp;<em class="ti">other</em>&nbsp;with singular uncountable nouns and with plural nouns:</span></p>\r\n<blockquote class="hbq lmt-15">\r\n<p class="p dp"><em class="ti">The embassy website has general information about visas.&nbsp;<span class="tb">Other</span>&nbsp;travel information can be obtained by calling the freephone number.</em>&nbsp;(additional&nbsp;<span class="target dtarget">or extra information)</span></p>\r\n</blockquote>\r\n<blockquote class="hbq">\r\n<p class="p dp"><em class="ti">Some music calms people;&nbsp;<span class="tb">other</span>&nbsp;music has the opposite effect.</em>&nbsp;(different types of music)</p>\r\n</blockquote>\r\n<blockquote class="hbq">\r\n<p class="p dp"><em class="ti">What&nbsp;<span class="tb">other</span>&nbsp;books by Charles Dickens have you read, apart from &lsquo;Oliver Twist&rsquo;?</em>&nbsp;(additional or extra books)</p>\r\n</blockquote>\r\n<blockquote class="hbq lmb-20">\r\n<p class="p dp"><em class="ti">This one&rsquo;s too big. Do you have it in&nbsp;<span class="tb">other</span>&nbsp;sizes?</em>&nbsp;(alternative sizes)</p>\r\n</blockquote>\r\n<p class="p dp">If we use&nbsp;<em class="ti">other</em>&nbsp;before a singular countable noun, we must use another determiner before it:</p>\r\n<blockquote class="hbq lmt-15">\r\n<p class="p dp"><em class="ti">I don&rsquo;t like the red one. I prefer&nbsp;<span class="tb">the</span>&nbsp;<span class="tb">other</span>&nbsp;colour.</em></p>\r\n</blockquote>\r\n<blockquote class="hbq">\r\n<p class="p dp">Not:&nbsp;<span class="tdl">I prefer other colour</span>.</p>\r\n</blockquote>\r\n<blockquote class="hbq">\r\n<p class="p dp"><em class="ti">Jeremy is at university;&nbsp;<span class="tb">our</span>&nbsp;<span class="tb">other</span>&nbsp;son is still at school.</em></p>\r\n</blockquote>\r\n<blockquote class="hbq">\r\n<p class="p dp"><em class="ti">He got 100% in the final examination.&nbsp;<span class="tb">No</span>&nbsp;<span class="tb">other</span>&nbsp;student has ever achieved that.</em></p>\r\n</blockquote>\r\n<blockquote class="hbq lmb-20">\r\n<p class="p dp"><em class="ti">There&rsquo;s&nbsp;<span class="tb">one</span>&nbsp;<span class="tb">other</span>&nbsp;thing we need to discuss before we finish.</em></p>\r\n</blockquote>\r\n<div class="panel dpnl dpnl-w pad-indent">\r\n<div class="panel-head dpanel-head"><span class="panel-title dpanel-title">Warning:</span></div>\r\n<div class="panel-body dpnl-b dpnl-b-wh pad-indent">\r\n<blockquote class="hbq lmt-15">\r\n<p class="p dp"><em class="ti">Other as a determiner does not have a plural form:</em></p>\r\n</blockquote>\r\n<blockquote class="hbq">\r\n<p class="p dp"><em class="ti">Mandy and Charlotte stayed behind. The&nbsp;<span class="tb">other</span>&nbsp;girls went home.</em></p>\r\n</blockquote>\r\n<blockquote class="hbq">\r\n<p class="p dp">Not:&nbsp;<span class="tdl">The others girls</span>&nbsp;&hellip;</p>\r\n</blockquote>\r\n</div>\r\n</div>\r\n<div class="nav dnav">\r\n<p class="p dp">See also:</p>\r\n<ul class="ul dul">\r\n<li class="li dli">\r\n<p class="p dp"><a class="Ref" href="https://dictionary.cambridge.org/ru/%D0%B3%D1%80%D0%B0%D0%BC%D0%BC%D0%B0%D1%82%D0%B8%D0%BA%D0%B0/%D0%B1%D1%80%D0%B8%D1%82%D0%B0%D0%BD%D1%81%D0%BA%D0%B0%D1%8F-%D0%B3%D1%80%D0%B0%D0%BC%D0%BC%D0%B0%D1%82%D0%B8%D0%BA%D0%B0/determiners-the-my-some-this">Determiners (<em class="ti">the, my</em>,&nbsp;<em class="ti">some</em>,&nbsp;<em class="ti">this</em>)</a></p>\r\n</li>\r\n</ul>\r\n</div>\r\n</div>\r\n<div class="section dsection">\r\n<div class="header dheader">\r\n<h3 class="title dtitle"><em class="ti">Other</em>&nbsp;as a pronoun</h3>\r\n</div>\r\n<p class="p dp"><span class="target dtarget">We&nbsp;can use&nbsp;<em class="ti">other</em>&nbsp;as a pronoun. As a pronoun,&nbsp;<em class="ti">other</em>&nbsp;has a&nbsp;plural form,&nbsp;<em class="ti">others</em>:</span></p>\r\n<blockquote class="hbq lmt-15">\r\n<p class="p dp"><em class="ti">We have to solve this problem, more than any&nbsp;<span class="tb">other</span>, today.</em></p>\r\n</blockquote>\r\n<blockquote class="hbq lmb-20">\r\n<p class="p dp"><em class="ti">I&rsquo;ll attach two photos to this email and I&rsquo;ll send&nbsp;<span class="tb"><span class="target dtarget">others</span>&nbsp;tomorrow.</span></em></p>\r\n</blockquote>\r\n</div>\r\n</div>\r\n<div class="section dsection pr">\r\n<div id="other-others-the-other-or-another__29" class="cid"></div>\r\n<div class="header dheader">\r\n<h2 class="title dtitle"><em class="ti">The other</em></h2>\r\n</div>\r\n<div class="section dsection dsection-f">\r\n<div class="header dheader">\r\n<h3 class="title dtitle"><em class="ti">The other</em>&nbsp;as a determiner</h3>\r\n</div>\r\n<p class="p dp"><em class="ti">The other</em>&nbsp;with a singular noun means the second of two things or people, or the&nbsp;<span class="target dtarget">opposite of a set of two:</span></p>\r\n<blockquote class="hbq lmt-15">\r\n<p class="p dp"><em class="ti">This computer here is new.&nbsp;<span class="tb">The other</span>&nbsp;computer is about five years old.</em></p>\r\n</blockquote>\r\n<blockquote class="hbq lmb-20">\r\n<div class="utterance dutterance">\r\n<div class="speaker hfl tb lmr-10 lmb-5 lmt--1 dspeaker">A:</div>\r\n<p class="p dp"><em class="ti">D&rsquo;you&nbsp;<span class="target dtarget">know the Indian restaurant in Palmer Street?</span></em></p>\r\n</div>\r\n<div class="utterance dutterance">\r\n<div class="speaker hfl tb lmr-10 lmb-5 lmt--1 dspeaker">B:</div>\r\n<p class="p dp"><em class="ti">Yes</em>.</p>\r\n</div>\r\n<div class="utterance dutterance">\r\n<div class="speaker hfl tb lmr-10 lmb-5 lmt--1 dspeaker">A:</div>\r\n<p class="p dp"><em class="ti">Well, the gift shop is on</em>&nbsp;<span class="tb"><em class="ti">the</em></span>&nbsp;<span class="tb"><em class="ti">other</em></span>&nbsp;<em class="ti">side of the street, directly opposite</em>. (the opposite side)</p>\r\n</div>\r\n</blockquote>\r\n<p class="p dp"><em class="ti">The other</em>&nbsp;with a plural noun means the remaining people or things in a group or set:</p>\r\n<blockquote class="hbq lmt-15">\r\n<p class="p dp"><em class="ti">Joel and Karen are here, but where are&nbsp;<span class="tb">the other</span>&nbsp;kids?</em>&nbsp;(the remaining people in a group)</p>\r\n</blockquote>\r\n<blockquote class="hbq lmb-20">\r\n<p class="p dp"><em class="ti">Where are&nbsp;<span class="tb">the other</span>&nbsp;two dinner plates? I can only find four.</em>&nbsp;(the remaining things in a set &ndash; here six plates)</p>\r\n</blockquote>\r\n</div>\r\n<div id="ad_contentslot_1" class="am-default_moreslots contentslot" data-google-query-id="CMOgk_utz_QCFQJOGQodWNQATw">\r\n<div id="google_ads_iframe_/23202586/cdo_mpuslot_0__container__"><iframe id="google_ads_iframe_/23202586/cdo_mpuslot_0" tabindex="0" title="3rd party ad content" role="region" src="https://e2d7d5a297451c6d5df7f0f2db095a43.safeframe.googlesyndication.com/safeframe/1-0-38/html/container.html" name="" width="336" height="280" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" sandbox="allow-forms allow-popups allow-popups-to-escape-sandbox allow-same-origin allow-scripts allow-top-navigation-by-user-activation" data-is-safeframe="true" aria-label="Advertisement" data-google-container-id="6" data-load-complete="true" data-mce-fragment="1"></iframe></div>\r\n</div>\r\n<div class="section dsection">\r\n<div class="header dheader">\r\n<h3 class="title dtitle"><em class="ti">The other</em>&nbsp;as a pronoun</h3>\r\n</div>\r\n<p class="p dp"><span class="target dtarget">We can use&nbsp;<em class="ti">the other</em>&nbsp;as a pronoun, especially to refer back to something which has been mentioned already in the sentence:</span></p>\r\n<blockquote class="hbq lmt-15">\r\n<p class="p dp"><em class="ti">He had his hat in one hand and a bunch of flowers in&nbsp;<span class="tb">the other</span>.</em></p>\r\n</blockquote>\r\n<blockquote class="hbq lmb-20">\r\n<p class="p dp"><em class="ti">She has two kittens, one is black and&nbsp;<span class="tb">the other</span>&nbsp;is all white.</em></p>\r\n</blockquote>\r\n</div>\r\n</div>\r\n<div class="section dsection pr">\r\n<div id="other-others-the-other-or-another__47" class="cid"></div>\r\n<div class="header dheader">\r\n<h2 class="title dtitle">Another</h2>\r\n</div>\r\n<p class="p dp"><span class="target dtarget">When we use the indefinite article&nbsp;<em class="ti">an</em>&nbsp;before&nbsp;<em class="ti">other</em>, we write it as one word:&nbsp;<em class="ti">another</em>.&nbsp;<em class="ti">Another</em>&nbsp;means &lsquo;one more&rsquo; or &lsquo;an additional or extra&rsquo;, or &lsquo;an alternative or different&rsquo;.</span></p>\r\n<div class="section dsection">\r\n<div class="header dheader">\r\n<h3 class="title dtitle"><em class="ti">Another</em>&nbsp;as a determiner</h3>\r\n</div>\r\n<p class="p dp"><span class="target dtarget">We use&nbsp;<em class="ti">another</em>&nbsp;with singular nouns:</span></p>\r\n<blockquote class="hbq lmt-15">\r\n<p class="p dp"><em class="ti">Would you like&nbsp;<span class="tb">another</span>&nbsp;cup of coffee?</em></p>\r\n</blockquote>\r\n<blockquote class="hbq">\r\n<p class="p dp"><em class="ti">You&rsquo;ve met Linda, but I have&nbsp;<span class="tb">another</span>&nbsp;sister who you haven&rsquo;t met, called Margaret.</em></p>\r\n</blockquote>\r\n<blockquote class="hbq lmb-20">\r\n<p class="p dp"><em class="ti">I don&rsquo;t like this place. Is there&nbsp;<span class="tb">another</span>&nbsp;caf&eacute; around here we could go to?</em>&nbsp;(alternative or different)</p>\r\n</blockquote>\r\n</div>\r\n<div class="section dsection">\r\n<div class="header dheader">\r\n<h3 class="title dtitle"><em class="ti">Another</em>&nbsp;as a pronoun</h3>\r\n</div>\r\n<p class="p dp"><span class="target dtarget">We&nbsp;can use&nbsp;<em class="ti">another</em> as a pronoun:</span></p>\r\n<p class="p dp"><em class="ti">The applications are examined by one committee, then passed on to&nbsp;<span class="tb"><span class="target dtarget">another</span>.</span></em></p>\r\n<blockquote class="hbq lmt-15">\r\n<p class="p dp"><em class="ti"><span class="tb">Other</span>&nbsp;interesting places to visit include the old harbour and the castle.</em></p>\r\n</blockquote>\r\n<blockquote class="hbq lmb-20">\r\n<p class="p dp">Not:&nbsp;<span class="tdl">Another interesting places to visit</span>&nbsp;&hellip;</p>\r\n</blockquote>\r\n</div>\r\n</div>\r\n</div>\r\n</article>\r\n<p class="p dp">&nbsp;</p>\r\n<p class="p dp">&nbsp;</p>\r\n</div>\r\n</div>\r\n</div>\r\n</article>\r\n</div>\r\n</div>\r\n</div>	\N	t	2021-11-27 19:10:01.660144+00	2021-12-07 07:27:40.854465+00	1	13
43	Present countinious	Present countinious	<p><img title="present continuous.jpg" src="/media/article/12_17_2021_20_30_44.png" alt="" width="1305" height="1849" /></p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p><img title="present continuos1.jpg" src="/media/article/12_17_2021_20_32_11.png" alt="" width="818" height="1105" /></p>\r\n<p><img title="present continuous2.jpg" src="/media/article/12_17_2021_20_34_56.png" alt="" width="1363" height="1927" /></p>	\N	t	2021-12-17 20:28:26.432175+00	2021-12-17 20:40:18.792936+00	1	32
27	past perfect	past perfect	<p>finished action before a second point in the past. When we arrived, the film had started<br />first the<br />film started, then we arrived). ...&nbsp;</p>\r\n<div class="example__line example__line_sound"><span class="eng js-sound" data-word="1"><span class="dword">I</span>&nbsp;<span class="dword">called</span>&nbsp;<span class="dword">Jim</span>&nbsp;<span class="dword">too</span>&nbsp;<span class="dword">late</span>,&nbsp;<span class="dword">he</span>&nbsp;<strong><span class="dword">had</span>&nbsp;</strong><span class="dword">already</span>&nbsp;<strong><span class="dword">left</span></strong>.</span><br /><span class="rus">Я позвонил Джиму слишком поздно, он уже ушел.</span></div>\r\n<div class="example__line example__line_sound example__line_break"><span class="eng js-sound" data-word="2"><span class="dword">We</span>&nbsp;<strong><span class="dword">had</span>&nbsp;<span class="dword">lived</span></strong>&nbsp;<span class="dword">in</span>&nbsp;<span class="dword">Paris</span>&nbsp;<span class="dword">for</span>&nbsp;12&nbsp;<span class="dword">years</span>&nbsp;<span class="dword">before</span>&nbsp;<span class="dword">we</span>&nbsp;<span class="dword">moved</span>&nbsp;<span class="dword">to</span>&nbsp;<span class="dword">America</span>.</span><br /><span class="rus">До переезда в Америку мы прожили в Париже 12 лет.</span></div>\r\n<p><span class="eng js-sound" data-word="9"><strong><span class="dword">Had</span></strong>&nbsp;<span class="dword">you</span>&nbsp;<strong><span class="dword">brushed</span></strong>&nbsp;<span class="dword">your</span>&nbsp;<span class="dword">teeth</span>&nbsp;<span class="dword">before</span>&nbsp;<span class="dword">you</span>&nbsp;<span class="dword">went</span>&nbsp;<span class="dword">to</span>&nbsp;<span class="dword">bed</span>?</span><br /><span class="rus">Ты почистил зубы, прежде чем пойти спать?</span></p>\r\n<p>В&nbsp;<strong>отрицательных предложениях</strong>&nbsp;за вспомогательным глаголом следует отрицательная частица&nbsp;<strong><span class="dword">not</span></strong>. При этом они могут быть сокращены до формы&nbsp;<strong><span class="dword">hadn't</span></strong>.</p>\r\n<div class="example">\r\n<div class="example__line example__line_sound"><span class="eng js-sound" data-word="10"><span class="dword">I</span>&nbsp;<strong><span class="dword">had</span>&nbsp;<span class="dword">not</span>&nbsp;<span class="dword">finished</span></strong>&nbsp;<span class="dword">my</span>&nbsp;<span class="dword">work</span>&nbsp;<span class="dword">when</span>&nbsp;<span class="dword">he</span>&nbsp;<span class="dword">came</span>&nbsp;<span class="dword">here</span>.</span><br /><span class="rus">Я не закончил свою работу, когда он сюда пришел.</span></div>\r\n<div class="example__line example__line_sound example__line_break"><span class="eng js-sound" data-word="11"><span class="dword">How</span>&nbsp;<span class="dword">did</span>&nbsp;<span class="dword">you</span>&nbsp;<span class="dword">hope</span>&nbsp;<span class="dword">to</span>&nbsp;<span class="dword">pass</span>&nbsp;<span class="dword">the</span>&nbsp;<span class="dword">exam</span>&nbsp;<span class="dword">if</span>&nbsp;<span class="dword">you</span>&nbsp;<strong><span class="dword">hadn't</span></strong>&nbsp;<span class="dword">even</span>&nbsp;<strong><span class="dword">opened</span></strong>&nbsp;<span class="dword">the</span>&nbsp;<span class="dword">textbook</span>?</span><br /><span class="rus">Как ты надеялся сдать экзамен, если ты до этого даже учебник не открыл?</span></div>\r\n<div class="example__line example__line_sound example__line_break">&nbsp;</div>\r\n<div class="example__line example__line_sound example__line_break">\r\n<h2 class="mtitle">Случаи употребления&nbsp;<span class="dword">Past</span>&nbsp;<span class="dword">Perfect</span>:</h2>\r\n<ul class="list">\r\n<li class="list__item">Действие, закончившееся до определенного момента в прошлом, на которое может указывать точная дата или час, начало другого действия или контекст:</li>\r\n</ul>\r\n<div class="example">\r\n<div class="example__line example__line_sound"><span class="eng js-sound" data-word="12"><span class="dword">After</span>&nbsp;<span class="dword">the</span>&nbsp;<span class="dword">Sun</span>&nbsp;<strong><span class="dword">had</span>&nbsp;<span class="dword">set</span></strong>,&nbsp;<span class="dword">we</span>&nbsp;<span class="dword">saw</span>&nbsp;<span class="dword">thousands</span>&nbsp;<span class="dword">of</span>&nbsp;<span class="dword">fireflies</span>.</span><br /><span class="rus">После того, как зашло солнце, мы увидели тысячи светлячков.</span></div>\r\n</div>\r\n<ul class="list">\r\n<li class="list__item">Перечисление действий в прошлом, произошедших до времени повествования в целом:</li>\r\n</ul>\r\n<div class="example">\r\n<div class="example__line example__line_sound"><span class="eng js-sound" data-word="13"><span class="dword">I</span>&nbsp;<span class="dword">finally</span>&nbsp;<span class="dword">caught</span>&nbsp;<span class="dword">Lucky</span>&nbsp;<span class="dword">and</span>&nbsp;<span class="dword">looked</span>&nbsp;<span class="dword">around</span>.&nbsp;<span class="dword">The</span>&nbsp;<span class="dword">nasty</span>&nbsp;<span class="dword">dog</span>&nbsp;<strong><span class="dword">had</span>&nbsp;<span class="dword">scratched</span></strong>&nbsp;<span class="dword">the</span>&nbsp;<span class="dword">furniture</span>,&nbsp;<strong><span class="dword">had</span>&nbsp;<span class="dword">torn</span></strong>&nbsp;<span class="dword">the</span>&nbsp;<span class="dword">wallpapers</span>&nbsp;<span class="dword">and</span>&nbsp;<strong><span class="dword">had</span>&nbsp;<span class="dword">eaten</span>&nbsp;</strong><span class="dword">my</span>&nbsp;<span class="dword">lunch</span>&nbsp;<span class="dword">on</span>&nbsp;<span class="dword">the</span>&nbsp;<span class="dword">table</span>.</span><br /><span class="rus">Я наконец поймал Лаки и осмотрелся вокруг. Мерзкая собака исцарапала мебель, порвала обои и съела мой обед на столе.</span></div>\r\n<div class="example__line example__line_sound">&nbsp;</div>\r\n</div>\r\n</div>\r\n</div>\r\n<p>Something that started in the past and continued up to another action or time in the past. ...&nbsp;</p>\r\n<p>To talk about unreal or imaginary things in the past.</p>\r\n<p>&nbsp;</p>\r\n<p><img title="parst_perfect.jpg" src="/media/article/11_27_2021_19_24_33.png" alt="" width="973" height="734" /></p>\r\n<p><img title="20_past_perfect.jpg" src="/media/article/11_27_2021_19_27_44.png" alt="" width="707" height="1000" /></p>	\N	t	2021-11-27 19:24:36.645687+00	2021-12-07 13:33:41.796239+00	1	14
31	articles	articles	<p><img src="/media/article/12_08_2021_16_09_02.png" alt="" width="1280" height="720" /></p>\r\n<p>&nbsp;</p>\r\n<p><img src="/media/article/12_08_2021_16_09_18.png" alt="" /></p>	\N	t	2021-12-08 16:07:23.226956+00	2021-12-08 16:09:26.865863+00	1	16
45	6 types of adverbs	6 types of adverbs	<p><img title="6 types of adverbs.jpg" src="/media/article/12_18_2021_13_59_28.png" alt="" width="1424" height="1677" /></p>\r\n<p>&nbsp;</p>\r\n<p>near far whatever you are</p>\r\n<p>Nearby, far away, miles apart. An adverb of place can indicate an object's position in relation to<br />another object. For example: Below, between, above, behind, through, around and so forth.</p>\r\n<p>&nbsp;</p>\r\n<p><img title="adverbs of place1.jpg" src="/media/article/12_18_2021_14_02_40.png" alt="" width="700" height="1015" /></p>\r\n<p>&nbsp;</p>\r\n<p><img title="placement of adverbs.jpg" src="/media/article/12_18_2021_14_04_53.png" alt="" width="1500" height="1800" /></p>\r\n<p>&nbsp;</p>\r\n<p><img title="examples of adverbs.jpg" src="/media/article/12_18_2021_14_05_54.png" alt="" width="745" height="1024" /></p>\r\n<p>&nbsp;</p>\r\n<p><img title="adverbs with ly.jpg" src="/media/article/12_18_2021_14_06_32.png" alt="" width="828" height="1792" /></p>\r\n<p>&nbsp;</p>\r\n<p><img title="adverbs without ly.jpg" src="/media/article/12_18_2021_14_07_47.png" alt="" width="1127" height="1337" /></p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>	\N	t	2021-12-18 13:59:17.142297+00	2021-12-18 14:07:54.056774+00	1	34
46	who whom	who whom	<p><img title="who whom.jpg" src="/media/article/12_18_2021_14_12_14.png" alt="" width="1500" height="1450" /></p>\r\n<p><img title="whom examples.jpg" src="/media/article/12_18_2021_14_12_29.png" alt="" width="800" height="740" /></p>	\N	t	2021-12-18 14:11:28.201885+00	2021-12-18 14:21:02.883576+00	1	35
32	Action Verbs	Action Verbs	<ol>\r\n<li><span style="font-size: 18pt;">add - добавлять</span></li>\r\n<li><span style="font-size: 18pt;">appear&nbsp; - появляться</span></li>\r\n<li><span style="font-size: 18pt;">arrive - прибывать</span></li>\r\n<li><span style="font-size: 18pt;">be - быть</span></li>\r\n<li><span style="font-size: 18pt;">be absent - отсутствовать</span></li>\r\n<li><span style="font-size: 18pt;">be late - опаздывать</span></li>\r\n<li><span style="font-size: 24px;">be present - присутствовать</span></li>\r\n<li><span style="font-size: 24px;">be sufficient&nbsp; - быть достаточным</span></li>\r\n<li><span style="font-size: 24px;">begin - начинать</span></li>\r\n<li><span style="font-size: 24px;">belong - принадлежать</span></li>\r\n<li><span style="font-size: 24px;">bend&nbsp; - сгибаться</span></li>\r\n<li><span style="font-size: 24px;">carry - нести</span></li>\r\n<li><span style="font-size: 24px;">catch - ловить</span></li>\r\n<li><span style="font-size: 24px;">cease - переставать</span></li>\r\n<li><span style="font-size: 24px;">climb - взбераться</span></li>\r\n<li><span style="font-size: 24px;">come - приходить</span></li>\r\n<li><span style="font-size: 24px;">contain - содержать</span></li>\r\n<li><span style="font-size: 24px;">continue - продолжать</span></li>\r\n<li><span style="font-size: 24px;">cross - пересекать</span></li>\r\n<li><span style="font-size: 24px;">crouch - красться</span></li>\r\n<li><span style="font-size: 24px;">depart - отправлять</span></li>\r\n<li><span style="font-size: 24px;">descend - спускать</span></li>\r\n<li><span style="font-size: 24px;">dive - нырять</span></li>\r\n<li><span style="font-size: 24px;">drag - тащить</span></li>\r\n<li><span style="font-size: 24px;">end - завершать</span></li>\r\n<li><span style="font-size: 24px;">escape - убежать</span></li>\r\n<li><span style="font-size: 24px;">exclude - исключать</span></li>\r\n<li><span style="font-size: 24px;">fill - наполнять</span></li>\r\n<li><span style="font-size: 24px;">finish - завершать</span></li>\r\n<li><span style="font-size: 24px;">flow - течь</span></li>\r\n<li><span style="font-size: 24px;">fly - летать</span></li>\r\n<li><span style="font-size: 24px;">follow - следовать</span></li>\r\n<li><span style="font-size: 24px;">gather - собирать</span></li>\r\n<li><span style="font-size: 24px;">go - идти</span></li>\r\n<li><span style="font-size: 24px;">go along - идти вдоль</span></li>\r\n<li><span style="font-size: 24px;">go in - вхоодить</span></li>\r\n<li><span style="font-size: 24px;">go on foot&nbsp; - идти пешком</span></li>\r\n<li><span style="font-size: 24px;">go out&nbsp; - выходить</span></li>\r\n<li><span style="font-size: 24px;">have - иметь</span></li>\r\n<li><span style="font-size: 24px;">hit - бить</span></li>\r\n<li><span style="font-size: 24px;">hold - держать</span></li>\r\n<li><span style="font-size: 24px;">hold up - поднимать</span></li>\r\n<li><span style="font-size: 24px;">hop - подпрыгивать</span></li>\r\n<li><span style="font-size: 24px;">hurry - спешить</span></li>\r\n<li><span style="font-size: 24px;">jump - прыгать</span></li>\r\n<li><span style="font-size: 24px;">keep - удерживать</span></li>\r\n<li><span style="font-size: 24px;">kick&nbsp; - бить ногой</span></li>\r\n<li><span style="font-size: 24px;">lean - прислоняться, облакачиваться</span></li>\r\n<li><span style="font-size: 24px;">leap - прыгать (на большое расстояние)</span></li>\r\n<li><span style="font-size: 24px;">lie down - лечь, прилечь</span></li>\r\n<li><span style="font-size: 24px;">lift - поднимать</span></li>\r\n<li><span style="font-size: 24px;">lower - опускать</span></li>\r\n<li><span style="font-size: 24px;">march - шагать</span></li>\r\n<li><span style="font-size: 24px;">meet - встречать</span></li>\r\n<li><span style="font-size: 24px;">move on - идти дальше</span></li>\r\n<li><span style="font-size: 24px;">move in - выезжать, поселяться; входить</span></li>\r\n<li><span style="font-size: 24px;">pass - пройти мимо, следовать</span></li>\r\n<li><span style="font-size: 24px;">pick up - подбирать</span></li>\r\n<li><span style="font-size: 24px;">posses -&nbsp; владеть</span></li>\r\n<li><span style="font-size: 24px;">pull - тянуть</span></li>\r\n<li><span style="font-size: 24px;">punch - бить кулаком</span></li>\r\n<li><span style="font-size: 24px;">push - толкать</span></li>\r\n<li><span style="font-size: 24px;">put off - откладывать</span></li>\r\n<li><span style="font-size: 24px;">raise - подниматься</span></li>\r\n<li><span style="font-size: 24px;">reduce - сокращать</span></li>\r\n<li><span style="font-size: 24px;">remain - оставаться</span></li>\r\n<li><span style="font-size: 24px;">return - возвращаться</span></li>\r\n<li><span style="font-size: 24px;">ride - скакать, ехать</span></li>\r\n<li><span style="font-size: 24px;">rise - подниматься</span></li>\r\n<li><span style="font-size: 24px;">rotate - вращаться вокруг оси</span></li>\r\n<li><span style="font-size: 24px;">run - бежать</span></li>\r\n<li><span style="font-size: 24px;">shake - трясти</span></li>\r\n<li><span style="font-size: 24px;">sink - погружаться</span></li>\r\n<li><span style="font-size: 24px;">sit - сидеть</span></li>\r\n<li><span style="font-size: 24px;">skip - скакать</span></li>\r\n<li><span style="font-size: 24px;">slap/hit - шлёпать</span></li>\r\n<li><span style="font-size: 24px;">slide - скользить</span></li>\r\n<li><span style="font-size: 24px;">spend - тратить</span></li>\r\n<li><span style="font-size: 24px;">spoil - портить</span></li>\r\n<li><span style="font-size: 24px;">squat - приседать</span></li>\r\n<li><span style="font-size: 24px;">stop - прекращать</span></li>\r\n<li><span style="font-size: 24px;">stretch - тянуться, вытягиваться</span></li>\r\n<li><span style="font-size: 24px;">surpass - перегонять</span></li>\r\n<li><span style="font-size: 24px;">swim - плыть</span></li>\r\n<li><span style="font-size: 24px;">take - занимать (время)</span></li>\r\n<li><span style="font-size: 24px;">throw - кидать</span></li>\r\n<li><span style="font-size: 24px;">tiptoe - идти на ципочках</span></li>\r\n<li><span style="font-size: 24px;">track - выслеживать</span></li>\r\n<li><span style="font-size: 24px;">walk - идти</span></li>\r\n<li><span style="font-size: 24px;">walk up - подойти, подходить</span></li>\r\n<li><span style="font-size: 24px;">walk around - ходить, бродить повсюду</span></li>\r\n<li><span style="font-size: 24px;">walk back - возвращаться, идти назад/обратно</span></li>\r\n<li><span style="font-size: 24px;">write down - записывать, излагать письменно</span></li>\r\n</ol>	\N	t	2021-12-11 20:35:18.686945+00	2021-12-12 20:21:09.219186+00	1	18
34	First conditional	First conditional	<p>If ... if it&rsquo;s rains I will get wet если пойдёт дождь то я намокну</p>\r\n<p>If you study well (condition ) you will make lots of money</p>\r\n<p><img title="first_condition.jpg" src="/media/article/12_13_2021_19_53_55.png" alt="" width="1200" height="628" /></p>\r\n<p>&nbsp;</p>	\N	t	2021-12-13 19:42:36.610447+00	2021-12-13 20:05:15.406749+00	1	24
36	Describtive words	Describtive words	<p>Fresh smell opposite musty (old mold ) noxious smell rancid sharp smell -acrid<br />Неприятный запах Foul smell<br />Свежий запах - fresh smell (старая плесень) затхлый - old mold ядовитый запах noxious<br />протухший- rancid<br />резкий запах-sharp<br />едкий acrid<br />Fresh -свежий<br />Fragrant -ароматный<br />sweet -сладкий<br />Nice smell -приятный запах</p>\r\n<p>Here&rsquo;s more Airy, acrid, aromatic, astonishing, balmy, balsamic, beautiful, bubbly, celestial, cheap,</p>\r\n<p>clean, cool, delicate, delicious, delightful, dewy, divine, exotic, exquisite, faint, familiar, favorite, fine,<br />floral, fresh, green, gentle, great, graceful, heady, heavenly, heavy, holy, immortal, light, lovely, mild,<br />musky, ...<br />Воздушный, едкий, ароматный, удивительный, мягкий, бальзамический, красивый, игристый,<br />небесный, дешевый, чистый, прохладный, нежный, восхитительный, восхитительный, росистый,<br />божественный, экзотический, изысканный, слабый, знакомый, любимый, тонкий, цветочный,<br />свежий, зеленый, нежный, великий, изящный, пьянящий, небесный, тяжелый, святой,<br />бессмертный, легкий, прекрасный, мягкий, мускусный, ...</p>\r\n<p>&nbsp;</p>\r\n<p>Как сказать прекрасный<br />Lovely<br />Изящный -graceful<br />Выбери слова подходящие синонимы<br />Небесный -heavenly<br />divine -божественный<br />immortal -бессмертный</p>	\N	t	2021-12-14 18:52:34.860063+00	2021-12-15 05:53:57.4909+00	1	26
33	Much, many	Much, many	<p>Much - used with nouns we cannot count (water, money, smoke)</p>\r\n<p>.Many - used with count we can count (cars, sunglasses, people)</p>\r\n<p>We don`t say I have <span style="text-decoration: line-through;">much</span> mone this is not natural.</p>\r\n<p>Kyle would like to travel more, but he doesn`t have much mone</p>\r\n<p>Phillip owns many properties in France.<br />We didn't earn much profit this year.<br />How much money have you got?<br />Sharon does not have many friends.<br />There are too many students in this class.<br />It doesn't need much milk.<br />We had so much fun.<br />I spent many days there.</p>\r\n<p><img title="much_many.jpg" src="/media/article/12_13_2021_19_29_40.png" alt="" width="1500" height="1431" /></p>	\N	f	2021-12-13 17:58:23.189088+00	2021-12-13 19:40:13.945193+00	\N	23
38	irregular plurals	irregular plurals	<p>Regular nouns become plural by the writer adding -s or -es to the ending of the noun. Irregular plural nouns do not follow the same rules as regular nouns when becoming plural.</p>\r\n<p><img title="irregular_plurals.jpg" src="/media/article/12_15_2021_17_50_14.png" alt="" width="828" height="1067" /></p>	\N	t	2021-12-15 17:43:59.004845+00	2021-12-15 17:51:43.469545+00	1	27
35	Present simple	Present simple	<p>What is the simple present tense?<br />The simple present tense is one of several forms of present tense in English. It is used to describe<br />habits, unchanging situations, general truths, and fixed arrangements. The simple present tense is<br />simple to form.</p>\r\n<p><img title="present_simple.jpg" src="/media/article/12_13_2021_20_05_51.png" alt="" width="1021" height="1599" /></p>	\N	t	2021-12-13 19:55:04.735045+00	2021-12-14 18:50:01.039898+00	1	25
37	Action verbs	Action verbs	<p>ACTION verbs -is a verbs that describes an action, like study, ride, jump, kick, eat, build, cry, smile,<br />or think, talk sing etc<br />Examples in Sentences<br />Anthony run fast kicking the football.<br />Alex accepted the job offer, after he thought about it for a while<br />My friend Julie visited me today then she went to see her Mom<br />She left in a hurry.<br />My son loves to play music ,his favorite instrument is piano.</p>\r\n<p><img title="action_verbs.jpg" src="/media/article/12_14_2021_18_57_49.png" alt="" width="850" height="1232" /></p>\r\n<p>&nbsp;</p>\r\n<p><img title="list_of_action_verbs.jpg" src="/media/article/12_14_2021_18_58_25.png" alt="" width="850" height="1232" /></p>	\N	t	2021-12-14 18:53:58.359842+00	2021-12-14 18:58:34.253194+00	1	18
39	Irregular Verbs	Irregular Verbs	<table summary="This table charts irregular verb list">\r\n<tbody>\r\n<tr valign="top">\r\n<td>\r\n<table summary="This table charts irregular verb list">\r\n<tbody>\r\n<tr>\r\n<th scope="col" bgcolor="#000080" width="100"><strong>Verb</strong></th>\r\n<th scope="col" bgcolor="#000080" width="100"><strong>Past Tense</strong></th>\r\n<th scope="col" bgcolor="#000080" width="100"><strong>Past Participle</strong></th>\r\n</tr>\r\n<tr align="center">\r\n<td>arise</td>\r\n<td bgcolor="#ffffff">arose</td>\r\n<td bgcolor="#ffffff">arisen</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">awake</td>\r\n<td bgcolor="#ffffff">awoke</td>\r\n<td bgcolor="#ffffff">awoke</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">be</td>\r\n<td bgcolor="#ffffff">was, were</td>\r\n<td bgcolor="#ffffff">been</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">bear</td>\r\n<td bgcolor="#ffffff">bore</td>\r\n<td bgcolor="#ffffff">borne</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">beat</td>\r\n<td bgcolor="#ffffff">beat</td>\r\n<td bgcolor="#ffffff">beaten</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">become</td>\r\n<td bgcolor="#ffffff">became</td>\r\n<td bgcolor="#ffffff">become</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">begin</td>\r\n<td bgcolor="#ffffff">began</td>\r\n<td bgcolor="#ffffff">begun</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">bend</td>\r\n<td bgcolor="#ffffff">bent</td>\r\n<td bgcolor="#ffffff">bent</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">bet</td>\r\n<td bgcolor="#ffffff">bet</td>\r\n<td bgcolor="#ffffff">bet</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">bind</td>\r\n<td bgcolor="#ffffff">bound</td>\r\n<td bgcolor="#ffffff">bound</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">bite</td>\r\n<td bgcolor="#ffffff">bit</td>\r\n<td bgcolor="#ffffff">bitten</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">bleed</td>\r\n<td bgcolor="#ffffff">bled</td>\r\n<td bgcolor="#ffffff">bled</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">blow</td>\r\n<td bgcolor="#ffffff">blew</td>\r\n<td bgcolor="#ffffff">blown</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">break</td>\r\n<td bgcolor="#ffffff">broke</td>\r\n<td bgcolor="#ffffff">broken</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">bring</td>\r\n<td bgcolor="#ffffff">brought</td>\r\n<td bgcolor="#ffffff">brought</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">build</td>\r\n<td bgcolor="#ffffff">built</td>\r\n<td bgcolor="#ffffff">built</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">burst</td>\r\n<td bgcolor="#ffffff">burst</td>\r\n<td bgcolor="#ffffff">burst</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">buy</td>\r\n<td bgcolor="#ffffff">bought</td>\r\n<td bgcolor="#ffffff">bought</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">catch</td>\r\n<td bgcolor="#ffffff">caught</td>\r\n<td bgcolor="#ffffff">caught</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">choose</td>\r\n<td bgcolor="#ffffff">chose</td>\r\n<td bgcolor="#ffffff">chosen</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">come</td>\r\n<td bgcolor="#ffffff">came</td>\r\n<td bgcolor="#ffffff">come</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">cost</td>\r\n<td bgcolor="#ffffff">cost</td>\r\n<td bgcolor="#ffffff">cost</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">creep</td>\r\n<td bgcolor="#ffffff">crept</td>\r\n<td bgcolor="#ffffff">crept</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">cut</td>\r\n<td bgcolor="#ffffff">cut</td>\r\n<td bgcolor="#ffffff">cut</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">deal</td>\r\n<td bgcolor="#ffffff">dealt</td>\r\n<td bgcolor="#ffffff">dealt</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">dig</td>\r\n<td bgcolor="#ffffff">dug</td>\r\n<td bgcolor="#ffffff">dug</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">dive</td>\r\n<td bgcolor="#ffffff">dived, dove</td>\r\n<td bgcolor="#ffffff">dived</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">do</td>\r\n<td bgcolor="#ffffff">did</td>\r\n<td bgcolor="#ffffff">done</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">draw</td>\r\n<td bgcolor="#ffffff">drew</td>\r\n<td bgcolor="#ffffff">drawn</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">dream</td>\r\n<td bgcolor="#ffffff">dreamed, dreamt</td>\r\n<td bgcolor="#ffffff">dreamed, dreamt</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">drink</td>\r\n<td bgcolor="#ffffff">drank</td>\r\n<td bgcolor="#ffffff">drunk</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">drive</td>\r\n<td bgcolor="#ffffff">drove</td>\r\n<td bgcolor="#ffffff">driven</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">eat</td>\r\n<td bgcolor="#ffffff">ate</td>\r\n<td bgcolor="#ffffff">eaten</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">fall</td>\r\n<td bgcolor="#ffffff">fell</td>\r\n<td bgcolor="#ffffff">fallen</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">feed</td>\r\n<td bgcolor="#ffffff">fed</td>\r\n<td bgcolor="#ffffff">fed</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">feel</td>\r\n<td bgcolor="#ffffff">felt</td>\r\n<td bgcolor="#ffffff">felt</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</td>\r\n<td>\r\n<table summary="This table charts irregular verb list">\r\n<tbody>\r\n<tr>\r\n<th scope="col" bgcolor="#000080" width="100"><strong>Verb</strong></th>\r\n<th scope="col" bgcolor="#000080" width="100"><strong>Past Tense</strong></th>\r\n<th scope="col" bgcolor="#000080" width="100"><strong>Past Participle</strong></th>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">fight</td>\r\n<td bgcolor="#ffffff">fought</td>\r\n<td bgcolor="#ffffff">fought</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">find</td>\r\n<td bgcolor="#ffffff">found</td>\r\n<td bgcolor="#ffffff">found</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">fit</td>\r\n<td bgcolor="#ffffff">fitted, fit</td>\r\n<td bgcolor="#ffffff">fitted, fit</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">flee</td>\r\n<td bgcolor="#ffffff">fled</td>\r\n<td bgcolor="#ffffff">fled</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">fling</td>\r\n<td bgcolor="#ffffff">flung</td>\r\n<td bgcolor="#ffffff">flung</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">fly</td>\r\n<td bgcolor="#ffffff">flew</td>\r\n<td bgcolor="#ffffff">flown</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">forbid</td>\r\n<td bgcolor="#ffffff">forbade</td>\r\n<td bgcolor="#ffffff">forbidden</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">forget</td>\r\n<td bgcolor="#ffffff">forgot</td>\r\n<td bgcolor="#ffffff">forgotten, forgot</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">forgive</td>\r\n<td bgcolor="#ffffff">forgave</td>\r\n<td bgcolor="#ffffff">forgiven</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">freeze</td>\r\n<td bgcolor="#ffffff">froze</td>\r\n<td bgcolor="#ffffff">frozen</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">get</td>\r\n<td bgcolor="#ffffff">got</td>\r\n<td bgcolor="#ffffff">got, gotten</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">give</td>\r\n<td bgcolor="#ffffff">gave</td>\r\n<td bgcolor="#ffffff">given</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">go</td>\r\n<td bgcolor="#ffffff">went</td>\r\n<td bgcolor="#ffffff">gone</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">grow</td>\r\n<td bgcolor="#ffffff">grew</td>\r\n<td bgcolor="#ffffff">grown</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">hang (to put to death)</td>\r\n<td bgcolor="#ffffff">hanged</td>\r\n<td bgcolor="#ffffff">hanged</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">hang (a picture)</td>\r\n<td bgcolor="#ffffff">hung</td>\r\n<td bgcolor="#ffffff">hung</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">have</td>\r\n<td bgcolor="#ffffff">had</td>\r\n<td bgcolor="#ffffff">had</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">hear</td>\r\n<td bgcolor="#ffffff">heard</td>\r\n<td bgcolor="#ffffff">heard</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">hide</td>\r\n<td bgcolor="#ffffff">hid</td>\r\n<td bgcolor="#ffffff">hidden</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">hit</td>\r\n<td bgcolor="#ffffff">hit</td>\r\n<td bgcolor="#ffffff">hit</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">hold</td>\r\n<td bgcolor="#ffffff">held</td>\r\n<td bgcolor="#ffffff">held</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">hurt</td>\r\n<td bgcolor="#ffffff">hurt</td>\r\n<td bgcolor="#ffffff">hurt</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">keep</td>\r\n<td bgcolor="#ffffff">kept</td>\r\n<td bgcolor="#ffffff">kept</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">kneel</td>\r\n<td bgcolor="#ffffff">kneeled, knelt</td>\r\n<td bgcolor="#ffffff">kneeled, knelt</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">knit</td>\r\n<td bgcolor="#ffffff">knitted</td>\r\n<td bgcolor="#ffffff">knitted</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">know</td>\r\n<td bgcolor="#ffffff">knew</td>\r\n<td bgcolor="#ffffff">known</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">lay</td>\r\n<td bgcolor="#ffffff">laid</td>\r\n<td bgcolor="#ffffff">laid</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">lead</td>\r\n<td bgcolor="#ffffff">led</td>\r\n<td bgcolor="#ffffff">led</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">leave</td>\r\n<td bgcolor="#ffffff">left</td>\r\n<td bgcolor="#ffffff">left</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">lend</td>\r\n<td bgcolor="#ffffff">lent</td>\r\n<td bgcolor="#ffffff">lent</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">let</td>\r\n<td bgcolor="#ffffff">let</td>\r\n<td bgcolor="#ffffff">let</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">lie (recline)</td>\r\n<td bgcolor="#ffffff">lay</td>\r\n<td bgcolor="#ffffff">lain</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">lie (tell a falsehood)</td>\r\n<td bgcolor="#ffffff">lied</td>\r\n<td bgcolor="#ffffff">lied</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">light</td>\r\n<td bgcolor="#ffffff">lighted, lit</td>\r\n<td bgcolor="#ffffff">lighted, lit</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">lose</td>\r\n<td bgcolor="#ffffff">lost</td>\r\n<td bgcolor="#ffffff">lost</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p>&nbsp;</p>\r\n<table summary="This table charts irregular verb list.">\r\n<tbody>\r\n<tr valign="top">\r\n<td width="50%">\r\n<table summary="This table charts irregular verb list.">\r\n<tbody>\r\n<tr>\r\n<th scope="col" bgcolor="#000080" width="100"><strong>Verb</strong></th>\r\n<th scope="col" bgcolor="#000080" width="100"><strong>Past Tense</strong></th>\r\n<th scope="col" bgcolor="#000080" width="100"><strong>Past Participle</strong></th>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">make</td>\r\n<td bgcolor="#ffffff">made</td>\r\n<td bgcolor="#ffffff">made</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">mean</td>\r\n<td bgcolor="#ffffff">meant</td>\r\n<td bgcolor="#ffffff">meant</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">meet</td>\r\n<td bgcolor="#ffffff">met</td>\r\n<td bgcolor="#ffffff">met</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">pay</td>\r\n<td bgcolor="#ffffff">paid</td>\r\n<td bgcolor="#ffffff">paid</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">plead</td>\r\n<td bgcolor="#ffffff">pleaded</td>\r\n<td bgcolor="#ffffff">pled</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">prove</td>\r\n<td bgcolor="#ffffff">proved</td>\r\n<td bgcolor="#ffffff">proved, proven</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">put</td>\r\n<td bgcolor="#ffffff">put</td>\r\n<td bgcolor="#ffffff">put</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">quit</td>\r\n<td bgcolor="#ffffff">quit</td>\r\n<td bgcolor="#ffffff">quit</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">read</td>\r\n<td bgcolor="#ffffff">read</td>\r\n<td bgcolor="#ffffff">read</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">ride</td>\r\n<td bgcolor="#ffffff">rode</td>\r\n<td bgcolor="#ffffff">ridden</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">ring</td>\r\n<td bgcolor="#ffffff">rang</td>\r\n<td bgcolor="#ffffff">rung</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">rise</td>\r\n<td bgcolor="#ffffff">rose</td>\r\n<td bgcolor="#ffffff">risen</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">run</td>\r\n<td bgcolor="#ffffff">ran</td>\r\n<td bgcolor="#ffffff">run</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">say</td>\r\n<td bgcolor="#ffffff">said</td>\r\n<td bgcolor="#ffffff">said</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">see</td>\r\n<td bgcolor="#ffffff">saw</td>\r\n<td bgcolor="#ffffff">seen</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">seek</td>\r\n<td bgcolor="#ffffff">sought</td>\r\n<td bgcolor="#ffffff">sought</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">sell</td>\r\n<td bgcolor="#ffffff">sold</td>\r\n<td bgcolor="#ffffff">sold</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">send</td>\r\n<td bgcolor="#ffffff">sent</td>\r\n<td bgcolor="#ffffff">sent</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">set</td>\r\n<td bgcolor="#ffffff">set</td>\r\n<td bgcolor="#ffffff">set</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">sew</td>\r\n<td bgcolor="#ffffff">sewed</td>\r\n<td bgcolor="#ffffff">sewed, sewn</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">shake</td>\r\n<td bgcolor="#ffffff">shook</td>\r\n<td bgcolor="#ffffff">shaken</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">shine (glow)</td>\r\n<td bgcolor="#ffffff">shone</td>\r\n<td bgcolor="#ffffff">shone</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">shine (rub)</td>\r\n<td bgcolor="#ffffff">shined</td>\r\n<td bgcolor="#ffffff">shined</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">shoot</td>\r\n<td bgcolor="#ffffff">shot</td>\r\n<td bgcolor="#ffffff">shot</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">show</td>\r\n<td bgcolor="#ffffff">showed</td>\r\n<td bgcolor="#ffffff">showed, shown</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">shrink</td>\r\n<td bgcolor="#ffffff">shrank</td>\r\n<td bgcolor="#ffffff">shrunk</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">shut</td>\r\n<td bgcolor="#ffffff">shut</td>\r\n<td bgcolor="#ffffff">shut</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">sing</td>\r\n<td bgcolor="#ffffff">sang</td>\r\n<td bgcolor="#ffffff">sung</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">sink</td>\r\n<td bgcolor="#ffffff">sank</td>\r\n<td bgcolor="#ffffff">sunk</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">sit</td>\r\n<td bgcolor="#ffffff">sat</td>\r\n<td bgcolor="#ffffff">sat</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">slay</td>\r\n<td bgcolor="#ffffff">slew</td>\r\n<td bgcolor="#ffffff">slain</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">sleep</td>\r\n<td bgcolor="#ffffff">slept</td>\r\n<td bgcolor="#ffffff">slept</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">slide</td>\r\n<td bgcolor="#ffffff">slid</td>\r\n<td bgcolor="#ffffff">slid</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">slit</td>\r\n<td bgcolor="#ffffff">slit</td>\r\n<td bgcolor="#ffffff">slit</td>\r\n</tr>\r\n<tr>\r\n<td align="center" bgcolor="#ffffff">speak</td>\r\n<td align="center" bgcolor="#ffffff">spoke</td>\r\n<td align="center" bgcolor="#ffffff">spoken</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</td>\r\n<td width="50%">\r\n<table summary="This table charts irregular verb list.">\r\n<tbody>\r\n<tr>\r\n<th scope="col" bgcolor="#000080" width="100"><strong>Verb</strong></th>\r\n<th scope="col" bgcolor="#000080" width="100"><strong>Past Tense</strong></th>\r\n<th scope="col" bgcolor="#000080" width="100"><strong>Past Participle</strong></th>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">spend</td>\r\n<td bgcolor="#ffffff">spent</td>\r\n<td bgcolor="#ffffff">spent</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">spin</td>\r\n<td bgcolor="#ffffff">spun</td>\r\n<td bgcolor="#ffffff">spun</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">spit</td>\r\n<td bgcolor="#ffffff">spat, spit</td>\r\n<td bgcolor="#ffffff">spat, spit</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">split</td>\r\n<td bgcolor="#ffffff">split</td>\r\n<td bgcolor="#ffffff">split</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">spread</td>\r\n<td bgcolor="#ffffff">spread</td>\r\n<td bgcolor="#ffffff">spread</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">spring</td>\r\n<td bgcolor="#ffffff">sprang</td>\r\n<td bgcolor="#ffffff">sprung</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">stand</td>\r\n<td bgcolor="#ffffff">stood</td>\r\n<td bgcolor="#ffffff">stood</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">steal</td>\r\n<td bgcolor="#ffffff">stole</td>\r\n<td bgcolor="#ffffff">stolen</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">stick</td>\r\n<td bgcolor="#ffffff">stuck</td>\r\n<td bgcolor="#ffffff">stuck</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">sting</td>\r\n<td bgcolor="#ffffff">stung</td>\r\n<td bgcolor="#ffffff">stung</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">stink</td>\r\n<td bgcolor="#ffffff">stank</td>\r\n<td bgcolor="#ffffff">stunk</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">stride</td>\r\n<td bgcolor="#ffffff">strode</td>\r\n<td bgcolor="#ffffff">stridden</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">strike (hit)</td>\r\n<td bgcolor="#ffffff">struck</td>\r\n<td bgcolor="#ffffff">struck</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">string</td>\r\n<td bgcolor="#ffffff">strung</td>\r\n<td bgcolor="#ffffff">strung</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">swear</td>\r\n<td bgcolor="#ffffff">swore</td>\r\n<td bgcolor="#ffffff">sworn</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">sweep</td>\r\n<td bgcolor="#ffffff">swept</td>\r\n<td bgcolor="#ffffff">swept</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">swell</td>\r\n<td bgcolor="#ffffff">swelled</td>\r\n<td bgcolor="#ffffff">swollen</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">swim</td>\r\n<td bgcolor="#ffffff">swam</td>\r\n<td bgcolor="#ffffff">swum</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">swing</td>\r\n<td bgcolor="#ffffff">swing</td>\r\n<td bgcolor="#ffffff">swung</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">take</td>\r\n<td bgcolor="#ffffff">took</td>\r\n<td bgcolor="#ffffff">taken</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">teach</td>\r\n<td bgcolor="#ffffff">taught</td>\r\n<td bgcolor="#ffffff">taught</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">tear</td>\r\n<td bgcolor="#ffffff">tore</td>\r\n<td bgcolor="#ffffff">torn</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">tell</td>\r\n<td bgcolor="#ffffff">told</td>\r\n<td bgcolor="#ffffff">told</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">think</td>\r\n<td bgcolor="#ffffff">thought</td>\r\n<td bgcolor="#ffffff">thought</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">throw</td>\r\n<td bgcolor="#ffffff">threw</td>\r\n<td bgcolor="#ffffff">thrown</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">understand</td>\r\n<td bgcolor="#ffffff">understood</td>\r\n<td bgcolor="#ffffff">understood</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">wake up</td>\r\n<td bgcolor="#ffffff">woke up</td>\r\n<td bgcolor="#ffffff">waked up, woken up</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">wear</td>\r\n<td bgcolor="#ffffff">wore</td>\r\n<td bgcolor="#ffffff">worn</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">wed</td>\r\n<td bgcolor="#ffffff">wed</td>\r\n<td bgcolor="#ffffff">wed</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">weep</td>\r\n<td bgcolor="#ffffff">wept</td>\r\n<td bgcolor="#ffffff">wept</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">welcome</td>\r\n<td bgcolor="#ffffff">welcomed</td>\r\n<td bgcolor="#ffffff">welcomed</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">wet</td>\r\n<td bgcolor="#ffffff">wet</td>\r\n<td bgcolor="#ffffff">wet</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">win</td>\r\n<td bgcolor="#ffffff">won</td>\r\n<td bgcolor="#ffffff">won</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">wring</td>\r\n<td bgcolor="#ffffff">wrung</td>\r\n<td bgcolor="#ffffff">wrung</td>\r\n</tr>\r\n<tr align="center">\r\n<td bgcolor="#ffffff">write</td>\r\n<td bgcolor="#ffffff">wrote</td>\r\n<td bgcolor="#ffffff">written</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>	\N	t	2021-12-15 17:53:31.077032+00	2021-12-15 17:53:31.077062+00	1	28
40	Relative pronauns	Relative pronauns	<p><img title="relative pronouns.jpg" src="/media/article/12_17_2021_20_13_34.png" alt="" width="1574" height="2224" /></p>	\N	t	2021-12-15 22:24:08.955298+00	2021-12-17 20:14:03.031532+00	1	29
41	Opposite verbs	Opposite verbs	<p><img title="adverbs of place.jpg" src="/media/article/12_17_2021_20_18_47.png" alt="" width="1456" height="2111" /></p>	\N	t	2021-12-17 20:17:42.668607+00	2021-12-17 20:20:01.673025+00	1	30
42	Prepopsition At, In, One	Prepopsition At, In, One	<p><img title="in_at_on.jpg" src="/media/article/12_17_2021_20_24_24.png" alt="" width="1280" height="720" /></p>	Prepopsition At, In, One	t	2021-12-17 20:24:27.486785+00	2021-12-17 20:26:22.742451+00	1	31
44	Prepositions of time	Prepositions of time	<p>Prepositions of Time - at, in, on (EnglishClub.com/search)<br />We use:<br />at for a PRECISE TIME<br />in for MONTHS, YEARS, CENTURIES and LONG PERIODS<br />on for DAYS and DATES</p>\r\n<table class="ec-table">\r\n<tbody>\r\n<tr>\r\n<th>at<br />PRECISE TIME</th>\r\n<th>in<br />MONTHS, YEARS, CENTURIES and LONG PERIODS</th>\r\n<th>on<br />DAYS and DATES</th>\r\n</tr>\r\n<tr>\r\n<td>at 3 o'clock</td>\r\n<td>in May</td>\r\n<td>on Sunday</td>\r\n</tr>\r\n<tr>\r\n<td>at 10.30am</td>\r\n<td>in summer</td>\r\n<td>on Tuesdays</td>\r\n</tr>\r\n<tr>\r\n<td>at noon</td>\r\n<td>in the summer</td>\r\n<td>on 6 March</td>\r\n</tr>\r\n<tr>\r\n<td>at dinnertime</td>\r\n<td>in 1990</td>\r\n<td>on 25 Dec. 2010</td>\r\n</tr>\r\n<tr>\r\n<td>at bedtime</td>\r\n<td>in the 1990s</td>\r\n<td>on Christmas Day</td>\r\n</tr>\r\n<tr>\r\n<td>at sunrise</td>\r\n<td>in the next century</td>\r\n<td>on Independence Day</td>\r\n</tr>\r\n<tr>\r\n<td>at sunset</td>\r\n<td>in the Ice Age</td>\r\n<td>on my birthday</td>\r\n</tr>\r\n<tr>\r\n<td>at the moment</td>\r\n<td>in the past/future</td>\r\n<td>on New Year's Eve</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p><br />Look at these examples:<br />I have a meeting at 9am.<br />The shop closes at midnight.<br />Jane went home at lunchtime.<br />In England, it often snows in December.<br />Do you think we will go to Jupiter in the future?<br />There should be a lot of progress in the next century.<br />Do you work on Mondays?<br />Her birthday is on 20 November.<br />Where will you be on New Year's Day?<br />https://www.englishclub.com/grammar/prepositions-at-in-on-time.htm<br />1/318.12.2021, 17:23<br />Prepositions of Time - at, in, on | Grammar | EnglishClub<br />Notice the use of the preposition of time at in the following standard expressions:</p>\r\n<table class="ec-table">\r\n<tbody>\r\n<tr>\r\n<th>Expression</th>\r\n<th>Example</th>\r\n</tr>\r\n<tr>\r\n<td>at night</td>\r\n<td>The stars shine&nbsp;<strong>at night</strong>.</td>\r\n</tr>\r\n<tr>\r\n<td>at the weekend*</td>\r\n<td>I don't usually work&nbsp;<strong>at the weekend</strong>.</td>\r\n</tr>\r\n<tr>\r\n<td>at Christmas*/Easter</td>\r\n<td>I stay with my family&nbsp;<strong>at Christmas</strong>.</td>\r\n</tr>\r\n<tr>\r\n<td>at the same time</td>\r\n<td>We finished the test&nbsp;<strong>at the same time</strong>.</td>\r\n</tr>\r\n<tr>\r\n<td>at present</td>\r\n<td>He's not home&nbsp;<strong>at present</strong>. Try later.</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p><br />*Note that in some varieties of English people say "on the weekend" and "on Christmas".<br />Notice the use of the prepositions of time in and on in these common expressions:</p>\r\n<table class="ec-table">\r\n<tbody>\r\n<tr>\r\n<td><strong>in</strong></td>\r\n<td><strong>on</strong></td>\r\n</tr>\r\n<tr>\r\n<td>in the morning</td>\r\n<td>on Tuesday morning</td>\r\n</tr>\r\n<tr>\r\n<td>in the mornings</td>\r\n<td>on Saturday mornings</td>\r\n</tr>\r\n<tr>\r\n<td>in the afternoon(s)</td>\r\n<td>on Sunday afternoon(s)</td>\r\n</tr>\r\n<tr>\r\n<td>in the evening(s)</td>\r\n<td>on Monday evening(s)</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p><br />When we say last, next, every, this we do not also use at, in, on.<br />I went to London last June. (not in last June)<br />He's coming back next Tuesday. (not on next Tuesday)<br />I go home every Easter. (not at every Easter)<br />We'll call you this evening. (not in this evening)<br />Mini Quiz<br />Test your understanding with this quick quiz.<br />1. Choose the correct prepositions: "Let's meet _______ midday _______ Saturday."<br />a) at, at b) in, on c) at, on<br />2. Choose the correct prepositions: "The manager isn't here _______ present, but she'll<br />be back _______ half an hour."<br />https://www.englishclub.com/grammar/prepositions-at-in-on-time.htm<br />2/318.12.2021, 17:23<br />Prepositions of Time - at, in, on | Grammar | EnglishClub<br />a) at, in b) at, at c) in, in<br />3. Which are correct? "You won't be working _______ Saturday nights _______ the<br />future, will you?"<br />a) at, in b) on, in c) on, at<br />4. Which are correct? "I'm busy _______ moment, but I'll be free ______ evening."<br />a) at the, in this b) in the, at this c) at the, this</p>	\N	t	2021-12-18 13:55:16.32156+00	2021-12-18 14:28:19.645149+00	1	33
48	Uncountable nouns	Uncountable nouns	<h1>Uncountable Nouns</h1>\r\n<p>Unlike&nbsp;<a href="https://www.englishclub.com/grammar/nouns-countable.htm">countable</a>&nbsp;nouns,&nbsp;<strong>uncountable nouns</strong>&nbsp;are substances, concepts etc that we cannot divide into separate elements. We cannot "count" them. For example, we cannot count "milk". We can count "bottles of milk" or "litres of milk", but we cannot count "milk" itself. Here are some more uncountable nouns:</p>\r\n<ul>\r\n<li>music, art, love, happiness</li>\r\n<li>advice, information, news</li>\r\n<li>furniture, luggage</li>\r\n<li>rice, sugar, butter, water</li>\r\n<li>electricity, gas, power</li>\r\n<li>money, currency</li>\r\n</ul>\r\n<p>We usually treat uncountable nouns as singular. We use a singular verb. For example:</p>\r\n<ul>\r\n<li><strong>This</strong>&nbsp;news&nbsp;<strong>is</strong>&nbsp;very important.</li>\r\n<li>Your luggage&nbsp;<strong>looks</strong>&nbsp;heavy.</li>\r\n</ul>\r\n<p>We do not usually use the indefinite article&nbsp;<strong>a/an</strong>&nbsp;with uncountable nouns. We cannot say "an information" or "a music". But we can say&nbsp;<strong>a "something" of</strong>:</p>\r\n<ul>\r\n<li><strong>a piece of</strong>&nbsp;news</li>\r\n<li><strong>a bottle of</strong>&nbsp;water</li>\r\n<li><strong>a grain of</strong>&nbsp;rice</li>\r\n</ul>\r\n<p>We can use&nbsp;<strong>some</strong>&nbsp;and&nbsp;<strong>any</strong>&nbsp;with uncountable nouns:</p>\r\n<ul>\r\n<li>I've got&nbsp;<strong>some</strong>&nbsp;money.</li>\r\n<li>Have you got&nbsp;<strong>any</strong>&nbsp;rice?</li>\r\n</ul>\r\n<p>We can use&nbsp;<strong>a little</strong>&nbsp;and&nbsp;<strong>much</strong>&nbsp;with uncountable nouns:</p>\r\n<ul>\r\n<li>I've got&nbsp;<strong>a little</strong>&nbsp;money.</li>\r\n<li>I haven't got&nbsp;<strong>much</strong>&nbsp;rice.</li>\r\n</ul>\r\n<div class="ECtip">Uncountable nouns are also called "mass nouns".</div>\r\n<p>Here are some more examples of countable and uncountable nouns:</p>\r\n<table class="ec-table">\r\n<thead>\r\n<tr>\r\n<th>Countable</th>\r\n<th>Uncountable</th>\r\n</tr>\r\n</thead>\r\n<tbody>\r\n<tr>\r\n<td>dollar</td>\r\n<td>money</td>\r\n</tr>\r\n<tr>\r\n<td>song</td>\r\n<td>music</td>\r\n</tr>\r\n<tr>\r\n<td>suitcase</td>\r\n<td>luggage</td>\r\n</tr>\r\n<tr>\r\n<td>table</td>\r\n<td>furniture</td>\r\n</tr>\r\n<tr>\r\n<td>battery</td>\r\n<td>electricity</td>\r\n</tr>\r\n<tr>\r\n<td>bottle</td>\r\n<td>wine</td>\r\n</tr>\r\n<tr>\r\n<td>report</td>\r\n<td>information</td>\r\n</tr>\r\n<tr>\r\n<td>tip</td>\r\n<td>advice</td>\r\n</tr>\r\n<tr>\r\n<td>journey</td>\r\n<td>travel</td>\r\n</tr>\r\n<tr>\r\n<td>job</td>\r\n<td>work</td>\r\n</tr>\r\n<tr>\r\n<td>view</td>\r\n<td>scenery</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p>When you learn a new word, it's a good idea to learn whether it's countable or uncountable.</p>\r\n<h2>Partitive Structure with Uncountable Nouns</h2>\r\n<p>To count or quantify an uncountable noun we use a unit of measurement - a&nbsp;<strong>measure word</strong>. For example, we cannot usually say &ldquo;two breads&rdquo; because &ldquo;bread&rdquo; is uncountable. So, if we want to specify a quantity of bread we use a measure word such as &ldquo;loaf&rdquo; or &ldquo;slice&rdquo; in a structure like &ldquo;two loaves of bread&rdquo; or &ldquo;two slices of bread&rdquo;. We call this structure a&nbsp;<strong>partitive structure</strong>.</p>\r\n<div class="ec-table-wrapper">\r\n<table class="ec-table">\r\n<tbody>\r\n<tr>\r\n<th>partitive structure:</th>\r\n<td>quantity</td>\r\n<td>measure word</td>\r\n<td><em>of</em></td>\r\n<td>uncountable noun</td>\r\n</tr>\r\n<tr>\r\n<th rowspan="3">examples:</th>\r\n<td>two</td>\r\n<td>cups</td>\r\n<td>of</td>\r\n<td>coffee</td>\r\n</tr>\r\n<tr>\r\n<td>several</td>\r\n<td>games</td>\r\n<td>of</td>\r\n<td>tennis</td>\r\n</tr>\r\n<tr>\r\n<td>a</td>\r\n<td>drop</td>\r\n<td>of</td>\r\n<td>water</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</div>\r\n<p>We can use the same uncountable noun in different partitive expressions with different meanings. For example,&nbsp;<strong>a loaf of bread</strong>&nbsp;and&nbsp;<strong>a slice of bread</strong>&nbsp;are partitive expressions with different meanings. A&nbsp;<strong>loaf</strong>&nbsp;of bread is what we call a whole unit of bread that we buy from a baker. A&nbsp;<strong>slice</strong>&nbsp;of bread is what we call a smaller unit of bread&nbsp;<em>after</em>&nbsp;it has been cut from a loaf.&nbsp;</p>\r\n<p>Here are some more examples:</p>\r\n<ul>\r\n<li>Don't forget to buy&nbsp;<strong>a bag of rice</strong>&nbsp;when you go shopping.</li>\r\n<li>Can I have&nbsp;<strong>one cup of coffee</strong>&nbsp;and&nbsp;<strong>two cups of tea</strong>.</li>\r\n<li>The police found&nbsp;<strong>some items of clothing</strong>&nbsp;scattered around the floor.</li>\r\n<li>I need a truck that will take at least&nbsp;<strong>three pieces of furniture</strong>.</li>\r\n<li>You'd think&nbsp;<strong>a tablespoon of honey</strong>&nbsp;would be more than enough.</li>\r\n</ul>\r\n<div class="ECtip">The word "partitive" indicates that only "part" of a whole is being referred to. The partitive structure using a measure word is common with uncountable nouns, but it can also be used with&nbsp;<em>countable nouns</em>, for example: a series of&nbsp;<em>accidents</em>, two boxes of&nbsp;<em>matches</em>, a can of&nbsp;<em>worms.</em></div>\r\n<ul>\r\n<li><a href="https://www.englishclub.com/vocabulary/nouns-uncountable-measure-words.htm">List of common measure words</a></li>\r\n<li><a href="https://www.englishclub.com/vocabulary/nouns-uncountable-partitive-list.htm">List of partitive expressions with example sentences</a></li>\r\n</ul>\r\n<div class="ECpractice">Test yourself with these fun&nbsp;<a href="https://www.englishclub.com/esl-quizzes/vocabulary/measure-words-quiz-1.htm">measure words quizzes</a></div>\r\n<p>&nbsp;</p>\r\n<h2>Nouns that can be Countable and Uncountable</h2>\r\n<p>Sometimes, the same noun can be countable&nbsp;<em>and</em>&nbsp;uncountable, often with a change of meaning.</p>\r\n<div class="ec-table-wrapper">\r\n<table class="ec-table">\r\n<thead>\r\n<tr>\r\n<th>Countable</th>\r\n<th>&nbsp;</th>\r\n<th>Uncountable</th>\r\n</tr>\r\n</thead>\r\n<tbody>\r\n<tr>\r\n<td>The US dollar and pound sterling are important currencies.</td>\r\n<td><strong>currency</strong></td>\r\n<td>The expression gained wider currency after 2001.</td>\r\n</tr>\r\n<tr>\r\n<td>There are two hairs in my coffee!</td>\r\n<td><strong>hair</strong></td>\r\n<td>I don't have much hair.</td>\r\n</tr>\r\n<tr>\r\n<td>There are two lights in our bedroom.</td>\r\n<td><strong>light</strong></td>\r\n<td>Close the curtain. There's too much light!</td>\r\n</tr>\r\n<tr>\r\n<td>Shhhhh! I thought I heard a noise.<br />There are so many different noises in the city.</td>\r\n<td><strong>noise</strong></td>\r\n<td>It's difficult to work when there is so much noise.</td>\r\n</tr>\r\n<tr>\r\n<td>Have you got a paper to read? (newspaper)<br />Hand me those student papers.</td>\r\n<td><strong>paper</strong></td>\r\n<td>I want to draw a picture. Have you got some paper?</td>\r\n</tr>\r\n<tr>\r\n<td>Our house has seven rooms.</td>\r\n<td><strong>room</strong></td>\r\n<td>Is there room for me to sit here?</td>\r\n</tr>\r\n<tr>\r\n<td>We had a great time at the party.<br />How many times have I told you no?</td>\r\n<td><strong>time</strong></td>\r\n<td>Have you got time for a cup of coffee?</td>\r\n</tr>\r\n<tr>\r\n<td><em>Macbeth</em>&nbsp;is one of Shakespeare's greatest works.</td>\r\n<td><strong>work</strong></td>\r\n<td>I have no money. I need work!</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</div>	\N	t	2021-12-20 13:23:57.191824+00	2021-12-20 13:25:39.950843+00	1	37
47	Linking verbs	Linking verbs	<p>Linking verbs are verbs that serve as a connection between a subject and further information about that subject. They do not show any action; rather, they &ldquo;link&rdquo; the subject with the rest of the sentence. The verb&nbsp;<em>to be</em>&nbsp;is the most common linking verb, but there are many others, including all the sense verbs.</p>\r\n<p>A handful&mdash;a very frequently used handful&mdash;of verbs are&nbsp;<mark class="tool__keyword">always</mark>&nbsp;linking verbs:</p>\r\n<ul>\r\n<li>all forms of to be (am, is, are, was, were, has been, are being, might be, etc.)</li>\r\n<li>to become</li>\r\n<li>to seem</li>\r\n</ul>\r\n<p>These verbs always link subjects to something that further describes the subject of the sentence.</p>\r\n<p>&nbsp;</p>\r\n<div class="tool__example ">\r\n<div class="tool__example-content">She&nbsp;<mark class="tool__keyword">is</mark>&nbsp;a nurse.</div>\r\n</div>\r\n<div class="tool__example ">\r\n<div class="tool__example-content">The moon&nbsp;<mark class="tool__keyword">is</mark>&nbsp;in outer space.</div>\r\n</div>\r\n<div class="tool__example ">\r\n<div class="tool__example-content">I&nbsp;<mark class="tool__keyword">have become</mark>&nbsp;weary of your methodical approach to waltzing.</div>\r\n</div>\r\n<div class="tool__example ">\r\n<div class="tool__example-content">The Dalai Lama&nbsp;<mark class="tool__keyword">seems</mark>&nbsp;like a nice guy.</div>\r\n</div>\r\n<p>&nbsp;</p>\r\n<p>These sentences show that a linking verb can connect the subject with a number of sentence elements.&nbsp;<em>Nurse</em>&nbsp;is a noun;&nbsp;<em>in outer space</em>&nbsp;is a prepositional phrase;&nbsp;<em>weary</em>&nbsp;is an adjective; and&nbsp;<em>a nice guy</em>&nbsp;is a phrase that contains both an adjective and a noun. All of them give us more information about what these subjects are, have become, or seem to be.</p>\r\n<h2>Some Verbs Can Be Both Action and Linking Verbs</h2>\r\n<p>Alas, English has many ambiguities, and some linking verbs can also function as action verbs. These include all the sense verbs, such as&nbsp;<em>look, touch, smell, appear, feel, sound,</em>&nbsp;and&nbsp;<em>taste</em>. There are also some outliers, such as&nbsp;<em>turn, grow, remain,</em>&nbsp;and&nbsp;<em>prove</em>. Used as linking verbs, these verbs can give added information about the sentence&rsquo;s subject.</p>\r\n<p>&nbsp;</p>\r\n<div class="tool__example ">\r\n<div class="tool__example-content">The ocean&nbsp;<mark class="tool__keyword">looked</mark>&nbsp;peaceful that fine Tuesday.</div>\r\n</div>\r\n<div class="tool__example ">\r\n<div class="tool__example-content">I&nbsp;<mark class="tool__keyword">felt</mark>&nbsp;so excited that day.</div>\r\n</div>\r\n<div class="tool__example ">\r\n<div class="tool__example-content">That man&nbsp;<mark class="tool__keyword">appears</mark>&nbsp;somewhat melancholy.</div>\r\n</div>\r\n<div class="tool__example ">\r\n<div class="tool__example-content">The soup&nbsp;<mark class="tool__keyword">tastes</mark>&nbsp;spicier than usual.</div>\r\n</div>\r\n<div class="tool__example ">\r\n<div class="tool__example-content">Rachel&rsquo;s theory about time management&nbsp;<mark class="tool__keyword">remains</mark>&nbsp;untested.</div>\r\n</div>\r\n<p>&nbsp;</p>\r\n<p>All these verbs can do double duty, however, as action verbs.</p>\r\n<p>&nbsp;</p>\r\n<div class="tool__example ">\r\n<div class="tool__example-content">I&nbsp;<mark class="tool__keyword">felt</mark>&nbsp;on the floor for my lost keys.</div>\r\n</div>\r\n<div class="tool__example ">\r\n<div class="tool__example-content">The man&nbsp;<mark class="tool__keyword">appeared</mark>&nbsp;suddenly right in front of me.</div>\r\n</div>\r\n<div class="tool__example ">\r\n<div class="tool__example-content">Would you&nbsp;<mark class="tool__keyword">taste</mark>&nbsp;that soup for me?</div>\r\n</div>\r\n<div class="tool__example ">\r\n<div class="tool__example-content"><mark class="tool__keyword">Remain</mark>&nbsp;here while I go ask Rachel about time management.</div>\r\n</div>	\N	t	2021-12-20 08:10:59.573261+00	2021-12-20 13:21:55.074391+00	1	36
2	Second conditional is used in	Second conditional is used in	<p>Second conditional is used in situations/actions in the present or future which are not likely to happen or are imaginary, hypothetical or impossible. If I won the lottery, I would travel around the world and buy a castle.</p>\r\n<p><iframe src="https://www.youtube.com/embed/3OuqzHxlrHc" width="560" height="314" allowfullscreen="allowfullscreen"></iframe></p>	\N	t	2021-11-21 06:52:24.981816+00	2023-01-21 12:31:22.056147+00	1	4
49	Future simple	Future simple	<p>The simple future is a verb tense that&rsquo;s used to talk about things that haven&rsquo;t happened yet.</p>\r\n<p>&nbsp;</p>\r\n<div class="tool__example ">\r\n<div class="tool__example-content">This year, Jen&nbsp;<strong>will read</strong>&nbsp;<em>War and Peace</em>. It&nbsp;<strong>will be</strong>&nbsp;hard, but she&rsquo;s determined to do it.</div>\r\n</div>\r\n<p>&nbsp;</p>\r\n<p>Use the simple future to talk about an action or condition that will begin and end in the future.</p>\r\n<div>\r\n<article class="fv-3Z-article">\r\n<div id="post-content" class="ZIGa1-content">\r\n<h2>How to Form the Simple Future</h2>\r\n<p>The formula for the simple future is&nbsp;<strong>will + [root form of verb]</strong>.</p>\r\n<p>&nbsp;</p>\r\n<div class="tool__example ">\r\n<div class="tool__example-content">I&nbsp;<strong>will learn</strong>&nbsp;a new language. Jen&nbsp;<strong>will read</strong>&nbsp;that book. My brothers&nbsp;<strong>will sleep</strong>&nbsp;till noon if no one wakes them up. You&nbsp;<strong>will see</strong>&nbsp;what I mean.</div>\r\n</div>\r\n<p>&nbsp;</p>\r\n<p>It doesn&rsquo;t matter if the subject is singular or plural; the formula for the simple future doesn&rsquo;t change.</p>\r\n<p>But&hellip;</p>\r\n<p>There is another way to show that something will happen in the future. It follows the formula&nbsp;<strong>[am/is/are] + going to + [root form verb]</strong>.</p>\r\n<p>&nbsp;</p>\r\n<div class="tool__example ">\r\n<div class="tool__example-content">I&nbsp;<strong>am going to learn</strong>&nbsp;a new language. Jen&nbsp;<strong>is going to read</strong>&nbsp;that book. My brothers&nbsp;<strong>are going to sleep</strong>&nbsp;till noon if no one wakes them up. You&nbsp;<strong>are going to see</strong>&nbsp;what I mean.</div>\r\n</div>\r\n<p>&nbsp;</p>\r\n<p>The &ldquo;going to&rdquo; construction is common in speech and casual writing. Keep in mind though that it&rsquo;s on the informal side, so it&rsquo;s a good idea to stick to the&nbsp;<strong>will + [root form]</strong>&nbsp;construction in formal writing.</p>\r\n<h2>How to Make the Simple Future Negative</h2>\r\n<p>To make the simple future negative, the formula is&nbsp;<strong>will + not + [root form]</strong>.</p>\r\n<p>&nbsp;</p>\r\n<div class="tool__example ">\r\n<div class="tool__example-content">Jen&nbsp;<strong>will not quit</strong>&nbsp;before she reaches her goal. Make sure you arrive on time tomorrow because the bus&nbsp;<strong>will not wait</strong>&nbsp;for you. He&nbsp;<strong>will not say</strong>&nbsp;anything bad about his boss. I&nbsp;<strong>will not finish</strong>&nbsp;my homework in time for class.</div>\r\n</div>\r\n<p>&nbsp;</p>\r\n<p>Using the &ldquo;going to&rdquo; construction, the formula is&nbsp;<strong>[am/is/are] + not + going to + [root form]</strong>.</p>\r\n<p>&nbsp;</p>\r\n<div class="tool__example ">\r\n<div class="tool__example-content">Jen&nbsp;<strong>is not going to quit</strong>&nbsp;before she reaches her goal. Make sure you arrive on time tomorrow because the bus&nbsp;<strong>is not going to wait</strong>&nbsp;for you. He&nbsp;<strong>is not going to say</strong>&nbsp;anything bad about his boss. I&nbsp;<strong>am not going to finish</strong>&nbsp;my homework in time for class.</div>\r\n</div>\r\n<p>&nbsp;</p>\r\n<h2>How to Ask a Question</h2>\r\n<p>To ask a question in the simple future, the formula is&nbsp;<strong>will + [subject] + [root form]</strong>.</p>\r\n<p>&nbsp;</p>\r\n<div class="tool__example ">\r\n<div class="tool__example-content"><strong>Will Jen finish</strong>&nbsp;<em>War and Peace</em>&nbsp;over the summer?&nbsp;<strong>Will I have</strong>&nbsp;the discipline to study Spanish every day? What&nbsp;<strong>will you buy</strong>&nbsp;with the money you found?</div>\r\n</div>\r\n<p>&nbsp;</p>\r\n<p>The formula for the &ldquo;going to&rdquo; construction is&nbsp;<strong>[am/is/are] + [subject] +going to + [root form]</strong>.</p>\r\n<p>&nbsp;</p>\r\n<div class="tool__example ">\r\n<div class="tool__example-content"><strong>Is Jen going to finish</strong>&nbsp;<em>War and Peace</em>&nbsp;over the summer?&nbsp;<strong>Am I going to have</strong>&nbsp;the discipline to study Spanish every day? What&nbsp;<strong>are you going to buy</strong>&nbsp;with the money you found?</div>\r\n</div>\r\n<p>&nbsp;</p>\r\n<h2>Common Verbs in the Simple&nbsp;Future</h2>\r\n<p><img class="alignnone size-full wp-image-23827 aligncenter lazyloaded" src="https://contenthub-static.grammarly.com/blog/wp-content/uploads/2016/03/The-simple-future-common-verbs.png" alt="The simple future common verbs" width="801" data-src="https://contenthub-static.grammarly.com/blog/wp-content/uploads/2016/03/The-simple-future-common-verbs.png" /></p>\r\n<h2>The &ldquo;Going to&rdquo; Construction</h2>\r\n<p><img class="size-full wp-image-23817 aligncenter lazyloaded" src="https://contenthub-static.grammarly.com/blog/wp-content/uploads/2016/03/Simple-future-going-to.png" alt="Simple future going to" width="1239" data-src="https://contenthub-static.grammarly.com/blog/wp-content/uploads/2016/03/Simple-future-going-to.png" /><img class="nc_pixel lazyloaded" src="https://pixel.newscred.com/px.gif?key=YXJ0aWNsZT1iNmYxODYzODZhZTNkNDAzMGIyNDk2YjI2ZmU0Yzc1ZQ==" alt="" width="1" height="1" data-src="https://pixel.newscred.com/px.gif?key=YXJ0aWNsZT1iNmYxODYzODZhZTNkNDAzMGIyNDk2YjI2ZmU0Yzc1ZQ==" /></p>\r\n</div>\r\n<div>&nbsp;</div>\r\n</article>\r\n</div>\r\n<div class="_16iRt-container">&nbsp;</div>	\N	t	2021-12-23 07:15:18.780923+00	2023-03-05 16:36:52.071879+00	8	38
\.


--
-- Data for Name: lesson_lesson; Type: TABLE DATA; Schema: public; Owner: video
--

COPY public.lesson_lesson (id, title, description, category, active, created, updated, user_id) FROM stdin;
2	Day 1 order of adjectives	Day 1 order of adjectives	1	t	2021-11-07 11:06:45.247718+00	2021-11-07 14:21:27.888382+00	1
3	Basic English	A1	1	t	2021-11-15 20:40:11.855535+00	2021-11-15 20:40:11.855574+00	4
4	Day 18 Second conditional	Second conditional	1	t	2021-11-21 06:43:07.946204+00	2021-11-21 06:44:17.16453+00	1
5	Day 19 Adverbs of place	Adverbs of place	1	t	2021-11-25 17:11:46.133957+00	2021-11-25 17:11:46.133986+00	1
6	Day 20 English slang	English slang	1	t	2021-11-26 19:54:44.258711+00	2021-11-26 19:54:44.258742+00	1
7	Day 21 Phrasal verb with GO	Phrasal verb with GO	0	t	2021-11-27 09:33:11.845437+00	2021-11-27 09:33:11.845464+00	1
8	Day 22 present perfect	present perfect	1	t	2021-11-27 12:19:49.344761+00	2021-11-27 12:19:49.344791+00	1
9	Day 23 Reported speech	23 Reported speech	1	t	2021-11-27 16:18:52.88272+00	2021-11-27 16:18:52.88275+00	1
10	Day 24 question words	question words	1	t	2021-11-27 16:34:17.200335+00	2021-11-27 16:34:17.200364+00	1
11	Day 25 Third conditional	Third conditional	1	t	2021-11-27 17:34:25.764163+00	2021-11-27 17:34:25.764191+00	1
12	Day 17 Phrasal verbs	Phrasal verbs	1	t	2021-11-27 17:40:56.694411+00	2021-11-27 17:40:56.694466+00	1
13	Day 26 Grammar another other	Grammar another other	0	t	2021-11-27 18:58:35.201032+00	2021-11-27 18:58:35.201059+00	1
14	Day 27 past perfect	past perfect	1	t	2021-11-27 19:22:18.016738+00	2021-11-27 19:22:18.016765+00	1
16	Day 29 Articles a, an, the	Articles a, an, the	1	t	2021-12-07 13:50:17.974592+00	2021-12-07 13:50:17.974622+00	1
17	Day 30 Irregular plurals	Irregular plurals	0	t	2021-12-07 13:51:12.661413+00	2021-12-07 20:27:34.760657+00	1
24	Day 4 First conditional	First conditional	1	t	2021-12-13 19:42:29.854095+00	2021-12-13 19:42:29.854126+00	1
26	Day 6 Describtive words	Describtive words	1	t	2021-12-14 18:52:25.955802+00	2021-12-14 18:52:25.955834+00	1
27	Day 7 Irregular plurals	Irregular plurals	1	f	2021-12-15 05:56:45.567895+00	2021-12-15 17:42:16.306415+00	1
29	Day 8 Relative pronauns	Relative pronauns	1	t	2021-12-15 17:54:52.21205+00	2021-12-15 17:54:52.212071+00	1
30	Day 10 Opposite verbs	Opposite verbs	1	t	2021-12-17 20:17:32.118037+00	2021-12-17 20:18:03.387926+00	1
31	Day 11 Prepopsition At, In, One	Prepopsition At, In, One	1	t	2021-12-17 20:21:20.383144+00	2021-12-17 20:21:20.383172+00	1
32	Day 12 Present countinious	Present countinious	1	t	2021-12-17 20:28:16.534613+00	2021-12-17 20:28:16.534644+00	1
33	Day 13 Prepositions of time	Prepositions of time	1	t	2021-12-18 13:55:03.424054+00	2021-12-18 13:55:03.424085+00	1
34	6 types of adverbs	6 types of adverbs	1	t	2021-12-18 13:59:11.230096+00	2021-12-18 13:59:11.230119+00	1
35	who whom	who whom	1	t	2021-12-18 14:11:21.976579+00	2021-12-18 14:11:21.976609+00	1
36	Day 14 Linking verbs	Linking verbs	1	t	2021-12-20 08:10:52.567188+00	2021-12-20 08:10:52.56722+00	1
37	Day 15 Uncountable nouns	Uncountable nouns	1	t	2021-12-20 13:23:51.407468+00	2021-12-20 13:23:51.407517+00	1
23	Day 3 Much, many	Much, many	1	t	2021-12-13 17:48:32.863637+00	2023-03-05 13:54:03.062548+00	8
28	Irregular Verbs	Irregular Verbs	1	f	2021-12-15 17:52:42.425876+00	2023-03-05 13:54:23.440023+00	8
25	Day 5 Present simple	Present simple	1	t	2021-12-13 19:54:56.066547+00	2023-03-05 13:54:35.973199+00	8
18	Day 2 Action Verbs	Action Verbs	1	t	2021-12-11 20:19:58.037018+00	2023-03-05 13:54:42.754274+00	8
15	Day 28 phrasal verbs with Take	phrasal verbs with Take	1	t	2021-11-27 19:49:23.355265+00	2023-03-05 13:54:50.854995+00	8
38	Day 16 Future simple	Future simple	1	t	2021-12-23 07:14:53.583311+00	2024-02-17 17:45:09.691119+00	806
\.


--
-- Data for Name: oauth2_provider_accesstoken; Type: TABLE DATA; Schema: public; Owner: video
--

COPY public.oauth2_provider_accesstoken (id, token, expires, scope, application_id, user_id, created, updated, source_refresh_token_id, id_token_id) FROM stdin;
\.


--
-- Data for Name: oauth2_provider_application; Type: TABLE DATA; Schema: public; Owner: video
--

COPY public.oauth2_provider_application (id, client_id, redirect_uris, client_type, authorization_grant_type, client_secret, name, user_id, skip_authorization, created, updated, algorithm) FROM stdin;
1	j9DaXQjMSGEGlKVs7QYTMxZfcv3sGUscUzZP3EV0		public	client-credentials	pbkdf2_sha256$180000$QL3XCZRxx5el$meNiv+sJ16DEEqteq/v5xO0sQmnNIryYnybe+dSQYSo=	video	5	f	2022-11-19 21:06:08.520842+00	2022-11-19 21:16:37.388354+00	
\.


--
-- Data for Name: oauth2_provider_grant; Type: TABLE DATA; Schema: public; Owner: video
--

COPY public.oauth2_provider_grant (id, code, expires, redirect_uri, scope, application_id, user_id, created, updated, code_challenge, code_challenge_method, nonce, claims) FROM stdin;
\.


--
-- Data for Name: oauth2_provider_idtoken; Type: TABLE DATA; Schema: public; Owner: video
--

COPY public.oauth2_provider_idtoken (id, jti, expires, scope, created, updated, application_id, user_id) FROM stdin;
\.


--
-- Data for Name: oauth2_provider_refreshtoken; Type: TABLE DATA; Schema: public; Owner: video
--

COPY public.oauth2_provider_refreshtoken (id, token, access_token_id, application_id, user_id, created, updated, revoked) FROM stdin;
\.


--
-- Data for Name: social_auth_association; Type: TABLE DATA; Schema: public; Owner: video
--

COPY public.social_auth_association (id, server_url, handle, secret, issued, lifetime, assoc_type) FROM stdin;
\.


--
-- Data for Name: social_auth_code; Type: TABLE DATA; Schema: public; Owner: video
--

COPY public.social_auth_code (id, email, code, verified, "timestamp") FROM stdin;
\.


--
-- Data for Name: social_auth_nonce; Type: TABLE DATA; Schema: public; Owner: video
--

COPY public.social_auth_nonce (id, server_url, "timestamp", salt) FROM stdin;
\.


--
-- Data for Name: social_auth_partial; Type: TABLE DATA; Schema: public; Owner: video
--

COPY public.social_auth_partial (id, token, next_step, backend, "timestamp", data) FROM stdin;
\.


--
-- Data for Name: social_auth_usersocialauth; Type: TABLE DATA; Schema: public; Owner: video
--

COPY public.social_auth_usersocialauth (id, provider, uid, user_id, created, modified, extra_data) FROM stdin;
\.


--
-- Data for Name: socialaccount_socialaccount; Type: TABLE DATA; Schema: public; Owner: video
--

COPY public.socialaccount_socialaccount (id, provider, uid, last_login, date_joined, extra_data, user_id) FROM stdin;
1	google	117272817956961682145	2024-02-17 17:14:26.636167+00	2022-08-07 20:11:09.677998+00	{"aud": "130656375808-p1qp1n942c5rb02em9bk3to562ib5h76.apps.googleusercontent.com", "azp": "130656375808-p1qp1n942c5rb02em9bk3to562ib5h76.apps.googleusercontent.com", "exp": 1708193666, "iat": 1708190066, "iss": "https://accounts.google.com", "sub": "117272817956961682145", "name": "Sergei Nosachyev", "locale": "ru", "at_hash": "sMp01tGagSyvIC8nKvEUlQ", "picture": "https://lh3.googleusercontent.com/a/ACg8ocJL7bXPP9-ZEtCANBSf1rwbXVXOv2bqsJofYcJvc4RUk8w=s96-c", "given_name": "Sergei", "family_name": "Nosachyev"}	1
3	google	1172728179569616821455	2024-01-31 12:32:31.024288+00	2024-01-31 12:32:31.024303+00	{"aud": "130656375808-p1qp1n942c5rb02em9bk3to562ib5h76.apps.googleusercontent.com", "azp": "130656375808-p1qp1n942c5rb02em9bk3to562ib5h76.apps.googleusercontent.com", "exp": 1706706921, "iat": 1706703321, "iss": "https://accounts.google.com", "sub": "117272817956961682145", "name": "Sergei Nosachyev", "locale": "ru", "at_hash": "6Ho_mGdmg0chgz8DkNhJvA", "picture": "https://lh3.googleusercontent.com/a/ACg8ocJL7bXPP9-ZEtCANBSf1rwbXVXOv2bqsJofYcJvc4RUk8w=s96-c", "given_name": "Sergei", "family_name": "Nosachyev"}	806
2	google	107728481804504324824	2024-01-21 17:54:17.443269+00	2023-02-01 22:48:17.08864+00	{"aud": "130656375808-p1qp1n942c5rb02em9bk3to562ib5h76.apps.googleusercontent.com", "azp": "130656375808-p1qp1n942c5rb02em9bk3to562ib5h76.apps.googleusercontent.com", "exp": 1705863257, "iat": 1705859657, "iss": "https://accounts.google.com", "sub": "107728481804504324824", "name": "юлия носачёва", "locale": "ru", "at_hash": "aOMIyrCTvyfKVqBRUbwrRg", "picture": "https://lh3.googleusercontent.com/a/ACg8ocJ-WHd1-vFtADREqLRwCX50LfoEuj16iZ0J8_yMXQYjCg=s96-c", "given_name": "юлия", "family_name": "носачёва"}	8
4	google	101695874068408888971	2024-02-15 17:04:13.361281+00	2024-02-02 12:26:03.756958+00	{"aud": "130656375808-p1qp1n942c5rb02em9bk3to562ib5h76.apps.googleusercontent.com", "azp": "130656375808-p1qp1n942c5rb02em9bk3to562ib5h76.apps.googleusercontent.com", "exp": 1708020253, "iat": 1708016653, "iss": "https://accounts.google.com", "sub": "101695874068408888971", "name": "Юлия Носачева", "locale": "ru", "at_hash": "3qbP0EzJ7Ld1NvGrOOSKaA", "picture": "https://lh3.googleusercontent.com/a/ACg8ocIyuWO3jaMIDPJpnkSsSAG03eXh1DWS5CEiPim3v-D4=s96-c", "given_name": "Юлия", "family_name": "Носачева"}	807
\.


--
-- Data for Name: socialaccount_socialapp; Type: TABLE DATA; Schema: public; Owner: video
--

COPY public.socialaccount_socialapp (id, provider, name, client_id, secret, key, provider_id, settings) FROM stdin;
2	vk	Сергей	51481033	y3h5jACCNkERb5yArx2O	ed4c04dced4c04dced4c04dc17ee5d8d15eed4ced4c04dc8e3507785150254969d9b83b		{}
\.


--
-- Data for Name: socialaccount_socialapp_sites; Type: TABLE DATA; Schema: public; Owner: video
--

COPY public.socialaccount_socialapp_sites (id, socialapp_id, site_id) FROM stdin;
3	2	1
4	2	2
\.


--
-- Data for Name: socialaccount_socialtoken; Type: TABLE DATA; Schema: public; Owner: video
--

COPY public.socialaccount_socialtoken (id, token, token_secret, expires_at, account_id, app_id) FROM stdin;
1	ya29.a0AfB_byDZpAhNhMI9PHJNLUTc275E7Zjuz0F1AFN7zX76_KkboZ-WQcmREumYmR1OoWLgfETu6EcJzNaOy97d62g4pUMBMEjgob3pmYJo5SUNSIw2h9uLGTKvXVg8nEg1YIM4hjDOj_n5keFChOMG6CAbaTEIvlPSHNTFaCgYKAZsSARISFQHGX2Miy-CMFqABPgA44eOtLAMh9A0171		2024-02-17 18:14:25.632942+00	1	\N
2	ya29.a0AfB_byA_FVTESPVENhNeGzP7tJI4lWO7G-nmgC2I8R0ysUSQ_NYNb-1hWKp3cci-xiAYedPNp7CAd98WtIl3a9tlWbVUzhlhUHsPtxGGOopv8yLlKyp5lvfh1UdovaDPYVOryrTSK8ojKGpRsHF83LfEfQm5omEVbO4aCgYKAYcSARMSFQHGX2MiaWs14s5x-rmxxUwpUjrG6A0170		2024-01-21 18:54:16.427719+00	2	\N
3	ya29.a0AfB_byCSpZFvKyAxiFAUGuLVCaaVe36mf9sPXODmRmnI7ntWIov_E6RfW3BX3OHprh3GOOILNXK110I2XlHUNwksxLWyCo-dtt5e6Qrh8CgUb7VTwAcMGNBvj7HsKvg8C2eDv1K4Hd60U36opaD_f7CbiFObaRVqCKoaCgYKAZgSARMSFQHGX2Mi-L8ybNyEVC9HLspB-pq62w0170		2024-02-15 18:04:12.357844+00	4	\N
\.


--
-- Data for Name: video_chat_token; Type: TABLE DATA; Schema: public; Owner: video
--

COPY public.video_chat_token (id, key, created, updated, user_id) FROM stdin;
4	eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjozLCJleHAiOjE2Nzk0MDk0MDIsInN1YiI6ImFjY2VzcyJ9.d60l9jFwp-s9Da6q0t1d5_q83KA_7T1MU7ahn_ZSq4w	2023-03-20 14:36:42.566471+00	2023-03-20 14:36:42.59843+00	3
2	eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxLCJleHAiOjE2ODAzODU2MjUsInN1YiI6ImFjY2VzcyJ9.O3_O4Ir6U1gHoej-PXICt-eYtgoIksKT8DYNTeRHfnQ	2022-12-15 13:53:59.839042+00	2023-03-31 21:47:05.681213+00	1
3	eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjo4LCJleHAiOjE2Nzk1NzU4NTIsInN1YiI6ImFjY2VzcyJ9.ZBOhlFM-GmldbEP3XQWWXnRbJbnrNCPoWjXgCvvpaEk	2023-02-01 22:48:31.275135+00	2023-03-22 12:50:52.195566+00	8
\.


--
-- Data for Name: video_chat_userprofile; Type: TABLE DATA; Schema: public; Owner: video
--

COPY public.video_chat_userprofile (id, avatar, user_id) FROM stdin;
1	photo_2022-09-25_14-36-50.jpg	1
\.


--
-- Name: account_emailaddress_id_seq; Type: SEQUENCE SET; Schema: public; Owner: video
--

SELECT pg_catalog.setval('public.account_emailaddress_id_seq', 4, true);


--
-- Name: account_emailconfirmation_id_seq; Type: SEQUENCE SET; Schema: public; Owner: video
--

SELECT pg_catalog.setval('public.account_emailconfirmation_id_seq', 1, false);


--
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: video
--

SELECT pg_catalog.setval('public.auth_group_id_seq', 1, false);


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: video
--

SELECT pg_catalog.setval('public.auth_group_permissions_id_seq', 1, false);


--
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: video
--

SELECT pg_catalog.setval('public.auth_permission_id_seq', 252, true);


--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: video
--

SELECT pg_catalog.setval('public.auth_user_groups_id_seq', 1, false);


--
-- Name: auth_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: video
--

SELECT pg_catalog.setval('public.auth_user_id_seq', 807, true);


--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: video
--

SELECT pg_catalog.setval('public.auth_user_user_permissions_id_seq', 47, true);


--
-- Name: chunks_chunk_id_seq; Type: SEQUENCE SET; Schema: public; Owner: video
--

SELECT pg_catalog.setval('public.chunks_chunk_id_seq', 1, false);


--
-- Name: chunks_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: video
--

SELECT pg_catalog.setval('public.chunks_group_id_seq', 1, false);


--
-- Name: chunks_image_id_seq; Type: SEQUENCE SET; Schema: public; Owner: video
--

SELECT pg_catalog.setval('public.chunks_image_id_seq', 1, false);


--
-- Name: chunks_media_id_seq; Type: SEQUENCE SET; Schema: public; Owner: video
--

SELECT pg_catalog.setval('public.chunks_media_id_seq', 1, false);


--
-- Name: comunicate_chat_id_seq; Type: SEQUENCE SET; Schema: public; Owner: video
--

SELECT pg_catalog.setval('public.comunicate_chat_id_seq', 189, true);


--
-- Name: comunicate_language_id_seq; Type: SEQUENCE SET; Schema: public; Owner: video
--

SELECT pg_catalog.setval('public.comunicate_language_id_seq', 268, true);


--
-- Name: comunicate_member_id_seq; Type: SEQUENCE SET; Schema: public; Owner: video
--

SELECT pg_catalog.setval('public.comunicate_member_id_seq', 350, true);


--
-- Name: comunicate_room_id_seq; Type: SEQUENCE SET; Schema: public; Owner: video
--

SELECT pg_catalog.setval('public.comunicate_room_id_seq', 367, true);


--
-- Name: comunicate_subject_id_seq; Type: SEQUENCE SET; Schema: public; Owner: video
--

SELECT pg_catalog.setval('public.comunicate_subject_id_seq', 149, true);


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: video
--

SELECT pg_catalog.setval('public.django_admin_log_id_seq', 2621, true);


--
-- Name: django_celery_beat_clockedschedule_id_seq; Type: SEQUENCE SET; Schema: public; Owner: video
--

SELECT pg_catalog.setval('public.django_celery_beat_clockedschedule_id_seq', 1, false);


--
-- Name: django_celery_beat_crontabschedule_id_seq; Type: SEQUENCE SET; Schema: public; Owner: video
--

SELECT pg_catalog.setval('public.django_celery_beat_crontabschedule_id_seq', 3, true);


--
-- Name: django_celery_beat_intervalschedule_id_seq; Type: SEQUENCE SET; Schema: public; Owner: video
--

SELECT pg_catalog.setval('public.django_celery_beat_intervalschedule_id_seq', 15, true);


--
-- Name: django_celery_beat_periodictask_id_seq; Type: SEQUENCE SET; Schema: public; Owner: video
--

SELECT pg_catalog.setval('public.django_celery_beat_periodictask_id_seq', 26, true);


--
-- Name: django_celery_beat_solarschedule_id_seq; Type: SEQUENCE SET; Schema: public; Owner: video
--

SELECT pg_catalog.setval('public.django_celery_beat_solarschedule_id_seq', 1, false);


--
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: video
--

SELECT pg_catalog.setval('public.django_content_type_id_seq', 64, true);


--
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: video
--

SELECT pg_catalog.setval('public.django_migrations_id_seq', 185, true);


--
-- Name: django_site_id_seq; Type: SEQUENCE SET; Schema: public; Owner: video
--

SELECT pg_catalog.setval('public.django_site_id_seq', 2, true);


--
-- Name: djangoseo_mymetadatamodel_id_seq; Type: SEQUENCE SET; Schema: public; Owner: video
--

SELECT pg_catalog.setval('public.djangoseo_mymetadatamodel_id_seq', 1, false);


--
-- Name: djangoseo_mymetadatamodelinstance_id_seq; Type: SEQUENCE SET; Schema: public; Owner: video
--

SELECT pg_catalog.setval('public.djangoseo_mymetadatamodelinstance_id_seq', 1, false);


--
-- Name: djangoseo_mymetadatapath_id_seq; Type: SEQUENCE SET; Schema: public; Owner: video
--

SELECT pg_catalog.setval('public.djangoseo_mymetadatapath_id_seq', 1, false);


--
-- Name: djangoseo_mymetadataview_id_seq; Type: SEQUENCE SET; Schema: public; Owner: video
--

SELECT pg_catalog.setval('public.djangoseo_mymetadataview_id_seq', 1, false);


--
-- Name: easy_thumbnails_source_id_seq; Type: SEQUENCE SET; Schema: public; Owner: video
--

SELECT pg_catalog.setval('public.easy_thumbnails_source_id_seq', 944, true);


--
-- Name: easy_thumbnails_thumbnail_id_seq; Type: SEQUENCE SET; Schema: public; Owner: video
--

SELECT pg_catalog.setval('public.easy_thumbnails_thumbnail_id_seq', 1662, true);


--
-- Name: easy_thumbnails_thumbnaildimensions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: video
--

SELECT pg_catalog.setval('public.easy_thumbnails_thumbnaildimensions_id_seq', 1, false);


--
-- Name: lesson_article_id_seq; Type: SEQUENCE SET; Schema: public; Owner: video
--

SELECT pg_catalog.setval('public.lesson_article_id_seq', 49, true);


--
-- Name: lesson_lesson_id_seq; Type: SEQUENCE SET; Schema: public; Owner: video
--

SELECT pg_catalog.setval('public.lesson_lesson_id_seq', 38, true);


--
-- Name: oauth2_provider_accesstoken_id_seq; Type: SEQUENCE SET; Schema: public; Owner: video
--

SELECT pg_catalog.setval('public.oauth2_provider_accesstoken_id_seq', 1, false);


--
-- Name: oauth2_provider_application_id_seq; Type: SEQUENCE SET; Schema: public; Owner: video
--

SELECT pg_catalog.setval('public.oauth2_provider_application_id_seq', 1, true);


--
-- Name: oauth2_provider_grant_id_seq; Type: SEQUENCE SET; Schema: public; Owner: video
--

SELECT pg_catalog.setval('public.oauth2_provider_grant_id_seq', 1, false);


--
-- Name: oauth2_provider_idtoken_id_seq; Type: SEQUENCE SET; Schema: public; Owner: video
--

SELECT pg_catalog.setval('public.oauth2_provider_idtoken_id_seq', 1, false);


--
-- Name: oauth2_provider_refreshtoken_id_seq; Type: SEQUENCE SET; Schema: public; Owner: video
--

SELECT pg_catalog.setval('public.oauth2_provider_refreshtoken_id_seq', 1, false);


--
-- Name: social_auth_association_id_seq; Type: SEQUENCE SET; Schema: public; Owner: video
--

SELECT pg_catalog.setval('public.social_auth_association_id_seq', 1, false);


--
-- Name: social_auth_code_id_seq; Type: SEQUENCE SET; Schema: public; Owner: video
--

SELECT pg_catalog.setval('public.social_auth_code_id_seq', 1, false);


--
-- Name: social_auth_nonce_id_seq; Type: SEQUENCE SET; Schema: public; Owner: video
--

SELECT pg_catalog.setval('public.social_auth_nonce_id_seq', 1, false);


--
-- Name: social_auth_partial_id_seq; Type: SEQUENCE SET; Schema: public; Owner: video
--

SELECT pg_catalog.setval('public.social_auth_partial_id_seq', 1, false);


--
-- Name: social_auth_usersocialauth_id_seq; Type: SEQUENCE SET; Schema: public; Owner: video
--

SELECT pg_catalog.setval('public.social_auth_usersocialauth_id_seq', 1, false);


--
-- Name: socialaccount_socialaccount_id_seq; Type: SEQUENCE SET; Schema: public; Owner: video
--

SELECT pg_catalog.setval('public.socialaccount_socialaccount_id_seq', 5, true);


--
-- Name: socialaccount_socialapp_id_seq; Type: SEQUENCE SET; Schema: public; Owner: video
--

SELECT pg_catalog.setval('public.socialaccount_socialapp_id_seq', 2, true);


--
-- Name: socialaccount_socialapp_sites_id_seq; Type: SEQUENCE SET; Schema: public; Owner: video
--

SELECT pg_catalog.setval('public.socialaccount_socialapp_sites_id_seq', 4, true);


--
-- Name: socialaccount_socialtoken_id_seq; Type: SEQUENCE SET; Schema: public; Owner: video
--

SELECT pg_catalog.setval('public.socialaccount_socialtoken_id_seq', 3, true);


--
-- Name: video_chat_token_id_seq; Type: SEQUENCE SET; Schema: public; Owner: video
--

SELECT pg_catalog.setval('public.video_chat_token_id_seq', 4, true);


--
-- Name: video_chat_userprofile_id_seq; Type: SEQUENCE SET; Schema: public; Owner: video
--

SELECT pg_catalog.setval('public.video_chat_userprofile_id_seq', 1, true);


--
-- Name: account_emailaddress account_emailaddress_pkey; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.account_emailaddress
    ADD CONSTRAINT account_emailaddress_pkey PRIMARY KEY (id);


--
-- Name: account_emailaddress account_emailaddress_user_id_email_987c8728_uniq; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.account_emailaddress
    ADD CONSTRAINT account_emailaddress_user_id_email_987c8728_uniq UNIQUE (user_id, email);


--
-- Name: account_emailconfirmation account_emailconfirmation_key_key; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.account_emailconfirmation
    ADD CONSTRAINT account_emailconfirmation_key_key UNIQUE (key);


--
-- Name: account_emailconfirmation account_emailconfirmation_pkey; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.account_emailconfirmation
    ADD CONSTRAINT account_emailconfirmation_pkey PRIMARY KEY (id);


--
-- Name: auth_group auth_group_name_key; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- Name: auth_group_permissions auth_group_permissions_group_id_0cd325b0_uniq; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_0cd325b0_uniq UNIQUE (group_id, permission_id);


--
-- Name: auth_group_permissions auth_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_group auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- Name: auth_permission auth_permission_content_type_id_01ab375a_uniq; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_01ab375a_uniq UNIQUE (content_type_id, codename);


--
-- Name: auth_permission auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups auth_user_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups auth_user_groups_user_id_94350c0c_uniq; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_94350c0c_uniq UNIQUE (user_id, group_id);


--
-- Name: auth_user auth_user_pkey; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions auth_user_user_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions auth_user_user_permissions_user_id_14a6b632_uniq; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_14a6b632_uniq UNIQUE (user_id, permission_id);


--
-- Name: auth_user auth_user_username_key; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_username_key UNIQUE (username);


--
-- Name: authtoken_token authtoken_token_pkey; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.authtoken_token
    ADD CONSTRAINT authtoken_token_pkey PRIMARY KEY (key);


--
-- Name: authtoken_token authtoken_token_user_id_key; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.authtoken_token
    ADD CONSTRAINT authtoken_token_user_id_key UNIQUE (user_id);


--
-- Name: chunks_chunk chunks_chunk_key_key; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.chunks_chunk
    ADD CONSTRAINT chunks_chunk_key_key UNIQUE (key);


--
-- Name: chunks_chunk chunks_chunk_pkey; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.chunks_chunk
    ADD CONSTRAINT chunks_chunk_pkey PRIMARY KEY (id);


--
-- Name: chunks_group chunks_group_pkey; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.chunks_group
    ADD CONSTRAINT chunks_group_pkey PRIMARY KEY (id);


--
-- Name: chunks_image chunks_image_key_key; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.chunks_image
    ADD CONSTRAINT chunks_image_key_key UNIQUE (key);


--
-- Name: chunks_image chunks_image_pkey; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.chunks_image
    ADD CONSTRAINT chunks_image_pkey PRIMARY KEY (id);


--
-- Name: chunks_media chunks_media_key_key; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.chunks_media
    ADD CONSTRAINT chunks_media_key_key UNIQUE (key);


--
-- Name: chunks_media chunks_media_pkey; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.chunks_media
    ADD CONSTRAINT chunks_media_pkey PRIMARY KEY (id);


--
-- Name: comunicate_chat comunicate_chat_pkey; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.comunicate_chat
    ADD CONSTRAINT comunicate_chat_pkey PRIMARY KEY (id);


--
-- Name: comunicate_language comunicate_language_pkey; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.comunicate_language
    ADD CONSTRAINT comunicate_language_pkey PRIMARY KEY (id);


--
-- Name: comunicate_member comunicate_member_pkey; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.comunicate_member
    ADD CONSTRAINT comunicate_member_pkey PRIMARY KEY (id);


--
-- Name: comunicate_member comunicate_member_user_id_room_id_11709d24_uniq; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.comunicate_member
    ADD CONSTRAINT comunicate_member_user_id_room_id_11709d24_uniq UNIQUE (social_account_id, room_id);


--
-- Name: comunicate_room comunicate_room_name_key; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.comunicate_room
    ADD CONSTRAINT comunicate_room_name_key UNIQUE (name);


--
-- Name: comunicate_room comunicate_room_pkey; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.comunicate_room
    ADD CONSTRAINT comunicate_room_pkey PRIMARY KEY (id);


--
-- Name: comunicate_subject comunicate_subject_name_20c06899_uniq; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.comunicate_subject
    ADD CONSTRAINT comunicate_subject_name_20c06899_uniq UNIQUE (name);


--
-- Name: comunicate_subject comunicate_subject_pkey; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.comunicate_subject
    ADD CONSTRAINT comunicate_subject_pkey PRIMARY KEY (id);


--
-- Name: django_admin_log django_admin_log_pkey; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- Name: django_celery_beat_clockedschedule django_celery_beat_clockedschedule_pkey; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.django_celery_beat_clockedschedule
    ADD CONSTRAINT django_celery_beat_clockedschedule_pkey PRIMARY KEY (id);


--
-- Name: django_celery_beat_crontabschedule django_celery_beat_crontabschedule_pkey; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.django_celery_beat_crontabschedule
    ADD CONSTRAINT django_celery_beat_crontabschedule_pkey PRIMARY KEY (id);


--
-- Name: django_celery_beat_intervalschedule django_celery_beat_intervalschedule_pkey; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.django_celery_beat_intervalschedule
    ADD CONSTRAINT django_celery_beat_intervalschedule_pkey PRIMARY KEY (id);


--
-- Name: django_celery_beat_periodictask django_celery_beat_periodictask_name_key; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.django_celery_beat_periodictask
    ADD CONSTRAINT django_celery_beat_periodictask_name_key UNIQUE (name);


--
-- Name: django_celery_beat_periodictask django_celery_beat_periodictask_pkey; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.django_celery_beat_periodictask
    ADD CONSTRAINT django_celery_beat_periodictask_pkey PRIMARY KEY (id);


--
-- Name: django_celery_beat_periodictasks django_celery_beat_periodictasks_pkey; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.django_celery_beat_periodictasks
    ADD CONSTRAINT django_celery_beat_periodictasks_pkey PRIMARY KEY (ident);


--
-- Name: django_celery_beat_solarschedule django_celery_beat_solar_event_latitude_longitude_ba64999a_uniq; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.django_celery_beat_solarschedule
    ADD CONSTRAINT django_celery_beat_solar_event_latitude_longitude_ba64999a_uniq UNIQUE (event, latitude, longitude);


--
-- Name: django_celery_beat_solarschedule django_celery_beat_solarschedule_pkey; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.django_celery_beat_solarschedule
    ADD CONSTRAINT django_celery_beat_solarschedule_pkey PRIMARY KEY (id);


--
-- Name: django_content_type django_content_type_app_label_76bd3d3b_uniq; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_app_label_76bd3d3b_uniq UNIQUE (app_label, model);


--
-- Name: django_content_type django_content_type_pkey; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- Name: django_migrations django_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- Name: django_session django_session_pkey; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- Name: django_site django_site_domain_a2e37b91_uniq; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.django_site
    ADD CONSTRAINT django_site_domain_a2e37b91_uniq UNIQUE (domain);


--
-- Name: django_site django_site_pkey; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.django_site
    ADD CONSTRAINT django_site_pkey PRIMARY KEY (id);


--
-- Name: djangoseo_mymetadatamodel djangoseo_mymetadatamodel__content_type_id_eb387422_uniq; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.djangoseo_mymetadatamodel
    ADD CONSTRAINT djangoseo_mymetadatamodel__content_type_id_eb387422_uniq UNIQUE (_content_type_id);


--
-- Name: djangoseo_mymetadatamodel djangoseo_mymetadatamodel_pkey; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.djangoseo_mymetadatamodel
    ADD CONSTRAINT djangoseo_mymetadatamodel_pkey PRIMARY KEY (id);


--
-- Name: djangoseo_mymetadatamodelinstance djangoseo_mymetadatamodelinstanc__content_type_id_01ada578_uniq; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.djangoseo_mymetadatamodelinstance
    ADD CONSTRAINT djangoseo_mymetadatamodelinstanc__content_type_id_01ada578_uniq UNIQUE (_content_type_id, _object_id);


--
-- Name: djangoseo_mymetadatamodelinstance djangoseo_mymetadatamodelinstance__path_a29426ec_uniq; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.djangoseo_mymetadatamodelinstance
    ADD CONSTRAINT djangoseo_mymetadatamodelinstance__path_a29426ec_uniq UNIQUE (_path);


--
-- Name: djangoseo_mymetadatamodelinstance djangoseo_mymetadatamodelinstance_pkey; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.djangoseo_mymetadatamodelinstance
    ADD CONSTRAINT djangoseo_mymetadatamodelinstance_pkey PRIMARY KEY (id);


--
-- Name: djangoseo_mymetadatapath djangoseo_mymetadatapath__path_f254a5c7_uniq; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.djangoseo_mymetadatapath
    ADD CONSTRAINT djangoseo_mymetadatapath__path_f254a5c7_uniq UNIQUE (_path);


--
-- Name: djangoseo_mymetadatapath djangoseo_mymetadatapath_pkey; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.djangoseo_mymetadatapath
    ADD CONSTRAINT djangoseo_mymetadatapath_pkey PRIMARY KEY (id);


--
-- Name: djangoseo_mymetadataview djangoseo_mymetadataview__view_61e54499_uniq; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.djangoseo_mymetadataview
    ADD CONSTRAINT djangoseo_mymetadataview__view_61e54499_uniq UNIQUE (_view);


--
-- Name: djangoseo_mymetadataview djangoseo_mymetadataview_pkey; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.djangoseo_mymetadataview
    ADD CONSTRAINT djangoseo_mymetadataview_pkey PRIMARY KEY (id);


--
-- Name: easy_thumbnails_source easy_thumbnails_source_pkey; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.easy_thumbnails_source
    ADD CONSTRAINT easy_thumbnails_source_pkey PRIMARY KEY (id);


--
-- Name: easy_thumbnails_source easy_thumbnails_source_storage_hash_481ce32d_uniq; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.easy_thumbnails_source
    ADD CONSTRAINT easy_thumbnails_source_storage_hash_481ce32d_uniq UNIQUE (storage_hash, name);


--
-- Name: easy_thumbnails_thumbnail easy_thumbnails_thumbnail_pkey; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.easy_thumbnails_thumbnail
    ADD CONSTRAINT easy_thumbnails_thumbnail_pkey PRIMARY KEY (id);


--
-- Name: easy_thumbnails_thumbnail easy_thumbnails_thumbnail_storage_hash_fb375270_uniq; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.easy_thumbnails_thumbnail
    ADD CONSTRAINT easy_thumbnails_thumbnail_storage_hash_fb375270_uniq UNIQUE (storage_hash, name, source_id);


--
-- Name: easy_thumbnails_thumbnaildimensions easy_thumbnails_thumbnaildimensions_pkey; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.easy_thumbnails_thumbnaildimensions
    ADD CONSTRAINT easy_thumbnails_thumbnaildimensions_pkey PRIMARY KEY (id);


--
-- Name: easy_thumbnails_thumbnaildimensions easy_thumbnails_thumbnaildimensions_thumbnail_id_key; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.easy_thumbnails_thumbnaildimensions
    ADD CONSTRAINT easy_thumbnails_thumbnaildimensions_thumbnail_id_key UNIQUE (thumbnail_id);


--
-- Name: lesson_article lesson_article_pkey; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.lesson_article
    ADD CONSTRAINT lesson_article_pkey PRIMARY KEY (id);


--
-- Name: lesson_lesson lesson_lesson_pkey; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.lesson_lesson
    ADD CONSTRAINT lesson_lesson_pkey PRIMARY KEY (id);


--
-- Name: oauth2_provider_accesstoken oauth2_provider_accesstoken_id_token_id_key; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.oauth2_provider_accesstoken
    ADD CONSTRAINT oauth2_provider_accesstoken_id_token_id_key UNIQUE (id_token_id);


--
-- Name: oauth2_provider_accesstoken oauth2_provider_accesstoken_pkey; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.oauth2_provider_accesstoken
    ADD CONSTRAINT oauth2_provider_accesstoken_pkey PRIMARY KEY (id);


--
-- Name: oauth2_provider_accesstoken oauth2_provider_accesstoken_source_refresh_token_id_key; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.oauth2_provider_accesstoken
    ADD CONSTRAINT oauth2_provider_accesstoken_source_refresh_token_id_key UNIQUE (source_refresh_token_id);


--
-- Name: oauth2_provider_accesstoken oauth2_provider_accesstoken_token_key; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.oauth2_provider_accesstoken
    ADD CONSTRAINT oauth2_provider_accesstoken_token_key UNIQUE (token);


--
-- Name: oauth2_provider_application oauth2_provider_application_client_id_key; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.oauth2_provider_application
    ADD CONSTRAINT oauth2_provider_application_client_id_key UNIQUE (client_id);


--
-- Name: oauth2_provider_application oauth2_provider_application_pkey; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.oauth2_provider_application
    ADD CONSTRAINT oauth2_provider_application_pkey PRIMARY KEY (id);


--
-- Name: oauth2_provider_grant oauth2_provider_grant_code_key; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.oauth2_provider_grant
    ADD CONSTRAINT oauth2_provider_grant_code_key UNIQUE (code);


--
-- Name: oauth2_provider_grant oauth2_provider_grant_pkey; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.oauth2_provider_grant
    ADD CONSTRAINT oauth2_provider_grant_pkey PRIMARY KEY (id);


--
-- Name: oauth2_provider_idtoken oauth2_provider_idtoken_jti_key; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.oauth2_provider_idtoken
    ADD CONSTRAINT oauth2_provider_idtoken_jti_key UNIQUE (jti);


--
-- Name: oauth2_provider_idtoken oauth2_provider_idtoken_pkey; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.oauth2_provider_idtoken
    ADD CONSTRAINT oauth2_provider_idtoken_pkey PRIMARY KEY (id);


--
-- Name: oauth2_provider_refreshtoken oauth2_provider_refreshtoken_access_token_id_key; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.oauth2_provider_refreshtoken
    ADD CONSTRAINT oauth2_provider_refreshtoken_access_token_id_key UNIQUE (access_token_id);


--
-- Name: oauth2_provider_refreshtoken oauth2_provider_refreshtoken_pkey; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.oauth2_provider_refreshtoken
    ADD CONSTRAINT oauth2_provider_refreshtoken_pkey PRIMARY KEY (id);


--
-- Name: oauth2_provider_refreshtoken oauth2_provider_refreshtoken_token_revoked_af8a5134_uniq; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.oauth2_provider_refreshtoken
    ADD CONSTRAINT oauth2_provider_refreshtoken_token_revoked_af8a5134_uniq UNIQUE (token, revoked);


--
-- Name: social_auth_association social_auth_association_pkey; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.social_auth_association
    ADD CONSTRAINT social_auth_association_pkey PRIMARY KEY (id);


--
-- Name: social_auth_association social_auth_association_server_url_handle_078befa2_uniq; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.social_auth_association
    ADD CONSTRAINT social_auth_association_server_url_handle_078befa2_uniq UNIQUE (server_url, handle);


--
-- Name: social_auth_code social_auth_code_email_code_801b2d02_uniq; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.social_auth_code
    ADD CONSTRAINT social_auth_code_email_code_801b2d02_uniq UNIQUE (email, code);


--
-- Name: social_auth_code social_auth_code_pkey; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.social_auth_code
    ADD CONSTRAINT social_auth_code_pkey PRIMARY KEY (id);


--
-- Name: social_auth_nonce social_auth_nonce_pkey; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.social_auth_nonce
    ADD CONSTRAINT social_auth_nonce_pkey PRIMARY KEY (id);


--
-- Name: social_auth_nonce social_auth_nonce_server_url_timestamp_salt_f6284463_uniq; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.social_auth_nonce
    ADD CONSTRAINT social_auth_nonce_server_url_timestamp_salt_f6284463_uniq UNIQUE (server_url, "timestamp", salt);


--
-- Name: social_auth_partial social_auth_partial_pkey; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.social_auth_partial
    ADD CONSTRAINT social_auth_partial_pkey PRIMARY KEY (id);


--
-- Name: social_auth_usersocialauth social_auth_usersocialauth_pkey; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.social_auth_usersocialauth
    ADD CONSTRAINT social_auth_usersocialauth_pkey PRIMARY KEY (id);


--
-- Name: social_auth_usersocialauth social_auth_usersocialauth_provider_uid_e6b5e668_uniq; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.social_auth_usersocialauth
    ADD CONSTRAINT social_auth_usersocialauth_provider_uid_e6b5e668_uniq UNIQUE (provider, uid);


--
-- Name: socialaccount_socialaccount socialaccount_socialaccount_pkey; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.socialaccount_socialaccount
    ADD CONSTRAINT socialaccount_socialaccount_pkey PRIMARY KEY (id);


--
-- Name: socialaccount_socialaccount socialaccount_socialaccount_provider_uid_fc810c6e_uniq; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.socialaccount_socialaccount
    ADD CONSTRAINT socialaccount_socialaccount_provider_uid_fc810c6e_uniq UNIQUE (provider, uid);


--
-- Name: socialaccount_socialapp_sites socialaccount_socialapp__socialapp_id_site_id_71a9a768_uniq; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.socialaccount_socialapp_sites
    ADD CONSTRAINT socialaccount_socialapp__socialapp_id_site_id_71a9a768_uniq UNIQUE (socialapp_id, site_id);


--
-- Name: socialaccount_socialapp socialaccount_socialapp_pkey; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.socialaccount_socialapp
    ADD CONSTRAINT socialaccount_socialapp_pkey PRIMARY KEY (id);


--
-- Name: socialaccount_socialapp_sites socialaccount_socialapp_sites_pkey; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.socialaccount_socialapp_sites
    ADD CONSTRAINT socialaccount_socialapp_sites_pkey PRIMARY KEY (id);


--
-- Name: socialaccount_socialtoken socialaccount_socialtoken_app_id_account_id_fca4e0ac_uniq; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.socialaccount_socialtoken
    ADD CONSTRAINT socialaccount_socialtoken_app_id_account_id_fca4e0ac_uniq UNIQUE (app_id, account_id);


--
-- Name: socialaccount_socialtoken socialaccount_socialtoken_pkey; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.socialaccount_socialtoken
    ADD CONSTRAINT socialaccount_socialtoken_pkey PRIMARY KEY (id);


--
-- Name: video_chat_token video_chat_token_pkey; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.video_chat_token
    ADD CONSTRAINT video_chat_token_pkey PRIMARY KEY (id);


--
-- Name: video_chat_userprofile video_chat_userprofile_pkey; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.video_chat_userprofile
    ADD CONSTRAINT video_chat_userprofile_pkey PRIMARY KEY (id);


--
-- Name: video_chat_userprofile video_chat_userprofile_user_id_key; Type: CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.video_chat_userprofile
    ADD CONSTRAINT video_chat_userprofile_user_id_key UNIQUE (user_id);


--
-- Name: account_emailaddress_upper; Type: INDEX; Schema: public; Owner: video
--

CREATE INDEX account_emailaddress_upper ON public.account_emailaddress USING btree (upper((email)::text));


--
-- Name: account_emailaddress_user_id_2c513194; Type: INDEX; Schema: public; Owner: video
--

CREATE INDEX account_emailaddress_user_id_2c513194 ON public.account_emailaddress USING btree (user_id);


--
-- Name: account_emailconfirmation_email_address_id_5b7f8c58; Type: INDEX; Schema: public; Owner: video
--

CREATE INDEX account_emailconfirmation_email_address_id_5b7f8c58 ON public.account_emailconfirmation USING btree (email_address_id);


--
-- Name: account_emailconfirmation_key_f43612bd_like; Type: INDEX; Schema: public; Owner: video
--

CREATE INDEX account_emailconfirmation_key_f43612bd_like ON public.account_emailconfirmation USING btree (key varchar_pattern_ops);


--
-- Name: auth_group_name_a6ea08ec_like; Type: INDEX; Schema: public; Owner: video
--

CREATE INDEX auth_group_name_a6ea08ec_like ON public.auth_group USING btree (name varchar_pattern_ops);


--
-- Name: auth_group_permissions_0e939a4f; Type: INDEX; Schema: public; Owner: video
--

CREATE INDEX auth_group_permissions_0e939a4f ON public.auth_group_permissions USING btree (group_id);


--
-- Name: auth_group_permissions_8373b171; Type: INDEX; Schema: public; Owner: video
--

CREATE INDEX auth_group_permissions_8373b171 ON public.auth_group_permissions USING btree (permission_id);


--
-- Name: auth_permission_417f1b1c; Type: INDEX; Schema: public; Owner: video
--

CREATE INDEX auth_permission_417f1b1c ON public.auth_permission USING btree (content_type_id);


--
-- Name: auth_user_groups_0e939a4f; Type: INDEX; Schema: public; Owner: video
--

CREATE INDEX auth_user_groups_0e939a4f ON public.auth_user_groups USING btree (group_id);


--
-- Name: auth_user_groups_e8701ad4; Type: INDEX; Schema: public; Owner: video
--

CREATE INDEX auth_user_groups_e8701ad4 ON public.auth_user_groups USING btree (user_id);


--
-- Name: auth_user_user_permissions_8373b171; Type: INDEX; Schema: public; Owner: video
--

CREATE INDEX auth_user_user_permissions_8373b171 ON public.auth_user_user_permissions USING btree (permission_id);


--
-- Name: auth_user_user_permissions_e8701ad4; Type: INDEX; Schema: public; Owner: video
--

CREATE INDEX auth_user_user_permissions_e8701ad4 ON public.auth_user_user_permissions USING btree (user_id);


--
-- Name: auth_user_username_6821ab7c_like; Type: INDEX; Schema: public; Owner: video
--

CREATE INDEX auth_user_username_6821ab7c_like ON public.auth_user USING btree (username varchar_pattern_ops);


--
-- Name: authtoken_token_key_10f0b77e_like; Type: INDEX; Schema: public; Owner: video
--

CREATE INDEX authtoken_token_key_10f0b77e_like ON public.authtoken_token USING btree (key varchar_pattern_ops);


--
-- Name: chunks_chunk_key_a2dee831_like; Type: INDEX; Schema: public; Owner: video
--

CREATE INDEX chunks_chunk_key_a2dee831_like ON public.chunks_chunk USING btree (key varchar_pattern_ops);


--
-- Name: chunks_image_key_63ddd375_like; Type: INDEX; Schema: public; Owner: video
--

CREATE INDEX chunks_image_key_63ddd375_like ON public.chunks_image USING btree (key varchar_pattern_ops);


--
-- Name: chunks_media_key_60fd76d4_like; Type: INDEX; Schema: public; Owner: video
--

CREATE INDEX chunks_media_key_60fd76d4_like ON public.chunks_media USING btree (key varchar_pattern_ops);


--
-- Name: comunicate_chat_room_id_05af7ec8; Type: INDEX; Schema: public; Owner: video
--

CREATE INDEX comunicate_chat_room_id_05af7ec8 ON public.comunicate_chat USING btree (room_id);


--
-- Name: comunicate_chat_shared_room_id_528ac781; Type: INDEX; Schema: public; Owner: video
--

CREATE INDEX comunicate_chat_shared_room_id_528ac781 ON public.comunicate_chat USING btree (shared_room_id);


--
-- Name: comunicate_member_room_id_d7019898; Type: INDEX; Schema: public; Owner: video
--

CREATE INDEX comunicate_member_room_id_d7019898 ON public.comunicate_member USING btree (room_id);


--
-- Name: comunicate_member_user_id_8494bdd2; Type: INDEX; Schema: public; Owner: video
--

CREATE INDEX comunicate_member_user_id_8494bdd2 ON public.comunicate_member USING btree (social_account_id);


--
-- Name: comunicate_room_language_id_58907517; Type: INDEX; Schema: public; Owner: video
--

CREATE INDEX comunicate_room_language_id_58907517 ON public.comunicate_room USING btree (language_id);


--
-- Name: comunicate_room_name_f27f15a7_like; Type: INDEX; Schema: public; Owner: video
--

CREATE INDEX comunicate_room_name_f27f15a7_like ON public.comunicate_room USING btree (name varchar_pattern_ops);


--
-- Name: comunicate_room_subject_id_1ebe788a; Type: INDEX; Schema: public; Owner: video
--

CREATE INDEX comunicate_room_subject_id_1ebe788a ON public.comunicate_room USING btree (subject_id);


--
-- Name: comunicate_room_user_id_226b6cc9; Type: INDEX; Schema: public; Owner: video
--

CREATE INDEX comunicate_room_user_id_226b6cc9 ON public.comunicate_room USING btree (social_account_id);


--
-- Name: comunicate_subject_name_20c06899_like; Type: INDEX; Schema: public; Owner: video
--

CREATE INDEX comunicate_subject_name_20c06899_like ON public.comunicate_subject USING btree (name varchar_pattern_ops);


--
-- Name: django_admin_log_417f1b1c; Type: INDEX; Schema: public; Owner: video
--

CREATE INDEX django_admin_log_417f1b1c ON public.django_admin_log USING btree (content_type_id);


--
-- Name: django_admin_log_e8701ad4; Type: INDEX; Schema: public; Owner: video
--

CREATE INDEX django_admin_log_e8701ad4 ON public.django_admin_log USING btree (user_id);


--
-- Name: django_celery_beat_periodictask_clocked_id_47a69f82; Type: INDEX; Schema: public; Owner: video
--

CREATE INDEX django_celery_beat_periodictask_clocked_id_47a69f82 ON public.django_celery_beat_periodictask USING btree (clocked_id);


--
-- Name: django_celery_beat_periodictask_crontab_id_d3cba168; Type: INDEX; Schema: public; Owner: video
--

CREATE INDEX django_celery_beat_periodictask_crontab_id_d3cba168 ON public.django_celery_beat_periodictask USING btree (crontab_id);


--
-- Name: django_celery_beat_periodictask_interval_id_a8ca27da; Type: INDEX; Schema: public; Owner: video
--

CREATE INDEX django_celery_beat_periodictask_interval_id_a8ca27da ON public.django_celery_beat_periodictask USING btree (interval_id);


--
-- Name: django_celery_beat_periodictask_name_265a36b7_like; Type: INDEX; Schema: public; Owner: video
--

CREATE INDEX django_celery_beat_periodictask_name_265a36b7_like ON public.django_celery_beat_periodictask USING btree (name varchar_pattern_ops);


--
-- Name: django_celery_beat_periodictask_solar_id_a87ce72c; Type: INDEX; Schema: public; Owner: video
--

CREATE INDEX django_celery_beat_periodictask_solar_id_a87ce72c ON public.django_celery_beat_periodictask USING btree (solar_id);


--
-- Name: django_session_de54fa62; Type: INDEX; Schema: public; Owner: video
--

CREATE INDEX django_session_de54fa62 ON public.django_session USING btree (expire_date);


--
-- Name: django_session_session_key_c0390e0f_like; Type: INDEX; Schema: public; Owner: video
--

CREATE INDEX django_session_session_key_c0390e0f_like ON public.django_session USING btree (session_key varchar_pattern_ops);


--
-- Name: django_site_domain_a2e37b91_like; Type: INDEX; Schema: public; Owner: video
--

CREATE INDEX django_site_domain_a2e37b91_like ON public.django_site USING btree (domain varchar_pattern_ops);


--
-- Name: djangoseo_mymetadatamodel_1cf7f132; Type: INDEX; Schema: public; Owner: video
--

CREATE INDEX djangoseo_mymetadatamodel_1cf7f132 ON public.djangoseo_mymetadatamodel USING btree (_content_type_id);


--
-- Name: djangoseo_mymetadatamodelinstance_1cf7f132; Type: INDEX; Schema: public; Owner: video
--

CREATE INDEX djangoseo_mymetadatamodelinstance_1cf7f132 ON public.djangoseo_mymetadatamodelinstance USING btree (_content_type_id);


--
-- Name: easy_thumbnails_source_b068931c; Type: INDEX; Schema: public; Owner: video
--

CREATE INDEX easy_thumbnails_source_b068931c ON public.easy_thumbnails_source USING btree (name);


--
-- Name: easy_thumbnails_source_b454e115; Type: INDEX; Schema: public; Owner: video
--

CREATE INDEX easy_thumbnails_source_b454e115 ON public.easy_thumbnails_source USING btree (storage_hash);


--
-- Name: easy_thumbnails_source_name_5fe0edc6_like; Type: INDEX; Schema: public; Owner: video
--

CREATE INDEX easy_thumbnails_source_name_5fe0edc6_like ON public.easy_thumbnails_source USING btree (name varchar_pattern_ops);


--
-- Name: easy_thumbnails_source_storage_hash_946cbcc9_like; Type: INDEX; Schema: public; Owner: video
--

CREATE INDEX easy_thumbnails_source_storage_hash_946cbcc9_like ON public.easy_thumbnails_source USING btree (storage_hash varchar_pattern_ops);


--
-- Name: easy_thumbnails_thumbnail_0afd9202; Type: INDEX; Schema: public; Owner: video
--

CREATE INDEX easy_thumbnails_thumbnail_0afd9202 ON public.easy_thumbnails_thumbnail USING btree (source_id);


--
-- Name: easy_thumbnails_thumbnail_b068931c; Type: INDEX; Schema: public; Owner: video
--

CREATE INDEX easy_thumbnails_thumbnail_b068931c ON public.easy_thumbnails_thumbnail USING btree (name);


--
-- Name: easy_thumbnails_thumbnail_b454e115; Type: INDEX; Schema: public; Owner: video
--

CREATE INDEX easy_thumbnails_thumbnail_b454e115 ON public.easy_thumbnails_thumbnail USING btree (storage_hash);


--
-- Name: easy_thumbnails_thumbnail_name_b5882c31_like; Type: INDEX; Schema: public; Owner: video
--

CREATE INDEX easy_thumbnails_thumbnail_name_b5882c31_like ON public.easy_thumbnails_thumbnail USING btree (name varchar_pattern_ops);


--
-- Name: easy_thumbnails_thumbnail_storage_hash_f1435f49_like; Type: INDEX; Schema: public; Owner: video
--

CREATE INDEX easy_thumbnails_thumbnail_storage_hash_f1435f49_like ON public.easy_thumbnails_thumbnail USING btree (storage_hash varchar_pattern_ops);


--
-- Name: lesson_article_lesson_id_a154a29d; Type: INDEX; Schema: public; Owner: video
--

CREATE INDEX lesson_article_lesson_id_a154a29d ON public.lesson_article USING btree (lesson_id);


--
-- Name: lesson_article_user_id_75385a5b; Type: INDEX; Schema: public; Owner: video
--

CREATE INDEX lesson_article_user_id_75385a5b ON public.lesson_article USING btree (user_id);


--
-- Name: lesson_lesson_user_id_1f039d56; Type: INDEX; Schema: public; Owner: video
--

CREATE INDEX lesson_lesson_user_id_1f039d56 ON public.lesson_lesson USING btree (user_id);


--
-- Name: oauth2_provider_accesstoken_application_id_b22886e1; Type: INDEX; Schema: public; Owner: video
--

CREATE INDEX oauth2_provider_accesstoken_application_id_b22886e1 ON public.oauth2_provider_accesstoken USING btree (application_id);


--
-- Name: oauth2_provider_accesstoken_token_8af090f8_like; Type: INDEX; Schema: public; Owner: video
--

CREATE INDEX oauth2_provider_accesstoken_token_8af090f8_like ON public.oauth2_provider_accesstoken USING btree (token varchar_pattern_ops);


--
-- Name: oauth2_provider_accesstoken_user_id_6e4c9a65; Type: INDEX; Schema: public; Owner: video
--

CREATE INDEX oauth2_provider_accesstoken_user_id_6e4c9a65 ON public.oauth2_provider_accesstoken USING btree (user_id);


--
-- Name: oauth2_provider_application_client_id_03f0cc84_like; Type: INDEX; Schema: public; Owner: video
--

CREATE INDEX oauth2_provider_application_client_id_03f0cc84_like ON public.oauth2_provider_application USING btree (client_id varchar_pattern_ops);


--
-- Name: oauth2_provider_application_client_secret_53133678; Type: INDEX; Schema: public; Owner: video
--

CREATE INDEX oauth2_provider_application_client_secret_53133678 ON public.oauth2_provider_application USING btree (client_secret);


--
-- Name: oauth2_provider_application_client_secret_53133678_like; Type: INDEX; Schema: public; Owner: video
--

CREATE INDEX oauth2_provider_application_client_secret_53133678_like ON public.oauth2_provider_application USING btree (client_secret varchar_pattern_ops);


--
-- Name: oauth2_provider_application_user_id_79829054; Type: INDEX; Schema: public; Owner: video
--

CREATE INDEX oauth2_provider_application_user_id_79829054 ON public.oauth2_provider_application USING btree (user_id);


--
-- Name: oauth2_provider_grant_application_id_81923564; Type: INDEX; Schema: public; Owner: video
--

CREATE INDEX oauth2_provider_grant_application_id_81923564 ON public.oauth2_provider_grant USING btree (application_id);


--
-- Name: oauth2_provider_grant_code_49ab4ddf_like; Type: INDEX; Schema: public; Owner: video
--

CREATE INDEX oauth2_provider_grant_code_49ab4ddf_like ON public.oauth2_provider_grant USING btree (code varchar_pattern_ops);


--
-- Name: oauth2_provider_grant_user_id_e8f62af8; Type: INDEX; Schema: public; Owner: video
--

CREATE INDEX oauth2_provider_grant_user_id_e8f62af8 ON public.oauth2_provider_grant USING btree (user_id);


--
-- Name: oauth2_provider_idtoken_application_id_08c5ff4f; Type: INDEX; Schema: public; Owner: video
--

CREATE INDEX oauth2_provider_idtoken_application_id_08c5ff4f ON public.oauth2_provider_idtoken USING btree (application_id);


--
-- Name: oauth2_provider_idtoken_user_id_dd512b59; Type: INDEX; Schema: public; Owner: video
--

CREATE INDEX oauth2_provider_idtoken_user_id_dd512b59 ON public.oauth2_provider_idtoken USING btree (user_id);


--
-- Name: oauth2_provider_refreshtoken_application_id_2d1c311b; Type: INDEX; Schema: public; Owner: video
--

CREATE INDEX oauth2_provider_refreshtoken_application_id_2d1c311b ON public.oauth2_provider_refreshtoken USING btree (application_id);


--
-- Name: oauth2_provider_refreshtoken_user_id_da837fce; Type: INDEX; Schema: public; Owner: video
--

CREATE INDEX oauth2_provider_refreshtoken_user_id_da837fce ON public.oauth2_provider_refreshtoken USING btree (user_id);


--
-- Name: social_auth_code_code_a2393167; Type: INDEX; Schema: public; Owner: video
--

CREATE INDEX social_auth_code_code_a2393167 ON public.social_auth_code USING btree (code);


--
-- Name: social_auth_code_code_a2393167_like; Type: INDEX; Schema: public; Owner: video
--

CREATE INDEX social_auth_code_code_a2393167_like ON public.social_auth_code USING btree (code varchar_pattern_ops);


--
-- Name: social_auth_code_timestamp_176b341f; Type: INDEX; Schema: public; Owner: video
--

CREATE INDEX social_auth_code_timestamp_176b341f ON public.social_auth_code USING btree ("timestamp");


--
-- Name: social_auth_partial_timestamp_50f2119f; Type: INDEX; Schema: public; Owner: video
--

CREATE INDEX social_auth_partial_timestamp_50f2119f ON public.social_auth_partial USING btree ("timestamp");


--
-- Name: social_auth_partial_token_3017fea3; Type: INDEX; Schema: public; Owner: video
--

CREATE INDEX social_auth_partial_token_3017fea3 ON public.social_auth_partial USING btree (token);


--
-- Name: social_auth_partial_token_3017fea3_like; Type: INDEX; Schema: public; Owner: video
--

CREATE INDEX social_auth_partial_token_3017fea3_like ON public.social_auth_partial USING btree (token varchar_pattern_ops);


--
-- Name: social_auth_usersocialauth_uid_796e51dc; Type: INDEX; Schema: public; Owner: video
--

CREATE INDEX social_auth_usersocialauth_uid_796e51dc ON public.social_auth_usersocialauth USING btree (uid);


--
-- Name: social_auth_usersocialauth_uid_796e51dc_like; Type: INDEX; Schema: public; Owner: video
--

CREATE INDEX social_auth_usersocialauth_uid_796e51dc_like ON public.social_auth_usersocialauth USING btree (uid varchar_pattern_ops);


--
-- Name: social_auth_usersocialauth_user_id_17d28448; Type: INDEX; Schema: public; Owner: video
--

CREATE INDEX social_auth_usersocialauth_user_id_17d28448 ON public.social_auth_usersocialauth USING btree (user_id);


--
-- Name: socialaccount_socialaccount_user_id_8146e70c; Type: INDEX; Schema: public; Owner: video
--

CREATE INDEX socialaccount_socialaccount_user_id_8146e70c ON public.socialaccount_socialaccount USING btree (user_id);


--
-- Name: socialaccount_socialapp_sites_site_id_2579dee5; Type: INDEX; Schema: public; Owner: video
--

CREATE INDEX socialaccount_socialapp_sites_site_id_2579dee5 ON public.socialaccount_socialapp_sites USING btree (site_id);


--
-- Name: socialaccount_socialapp_sites_socialapp_id_97fb6e7d; Type: INDEX; Schema: public; Owner: video
--

CREATE INDEX socialaccount_socialapp_sites_socialapp_id_97fb6e7d ON public.socialaccount_socialapp_sites USING btree (socialapp_id);


--
-- Name: socialaccount_socialtoken_account_id_951f210e; Type: INDEX; Schema: public; Owner: video
--

CREATE INDEX socialaccount_socialtoken_account_id_951f210e ON public.socialaccount_socialtoken USING btree (account_id);


--
-- Name: socialaccount_socialtoken_app_id_636a42d7; Type: INDEX; Schema: public; Owner: video
--

CREATE INDEX socialaccount_socialtoken_app_id_636a42d7 ON public.socialaccount_socialtoken USING btree (app_id);


--
-- Name: unique_verified_email; Type: INDEX; Schema: public; Owner: video
--

CREATE UNIQUE INDEX unique_verified_email ON public.account_emailaddress USING btree (email) WHERE verified;


--
-- Name: video_chat_token_user_id_a00dcbf0; Type: INDEX; Schema: public; Owner: video
--

CREATE INDEX video_chat_token_user_id_a00dcbf0 ON public.video_chat_token USING btree (user_id);


--
-- Name: account_emailaddress account_emailaddress_user_id_2c513194_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.account_emailaddress
    ADD CONSTRAINT account_emailaddress_user_id_2c513194_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: account_emailconfirmation account_emailconfirm_email_address_id_5b7f8c58_fk_account_e; Type: FK CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.account_emailconfirmation
    ADD CONSTRAINT account_emailconfirm_email_address_id_5b7f8c58_fk_account_e FOREIGN KEY (email_address_id) REFERENCES public.account_emailaddress(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions auth_group_permiss_permission_id_84c5c92e_fk_auth_permission_id; Type: FK CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permiss_permission_id_84c5c92e_fk_auth_permission_id FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions auth_group_permissions_group_id_b120cbf9_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_permission auth_permiss_content_type_id_2f476e4b_fk_django_content_type_id; Type: FK CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permiss_content_type_id_2f476e4b_fk_django_content_type_id FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups auth_user_groups_group_id_97559544_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_group_id_97559544_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups auth_user_groups_user_id_6a12ed8b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_6a12ed8b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions auth_user_user_per_permission_id_1fbb5f2c_fk_auth_permission_id; Type: FK CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_per_permission_id_1fbb5f2c_fk_auth_permission_id FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: authtoken_token authtoken_token_user_id_35299eff_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.authtoken_token
    ADD CONSTRAINT authtoken_token_user_id_35299eff_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: comunicate_chat comunicate_chat_room_id_05af7ec8_fk_comunicate_room_id; Type: FK CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.comunicate_chat
    ADD CONSTRAINT comunicate_chat_room_id_05af7ec8_fk_comunicate_room_id FOREIGN KEY (room_id) REFERENCES public.comunicate_room(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: comunicate_chat comunicate_chat_shared_room_id_528ac781_fk_comunicate_room_id; Type: FK CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.comunicate_chat
    ADD CONSTRAINT comunicate_chat_shared_room_id_528ac781_fk_comunicate_room_id FOREIGN KEY (shared_room_id) REFERENCES public.comunicate_room(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: comunicate_member comunicate_member_room_id_d7019898_fk_comunicate_room_id; Type: FK CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.comunicate_member
    ADD CONSTRAINT comunicate_member_room_id_d7019898_fk_comunicate_room_id FOREIGN KEY (room_id) REFERENCES public.comunicate_room(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: comunicate_member comunicate_member_social_account_id_b0612350_fk_socialacc; Type: FK CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.comunicate_member
    ADD CONSTRAINT comunicate_member_social_account_id_b0612350_fk_socialacc FOREIGN KEY (social_account_id) REFERENCES public.socialaccount_socialaccount(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: comunicate_room comunicate_room_language_id_58907517_fk_comunicate_language_id; Type: FK CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.comunicate_room
    ADD CONSTRAINT comunicate_room_language_id_58907517_fk_comunicate_language_id FOREIGN KEY (language_id) REFERENCES public.comunicate_language(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: comunicate_room comunicate_room_social_account_id_329277eb_fk_socialacc; Type: FK CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.comunicate_room
    ADD CONSTRAINT comunicate_room_social_account_id_329277eb_fk_socialacc FOREIGN KEY (social_account_id) REFERENCES public.socialaccount_socialaccount(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: comunicate_room comunicate_room_subject_id_1ebe788a_fk_comunicate_subject_id; Type: FK CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.comunicate_room
    ADD CONSTRAINT comunicate_room_subject_id_1ebe788a_fk_comunicate_subject_id FOREIGN KEY (subject_id) REFERENCES public.comunicate_subject(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_content_type_id_c4bce8eb_fk_django_content_type_id; Type: FK CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_content_type_id_c4bce8eb_fk_django_content_type_id FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_user_id_c564eba6_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_c564eba6_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_celery_beat_periodictask django_celery_beat_p_clocked_id_47a69f82_fk_django_ce; Type: FK CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.django_celery_beat_periodictask
    ADD CONSTRAINT django_celery_beat_p_clocked_id_47a69f82_fk_django_ce FOREIGN KEY (clocked_id) REFERENCES public.django_celery_beat_clockedschedule(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_celery_beat_periodictask django_celery_beat_p_crontab_id_d3cba168_fk_django_ce; Type: FK CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.django_celery_beat_periodictask
    ADD CONSTRAINT django_celery_beat_p_crontab_id_d3cba168_fk_django_ce FOREIGN KEY (crontab_id) REFERENCES public.django_celery_beat_crontabschedule(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_celery_beat_periodictask django_celery_beat_p_interval_id_a8ca27da_fk_django_ce; Type: FK CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.django_celery_beat_periodictask
    ADD CONSTRAINT django_celery_beat_p_interval_id_a8ca27da_fk_django_ce FOREIGN KEY (interval_id) REFERENCES public.django_celery_beat_intervalschedule(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_celery_beat_periodictask django_celery_beat_p_solar_id_a87ce72c_fk_django_ce; Type: FK CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.django_celery_beat_periodictask
    ADD CONSTRAINT django_celery_beat_p_solar_id_a87ce72c_fk_django_ce FOREIGN KEY (solar_id) REFERENCES public.django_celery_beat_solarschedule(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: djangoseo_mymetadatamodelinstance djangoseo_m__content_type_id_d6b22c57_fk_django_content_type_id; Type: FK CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.djangoseo_mymetadatamodelinstance
    ADD CONSTRAINT djangoseo_m__content_type_id_d6b22c57_fk_django_content_type_id FOREIGN KEY (_content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: djangoseo_mymetadatamodel djangoseo_m__content_type_id_eb387422_fk_django_content_type_id; Type: FK CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.djangoseo_mymetadatamodel
    ADD CONSTRAINT djangoseo_m__content_type_id_eb387422_fk_django_content_type_id FOREIGN KEY (_content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: easy_thumbnails_thumbnaildimensions easy_thum_thumbnail_id_c3a0c549_fk_easy_thumbnails_thumbnail_id; Type: FK CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.easy_thumbnails_thumbnaildimensions
    ADD CONSTRAINT easy_thum_thumbnail_id_c3a0c549_fk_easy_thumbnails_thumbnail_id FOREIGN KEY (thumbnail_id) REFERENCES public.easy_thumbnails_thumbnail(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: easy_thumbnails_thumbnail easy_thumbnails_source_id_5b57bc77_fk_easy_thumbnails_source_id; Type: FK CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.easy_thumbnails_thumbnail
    ADD CONSTRAINT easy_thumbnails_source_id_5b57bc77_fk_easy_thumbnails_source_id FOREIGN KEY (source_id) REFERENCES public.easy_thumbnails_source(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: lesson_article lesson_article_lesson_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.lesson_article
    ADD CONSTRAINT lesson_article_lesson_id_fkey FOREIGN KEY (lesson_id) REFERENCES public.lesson_lesson(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: lesson_article lesson_article_user_id_75385a5b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.lesson_article
    ADD CONSTRAINT lesson_article_user_id_75385a5b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: lesson_lesson lesson_lesson_user_id_1f039d56_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.lesson_lesson
    ADD CONSTRAINT lesson_lesson_user_id_1f039d56_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: oauth2_provider_accesstoken oauth2_provider_acce_application_id_b22886e1_fk_oauth2_pr; Type: FK CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.oauth2_provider_accesstoken
    ADD CONSTRAINT oauth2_provider_acce_application_id_b22886e1_fk_oauth2_pr FOREIGN KEY (application_id) REFERENCES public.oauth2_provider_application(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: oauth2_provider_accesstoken oauth2_provider_accesstoken_id_token_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.oauth2_provider_accesstoken
    ADD CONSTRAINT oauth2_provider_accesstoken_id_token_id_fkey FOREIGN KEY (id_token_id) REFERENCES public.oauth2_provider_idtoken(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: oauth2_provider_accesstoken oauth2_provider_accesstoken_source_refresh_token_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.oauth2_provider_accesstoken
    ADD CONSTRAINT oauth2_provider_accesstoken_source_refresh_token_id_fkey FOREIGN KEY (source_refresh_token_id) REFERENCES public.oauth2_provider_refreshtoken(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: oauth2_provider_accesstoken oauth2_provider_accesstoken_user_id_6e4c9a65_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.oauth2_provider_accesstoken
    ADD CONSTRAINT oauth2_provider_accesstoken_user_id_6e4c9a65_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: oauth2_provider_application oauth2_provider_application_user_id_79829054_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.oauth2_provider_application
    ADD CONSTRAINT oauth2_provider_application_user_id_79829054_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: oauth2_provider_grant oauth2_provider_gran_application_id_81923564_fk_oauth2_pr; Type: FK CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.oauth2_provider_grant
    ADD CONSTRAINT oauth2_provider_gran_application_id_81923564_fk_oauth2_pr FOREIGN KEY (application_id) REFERENCES public.oauth2_provider_application(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: oauth2_provider_grant oauth2_provider_grant_user_id_e8f62af8_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.oauth2_provider_grant
    ADD CONSTRAINT oauth2_provider_grant_user_id_e8f62af8_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: oauth2_provider_idtoken oauth2_provider_idto_application_id_08c5ff4f_fk_oauth2_pr; Type: FK CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.oauth2_provider_idtoken
    ADD CONSTRAINT oauth2_provider_idto_application_id_08c5ff4f_fk_oauth2_pr FOREIGN KEY (application_id) REFERENCES public.oauth2_provider_application(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: oauth2_provider_idtoken oauth2_provider_idtoken_user_id_dd512b59_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.oauth2_provider_idtoken
    ADD CONSTRAINT oauth2_provider_idtoken_user_id_dd512b59_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: oauth2_provider_refreshtoken oauth2_provider_refr_access_token_id_775e84e8_fk_oauth2_pr; Type: FK CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.oauth2_provider_refreshtoken
    ADD CONSTRAINT oauth2_provider_refr_access_token_id_775e84e8_fk_oauth2_pr FOREIGN KEY (access_token_id) REFERENCES public.oauth2_provider_accesstoken(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: oauth2_provider_refreshtoken oauth2_provider_refr_application_id_2d1c311b_fk_oauth2_pr; Type: FK CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.oauth2_provider_refreshtoken
    ADD CONSTRAINT oauth2_provider_refr_application_id_2d1c311b_fk_oauth2_pr FOREIGN KEY (application_id) REFERENCES public.oauth2_provider_application(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: oauth2_provider_refreshtoken oauth2_provider_refreshtoken_user_id_da837fce_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.oauth2_provider_refreshtoken
    ADD CONSTRAINT oauth2_provider_refreshtoken_user_id_da837fce_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: social_auth_usersocialauth social_auth_usersocialauth_user_id_17d28448_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.social_auth_usersocialauth
    ADD CONSTRAINT social_auth_usersocialauth_user_id_17d28448_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: socialaccount_socialtoken socialaccount_social_account_id_951f210e_fk_socialacc; Type: FK CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.socialaccount_socialtoken
    ADD CONSTRAINT socialaccount_social_account_id_951f210e_fk_socialacc FOREIGN KEY (account_id) REFERENCES public.socialaccount_socialaccount(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: socialaccount_socialtoken socialaccount_social_app_id_636a42d7_fk_socialacc; Type: FK CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.socialaccount_socialtoken
    ADD CONSTRAINT socialaccount_social_app_id_636a42d7_fk_socialacc FOREIGN KEY (app_id) REFERENCES public.socialaccount_socialapp(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: socialaccount_socialapp_sites socialaccount_social_site_id_2579dee5_fk_django_si; Type: FK CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.socialaccount_socialapp_sites
    ADD CONSTRAINT socialaccount_social_site_id_2579dee5_fk_django_si FOREIGN KEY (site_id) REFERENCES public.django_site(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: socialaccount_socialapp_sites socialaccount_social_socialapp_id_97fb6e7d_fk_socialacc; Type: FK CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.socialaccount_socialapp_sites
    ADD CONSTRAINT socialaccount_social_socialapp_id_97fb6e7d_fk_socialacc FOREIGN KEY (socialapp_id) REFERENCES public.socialaccount_socialapp(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: socialaccount_socialaccount socialaccount_socialaccount_user_id_8146e70c_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.socialaccount_socialaccount
    ADD CONSTRAINT socialaccount_socialaccount_user_id_8146e70c_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: video_chat_token video_chat_token_user_id_a00dcbf0_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.video_chat_token
    ADD CONSTRAINT video_chat_token_user_id_a00dcbf0_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: video_chat_userprofile video_chat_userprofile_user_id_2faec847_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: video
--

ALTER TABLE ONLY public.video_chat_userprofile
    ADD CONSTRAINT video_chat_userprofile_user_id_2faec847_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: video
--

REVOKE USAGE ON SCHEMA public FROM PUBLIC;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

--
-- PostgreSQL database cluster dump complete
--


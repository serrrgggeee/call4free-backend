#!/bin/sh
backup_date=$(date +'%m-%d-%Y')
PGPASSWORD=$POSTGRES_PASSWORD pg_dumpall -c -U ${POSTGRES_USER} -p ${POSTGRES_PORT} -h ${POSTGRES_HOST} > "/app/backap/${POSTGRES_DB}-${backup_date}.sql"



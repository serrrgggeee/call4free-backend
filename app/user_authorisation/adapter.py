from allauth.account.adapter import DefaultAccountAdapter
from django.conf import settings
from django.shortcuts import resolve_url
from allauth.socialaccount.models import SocialToken, SocialAccount
from django.http import HttpResponseRedirect
from django.contrib import messages
from allauth.account import signals


class AccountAdapter(DefaultAccountAdapter):

    def get_service(self, path: str = 'google'):
        services = ['google', 'vk', 'facebook', 'instagram']
        for service in services:
            if service in path:
                return service
        return 'google'

    def post_login(
            self,
            request,
            user,
            *,
            email_verification,
            signal_kwargs,
            email,
            signup,
            redirect_url
    ):
        response = HttpResponseRedirect(
            '/'
        )
        if signal_kwargs is None:
            signal_kwargs = {}
        signals.user_logged_in.send(
            sender=user.__class__,
            request=request,
            response=response,
            user=user,
            **signal_kwargs,
        )
        self.add_message(
            request,
            messages.SUCCESS,
            "profile/messages/logged_in.txt",
            {"user": user},
        )

        token = SocialToken.objects.filter(account__user=request.user, account__provider=self.get_service(request.path)).first()
        response.set_cookie('token', token.token, max_age=None)
        return response

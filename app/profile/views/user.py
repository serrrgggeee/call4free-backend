"""Comunicate api view."""
from allauth.socialaccount.models import SocialAccount
from django.contrib.auth.models import User
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from .serializers import SocialAccountSerializer
from core.authentication import BackendAuthentication, UserAuthentication


class UserViewSet(APIView):
    """
    A viewset for viewing and editing user instances.
    """
    queryset = User.objects.all()
    authentication_classes = (BackendAuthentication, UserAuthentication)

    def get(self, request):
        if request.user.is_anonymous:
            return Response({}, status=status.HTTP_401_UNAUTHORIZED)
        social_account = SocialAccount.objects.get(user=request.user)
        serializer = SocialAccountSerializer(social_account)
        return Response(serializer.data, status=status.HTTP_200_OK)


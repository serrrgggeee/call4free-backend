from allauth.socialaccount.models import SocialAccount
from rest_framework import serializers
from django.contrib.auth import get_user_model


class UserSerializer(serializers.ModelSerializer):
    ID = serializers.IntegerField(source='id')
    name = serializers.ReadOnlyField(source="get_full_name")

    class Meta:
        model = get_user_model()
        fields = ('ID', 'name', 'email', 'email')


class SocialAccountSerializer(serializers.ModelSerializer):
    accounts = serializers.ReadOnlyField(source="provider")
    img = serializers.ReadOnlyField(source="get_avatar_url")
    user = UserSerializer(read_only=True)

    class Meta:
        model = SocialAccount
        fields = ('pk', 'img', 'accounts', 'user')




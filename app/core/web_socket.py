import sys
import threading
from django.conf import settings
import socketio


def socket_message(data, event=None):
    if event is None:
        return
    sio = socketio.Client()

    @sio.event
    def connect():
        sio.emit(event, data)
        sys.exit()

    @sio.event
    def disconnect():
        sio.disconnect()

    @sio.event
    async def chat_message(sid, data):
        print("message ", data)

    # sio.connect(f'{settings.APP_SOCkET_BACKEND_HOST}:{settings.APP_SOCkET_BACKEND_PORT}') TO DO connection settings from  settings
    sio.connect('http://localhost:3000')


def emit_socket_message(instance, name='child procs1'):
    t = threading.Thread(name=name, target=socket_message, args=(instance, name))
    t.start()


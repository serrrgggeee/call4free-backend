from django.db import models
from django.utils.translation import gettext_lazy as _


class DatetimeMixin(models.Model):
    created = models.DateTimeField(_('Created at'), auto_now_add=True)
    updated = models.DateTimeField(_('Updated at'), auto_now=True)

    class Meta:
        abstract = True

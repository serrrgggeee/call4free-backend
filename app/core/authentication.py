from allauth.socialaccount.models import SocialToken
from django.utils import timezone
from django.conf import settings
from django.contrib.auth.models import User
from rest_framework.authentication import TokenAuthentication


class UserAuthentication(TokenAuthentication):
    def authenticate(self, request):
        authorisation_header = request.headers.get('Authorization')
        if not authorisation_header:
            return
        try:
            token = authorisation_header.split(" ")[1]
        except IndexError:
            return
        token_queryset = SocialToken.objects.filter(token=token).first()
        if not token_queryset:
            return
        if timezone.now() >= token_queryset.expires_at:
            return
        account = token_queryset.account
        user = account.user
        return user, None


class BackendAuthentication(TokenAuthentication):
    def authenticate(self, request):
        authorisation_header = request.headers.get('Authorization')
        if not authorisation_header:
            return
        try:
            token = authorisation_header.split(" ")[1]
        except IndexError:
            return
        if token not in settings.BACKEND_TOKEN:
            return
        user = User.objects.get(username=settings.BACKEND_USER)
        return user, None

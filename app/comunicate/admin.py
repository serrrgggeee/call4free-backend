from django.contrib import admin

from .models import Language, Subject, Room, Chat, Member

admin.site.register(Language)
admin.site.register(Member)
admin.site.register(Subject)
admin.site.register(Room)
admin.site.register(Chat)

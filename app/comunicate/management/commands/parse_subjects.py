import requests, re
from bs4 import BeautifulSoup
from django.core.management.base import BaseCommand
from comunicate.models import Subject


class Command(BaseCommand):
    help = 'Translate advertisements for some period'

    def add_arguments(self, parser):
        parser.add_argument(
            '-s',
            '--site',
            nargs='?',
            type=str,
            default='wiki',
            help='сайт для парсинга')


    def handle(self, *args, **options):
        site = options['site']
        getattr(self, site)(*args)

    def wiki(self):
        url = 'https://en.wikipedia.org/wiki/List_of_Advanced_Level_subjects'
        page = requests.get(url)
        soup = BeautifulSoup(page.text)
        results = soup.findAll('table')[1].findAll('ul')
        if results == []:
            return
        subjects = []
        for result in results:
            for res in result.findAll('li'):
                try:
                    subjects.append(Subject(name=re.sub(r" ?\[[^)]+\]", "", res.get_text())))
                except IndexError:
                    pass
        Subject.objects.bulk_create(subjects, ignore_conflicts=True)

    def choices4learning(self):
        url = 'http://choices4learning.com/home/quick-stop-resources-2/articles-on-learning/school-subjects-list/'
        page = requests.get(url)
        soup = BeautifulSoup(page.text)
        results = soup.find(class_='art-postcontent').findAll('p')
        if results == []:
            return
        subjects = []
        for result in results:
            try:
                subjects.append(Subject(name=re.sub(r" ?\([^)]+\)", "", result.get_text())))
            except IndexError:
                pass
        Subject.objects.bulk_create(subjects, ignore_conflicts=True)

import datetime
import requests, uuid, json
from bs4 import BeautifulSoup
from django.core.management.base import BaseCommand
from comunicate.models import Language


class Command(BaseCommand):
    help = 'Translate advertisements for some period'

    def add_arguments(self, parser):
        parser.add_argument(
            '-d',
            '--days',
            nargs='?',
            type=int,
            default=90,
            help='количество дней')


    def handle(self, *args, **options):
        url = 'https://meta.wikimedia.org/wiki/Template:List_of_language_names_ordered_by_code'
        page = requests.get(url)
        soup = BeautifulSoup(page.text)
        results = soup.find(class_='wikitable').findAll('tr')
        if results == []:
            return
        languages = []
        results.pop(0)
        for result in results:
            res = result.findAll('td')
            try:
                langauge = {
                    "code": res[0].get_text().strip(),
                    "name": res[1].get_text().strip(),
                    "directionality": res[2].get_text().strip(),
                    "natural_name": res[3].get_text().strip(),
                    "article": res[4].get_text().strip(),
                    "comment": res[5].get_text().strip()
                }
                languages.append(Language(**langauge))
            except IndexError:
                pass
        Language.objects.bulk_create(languages)

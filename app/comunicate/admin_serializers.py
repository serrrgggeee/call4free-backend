from rest_framework import serializers
from profile.views.serializers import SocialAccountSerializer


class LanguageSerializer(serializers.Serializer):
    pk = serializers.IntegerField(required=True)
    name = serializers.CharField(required=True)


class SubjectSerializer(serializers.Serializer):
    pk = serializers.IntegerField(required=True)
    name = serializers.CharField(required=True)


class MemberSerializer(serializers.Serializer):
     user_info = SocialAccountSerializer(source='social_account', read_only=True, required=False)

class RoomSerializer(serializers.Serializer):
    pk = serializers.IntegerField(required=True)
    name = serializers.CharField(required=False)
    members = MemberSerializer(source='member_set', many=True, required=False)
    subject = SubjectSerializer(required=False)
    language = LanguageSerializer(required=False)
    created = serializers.CharField(required=False)


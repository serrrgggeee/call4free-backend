"""Video chat model files."""
import logging
from allauth.socialaccount.models import SocialAccount
from core.models import DatetimeMixin
from core.web_socket import emit_socket_message
from django.db import models
from django.db.models import JSONField
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.translation import gettext_lazy as _

from .admin_serializers import RoomSerializer

logger = logging.getLogger(__name__)


class Language(models.Model):
    """Language model."""

    code = models.CharField('code', max_length=28, default='')
    name = models.CharField('English language name',
                            max_length=128, default='')
    directionality = models.CharField(
        'Directionality', max_length=3, default='')
    natural_name = models.CharField(
        'Local language name', max_length=128, default='')
    article = models.CharField(
        'Local or English Wikipedia article', max_length=128, default='')
    comment = models.CharField('Comment', max_length=128, default='')
    active = models.BooleanField(
        default=False, verbose_name='Отображать на сайте')

    def __str__(self):
        """String representation."""
        return self.name or ''


class Subject(models.Model):
    """Subject model."""

    name = models.CharField('English language name',
                            unique=True, max_length=128, default='')
    comment = models.CharField('Comment', max_length=128, default='')
    active = models.BooleanField(
        default=False, verbose_name='Отображать на сайте')

    def __str__(self):
        """String representation."""
        return self.name or ''


class Room(DatetimeMixin):
    """Room model."""
    name = models.CharField('Название комнаты', unique=True, max_length=128)
    subject = models.ForeignKey(
        Subject, on_delete=models.SET_NULL, blank=True, null=True)
    language = models.ForeignKey(
        Language, on_delete=models.SET_NULL, blank=True, null=True)
    social_account = models.ForeignKey(
        SocialAccount,
        related_name="social_account_room",
        on_delete=models.SET_NULL,
        blank=True, null=True
    )
    active = models.BooleanField(
        default=False, verbose_name='Отображать на сайте')

    def __str__(self):
        """String representation."""
        return self.name or str(self.pk)


class Member(DatetimeMixin):
    """Member model."""
    room = models.ForeignKey(Room, on_delete=models.SET_NULL, blank=True, null=True)
    active = models.BooleanField(
        default=False, verbose_name='Отображать на сайте')
    social_account = models.ForeignKey(
        SocialAccount,
        related_name="social_account_member",
        on_delete=models.SET_NULL,
        blank=True, null=True
    )

    def __str__(self):
        """String representation."""
        return self.room.name or self.social_account or str(self.pk)

    class Meta:
        unique_together = ("social_account", "room")


class Chat(DatetimeMixin):
    """Chat model."""

    message = models.TextField(verbose_name=_('Text of message'))
    user_info = JSONField(default=dict)
    room = models.ForeignKey(
        Room, on_delete=models.SET_NULL, blank=True, null=True)
    shared_room = models.ForeignKey(
        Room, related_name="chat_shared_room", on_delete=models.SET_NULL, blank=True, null=True, default=None)
    active = models.BooleanField(
        default=False, verbose_name='Отображать на сайте')
    shared = models.BooleanField(
        default=False, verbose_name='Шариное сообщение')

    def __str__(self):
        """String representation."""
        return self.room.name if self.room else str(self.pk)


@receiver(post_save, sender=Chat)
def chat_post_update(sender, instance, created, **kwargs):
    event = 'sendAdminChat'
    data = {'room': instance.room.name, 'editId': instance.pk, 'message': instance.message, 'userInfo': {}}
    emit_socket_message(data, event)


@receiver(post_save, sender=Room)
def room_post_update(sender, instance, created, **kwargs):
    event = 'sendAdminRoom'
    room = RoomSerializer(instance)
    emit_socket_message(room.data, event)

"""Comunicate url."""
from django.urls import path
from comunicate.api import views


urlpatterns = [
    path('language/', view=views.LanguageModelViewSet.as_view({
        'get': 'list'
    }), name='language'),
    path('subject/', view=views.SubjectModelViewSet.as_view({
        'get': 'list'
    }), name='language'),
    path('member/', view=views.MemberModelViewSet.as_view({
        'post': 'create',
        'put': 'update'
    }), name='member'),
    path('member/<str:room_name>/<int:social_account_id>/', view=views.MemberModelViewSet.as_view({
        'get': 'get_member'
    }), name='member'),
    path('rooms/', view=views.RoomsModelViewSet.as_view({
        'get': 'list',
        'post': 'update'
    }), name='rooms'),
    path('room/', view=views.RoomModelViewSet.as_view({
        'post': 'create',
    }), name='create_room'),
    path('room/<int:pk>/update/', view=views.RoomModelViewSet.as_view({
        'put': 'update',
    }), name='update_room'),
    path('room/<int:pk>/get/', view=views.RoomModelViewSet.as_view({
        'get': 'get',
    }), name='get_room'),

    path('messages/<int:room_id>', view=views.MessagesModelViewSet.as_view({
        'get': 'list'
    }), name='messages'),

    path('message/', view=views.MessageModelViewSet.as_view({
        'post': 'create'
    }), name='message'),
]

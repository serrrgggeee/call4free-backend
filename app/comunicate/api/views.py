"""Comunicate api view."""
import json

from allauth.socialaccount.models import SocialAccount

from django.http import JsonResponse
from comunicate.models import Language, Subject, Member, Room, Chat
from comunicate.api.serializers import (LanguageSerializer, SubjectSerializer, MemberSerializer, MemberCreateSerializer, MemberUpdateSerializer, RoomSerializer, RoomCreateSerializer,
                          ChatSerializer, ChatMessageSerializer)
from core.authentication import BackendAuthentication, UserAuthentication
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet
from rest_framework import status


class LanguageModelViewSet(ModelViewSet):
    u"""Создать или получить язык."""
    queryset = Language.objects.all().order_by('name')
    serializer_class = LanguageSerializer
    permission_classes = (AllowAny,)
    authentication_classes = (BackendAuthentication, UserAuthentication)

    def list(self, request, *args, **kwargs):
        queryset = Language.objects.all()
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

class SubjectModelViewSet(ModelViewSet):
    u"""Создать или получить предмет."""
    queryset = Subject.objects.all().order_by('name')
    serializer_class = SubjectSerializer
    permission_classes = (IsAuthenticated,)
    authentication_classes = (BackendAuthentication, UserAuthentication)

    def list(self, request, *args, **kwargs):
        queryset = Subject.objects.all()
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class MessagesModelViewSet(ModelViewSet):
    u"""Получить сообщения комнаты"""
    queryset = Chat.objects.order_by('created')
    serializer_class = ChatMessageSerializer
    permission_classes = (IsAuthenticated,)
    authentication_classes = (BackendAuthentication,)

    def list(self, room_id, request, *args, **kwargs):
        queryset = Chat.objects.filter(room=room_id).order_by('created')
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class MessageModelViewSet(ModelViewSet):
    u"""Создать сообщение комнаты"""
    queryset = Chat.objects.all()
    serializer_class = ChatSerializer
    permission_classes = (IsAuthenticated,)
    authentication_classes = (BackendAuthentication,)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        return Response(serializer.data, status=status.HTTP_201_CREATED)


class MemberModelViewSet(ModelViewSet):
    u"""Создать или получить участника."""
    queryset = Member.objects.all()

    serializer_class = MemberUpdateSerializer
    permission_classes = (IsAuthenticated,)
    authentication_classes = (BackendAuthentication, UserAuthentication)

    def create(self, request, *args, **kwargs):
        serializer = MemberCreateSerializer(data=request.data)
        if 'room' not in request.data:
            try:
                room = Room.objects.get(name=request.data.get('room_name'))
                request.data['room'] = room.pk
            except Room.DoesNotExist:
                return JsonResponse({"errors": ['комната не найдена']}, status=status.HTTP_201_CREATED)
        if not serializer.is_valid():
            return JsonResponse({"errors": str(serializer.errors)}, status=status.HTTP_400_BAD_REQUEST)
        serializer.save()
        return JsonResponse(serializer.data, status=status.HTTP_200_OK)
    
    def update(self, request, *args, **kwargs):
        user_info = json.loads(request.data['user_info'])
        room = Room.objects.get(name=request.data['room'])
        member = self.queryset.get(social_account=user_info['pk'], room=room)
        data = {'active': request.data.get('active')}
        serializer = self.serializer_class(instance=member, data=data)
        if not serializer.is_valid():
            return JsonResponse({"errors": str(serializer.errors)}, status=status.HTTP_400_BAD_REQUEST)
        self.perform_update(serializer)
        return JsonResponse(serializer.data, status=status.HTTP_200_OK)


    def get_member(self, request, room_name, social_account_id, *args, **kwargs):
        try:
            instance = self.queryset.get(social_account=social_account_id, room__name=room_name)
            is_in_room = True if instance.active else False
            if not instance.active:
                instance.active = True
                instance.save()
        except Member.DoesNotExist:
            return JsonResponse({"errors": ['участник не найден']}, status=status.HTTP_404_NOT_FOUND)
        serializer = self.get_serializer(instance)
        response_data = {"is_in_room": is_in_room}
        response_data.update(serializer.data)
        return JsonResponse(response_data, status=status.HTTP_200_OK)


class RoomsModelViewSet(ModelViewSet):
    u"""Получить комнаты"""
    queryset = Room.objects.all()
    serializer_class = RoomSerializer
    permission_classes = (IsAuthenticated,)
    authentication_classes = (BackendAuthentication,)

    def list(self, request, *args, **kwargs):
        queryset = Room.objects.filter(active=True)
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    def update(self, request, *args, **kwargs):
        response = []
        for queryset in self.queryset:
            instance = queryset
            instance.active = request.data.get("active")
            instance.save()
            serializer = self.get_serializer(queryset)
            response.append(serializer.data)

        return Response(response, status=status.HTTP_200_OK)


class RoomModelViewSet(ModelViewSet):
    u"""Создать или получить комнату."""
    queryset = Room.objects.all()
    serializer_class = RoomCreateSerializer
    permission_classes = (IsAuthenticated,)
    authentication_classes = (BackendAuthentication,)

    def create(self, request, *args, **kwargs):

        serializer = self.get_serializer(data=request.data)
        serializer.is_valid()
        social_account = SocialAccount.objects.get(user=request.user)
        serializer.save(social_account=social_account)
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def update(self, request, pk, *args, **kwargs):
        room = self.queryset.get(pk=pk)
        serializer = self.get_serializer(room, data=request.data)
        serializer.is_valid()
        self.perform_update(serializer)
        return Response(serializer.data, status=status.HTTP_200_OK)


    def get(self, request, pk, *args, **kwargs):
        self.queryset = self.queryset.get(pk=pk)
        serializer = RoomSerializer(self.queryset)
        return Response(serializer.data, status=status.HTTP_200_OK)

from comunicate.models import Language, Subject, Member, Room, Chat
from profile.views.serializers import SocialAccountSerializer
from rest_framework import serializers


class MemberCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Member
        fields = (
            'pk', 'room', 'active', 'social_account'
        )

class MemberUpdateSerializer(serializers.ModelSerializer):
    room = serializers.CharField(required=False)
    active = serializers.BooleanField(required=False)
    social_account = SocialAccountSerializer(read_only=True, required=False)
    class Meta:
        model = Member
        fields = (
            'pk', 'room', 'active', 'social_account'
        )

class MemberSerializer(serializers.ModelSerializer):
    user_info = SocialAccountSerializer(source='social_account', read_only=True, required=False)
    class Meta:
        model = Member
        fields = (
            'pk', 'room', 'active', 'user_info'
        )

class LanguageSerializer(serializers.ModelSerializer):
    name = serializers.CharField(required=True)

    class Meta:
        model = Language
        fields = (
            'pk', 'name',
        )


class SubjectSerializer(serializers.ModelSerializer):
    name = serializers.CharField(required=True)

    class Meta:
        model = Subject
        fields = (
            'pk', 'name',
        )


class RoomSerializer(serializers.ModelSerializer):
    name = serializers.CharField(required=False)
    members = MemberSerializer(source='member_set', many=True, required=False)
    subject = SubjectSerializer(required=False)
    language = LanguageSerializer(required=False)

    class Meta:
        model = Room
        fields = (
            'pk', 'name', 'subject', 'language', 'members', 'created'
        )


class RoomCreateSerializer(serializers.ModelSerializer):
    name = serializers.CharField(required=False)

    class Meta:
        model = Room
        fields = (
            'pk', 'name', 'subject', 'language', 'active', 'created'
        )


class ChatMessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Chat
        fields = (
            'pk', 'message'
        )


class ChatSerializer(serializers.ModelSerializer):
    class Meta:
        model = Chat
        fields = (
            'pk', 'message'
        )


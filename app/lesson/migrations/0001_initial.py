# Generated by Django 4.2.7 on 2024-02-25 13:25

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import tinymce.models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Lesson',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('updated', models.DateTimeField(auto_now=True, verbose_name='Updated at')),
                ('title', models.CharField(blank=True, default='', max_length=128, null=True, verbose_name='lesson title')),
                ('description', models.CharField(blank=True, default='', max_length=128, null=True, verbose_name='Lesson description')),
                ('active', models.BooleanField(default=False, verbose_name='Отображать на сайте')),
                ('user', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='lessons', to=settings.AUTH_USER_MODEL, verbose_name='User')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Article',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('updated', models.DateTimeField(auto_now=True, verbose_name='Updated at')),
                ('title', models.CharField(blank=True, default='', max_length=128, null=True, verbose_name='article title')),
                ('description', models.CharField(blank=True, default='', max_length=128, null=True, verbose_name='Article description')),
                ('text', tinymce.models.HTMLField(blank=True, default='', null=True, verbose_name='Article text')),
                ('answer', models.CharField(blank=True, default='', max_length=128, null=True, verbose_name='answers separated by commas')),
                ('active', models.BooleanField(default=False, verbose_name='Отображать на сайте')),
                ('lesson', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='articles', to='lesson.lesson', verbose_name='Lesson')),
                ('user', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='articles', to=settings.AUTH_USER_MODEL, verbose_name='User')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]

from rest_framework import serializers
from lesson.models import Lesson, Article


class ArticleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Article
        fields = (
            'pk', 'title', 'description', 'text'
        )


class LessonSerializer(serializers.ModelSerializer):
    articles = ArticleSerializer(many=True)

    class Meta:
        model = Lesson
        fields = (
            'pk', 'title', 'description', 'articles'
        )


class LessonsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Lesson
        fields = (
            'pk',
        )

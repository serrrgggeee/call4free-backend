"""Comunicate api view."""
from django.http import JsonResponse
from core.authentication import BackendAuthentication, UserAuthentication
from lesson.models import Lesson
from lesson.api.serializers import LessonSerializer, LessonsSerializer
from rest_framework import status
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet
from rest_framework.authentication import SessionAuthentication


class LessonModelViewSet(ModelViewSet):
    u"""Получить язык."""
    queryset = Lesson.objects.all().order_by('title')
    serializer_class = LessonSerializer
    serializer_class_lessons = LessonsSerializer
    authentication_classes = (SessionAuthentication, BackendAuthentication, UserAuthentication)
    permission_classes = (AllowAny,)

    def retrieve(self, request, pk, *args, **kwargs):
        try:
            lesson = Lesson.objects.get(user=request.user, pk=pk)
        except Lesson.DoesNotExist:
            return JsonResponse({"errors": ['урок не найден']}, status=status.HTTP_404_NOT_FOUND)

        serializer = self.serializer_class(lesson)
        return JsonResponse(serializer.data, status=status.HTTP_200_OK)

    def list(self, request, *args, **kwargs):
        lessons = Lesson.objects.filter(user=request.user)
        serializer = self.serializer_class_lessons(lessons, many=True)
        return Response(serializer.data)

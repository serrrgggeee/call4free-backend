"""Comunicate url."""
from django.urls import path
from lesson.api import views

urlpatterns = [
    path('', view=views.LessonModelViewSet.as_view({
        'get': 'list'}), name='lessons'),
    path('<int:pk>/', view=views.LessonModelViewSet.as_view({
        'get': 'retrieve'}), name='lesson'),
]

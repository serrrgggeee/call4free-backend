"""Lesson model files."""
import logging
from core.models import DatetimeMixin
from core.web_socket import emit_socket_message

from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.translation import gettext_lazy as _

from tinymce import models as tinymce_models


logger = logging.getLogger(__name__)


class Lesson(DatetimeMixin):
    """Lesson model."""

    title = models.CharField('lesson title', max_length=128, default='', blank=True, null=True)
    description = models.CharField(
        'Lesson description', max_length=128, default='', blank=True, null=True)

    user = models.ForeignKey(User, verbose_name=_('User'), blank=True, null=True,
                             related_name='lessons', on_delete=models.SET_NULL)

    active = models.BooleanField(
        default=False, verbose_name='Отображать на сайте')

    def __str__(self):
        """String representation."""
        return self.title or ''


class Article(DatetimeMixin):
    """Article model."""

    title = models.CharField('article title', max_length=128, default='', blank=True, null=True)
    description = models.CharField(
        'Article description', max_length=128, default='', blank=True, null=True)
    text = tinymce_models.HTMLField(_("Article text"), default='', blank=True, null=True)
    answer = models.CharField(
        'answers separated by commas', max_length=128, default='', blank=True, null=True)

    lesson = models.ForeignKey(Lesson, verbose_name=_('Lesson'), blank=True, null=True,
                               related_name='articles', on_delete=models.SET_NULL)

    user = models.ForeignKey(User, verbose_name=_('User'), blank=True, null=True,
                             related_name='articles', on_delete=models.SET_NULL)
    active = models.BooleanField(
        default=False, verbose_name='Отображать на сайте')

    def __str__(self):
        """String representation."""
        return self.title or ''


@receiver(post_save, sender=Lesson)
def lesson_post_update(sender, instance, created, **kwargs):
    """Post update model fuction sender."""
    event = 'updateAdminLesson'
    data = {'id': instance.pk, 'title': instance.title}
    emit_socket_message(data, event)


@receiver(post_save, sender=Article)
def article_post_update(sender, instance, created, **kwargs):
    """Post update model fuction sender."""
    event = 'updateAdminArticle'
    data = {'id': instance.pk, 'title': instance.title}
    emit_socket_message(data, event)

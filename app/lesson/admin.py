from django.contrib import admin
from .models import Lesson, Article

admin.site.register(Lesson)
admin.site.register(Article)

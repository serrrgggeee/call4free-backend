from django.http import HttpResponseRedirect


def redirect_403_middleware(get_response):
    def middleware(request):
        response = get_response(request)
        if response.status_code == 403:
            return HttpResponseRedirect('/')
        return response

    return middleware

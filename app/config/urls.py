"""Main url."""
from django.contrib import admin
from django.conf.urls.static import static
from django.conf.urls import include
from django.urls import path
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf import settings

from profile.views.user import UserViewSet
from user_authorisation.views import index


urlpatterns = [
    path('login/', index, name='login'),
    path('profile/', include('allauth.urls')),
    path('user_info/', UserViewSet.as_view()),
    path('admin/', admin.site.urls),
    path('communicate/', include('comunicate.urls')),
    path('lessons/', include('lesson.urls')),
    path('tinymce/', include('tinymce.urls')),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += staticfiles_urlpatterns()

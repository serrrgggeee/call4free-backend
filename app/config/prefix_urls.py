"""Main url."""
from django.conf.urls import include
from django.conf import settings
from django.urls import path

urlpatterns = [
    path(f'{settings.URL_PREFIX}', include('config.urls')),
]

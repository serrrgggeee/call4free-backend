server {
    listen 80;
    server_name ${DOMAIN} www.${DOMAIN};

    location ~ (^/admin|^/member|^/communicate|^/user_info|^/accounts|^/login|^/lesson|^/profile){
        include   /etc/nginx/uwsgi_params;
        return 301 https://$host$request_uri;
    }
}
server {
    listen      ${PORT} ${SSL} ${HTTP2};
    server_name ${DOMAIN} www.${DOMAIN};

    ssl_certificate     /etc/letsencrypt/live/${DOMAIN}/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/${DOMAIN}/privkey.pem;

    include     /etc/nginx/options-ssl-nginx.conf;

    ssl_dhparam /vol/proxy/ssl-dhparams.pem;

    add_header Strict-Transport-Security "max-age=31536000; includeSubDomains" always;

    include locations.tpl
}


location /static/ {
        alias /app/static/;
}
location ~ (^/admin|^/member|^/communicate|^/user_info|^/accounts|^/login|^/lessons|^/profile){
    ${PASS}           ${APP_HOST}:${APP_PORT};
    include              /etc/nginx/${APP_PARAMS};
    client_max_body_size 10M;
}

location = /  {
    root /vol/www/public;
    try_files /index.html =404;
}

location /video {
    root /vol/www/public;
    try_files /video.html =404;
}
location /room/ {
    root /vol/www/public/;
    try_files /room.html =404;
}
location /css {
    alias /vol/www/public/css;
}
location /img {
    alias /vol/www/public/img;
}
location /js {
    alias /vol/www/public/js;
}

location / {
    proxy_pass ${APP_SOCkET_BACKEND_HOST}:${APP_SOCkET_BACKEND_PORT};
}
